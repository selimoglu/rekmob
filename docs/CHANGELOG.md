# Change Log

### 1.0

UI: 

 - Initial version containing the whole UI and Backend code
 - Auth integration, JWT token is used
 - Nebular Auth integration
 - Mock services to change api data and mock data
 - ...

Backend:

 - Initial version of java solution structure
 - Spring 4.0.0
 - Spring Boot 2.1.4.RELEASE
 - Maven 3.6.0
 - Json Web Token 0.9.1. Auth with JWT tokens including refresh token functionality
 - Model Mapper 2.3.3
 - Springfox-swagger2 2.9.2
 - Findbugs plugin 3.0.5
 - Maven Checkstyle plugin 3.0.0
 - H2 database 1.4.197
 - ...
