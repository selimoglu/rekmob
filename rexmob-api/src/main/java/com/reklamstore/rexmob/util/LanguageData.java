package com.reklamstore.rexmob.util;

import org.springframework.util.StringUtils;

import java.util.HashMap;

public class LanguageData {
    public static HashMap<String, HashMap<String, String>> data;
    public static final String APPROVED_APP_MAIL_SUBJECT_KEY             = "APPROVED_APP_MAIL_SUBJECT";
    public static final String APPROVED_APP_MAIL_TITLE_KEY               = "APPROVED_APP_MAIL_TITLE";
    public static final String APPROVED_APP_MAIL_DESCRIPTION_1_1_KEY     = "APPROVED_APP_MAIL_DESCRIPTION_1_1";
    public static final String APPROVED_APP_MAIL_DESCRIPTION_1_2_KEY     = "APPROVED_APP_MAIL_DESCRIPTION_1_2";
    public static final String APPROVED_APP_MAIL_DESCRIPTION_2_KEY       = "APPROVED_APP_MAIL_DESCRIPTION_2";
    public static final String APPROVED_APP_MAIL_DESCRIPTION_3_KEY       = "APPROVED_APP_MAIL_DESCRIPTION_3";
    public static final String APPROVED_APP_MAIL_DESCRIPTION_4_KEY       = "APPROVED_APP_MAIL_DESCRIPTION_4";
    //public static final String APPROVED_APP_MAIL_DESCRIPTION_5_KEY       = "APPROVED_APP_MAIL_DESCRIPTION_5";
    public static final String APPROVED_APP_MAIL_BUTTON_TEXT_KEY         = "APPROVED_APP_MAIL_BUTTON_TEXT";

    public static final String REJECT_APP_NO_REASON_MAIL_SUBJECT_KEY             = "REJECT_APP_NO_REASON_MAIL_SUBJECT";
    public static final String REJECT_APP_NO_REASON_MAIL_TITLE_KEY               = "REJECT_APP_NO_REASON_MAIL_TITLE";
    public static final String REJECT_APP_NO_REASON_MAIL_DESCRIPTION_1_1_KEY     = "REJECT_APP_NO_REASON_MAIL_DESCRIPTION_1_1";
    public static final String REJECT_APP_NO_REASON_MAIL_DESCRIPTION_1_2_KEY     = "REJECT_APP_NO_REASON_MAIL_DESCRIPTION_1_2";
    public static final String REJECT_APP_NO_REASON_MAIL_DESCRIPTION_2_KEY       = "REJECT_APP_NO_REASON_MAIL_DESCRIPTION_2";
    public static final String REJECT_APP_NO_REASON_MAIL_DESCRIPTION_3_KEY     = "REJECT_APP_NO_REASON_MAIL_DESCRIPTION_3";

    public static final String REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_SUBJECT_KEY             = "REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_SUBJECT";
    public static final String REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_TITLE_KEY               = "REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_TITLE";
    public static final String REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_1_1_KEY     = "REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_1_1";
    public static final String REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_1_2_KEY     = "REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_1_2";
    public static final String REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_2_KEY       = "REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_2";
    public static final String REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_3_KEY     = "REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_3";
    public static final String REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_4_KEY     = "REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_4";
    public static final String REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_5_KEY     = "REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_5";

    public static final String REJECT_APP_FREE_DOMAIN_NAME_MAIL_SUBJECT_KEY             = "REJECT_APP_FREE_DOMAIN_NAME_MAIL_SUBJECT";
    public static final String REJECT_APP_FREE_DOMAIN_NAME_MAIL_TITLE_KEY               = "REJECT_APP_FREE_DOMAIN_NAME_MAIL_TITLE";
    public static final String REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_1_1_KEY     = "REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_1_1";
    public static final String REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_1_2_KEY     = "REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_1_2";
    public static final String REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_2_KEY       = "REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_2";
    public static final String REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_3_KEY       = "REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_3";
    public static final String REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_4_KEY       = "REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_4";

    public static final String REJECT_APP_INCENTIVE_TO_CLICK_MAIL_SUBJECT_KEY             = "REJECT_APP_INCENTIVE_TO_CLICK_MAIL_SUBJECT";
    public static final String REJECT_APP_INCENTIVE_TO_CLICK_MAIL_TITLE_KEY               = "REJECT_APP_INCENTIVE_TO_CLICK_MAIL_TITLE";
    public static final String REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_1_1_KEY     = "REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_1_1";
    public static final String REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_1_2_KEY     = "REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_1_2";
    public static final String REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_2_KEY       = "REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_2";
    public static final String REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_3_KEY       = "REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_3";
    public static final String REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_4_KEY       = "REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_4";

    public static final String REJECT_APP_ADULT_CONTENT_MAIL_SUBJECT_KEY             = "REJECT_APP_ADULT_CONTENT_MAIL_SUBJECT";
    public static final String REJECT_APP_ADULT_CONTENT_MAIL_TITLE_KEY               = "REJECT_APP_ADULT_CONTENT_MAIL_TITLE";
    public static final String REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_1_1_KEY     = "REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_1_1";
    public static final String REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_1_2_KEY     = "REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_1_2";
    public static final String REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_2_KEY       = "REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_2";
    public static final String REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_3_KEY       = "REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_3";
    public static final String REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_4_KEY       = "REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_4";
    public static final String REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_5_KEY       = "REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_5";

    static {
        data = new HashMap<>();

        // language: en_US
        HashMap<String, String> en_US = new HashMap<>();
        data.put("en-US", en_US);
        en_US.put(APPROVED_APP_MAIL_SUBJECT_KEY, "Your application to ReklamStore SSP is approved");
        en_US.put(APPROVED_APP_MAIL_TITLE_KEY, "Dear");
        en_US.put(APPROVED_APP_MAIL_DESCRIPTION_1_1_KEY, "We are happy to announce that your website");
        en_US.put(APPROVED_APP_MAIL_DESCRIPTION_1_2_KEY, "has been accepted.");
        en_US.put(APPROVED_APP_MAIL_DESCRIPTION_2_KEY, "Please add the code for the advertising space you created from the user panel to your website.");
        //en_US.put(APPROVED_APP_MAIL_DESCRIPTION_3_KEY, "Advertising delivery is activated 2 hours after you add your site banner codes.");
        en_US.put(APPROVED_APP_MAIL_DESCRIPTION_3_KEY, "To learn more, you can check the related article on our knowledge base: http://support.reklamstore.com");
        en_US.put(APPROVED_APP_MAIL_DESCRIPTION_4_KEY, "Best Regards,");
        en_US.put(APPROVED_APP_MAIL_BUTTON_TEXT_KEY, "Login");

        en_US.put(REJECT_APP_NO_REASON_MAIL_SUBJECT_KEY, "Your application to ReklamStore SSP is rejected");
        en_US.put(REJECT_APP_NO_REASON_MAIL_TITLE_KEY, "Dear");
        en_US.put(REJECT_APP_NO_REASON_MAIL_DESCRIPTION_1_1_KEY, "Unfortunately, the publisher application you made to the ReklamStore with the");
        en_US.put(REJECT_APP_NO_REASON_MAIL_DESCRIPTION_1_2_KEY, "site has not been accepted. The reason for not being accepted may be one or more of the following.");
        en_US.put(REJECT_APP_NO_REASON_MAIL_DESCRIPTION_2_KEY, "<b>Program, warez, crack, serial or cd key sharing<br>" +
                "Adult content<br>" +
                "Lack of content<br>" +
                "Incorrect ad space usage or too many ads<br>" +
                "Multiplicity of similar sites</b>");
        en_US.put(REJECT_APP_NO_REASON_MAIL_DESCRIPTION_3_KEY, "Our affiliate network, ReklamAction, has performance campaigns suitable for your site.<br> " +
                "You can apply to ReklamAction as publisher, from here You can get support from pub@reklamaction.com about ReklamAction publishing.");

        en_US.put(REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_SUBJECT_KEY, "Your application to ReklamStore SSP is rejected");
        en_US.put(REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_TITLE_KEY, "Dear");
        en_US.put(REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_1_1_KEY, "Unfortunately, the publisher application you made to the ReklamStore with the");
        en_US.put(REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_1_2_KEY, "has not been accepted for the reason stated below;");
        en_US.put(REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_2_KEY, "<b>Insufficient Content or traffic</b>");
        en_US.put(REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_3_KEY, "Your application was not approved because of the insufficient content or traffic on your website. You can request an analyzing claim in the future.");
        en_US.put(REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_4_KEY, "After you have logged in to your account, you can send us your questions about our system from the support section.");
        en_US.put(REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_5_KEY, "Our affiliate network, ReklamAction, has performance campaigns suitable for your site.<br>" +
                "You can apply to ReklamAction as publisher, from <a href=\"http://www.reklamaction.com/signup?utm_source=rsmail\" target=\"_blank\">here</a> " +
                "You can get support from <a>pub@reklamaction.com<a/> about ReklamAction publishing.");

        en_US.put(REJECT_APP_FREE_DOMAIN_NAME_MAIL_SUBJECT_KEY, "Your application to ReklamStore SSP is rejected");
        en_US.put(REJECT_APP_FREE_DOMAIN_NAME_MAIL_TITLE_KEY, "Dear");
        en_US.put(REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_1_1_KEY, "Unfortunately, the publisher application you made to ReklamStore with");
        en_US.put(REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_1_2_KEY, "has been not accepted for the reason given below.");
        en_US.put(REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_2_KEY, "<b>Free Domain Name</b>");
        en_US.put(REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_3_KEY, "Sites with free domain names are not accepted into our publisher network, so your application has not been approved. You can apply with the sites with paid domain names");
        en_US.put(REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_4_KEY, "Our affiliate network, ReklamAction, has performance campaigns suitable for your site.<br> " +
                "You can apply to ReklamAction as publisher, from <a href=\"http://www.reklamaction.com/signup?utm_source=rsmail\" target=\"_blank\">here</a>" +
                "You can get support from <a>pub@reklamaction.com<a/> about ReklamAction publishing.");

        en_US.put(REJECT_APP_INCENTIVE_TO_CLICK_MAIL_SUBJECT_KEY, "Your application to ReklamStore SSP is rejected");
        en_US.put(REJECT_APP_INCENTIVE_TO_CLICK_MAIL_TITLE_KEY, "Dear");
        en_US.put(REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_1_1_KEY, "The ad publishing with the");
        en_US.put(REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_1_2_KEY, "on ReklamStore is unfortunately stopped due to the following reason.");
        en_US.put(REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_2_KEY, "<b>Encouragement to click</b>");
        en_US.put(REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_3_KEY, "Our affiliate network, ReklamAction, has performance campaigns suitable for your site.<br> " +
                "You can apply to ReklamAction as publisher, from <a href=\"http://www.reklamaction.com/signup?utm_source=rsmail\" target=\"_blank\">here</a>" +
                "You can get support from <a>pub@reklamaction.com<a/> about ReklamAction publishing.");
        en_US.put(REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_4_KEY, "You can see the publisher terms and rules at " +
                "<a href=\"https://my.reklamstore.com/Account/Signup\" target=\"_blank\">https://my.reklamstore.com/Account/Signup</a> " +
                "After you have logged in to your account, you can send us your questions about the system from the support section. " +
                "Thank you for the interest you have shown to ReklamStore and wish you have a nice working day.");

        en_US.put(REJECT_APP_ADULT_CONTENT_MAIL_SUBJECT_KEY, "Your application to ReklamStore SSP is rejected");
        en_US.put(REJECT_APP_ADULT_CONTENT_MAIL_TITLE_KEY, "Dear");
        en_US.put(REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_1_1_KEY, "Unfortunately, the publisher application you made to ReklamStore with");
        en_US.put(REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_1_2_KEY, "has been not accepted for the reason given below.");
        en_US.put(REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_2_KEY, "<b>Adult content</b>");
        en_US.put(REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_3_KEY, "After you have logged into your account, you can ask us your questions about the system from the Support section.");
        en_US.put(REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_4_KEY, "Thank you for your interest in ReklamStore and wish you a good work.");
        en_US.put(REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_5_KEY, "Our affiliate network, ReklamAction, has performance campaigns suitable for your site.<br> " +
                "You can apply to ReklamAction as publisher, from <a href=\"http://www.reklamaction.com/signup?utm_source=rsmail\" target=\"_blank\">here</a>" +
                "You can get support from <a>pub@reklamaction.com<a/> about ReklamAction publishing.");

        // language: tr_TR
        HashMap<String, String> tr_TR = new HashMap<>();
        data.put("tr-TR", tr_TR);
        tr_TR.put(APPROVED_APP_MAIL_SUBJECT_KEY, "ReklamStore SSP'ye başvurunuz onaylandı");
        tr_TR.put(APPROVED_APP_MAIL_TITLE_KEY, "Sayın");
        tr_TR.put(APPROVED_APP_MAIL_DESCRIPTION_1_1_KEY, "");
        tr_TR.put(APPROVED_APP_MAIL_DESCRIPTION_1_2_KEY, "web sitenizin kabul edildiğini bildirmekten mutluluk duyuyoruz.");
        tr_TR.put(APPROVED_APP_MAIL_DESCRIPTION_2_KEY, "Lütfen kullanıcı panelinden oluşturduğunuz reklam alanı kodunu web sitenize ekleyin.");
        //tr_TR.put(APPROVED_APP_MAIL_DESCRIPTION_3_KEY, "Reklam yayını, site banner kodlarınızı ekledikten 2 saat sonra etkinleştirilir.");
        tr_TR.put(APPROVED_APP_MAIL_DESCRIPTION_3_KEY, "Daha fazla bilgi edinmek için bilgi tabanımızdaki ilgili makaleye göz atabilirsiniz: <a>http://support.reklamstore.com</a>");
        tr_TR.put(APPROVED_APP_MAIL_DESCRIPTION_4_KEY, "Saygılarımla,");
        tr_TR.put(APPROVED_APP_MAIL_BUTTON_TEXT_KEY, "Giriş Yap");

        tr_TR.put(REJECT_APP_NO_REASON_MAIL_SUBJECT_KEY, "ReklamStore SSP'ye başvurunuz reddedildi");
        tr_TR.put(REJECT_APP_NO_REASON_MAIL_TITLE_KEY, "Sayın");
        tr_TR.put(REJECT_APP_NO_REASON_MAIL_DESCRIPTION_1_1_KEY, "Ne yazık ki,");
        tr_TR.put(REJECT_APP_NO_REASON_MAIL_DESCRIPTION_1_2_KEY, "sitesiyle ReklamStore'a yaptığınız yayıncı başvurusu kabul edilmedi. Kabul edilmemesinin nedeni aşağıdakilerden biri veya daha fazlası olabilir.");
        tr_TR.put(REJECT_APP_NO_REASON_MAIL_DESCRIPTION_2_KEY, "<b>Program, warez, crack, serial veya cd key paylaşım<br>" +
                "Yetişkinlere yönelik içerik<br>" +
                "İçerik bulunmaması veya çok az bulunması<br>" +
                "Hatalı reklam alanı kullanımı veya çok fazla reklam içermesi<br>" +
                "Benzer sitelerin çokluğu<br></b>");
        tr_TR.put(REJECT_APP_NO_REASON_MAIL_DESCRIPTION_3_KEY, "Bağlı kuruluş ağımız ReklamAction, sitenize uygun performans kampanyalarına sahiptir.<br>" +
                "ReklamAction'a yayıncı olarak <a href=\"http://www.reklamaction.com/signup?utm_source=rsmail\" target=\"_blank\">buradan</a> başvurabilirsiniz. " +
                "ReklamAction yayıncılığı hakkında <a>pub@reklamaction.com<a/> adresinden destek alabilirsiniz.");

        tr_TR.put(REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_SUBJECT_KEY, "ReklamStore SSP'ye başvurunuz reddedildi");
        tr_TR.put(REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_TITLE_KEY, "Sayın");
        tr_TR.put(REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_1_1_KEY, "Maalesef,");
        tr_TR.put(REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_1_2_KEY, "ile ReklamStore'a yaptığınız yayıncı başvurusu aşağıda belirtilen nedenle kabul edilmemiştir;");
        tr_TR.put(REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_2_KEY, "<b>Yetersiz İçerik veya trafik</b>");
        tr_TR.put(REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_3_KEY, "Web sitenizdeki yetersiz içerik veya trafik nedeniyle başvurunuz onaylanmadı. İleride analiz talebi talep edebilirsiniz.");
        tr_TR.put(REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_4_KEY, "Hesabınıza giriş yaptıktan sonra, sistemimizle ilgili sorularınızı destek bölümünden bize gönderebilirsiniz.");
        tr_TR.put(REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_5_KEY, "Bağlı kuruluş ağımız ReklamAction, sitenize uygun performans kampanyalarına sahiptir.<br>" +
                "ReklamAction'a yayıncı olarak <a href=\"http://www.reklamaction.com/signup?utm_source=rsmail\" target=\"_blank\">buradan</a> başvurabilirsiniz. " +
                "ReklamAction yayıncılığı hakkında <a>pub@reklamaction.com<a/> adresinden destek alabilirsiniz.");

        tr_TR.put(REJECT_APP_FREE_DOMAIN_NAME_MAIL_SUBJECT_KEY, "ReklamStore SSP'ye başvurunuz reddedildi");
        tr_TR.put(REJECT_APP_FREE_DOMAIN_NAME_MAIL_TITLE_KEY, "Sayın");
        tr_TR.put(REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_1_1_KEY, "ReklamStore'da");
        tr_TR.put(REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_1_2_KEY, "ile yayınlanan reklam maalesef aşağıdaki nedenle durdurulmuştur.");
        tr_TR.put(REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_2_KEY, "<b>Ücretsiz Alan Adı</b>");
        tr_TR.put(REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_3_KEY, "Ücretsiz alan adlarına sahip siteler yayıncı ağımıza kabul edilmediğinden başvurunuz onaylanmadı. Ücretli alan adlarına sahip sitelerle başvurabilirsiniz.");
        tr_TR.put(REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_4_KEY, "Bağlı kuruluş ağımız ReklamAction, sitenize uygun performans kampanyalarına sahiptir.<br>" +
                "ReklamAction'a yayıncı olarak <a href=\"http://www.reklamaction.com/signup?utm_source=rsmail\" target=\"_blank\">buradan</a> başvurabilirsiniz. " +
                "ReklamAction yayıncılığı hakkında <a>pub@reklamaction.com<a/> adresinden destek alabilirsiniz.");

        tr_TR.put(REJECT_APP_INCENTIVE_TO_CLICK_MAIL_SUBJECT_KEY, "ReklamStore SSP'ye başvurunuz reddedildi");
        tr_TR.put(REJECT_APP_INCENTIVE_TO_CLICK_MAIL_TITLE_KEY, "Sayın");
        tr_TR.put(REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_1_1_KEY, "ReklamStore'da");
        tr_TR.put(REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_1_2_KEY, "sitesi ile reklam yayını aşağıda belirtilen sebepten dolayı maalesef durdurulmuştur.");
        tr_TR.put(REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_2_KEY, "<b>Tıklamaya teşvik</b>");
        tr_TR.put(REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_3_KEY, "Bağlı kuruluş ağımız ReklamAction, sitenize uygun performans kampanyalarına sahiptir.<br>" +
                "ReklamAction'a yayıncı olarak <a href=\"http://www.reklamaction.com/signup?utm_source=rsmail\" target=\"_blank\">buradan</a> başvurabilirsiniz. " +
                "ReklamAction yayıncılığı hakkında <a>pub@reklamaction.com<a/> adresinden destek alabilirsiniz.");
        tr_TR.put(REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_4_KEY, "Yayıncı şartlarını ve kurallarını " +
                "<a href=\"https://my.reklamstore.com/Account/Signup\" target=\"_blank\">https://my.reklamstore.com/Account/Signup</a> adresinde görebilirsiniz. " +
                "Hesabınıza giriş yaptıktan sonra, sistemle ilgili sorularınızı destek bölümünden bize gönderebilirsiniz. " +
                "ReklamStore'a gösterdiğiniz ilgi için teşekkür eder, iyi çalışma günleri dilerim.");

        tr_TR.put(REJECT_APP_ADULT_CONTENT_MAIL_SUBJECT_KEY, "ReklamStore SSP'ye başvurunuz reddedildi");
        tr_TR.put(REJECT_APP_ADULT_CONTENT_MAIL_TITLE_KEY, "Sayın");
        tr_TR.put(REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_1_1_KEY, "Maalesef,");
        tr_TR.put(REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_1_2_KEY, "ile ReklamStore'a yaptığınız yayıncı başvurusu aşağıdaki nedenle kabul edilmedi");
        tr_TR.put(REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_2_KEY, "<b>Yetişkinlere yönelik içerik</b>");
        tr_TR.put(REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_3_KEY, "Hesabınıza giriş yaptıktan sonra, Destek bölümünden bize sistem hakkındaki sorularınızı sorabilirsiniz.");
        tr_TR.put(REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_4_KEY, "ReklamStore'a gösterdiğiniz ilgi için teşekkür eder, iyi çalışmalar dileriz.");
        tr_TR.put(REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_5_KEY, "Bağlı kuruluş ağımız ReklamAction, sitenize uygun performans kampanyalarına sahiptir.<br>" +
                "ReklamAction'a yayıncı olarak <a href=\"http://www.reklamaction.com/signup?utm_source=rsmail\" target=\"_blank\">buradan</a> başvurabilirsiniz. " +
                "ReklamAction yayıncılığı hakkında <a>pub@reklamaction.com<a/> adresinden destek alabilirsiniz.");
    }

    public static String get(String lang, String key){
        try{
            if(StringUtils.isEmpty(lang))
                lang = "en-US";

            return data.get(lang).get(key);
        }catch (Exception ignore){}
        return "";
    }
}
