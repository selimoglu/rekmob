package com.reklamstore.rexmob.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum AdSize {
	BANNER(0,320,50,new int[]{0},"label.appunit.type.banner"),
	MEDIUM_RECTANGLE(1,300,250,new int[]{0},"label.appunit.type.mediumRectangle"),
	FULL_BANNER(2,468,60,new int[]{0},"Banner (468X60)"),
	LEADERBOARD(3,728,90,new int[]{0},"LeaderBoard (728x90)"),
	SMARTBANNER(4,0,0,new int[]{0},""),
	SMARTPHONE_INTERSTITAL_PORTRAIT(5,320,480,new int[]{1},""),
	SMARTPHONE_INTERSTITIAL_LANDSCAPE(6,480,320,new int[]{1},""),
	TABLET_INTERSTITIAL_PORTRAIT(7,768,1024,new int[]{1},""),
	TABLET_INTERSTITIAL_LANDSCAPE(8,1024,768,new int[]{1},""),
	GENERIC_FULLSCREEN(9,0,0,new int[]{1},"label.appunit.type.interstitial"),
	SIZE_320_568(10,320,568,new int[]{0},""),
	SIZE_300_600(11,300,600,new int[]{0},"Large Skyscraper (300x600)"),
	SIZE_160_600(12,160,600,new int[]{0},""),
	LARGE_RECTANGLE(13,336,280,new int[]{0},"Large Rectangle (336x280)"),
	SCROLL_300_250(14,300,250,new int[]{0},"label.appunit.type.scroll"),
	SIZE_360_592(15,360,592,new int[]{0},""),
	SIZE_360_640(16,360,640,new int[]{0},""),
	DESKTOP_FULLSCREEN(17,800,600,new int[]{1},"label.appunit.type.desktopInterstitial"),
	PASS_FULLSCREEN(18,0,0,new int[]{1},"label.appunit.type.pass"),
	SKYSCRAPER(19,120,600,new int[]{0},"Skyscraper (120x600)"),
	SQUARE(20,250,250,new int[]{0},"Square (250x250)"),
	MASTHEAD(21,970,250,new int[]{0},"Masthead (970x250)"),
	LARGE_SKYSCRAPER(22,300,600,new int[]{0},"Large Rectangle (300x600)"),
	WIDE_SKYSCRAPER(23,160,600,new int[]{0},"Wide Skyscraper (160x600)"),
	VIDEO(24,0,0,new int[]{0},""),
	NATIVE(25,0,0,new int[] {0}, "label.appunit.type.native"),
	FLIP(26,300,250,new int[]{0},"label.appunit.type.flip"),
	IN_READ(27,300,250, new int[]{0}, "label.appunit.type.inRead"),
	SMARTLINK(28,0,0,new int[]{0},"label.appunit.type.smartlink"),
	POPUP(29,0,0,new int[]{0},"label.appunit.type.popup"),
	POPUNDER(30,0,0,new int[]{0},"label.appunit.type.popunder"),
	PUSH(31,0,0,new int[]{0},"label.appunit.type.push"),
	REFERRAL(32,0,0,new int[]{0},"label.appunit.type.referral"),
	TOAST(33,320,50,new int[]{0},"label.appunit.type.toast");

	public int code;
	public int width;
	public int height;
	public int[] creativeFormat;
	public String label;

	public static  Map<Integer, Integer> RUBICON_SIZE_MAP =
			Collections.unmodifiableMap(new HashMap<Integer, Integer>() {{
				put(AdSize.BANNER.code, 43);
				put(AdSize.MEDIUM_RECTANGLE.code, 15);
				put(AdSize.FULL_BANNER.code, 1 );
				put(AdSize.LEADERBOARD.code, 2 );
				put(AdSize.SMARTPHONE_INTERSTITAL_PORTRAIT.code, 67);
				put(AdSize.SMARTPHONE_INTERSTITIAL_LANDSCAPE.code, 101);
				put(AdSize.LARGE_RECTANGLE.code, 16);
				put(AdSize.DESKTOP_FULLSCREEN.code, 185);
				put(AdSize.SKYSCRAPER.code, 8);
				put(AdSize.MASTHEAD.code, 57);
				put(AdSize.LARGE_SKYSCRAPER.code, 10);
				put(AdSize.WIDE_SKYSCRAPER.code, 9);
				put(AdSize.SCROLL_300_250.code, 15);
				put(AdSize.FLIP.code, 15);
				put(AdSize.IN_READ.code, 15);
			}});

	public static  Map<String, Integer> SMARTLINK_IMPRESSION_ID_MAP =
			Collections.unmodifiableMap(new HashMap<String, Integer>() {{
				put("PROPELLER_SMART", 1145);
				put("PROPELLER_POP", 1146);
				put("PROPELLER2_SMART", 1145);
				put("PROPELLER2_POP", 1146);
				put("ADCASH_SMART", 1211);
				put("ADCASH_POP", 1210);
				put("PEAKADX_SMART", 1175);
				put("PEAKADX_POP", 1174);
				put("POPCASH_SMART", 1213);
				put("POPCASH_POP", 1212);
				put("CLICKADU_SMART", 1227);
				put("CLICKADU_POP", 1228);
				put("ADSTERRA_SMART", 1255);
				put("ADSTERRA_POP", 1256);
				put("ADMAVEN_SMART", 1253);
				put("ADMAVEN_POP", 1254);
				put("POPADS_SMART", 1257);
				put("POPADS_POP", 1258);
				put("RSDSP_SMART", 1175);
				put("RSDSP_POP", 1174);
			}});
	public static  Map<Integer, Integer> RS_IMPRESSION_ID_MAP =
			Collections.unmodifiableMap(new HashMap<Integer, Integer>() {{
				put(AdSize.BANNER.code, 921);
				put(AdSize.MEDIUM_RECTANGLE.code, 922);
				put(AdSize.FULL_BANNER.code, 916 );
				put(AdSize.LEADERBOARD.code, 919 );
				put(AdSize.SMARTPHONE_INTERSTITAL_PORTRAIT.code, 923);
				put(AdSize.LARGE_RECTANGLE.code, 915);
				put(AdSize.DESKTOP_FULLSCREEN.code, 924);
				put(AdSize.SKYSCRAPER.code, 913);
				put(AdSize.SQUARE.code, 914);
				put(AdSize.MASTHEAD.code, 920);
				put(AdSize.LARGE_SKYSCRAPER.code, 918);
				put(AdSize.WIDE_SKYSCRAPER.code, 917);
				put(AdSize.SCROLL_300_250.code, 922);
				put(AdSize.FLIP.code, 922);
				put(AdSize.IN_READ.code, 922);
			}});

	/* LIVE*/
	public static  Map<Integer, Integer> BS_IMPRESSION_ID_MAP =
			Collections.unmodifiableMap(new HashMap<Integer, Integer>() {{
				put(AdSize.DESKTOP_FULLSCREEN.code, 965);
				put(AdSize.SMARTPHONE_INTERSTITAL_PORTRAIT.code, 966);
				put(AdSize.GENERIC_FULLSCREEN.code, 966);
				put(AdSize.PASS_FULLSCREEN.code, 966);
				put(AdSize.MEDIUM_RECTANGLE.code, 967);
				put(AdSize.BANNER.code, 968);
				put(AdSize.MASTHEAD.code, 969);
				put(AdSize.LEADERBOARD.code, 970);
				put(AdSize.LARGE_SKYSCRAPER.code, 971);
				put(AdSize.WIDE_SKYSCRAPER.code, 972);
				put(AdSize.FULL_BANNER.code, 973);
				put(AdSize.LARGE_RECTANGLE.code, 974);
				put(AdSize.SQUARE.code, 975);
				put(AdSize.SKYSCRAPER.code, 976);
				put(AdSize.SCROLL_300_250.code, 1089);
			}});

	private AdSize(int code,int width,int height,int arr[], String label) {
		this.code = code;
		this.width = width;
		this.height = height;
		this.creativeFormat = arr;
		this.label = label;
	}

	public static AdSize from(int code){
		for(AdSize adsize : AdSize.values()){
			if(adsize.code == code){
				return adsize;
			}
		}
		return BANNER;
	}

	public static String prop(AdSize adsize){
		return adsize.width+"_"+adsize.height;
	}

	public static String propX(AdSize adsize){
		return adsize.width+"x"+adsize.height;
	}

	public static HashMap<String, AdSize> loadAdSize(){
		HashMap<String, AdSize> map = new HashMap<String,AdSize>();
		for(AdSize adSize : AdSize.values()){
			map.put(AdSize.prop(adSize), adSize);
		}
		return map;
	}

	public static HashMap<Integer,AdSize> getAdSizeMap(){
		HashMap<Integer, AdSize> map = new HashMap<Integer,AdSize>();
		for(AdSize adSize : AdSize.values()){
			map.put(adSize.code, adSize);
		}
		return map;
	}


	public static boolean needDiv(AdSize adSize){
		int needDivSizeCodes []=new int[]{0,1,3,2,19,13,20,11,21,22,23};
		for(int k : needDivSizeCodes){
			if(adSize.code ==k){
				return true;
			}
		}
		return false;
	}

	public static boolean isRekmobIgnoreSize(int v){
		int rekIgnores []=new int[]{0,1,9,17,18,14};
		for(int k : rekIgnores){
			if(v ==k){
				return false;
			}
		}
		return true;

	}

	public static String getLabelString(int v){
		return AdSize.from(v).label;
	}

	public static AdSize fromSizes(int width,int height){
		for(AdSize adSize : AdSize.values()){
			if(adSize.width==width && adSize.height == height  ){
				return adSize;
			}
		}
		return null;
	}

}