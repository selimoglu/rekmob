package com.reklamstore.rexmob.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.Transient;

public class TotalStatUtil {
  
    @Transient
    private TotalStatUtil adxStatUtil;
    
    @Transient
    private TotalStatUtil anxStatUtil;
    
    @Transient
    private TotalStatUtil rekmobStatUtil;
    
    @Transient
    private TotalStatUtil adfStatUtil;

    public TotalStatUtil getAdfStatUtil() {
      return adfStatUtil;
    }

    public void setAdfStatUtil(TotalStatUtil adfStatUtil) {
      this.adfStatUtil = adfStatUtil;
    }

    @Transient
    private long totalReq;

    @Transient
    private long totalImp;

    @Transient
    private long totalClick;

    @Transient
    private double totalIncome;
    
    @Transient
    private double totalNetIncome;

    @Transient
    private long totalAreq;

    @Transient
    private long totalAimp;

    @Transient
    private long totalAclick;
    
    @Transient
    private long totalHreq;

    @Transient
    private long totalHimp;

    @Transient
    private long totalHclick;
    
    @Transient
    private double totalCost;
    
    @Transient
    private long totalClickV;
    
    @Transient
    private long totalIns;

    @Transient
    private double ctr;
    
    @Transient
    private double actr;
    
    @Transient
    private double hctr;
    
    @Transient
    private double fr;
    
    @Transient
    private double afr;
    
    @Transient
    private double hfr;

    @Transient
    private double cpm;
    
   
    

    /**
     * @return {@link #name}
     */
    public long getTotalReq() {
        return totalReq;
    }

    /**
     * @param totalReq the totalReq to set
     */
    public void setTotalReq(long totalReq) {
        this.totalReq = totalReq;
    }

    /**
     * @return {@link #name}
     */
    public long getTotalImp() {
        return totalImp;
    }

    /**
     * @param totalImp the totalImp to set
     */
    public void setTotalImp(long totalImp) {
        this.totalImp = totalImp;
    }

    /**
     * @return {@link #name}
     */
    public long getTotalClick() {
        return totalClick;
    }

    /**
     * @param totalClick the totalClick to set
     */
    public void setTotalClick(long totalClick) {
        this.totalClick = totalClick;
    }

    /**
     * @return {@link #name}
     */
    public double getTotalIncome() {
        return totalIncome;
    }

    /**
     * @param totalIncome the totalIncome to set
     */
    public void setTotalIncome(double totalIncome) {
        this.totalIncome = totalIncome;
    }

    public double getTotalNetIncome() {
      return totalNetIncome;
    }

    public void setTotalNetIncome(double totalNetIncome) {
      this.totalNetIncome = totalNetIncome;
    }

    /**
     * @return {@link #name}
     */
    public long getTotalAreq() {
        return totalAreq;
    }

    /**
     * @param totalAreq the totalAreq to set
     */
    public void setTotalAreq(long totalAreq) {
        this.totalAreq = totalAreq;
    }

    /**
     * @return {@link #name}
     */
    public long getTotalAimp() {
        return totalAimp;
    }

    /**
     * @param totalAimp the totalAimp to set
     */
    public void setTotalAimp(long totalAimp) {
        this.totalAimp = totalAimp;
    }

    /**
     * @return {@link #name}
     */
    public long getTotalAclick() {
        return totalAclick;
    }

    /**
     * @param totalAclick the totalAclick to set
     */
    public void setTotalAclick(long totalAclick) {
        this.totalAclick = totalAclick;
    }

    /**
     * @return {@link #name}
     */
    public long getTotalHreq() {
        return totalHreq;
    }

    /**
     * @param totalHreq the totalHreq to set
     */
    public void setTotalHreq(long totalHreq) {
        this.totalHreq = totalHreq;
    }

    /**
     * @return {@link #name}
     */
    public long getTotalHimp() {
        return totalHimp;
    }

    /**
     * @param totalHimp the totalHimp to set
     */
    public void setTotalHimp(long totalHimp) {
        this.totalHimp = totalHimp;
    }

    /**
     * @return {@link #name}
     */
    public long getTotalHclick() {
        return totalHclick;
    }

    /**
     * @param totalHclick the totalHclick to set
     */
    public void setTotalHclick(long totalHclick) {
        this.totalHclick = totalHclick;
    }

    /**
     * @return {@link #name}
     */
    public double getTotalCost() {
        return totalCost;
    }

    /**
     * @param totalCost the totalCost to set
     */
    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    /**
     * @return {@link #name}
     */
    public double getCtr() {
        return ctr;
    }

    /**
     * @param ctr the ctr to set
     */
    public void setCtr(double ctr) {
        this.ctr = ctr;
    }

    /**
     * @return {@link #name}
     */
    public double getActr() {
        return actr;
    }

    /**
     * @param actr the actr to set
     */
    public void setActr(double actr) {
        this.actr = actr;
    }

    /**
     * @return {@link #name}
     */
    public double getHctr() {
        return hctr;
    }

    /**
     * @param hctr the hctr to set
     */
    public void setHctr(double hctr) {
        this.hctr = hctr;
    }

    public double getFr() {
        return fr;
    }

    public void setFr(double fr) {
        this.fr = fr;
    }

    public double getAfr() {
        return afr;
    }

    public void setAfr(double afr) {
        this.afr = afr;
    }

    public double getHfr() {
        return hfr;
    }

    public void setHfr(double hfr) {
        this.hfr = hfr;
    }

    /**
     * @return {@link #name}
     */
    public double getCpm() {
        return cpm;
    }

    /**
     * @param ecpm the ecpm to set
     */
    public void setCpm(double cpm) {
        this.cpm = cpm;
    }

    /**
     * 
     * @param req
     */
    public void addReq(long req) {
	this.totalReq += req;
    }

    /**
     * 
     * @param imp
     */
    public void addImp(long imp) {
	this.totalImp += imp;
    }

    /**
     * 
     * @param click
     */
    public void addClick(long click) {
	this.totalClick += click;
    }
    
    /**
     * add income
     * 
     * @param income
     */
    public void addIncome(double income) {
	this.totalIncome += income;
    }
    
    public void addNetIncome(double netIncome) {
      this.totalNetIncome += netIncome;
      }
    
    public void addCost(double cost){
	this.totalCost+=cost;
    }
    
    /**
     * 
     * @param req
     */
    public void addAreq(long req) {
	this.totalAreq += req;
    }
    
    /**
     * 
     * @param imp
     */
    public void addAimp(long imp) {
	this.totalAimp += imp;
    }
    
    /**
     * 
     * @param click
     */
    public void addAclick(long click) {
	this.totalAclick += click;
    }
    /**
     * 
     * @param req
     */
    public void addHreq(long req) {
	this.totalHreq += req;
    }
    
    /**
     * @return {@link #name}
     */
    public long getTotalClickV() {
        return totalClickV;
    }

    /**
     * @param totalClickV the totalClickV to set
     */
    public void setTotalClickV(long totalClickV) {
        this.totalClickV = totalClickV;
    }

    /**
     * @return {@link #name}
     */
    public long getTotalIns() {
        return totalIns;
    }

    /**
     * @param totalIns the totalIns to set
     */
    public void setTotalIns(long totalIns) {
        this.totalIns = totalIns;
    }

    /**
     * 
     * @param imp
     */
    public void addHimp(long imp) {
	this.totalHimp += imp;
    }
    
    /**
     * 
     * @param click
     */
    public void addHclick(long click) {
	this.totalHclick += click;
    }
    
    public void addClickV(long clickV){
	this.totalClickV += clickV;
    }
    
    public void addIns(long ins){
	this.totalIns += ins;
    }

    public TotalStatUtil getAdxStatUtil() {
      return adxStatUtil;
    }

    public void setAdxStatUtil(TotalStatUtil adxStatUtil) {
      this.adxStatUtil = adxStatUtil;
    }

    public TotalStatUtil getAnxStatUtil() {
      return anxStatUtil;
    }

    public void setAnxStatUtil(TotalStatUtil anxStatUtil) {
      this.anxStatUtil = anxStatUtil;
    }

    public TotalStatUtil getRekmobStatUtil() {
      return rekmobStatUtil;
    }

    public void setRekmobStatUtil(TotalStatUtil rekmobStatUtil) {
      this.rekmobStatUtil = rekmobStatUtil;
    }
    
    public double getProfit(){
        if(totalNetIncome==0){
          return 0;
        }
        else return 100*((totalNetIncome-totalIncome)/totalNetIncome);
    }
    
    
    /*profit amount*/
    public double getPamount(){
      double value = totalNetIncome-totalIncome;
      BigDecimal bd = new BigDecimal(value);
      bd = bd.setScale(2, RoundingMode.HALF_UP);
      return bd.doubleValue();
    }
    
    public static TotalStatUtil createTotalStatUtil(){
      TotalStatUtil appRekmobTotalStat  = new TotalStatUtil();
      TotalStatUtil adxStatTotalStat = new TotalStatUtil();
      TotalStatUtil anxStatTotalStat = new TotalStatUtil();
      TotalStatUtil adfStatTotalStat = new TotalStatUtil();
      
      TotalStatUtil mainStatUtil = new TotalStatUtil();
      
      mainStatUtil.setRekmobStatUtil(appRekmobTotalStat);
      mainStatUtil.setAdxStatUtil(adxStatTotalStat);
      mainStatUtil.setAnxStatUtil(anxStatTotalStat);
      mainStatUtil.setAdfStatUtil(adfStatTotalStat);

      
      return mainStatUtil;
    }

}
