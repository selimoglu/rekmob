package com.reklamstore.rexmob.util;

import java.util.ArrayList;
import java.util.List;

public class GlobalAccManager {

    private static List<String> accManagers = new ArrayList<>();
    private static Integer index = -1;

    static {
        accManagers.add("Esra");
    }

    public static String getNext(){
        index++;
        if(index == accManagers.size())
            index = 0;

        return accManagers.get(index);
    }

}
