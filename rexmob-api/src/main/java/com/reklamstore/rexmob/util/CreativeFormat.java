package com.reklamstore.rexmob.util;

public enum CreativeFormat {
    BANNER(0), FULLSCREEN(1);

    private int code;

    private CreativeFormat(int code) {
	this.code = code;
    }

    public int getCode() {
	return code;
    }

}
