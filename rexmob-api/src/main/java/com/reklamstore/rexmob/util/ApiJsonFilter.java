package com.reklamstore.rexmob.util;

public class ApiJsonFilter  {
  public interface PublicView {};
  
  public interface AdminView extends PublicView{};
  
  public interface NetworkView extends AdminView{};
}
