package com.reklamstore.rexmob.util;

/**
 * @author Mehmet AGIRTOPCU
 * 
 */
public enum Status {
    PENDING(0), APPROVED(1), REJECTED(2),INVALID(3),DELETED(4);

    private int status;

    Status(int status) {
	this.status = status;
    }

    public int getStatus() {
	return status;
    }

    public boolean equals(Status status){
        return status != null && this.status == status.getStatus();
    }

    public boolean equals(Integer status){
        return status != null && this.status == status;
    }
}
