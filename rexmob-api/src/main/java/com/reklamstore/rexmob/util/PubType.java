package com.reklamstore.rexmob.util;

public enum PubType {

    DISPLAY(0),VIDEO(1), ALL(2);
  
    private int code;
  
    PubType(int code) {
      this.code = code;
  }
  
    public int getCode(){
        return code;
    }

    public boolean equals(PubType pubType){
        if(pubType == null)
            return false;
        return code == pubType.getCode();
    }

    public boolean equals(Integer pubType){
        if(pubType == null)
            return false;
        return code == pubType;
    }
}
