package com.reklamstore.rexmob.util;

public enum SellType {
    CPM(0), CPC(1);
    private int code;

    private SellType(int code) {
	this.code = code;
    }

    public int getType() {
	return code;
    }

}
