package com.reklamstore.rexmob.util;

import com.reklamstore.rexmob.app.AppUnitStat;
import org.json.JSONArray;
import org.json.JSONObject;

public class AppUnitJsonHelper {
        JSONObject unitObj;
        String timezone;
        
        JSONArray clickStats = new JSONArray();
        JSONArray impStats = new JSONArray();
        JSONArray reqStats = new JSONArray();
        JSONArray incomeStats = new JSONArray();
        JSONArray hclickStats = new JSONArray();
        JSONArray himpStats = new JSONArray();
        JSONArray hreqStats = new JSONArray();
        
        public AppUnitJsonHelper(String timezone,String unitName,Integer unitId) {
           this.unitObj = new JSONObject();
           unitObj.put("unitName",  unitName);
           unitObj.put("unitId", unitId);
           this.timezone = timezone;
        }
        
        public void putTotalStat(TotalStatUtil totalStatUtil){
          unitObj.put("totalReq", totalStatUtil.getTotalReq());
          unitObj.put("totalImp", totalStatUtil.getTotalImp());
          unitObj.put("totalClick", totalStatUtil.getTotalClick());
          unitObj.put("totalIncome", totalStatUtil.getTotalIncome());

          unitObj.put("totalHReq", totalStatUtil.getTotalHreq());
          unitObj.put("totalHImp", totalStatUtil.getTotalHimp());
          unitObj.put("totalHClick", totalStatUtil.getTotalHclick());
          
          // unitObj.put("totalAReq", getTotalStat().getTotalAreq());
          //  unitObj.put("totalAImp", getTotalStat().getTotalAimp());
          //  unitObj.put("totalAClick", getTotalStat().getTotalAclick());
        }
        private void putStatsToRoot(){
          unitObj.put("reqs", reqStats);
          unitObj.put("imps", impStats);
          unitObj.put("clicks", clickStats);
          unitObj.put("incomes", incomeStats);

          unitObj.put("hreqs", hreqStats);
          unitObj.put("himps", himpStats);
          unitObj.put("hclicks", hclickStats);
        }
        public void putStatsValues(AppUnitStat stat){
          long unixTime = DateUtil.sameDateNewTimezone(stat.getStatTime(), timezone).getTime();
          reqStats.put(getFieldJSON(unixTime, stat.getReq()));
          impStats.put(getFieldJSON(unixTime, stat.getImp()));
          clickStats.put(getFieldJSON(unixTime, stat.getClick()));
          incomeStats.put(getFieldJSON(unixTime, stat.getIncome()));

          //hreqStats.put(getFieldJSON(unixTime, stat.getHreq()));
          //himpStats.put(getFieldJSON(unixTime, stat.getHimp()));
          //hclickStats.put(getFieldJSON(unixTime, stat.getHclick()));
          hreqStats.put(getFieldJSON(unixTime, 0));
          himpStats.put(getFieldJSON(unixTime, 0));
          hclickStats.put(getFieldJSON(unixTime, 0));
        
          
        }
  public JSONObject getJSONObject() {
         putStatsToRoot();
         return unitObj;
  }

 
  
  private JSONArray getFieldJSON(long unixTime, Object count) {
    JSONArray ja = new JSONArray();
    ja.put(unixTime);
    ja.put(count);
    return ja;
  }
}
