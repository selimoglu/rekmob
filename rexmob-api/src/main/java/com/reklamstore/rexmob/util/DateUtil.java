package com.reklamstore.rexmob.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.util.StringUtils;

public class DateUtil {

    public static final TimeZone DEFAULT_TIMEZONE = TimeZone.getTimeZone("GMT");

    public static void main(String[] args) {

    	SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    	Date date1=null;
		try {
			date1 = formatter.parse("2018-01-01T12:00:00");
			System.out.println(getRubiNetworkFormatDate(date1, false));
			date1 = formatter.parse("2018-03-11T12:00:00");
			System.out.println(getRubiNetworkFormatDate(date1, false));
			date1 = formatter.parse("2018-05-11T12:00:00");
			System.out.println(getRubiNetworkFormatDate(date1, false));
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	
    	Date today = new Date(System.currentTimeMillis());
		TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
		today = DateUtils.truncate(today, Calendar.DATE);
		System.out.println(today.getTime());
	
		

    	
	TimeZone timeZone1 = TimeZone.getTimeZone("Europe/Istanbul"); // +2
	TimeZone timeZone7 = TimeZone.getTimeZone("MST"); // -7
	TimeZone timeZone4 = TimeZone.getTimeZone("SystemV/AST4"); // +4
	TimeZone timeZone6 = TimeZone.getTimeZone("BST"); // +6
	TimeZone timeZone14 = TimeZone.getTimeZone("Pacific/Kiritimati"); // +14

	// TEST 1
	// Date d = getCurrentGMTDate();
	// log(d.toString());
	// Date d2 = getCurrentDateWithTimeZone(timeZone7);
	// log(d2.toString());
	Calendar cx = new GregorianCalendar(2013, 11, 24);
	// c.setTimeZone(timeZone6);
	// log(c.getTime().toString());
	Calendar c = Calendar.getInstance();
	Calendar c1 = new GregorianCalendar(timeZone6);
	c1.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), c.get(Calendar.SECOND));

	Calendar c3=getToday();
	c3.add(Calendar.DAY_OF_MONTH, 1);
	System.out.println(c3);
    }

    public static void log(String value) {
	System.out.println(value);
    }

    /**
     * Overload of dateTimeConcate() method
     * 
     * @param date
     * @param time
     * @param timezone
     * @return
     */
    public static Date dateTimeConcateWithTimezone(Date date, String time, String timezone) {
	if (StringUtils.isEmpty(timezone)) {
	    timezone = "GMT";
	}
	return dateTimeConcateWithTimezone(date, time, TimeZone.getTimeZone(timezone));
    }

    /**
     * gets date with 0 hours, mins. Gets time HH:MM format as string. Combine
     * theese
     * 
     * @param date
     * @param time
     * @return
     */
    public static Date dateTimeConcateWithTimezone(Date date, String time, TimeZone timezone) {
	int hour = 0, min = 0;
	if (!StringUtils.isEmpty(time) && time.contains(":")) {
	    String[] hm = time.split(":", 2);
	    try {
		hour = Integer.parseInt(hm[0]);
		min = Integer.parseInt(hm[1]);
	    } catch (Exception e) {
		hour=0;
		min=0;
	    }
	}
	Calendar c = new GregorianCalendar(timezone);
	c.setTime(date);
	c.set(Calendar.HOUR_OF_DAY, hour);
	c.set(Calendar.MINUTE, min);
	return c.getTime();
    }
    
    
   

    /**
     * Ex: 01-12-2013 00:00 is java.util.Date with Default timezone +02:00. We
     * want to 01-12-2013 00:00 Date timezone +06:00. This method returns: 04:00
     * as time.
     * 
     * @param date
     * @param timezone
     * @return
     */
    public static Date sameDateNewTimezone(Date date, String timezone) {
	if (StringUtils.isEmpty(timezone) || date == null) {
	    return date;
	}
	Calendar c = Calendar.getInstance();
	c.setTime(date);
	Calendar c1 = new GregorianCalendar(TimeZone.getTimeZone(timezone));
	c1.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), c.get(Calendar.SECOND));
	return c1.getTime();
    }

    public static Date convertToTimezone(Calendar calendar, String timezone) {
	TimeZone tz = null;
	if (StringUtils.isEmpty(timezone)) {
	    tz = TimeZone.getDefault();
	} else {
	    tz = TimeZone.getTimeZone(timezone);
	}
	if (calendar == null) {
	    calendar = new GregorianCalendar();
	    calendar = DateUtils.truncate(calendar, Calendar.DAY_OF_MONTH);
	}

	calendar.setTimeZone(tz);
	return calendar.getTime();
    }

    public static Date convertToTimezone(Date date, String timezone) {
	TimeZone tz = null;
	if (StringUtils.isEmpty(timezone)) {
	    tz = TimeZone.getDefault();
	} else {
	    tz = TimeZone.getTimeZone(timezone);
	}

	return date;
    }

    public static Calendar getToday() {
	Calendar start = new GregorianCalendar();
	start = DateUtils.truncate(start, Calendar.DAY_OF_MONTH);
	return start;
    }

    public static Calendar getTomorrow() {
	Calendar end = new GregorianCalendar();
	end = DateUtils.truncate(end, Calendar.DAY_OF_MONTH);
	end.add(Calendar.DAY_OF_MONTH, 1);
	return end;
    }

    // public static Date getCurrentGMTDate() {
    // return getCurrentDateWithTimeZone(TimeZone.getTimeZone("GMT"));
    // }
    // public static Date getConvertedGMTDate(Date d) {
    // return getConvertedDateForTimezone(d, TimeZone.getTimeZone("GMT"));
    // }
    //
    // public static Date getCurrentDateWithTimeZone(TimeZone timeZone) {
    // return getConvertedDateForTimezone(new Date(System.currentTimeMillis()),
    // timeZone);
    // }
    //
    // public static Date getConvertedDateForTimezone(int year, int month, int
    // day, TimeZone timeZone) {
    // Calendar c = new GregorianCalendar(year, (month-1), day);
    // c.setTimeZone(DEFAULT_TIMEZONE);
    // return getConvertedDateForTimezone(c.getTime(), timeZone);
    // }
    //
    public static String getConvertedDateForTimezone(Date d) {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
		String dateString;
		dateString = formatter.format(d);
		System.out.println(dateString);
		return dateString;
    }
    
    public static String getPmNetworkFormatDate(Date date){
    	
		SimpleDateFormat pmDateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		String dateString = pmDateFormatter.format(date);		
		return dateString;
    }
    
    public static String getRubiNetworkFormatDate(Date date){
    	
    	
		SimpleDateFormat rubiDateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		String dateString = rubiDateFormatter.format(date);
		
		dateString += "-07:00";
		return dateString;
		
    }
    public static String getRubiNetworkFormatDate(Date date,boolean isStartDate){
    	
    	SimpleDateFormat rubiDateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		String dateString = rubiDateFormatter.format(date);
		
		Date dstDate=null;
		try {
			//2018 DST Sunday, 11 March	Sunday, 4 November
			dstDate = rubiDateFormatter.parse("2018-03-11T02:00:00");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		Calendar c = Calendar.getInstance(); 
		c.setTime(dstDate);
		int dstDay=c.get(Calendar.DAY_OF_YEAR);
		
		c.setTime(date);
		int currentDay=c.get(Calendar.DAY_OF_YEAR);
		
		if(currentDay==dstDay) {
			if(isStartDate)
				dateString += "-08:00";
			else dateString += "-07:00";
		}else if(date.before(dstDate)) {
			dateString += "-08:00";
		}else{
			dateString += "-08:00";
		}
		return dateString;
		
    }


    public static Date getMinimumDateOfMountByDate(Date date){
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(date);
    	cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
		cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Date getMaximumDateOfMountByDate(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		cal.set(Calendar.HOUR_OF_DAY, cal.getActualMaximum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getActualMaximum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getActualMaximum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getActualMaximum(Calendar.MILLISECOND));
		return cal.getTime();
	}
    

}
