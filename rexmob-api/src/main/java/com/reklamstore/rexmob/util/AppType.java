package com.reklamstore.rexmob.util;

public enum AppType {
	
	BRANDSAFE(0), MEDIUM(1), AGGRESSIVE(2);
	
	private int type;
	
	private AppType(int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}	

}
