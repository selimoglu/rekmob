package com.reklamstore.rexmob.admin;

import com.reklamstore.rexmob.app.AppUpdateRequest;
import com.reklamstore.rexmob.app.AppService;
import com.reklamstore.rexmob.app.RejectReasonType;
import com.reklamstore.rexmob.app.request.AppUnitIdsRequestDTO;
import com.reklamstore.rexmob.app.response.AppListResponse;
import com.reklamstore.rexmob.app.response.AppResponse;
import com.reklamstore.rexmob.app.response.AppUnitResponse;
import com.reklamstore.rexmob.authentication.AuthService;
import com.reklamstore.rexmob.authentication.Token;
import com.reklamstore.rexmob.base.BaseException;
import com.reklamstore.rexmob.base.BaseResponse;
import com.reklamstore.rexmob.payment.PaymentService;
import com.reklamstore.rexmob.payment.PubPaymentStatusType;
import com.reklamstore.rexmob.payment.request.PubPaymentCreateRequest;
import com.reklamstore.rexmob.payment.request.PubPaymentDetailCreateRequest;
import com.reklamstore.rexmob.payment.response.PubPaymentCreateResponse;
import com.reklamstore.rexmob.payment.response.PubPaymentDetailResponse;
import com.reklamstore.rexmob.payment.response.PubPaymentResponse;
import com.reklamstore.rexmob.payment.response.PubPaymentUserResponse;
import com.reklamstore.rexmob.user.User;
import com.reklamstore.rexmob.user.UserService;
import com.reklamstore.rexmob.user.request.UserUpdateRequest;
import com.reklamstore.rexmob.user.response.UserListResponse;
import com.reklamstore.rexmob.user.response.UserResponse;
import com.reklamstore.rexmob.util.Constants;
import com.reklamstore.rexmob.util.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import static org.springframework.http.ResponseEntity.ok;

@Controller
@PreAuthorize("hasAuthority('ADMIN')")
@RequestMapping("/admin")
public class AdminController {

    private UserService userService;
    private AuthService authService;
    private AppService appService;
    private PaymentService paymentService;

    @Autowired
    public AdminController(UserService userService,
                           AuthService authService,
                           AppService appService,
                           PaymentService paymentService) {
        this.userService = userService;
        this.authService = authService;
        this.appService = appService;
        this.paymentService = paymentService;
    }

    @ResponseBody()
    @PostMapping(value = "/user/update/{userId}")
    public UserResponse updateUser(@PathVariable Integer userId, @RequestBody UserUpdateRequest updateRequest) {
        try {
            return new UserResponse(userService.update(userId, updateRequest));
        } catch (BaseException e) {
            return new UserResponse(e);
        }
    }

    @ResponseBody()
    @GetMapping("/app/{id}/app-units")
    public ResponseEntity getAppUnitsByApp(@PathVariable Integer id) {
        return ok(appService.getAppUnitByApp(id));
    }

    @ResponseBody()
    @GetMapping(value = "/user/{userId}")
    public UserResponse getUser(@PathVariable Integer userId) {
        try {
            return new UserResponse(userService.get(userId));
        } catch (BaseException e) {
            return new UserResponse(e);
        }
    }

    @ResponseBody()
    @GetMapping(value = "/users/last")
    public UserListResponse getLastUsers() {
        return new UserListResponse(userService.getLastUsers());
    }


    @ResponseBody()
    @GetMapping(value = "/user/login-as/{userId}")
    public ResponseEntity loginAs(@PathVariable Integer userId) {
        Token token = authService.loginAs(userId);
        return toResponse(token);
    }

    @ResponseBody()
    @PostMapping(value = "/user/ban/{userId}")
    public BaseResponse banUser(@PathVariable Integer userId) {
        try {
            userService.ban(userId);
            return appService.banByUserId(userId);
        } catch (BaseException e) {
            return new BaseResponse(e);
        }
    }
    @ResponseBody()
    @PostMapping(value = "/user/reactivate/{userId}")
    public BaseResponse reactivateUser(@PathVariable Integer userId) {
        try {
            userService.reactivateUser(userId);
            return appService.reactivateByUserId(userId);
        } catch (BaseException e) {
            return new BaseResponse(e);
        }
    }

    @ResponseBody()
    @GetMapping(value = "/app/status")
    public AppListResponse getAppByStatus(@RequestParam Status status) {
        return new AppListResponse(appService.getAppByStatus(status));
    }

    @ResponseBody()
    @GetMapping(value = "/app/user/{userId}")
    public AppListResponse getAppByUser(@PathVariable Integer userId) {
        return new AppListResponse(appService.getAppByUser(userId));
    }

    @ResponseBody()
    @PostMapping(value = "/app/status/{id}")
    public AppResponse changeAppStatus(@PathVariable Integer id,
                                       @RequestParam Status status,
                                       @RequestParam(required = false) RejectReasonType rejectReasonType) {
        try {
            return new AppResponse(appService.changeAppStatusById(id, status, rejectReasonType));
        } catch (BaseException e) {
            return new AppResponse(e);
        }
    }

    @ResponseBody()
    @PostMapping(value = "/app/update/{id}")
    public AppResponse updateApp(@PathVariable Integer id,
                                 @RequestBody AppUpdateRequest updateRequest) {
        try {
            return new AppResponse(appService.updateApp(id, updateRequest));
        } catch (BaseException e) {
            return new AppResponse(e);
        }
    }

    @ResponseBody()
    @GetMapping(value = "/app-unit/user/{userId}")
    public AppUnitResponse getAppUnitsByUser(@PathVariable Integer userId) {
        return new AppUnitResponse(appService.getAppUnitByUser(userId));
    }

    @ResponseBody()
    @PostMapping(value = "/app/ban/{appId}")
    public BaseResponse banApp(@PathVariable Integer appId) {
        try {
            return appService.ban(appId);
        } catch (BaseException e) {
            return new BaseResponse(e);
        }
    }

    @ResponseBody()
    @PostMapping(value = "/app-unit/ban/{appUnitId}")
    public BaseResponse banAppUnit(@PathVariable Integer appUnitId) {
        try {
            return appService.banUnit(appUnitId);
        } catch (BaseException e) {
            return new BaseResponse(e);
        }
    }

    @ResponseBody()
    @PostMapping(value = "/app-unit/prices/{appUnitId}")
    public BaseResponse changePrices(@PathVariable Integer appUnitId,
                                     @RequestParam(required = false) Double topCpm,
                                     @RequestParam(required = false) Double fixCpm,
                                     @RequestParam(required = false) Double rekmobPercent
    ) {
        try {
            return appService.changeAppUnitPrices(appUnitId, topCpm, fixCpm, rekmobPercent);
        } catch (BaseException e) {
            return new BaseResponse(e);
        }
    }

    @ResponseBody()
    @PostMapping(value = "/app-unit/network-ids")
    public BaseResponse changeNetworkIds(@RequestBody AppUnitIdsRequestDTO requestDTO
    ) {
        try {
            return appService.changeAppUnitNetworkIds(requestDTO);
        } catch (BaseException e) {
            return new BaseResponse(e);
        }
    }

    @ResponseBody()
    @PostMapping(value = "/pub-payment/calculate")
    public BaseResponse calculatePubPayment() {
        paymentService.calculatePaymentsToPaymentApi();
        return new BaseResponse();
    }

    @ResponseBody()
    @PostMapping(value = "/pub-payment/create")
    public PubPaymentCreateResponse createPubPayment(@RequestBody PubPaymentCreateRequest createRequest) {
        try {
            return new PubPaymentCreateResponse(paymentService.createPubPayment(createRequest));
        } catch (BaseException e) {
            return new PubPaymentCreateResponse(e);
        }
    }

    @ResponseBody()
    @PostMapping(value = "/pub-payment/status/{id}")
    public PubPaymentCreateResponse changePubPaymentStatus(@PathVariable Integer id,
                                                           @RequestParam PubPaymentStatusType statusType) {
        try {
            return new PubPaymentCreateResponse(paymentService.changePubPaymentStatus(id, statusType));
        } catch (BaseException e) {
            return new PubPaymentCreateResponse(e);
        }
    }

    @ResponseBody()
    @PostMapping(value = "/pub-payment/delete/{id}")
    public BaseResponse deletePubPayment(@PathVariable Integer id) {
        paymentService.deletePubPaymentById(id);
        return new BaseResponse();
    }

    @ResponseBody()
    @GetMapping(value = "/pub-payment/{id}")
    public PubPaymentCreateResponse getPubPayment(@PathVariable Integer id) {
        try {
            return new PubPaymentCreateResponse(paymentService.getPubPaymentById(id));
        } catch (BaseException e) {
            return new PubPaymentCreateResponse(e);
        }
    }

    @ResponseBody()
    @GetMapping(value = "/pub-payment")
    public PubPaymentUserResponse getPubPaymentUsersByDate(
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM") Date paymentDate,
            @RequestParam(required = false) PubPaymentStatusType statusType) {

        return new PubPaymentUserResponse(paymentService.getPubPaymentUserListByPaymentDate(paymentDate, statusType));
    }

    @ResponseBody()
    @GetMapping(value = "/pub-payment/report/{userId}")
    public Object getPubPaymentReportByUser(@PathVariable Integer userId,
                                            @RequestParam(required = false) Integer limit,
                                            @RequestParam(required = false) Boolean asc) {
        try {
            return paymentService.getPaymentReportsByUser(userId, limit, asc);
        } catch (BaseException e) {
            return new BaseResponse(e);
        }
    }

    @ResponseBody()
    @GetMapping(value = "/pub-payment/report/all")
    public Object getPubPaymentReportByUser(@RequestParam(required = true) String progressPaymentEnd,
                                            @RequestParam(required = true) Integer prevMonth) {
        Object apiPayments = null;
        paymentService.comparePayments(progressPaymentEnd, prevMonth);
        return apiPayments;
    }

    @ResponseBody()
    @PostMapping(value = "/pub-payment/sync")
    public void syncUsersToPaymentApi(@RequestBody ArrayList<Integer> userIds) {
        paymentService.syncUsersToPaymentApi(userIds);
    }
    @ResponseBody()
    @GetMapping(value = "/pub-payment/transaction-sync")
    public Object syncTransactionToPaymentApi(@RequestParam(required = true) String progressPaymentEnd,
                                            @RequestParam(required = true) Integer prevMonth) {
        Object transactions = null;
        paymentService.syncTransactions(progressPaymentEnd, prevMonth);
        return transactions;
    }
    @ResponseBody()
    @PostMapping(value = "/pub-payment/upload/invoice")
    public BaseResponse uploadInvoice(@RequestParam("file") MultipartFile file,
                                      @RequestParam("paymentId") Integer paymentId,
                                      @RequestParam("userId") Integer userId,
                                      @RequestParam("invoiceStartDate") String invoiceStartDate,
                                      @RequestParam("invoiceEndDate") String invoiceEndDate){
        paymentService.uploadInvoice(file,paymentId,userId,invoiceStartDate,invoiceEndDate);
        return new BaseResponse();
    }

    @ResponseBody()
    @PostMapping(value = "/pub-payment/calculate/custom")
    public void calculateOnePaymentToPaymentApi(@RequestBody ArrayList<Integer> userIds) {
        for (Integer userId : userIds) {
            paymentService.calculateOnePaymentToPaymentApi(userId);
        }
    }


    @ResponseBody()
    @GetMapping(value = "/pub-payment/user/{userId}")
    public PubPaymentResponse getPubPaymentByUser(@PathVariable Integer userId) {
        return new PubPaymentResponse(paymentService.getPubPaymentByUser(userId));
    }

    @ResponseBody()
    @PostMapping(value = "/pub-payment-detail/create")
    public PubPaymentDetailResponse createPubPaymentDetail(@RequestBody PubPaymentDetailCreateRequest createRequest) {
        return new PubPaymentDetailResponse(paymentService.createPubPaymentDetail(createRequest));
    }

    @ResponseBody()
    @GetMapping(value = "/pub-payment-detail/{userId}")
    public PubPaymentDetailResponse getPubPaymentDetailByUser(@PathVariable Integer userId) {
        try {
            return new PubPaymentDetailResponse(paymentService.getPubPaymentDetailByUserOrThrow(userId));
        } catch (BaseException e) {
            return new PubPaymentDetailResponse(e);
        }
    }

    /**
     * Test Start /
     *
     * @ResponseBody()
     * @PostMapping(value = "/test/{email}")
     * public BaseResponse signUpRequest(@PathVariable String email){
     * paymentService.testSignUp(email);
     * return new BaseResponse();
     * }
     * /** Test End
     **/

    @ResponseBody()
    @PostMapping(value = "/elastic-proxy")
    public String simpleProxy(@RequestBody String reqBody, @RequestParam String path) {

        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        HttpEntity<String> entity = new HttpEntity<String>(reqBody, headers);
        template.setErrorHandler(new ResponseErrorHandler() {

            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                return response.getRawStatusCode() != 200;
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {
                //System.out.println(""+IOUtils.toString(response.getBody()));
            }
        });
        return template.postForObject("http://" + Constants.ELASTIC_SEARCH_HOST + ":9200/" + path, entity, String.class);
    }

    private ResponseEntity toResponse(Token token) {
        return ok(Collections.singletonMap("token", token));
    }
}
