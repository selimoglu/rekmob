package com.reklamstore.rexmob.address;

import com.reklamstore.rexmob.user.User;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "address")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ADR_ID")
    private Integer id;

    @NotEmpty
    @Size(min = 1, max = 400)
    @Column(name = "ADR_TEXT")
    private String adrText;

    @Size(min = 1, max = 100)
    @Column(name = "TOWN")
    private String town;

    @NotEmpty
    @Size(min = 1, max = 100)
    @Column(name = "CITY")
    private String city;

    @NotEmpty
    @Size(min = 1, max = 100)
    @Column(name = "COUNTRY")
    private String country;

    @NotEmpty
    @Size(min = 1, max = 10)
    @Column(name = "ZIP_CODE")
    private String zipCode;

    @Column(name = "USER_ID")
    private int userId;

    @Transient
    private User user;

    @Column(name = "ACTIVE", nullable = true)
    private int active;

    @Column(name = "ARCHIVED", nullable = true)
    private int archived;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAdrText() {
        return adrText;
    }

    public void setAdrText(String adrText) {
        this.adrText = adrText;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getArchived() {
        return archived;
    }

    public void setArchived(int archived) {
        this.archived = archived;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public boolean isActive() {
        return active == 1;
    }

    public void setActive(boolean var) {
        this.active = var ? 1 : 0;
    }

}
