package com.reklamstore.rexmob.tag;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "tag")
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tag_id")
    private Integer id;
    
    @Column(name="name")
    private String name;
    
    @Column(name="super_id")
    private Integer superId;
    
    @Transient
    private List<Integer> adIdList;

    /**
     * @return {@link #name}
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return {@link #name}
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return {@link #name}
     */
    public Integer getSuperId() {
        return superId;
    }

    /**
     * @param superId the superId to set
     */
    public void setSuperId(Integer superId) {
        this.superId = superId;
    }
    
    /**
     * @return {@link #name}
     */
    public List<Integer> getAdIdList() {
        return adIdList;
    }

    /**
     * @param adIdList the adIdList to set
     */
    public void setAdIdList(List<Integer> adIdList) {
        this.adIdList = adIdList;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
