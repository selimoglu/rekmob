package com.reklamstore.rexmob.base;

public class BaseResponse {
    private Integer code = 200;
    private String message = "success";
    private Boolean isArray;

    public BaseResponse(){
    }

    public BaseResponse(BaseException exception){
        this.code = exception.getCode();
        this.message = exception.getMessage();
    }

    public BaseResponse(Boolean isArray) {
        this.isArray = isArray;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getIsArray() {
        return isArray;
    }

    public void setIsArray(Boolean array) {
        isArray = array;
    }
}
