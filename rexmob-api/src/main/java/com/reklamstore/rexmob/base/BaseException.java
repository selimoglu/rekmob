package com.reklamstore.rexmob.base;

public class BaseException extends Exception{
    private Integer code = 400;

    public BaseException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public BaseException(String message) {
        super(message);
    }

    public Integer getCode() {
        return code;
    }
}
