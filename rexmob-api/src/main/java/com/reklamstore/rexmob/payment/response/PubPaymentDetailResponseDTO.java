package com.reklamstore.rexmob.payment.response;

import com.reklamstore.rexmob.payment.detail.PubPaymentDetail;

import java.util.Date;

public class PubPaymentDetailResponseDTO {

    private Integer id;
    private Integer userId;
    private String iban;
    private String bankName;
    private String benchCode;
    private String accountOwner;
    private String accountNumber;
    private String swiftCode;
    private String paypal;

    private Integer invoiced;

    private String taxId;
    private double taxRate;
    private double vatRate;

    private String tckn;
    private String invoiceAddress;
    private String paymentMethod;
    private String taxAdmin;
    private String companyName;
    private Integer companyType;
    private Date createDate;
    private Date updateDate;

    public PubPaymentDetailResponseDTO() {
    }

    public PubPaymentDetailResponseDTO(PubPaymentDetail detail) {
        this.id = detail.getId();
        this.userId = detail.getUserId();

        this.iban = detail.getIban();
        this.bankName = detail.getBankName();
        this.benchCode = detail.getBenchCode();
        this.accountOwner = detail.getAccountOwner();
        this.accountNumber = detail.getAccountNumber();
        this.swiftCode = detail.getSwiftCode();
        this.paypal = detail.getPaypal();

        this.invoiced = detail.getInvoiced();

        this.taxId = detail.getTaxId();
        this.taxRate = detail.getTaxRate();
        this.vatRate = detail.getVatRate();

        this.tckn = detail.getTckn();
        this.invoiceAddress = detail.getInvoiceAddress();
        this.paymentMethod = detail.getPaymentMethod();
        this.taxAdmin = detail.getTaxAdmin();
        this.companyName = detail.getCompanyName();
        this.companyType = detail.getCompanyType();
        this.createDate = detail.getCreateDate();
        this.updateDate = detail.getUpdateDate();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBenchCode() {
        return benchCode;
    }

    public void setBenchCode(String benchCode) {
        this.benchCode = benchCode;
    }

    public String getAccountOwner() {
        return accountOwner;
    }

    public void setAccountOwner(String accountOwner) {
        this.accountOwner = accountOwner;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getPaypal() {
        return paypal;
    }

    public void setPaypal(String paypal) {
        this.paypal = paypal;
    }

    public Integer getInvoiced() {
        return invoiced;
    }

    public void setInvoiced(Integer invoiced) {
        this.invoiced = invoiced;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public double getVatRate() {
        return vatRate;
    }

    public void setVatRate(double vatRate) {
        this.vatRate = vatRate;
    }

    public String getTckn() {
        return tckn;
    }

    public void setTckn(String tckn) {
        this.tckn = tckn;
    }

    public String getInvoiceAddress() {
        return invoiceAddress;
    }

    public void setInvoiceAddress(String invoiceAddress) {
        this.invoiceAddress = invoiceAddress;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getTaxAdmin() {
        return taxAdmin;
    }

    public void setTaxAdmin(String taxAdmin) {
        this.taxAdmin = taxAdmin;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Integer getCompanyType() {
        return companyType;
    }

    public void setCompanyType(Integer companyType) {
        this.companyType = companyType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
