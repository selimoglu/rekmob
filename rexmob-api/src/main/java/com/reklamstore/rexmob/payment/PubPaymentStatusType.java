package com.reklamstore.rexmob.payment;

public enum PubPaymentStatusType {

    WAITING_INVOICE(0),PAID(1), WRONG_ID(2),WRONG_ACCOUNT(3),
    WRONG_ADDRESS(4),WRONG_BANK(5),TRANSFERRED(6),PUB_LATER(7),
    INVOICE_APPROVAL(8),PAYING(9),WAITING_DATE(10);
  
    private int code;

    PubPaymentStatusType(int code) {
      this.code = code;
    }
  
    public int getCode(){
      return code;
    }

    public static PubPaymentStatusType valueOf(int i) {
        return values()[i];
    }
}
