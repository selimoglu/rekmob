package com.reklamstore.rexmob.payment.api.request;


public class PaymentInvoiceUploadRequest {

    private String invoice_file;
    private String progress_payment_start;
    private Integer user;

    public PaymentInvoiceUploadRequest() {
    }

    public String getInvoice_file() {
        return invoice_file;
    }

    public void setInvoice_file(String invoice_file) {
        this.invoice_file = invoice_file;
    }

    public String getProgress_payment_start() {
        return progress_payment_start;
    }

    public void setProgress_payment_start(String progress_payment_start) {
        this.progress_payment_start = progress_payment_start;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }
}
