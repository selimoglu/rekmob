package com.reklamstore.rexmob.payment.api.request;


public class PaymentInvoiceStatusRequest {

    private Boolean invoice_status;

    public PaymentInvoiceStatusRequest() {
    }

    public Boolean getInvoice_status() {
        return invoice_status;
    }

    public void setInvoice_status(Boolean invoice_status) {
        this.invoice_status = invoice_status;
    }
}
