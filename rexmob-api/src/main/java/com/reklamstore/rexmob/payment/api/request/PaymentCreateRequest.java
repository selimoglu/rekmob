package com.reklamstore.rexmob.payment.api.request;

import netscape.javascript.JSObject;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

public class PaymentCreateRequest {

    private Double accumulated_revenue = 0.0;
    private Integer currency;
    private Integer payment_period;
    private String description;
    private Double amount_of_payment;
    private Integer platform;
    private String status;
    private Integer user;
    private Integer payment_method;
    private Integer branch = 1;
    private String payment_date;
    private String progress_payment_start;
    private String progress_payment_end;

    public Double getAccumulated_revenue() {
        return accumulated_revenue;
    }

    public void setAccumulated_revenue(Double accumulated_revenue) {
        this.accumulated_revenue = accumulated_revenue;
    }

    public Integer getCurrency() {
        return currency;
    }

    public void setCurrency(Integer currency) {
        this.currency = currency;
    }

    public Integer getPayment_period() {
        return payment_period;
    }

    public void setPayment_period(Integer payment_period) {
        this.payment_period = payment_period;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAmount_of_payment() {
        return amount_of_payment;
    }

    public void setAmount_of_payment(Double amount_of_payment) {
        this.amount_of_payment = amount_of_payment;
    }

    public Integer getPlatform() {
        return platform;
    }

    public void setPlatform(Integer platform) {
        this.platform = platform;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public Integer getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(Integer payment_method) {
        this.payment_method = payment_method;
    }

    public Integer getBranch() {
        return branch;
    }

    public void setBranch(Integer branch) {
        this.branch = branch;
    }

    public String getPayment_date() {
        return payment_date;
    }

    public void setPayment_date(String payment_date) {
        this.payment_date = payment_date;
    }

    public String getProgress_payment_start() {
        return progress_payment_start;
    }

    public void setProgress_payment_start(String progress_payment_start) {
        this.progress_payment_start = progress_payment_start;
    }

    public String getProgress_payment_end() {
        return progress_payment_end;
    }

    public void setProgress_payment_end(String progress_payment_end) {
        this.progress_payment_end = progress_payment_end;
    }
}
