package com.reklamstore.rexmob.payment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.ByteStreams;
import com.reklamstore.rexmob.base.BaseException;

import com.reklamstore.rexmob.payment.api.PaymentApiUser;
import com.reklamstore.rexmob.payment.api.request.*;
import com.reklamstore.rexmob.payment.api.response.PaymentApiResponse;
import com.reklamstore.rexmob.payment.api.response.PaymentApiUserResponse;
import com.reklamstore.rexmob.user.CurrencyCodeType;
import com.reklamstore.rexmob.payment.detail.PubPaymentDetail;
import com.reklamstore.rexmob.payment.detail.PubPaymentDetailRepository;
import com.reklamstore.rexmob.payment.request.PubPaymentCreateRequest;
import com.reklamstore.rexmob.payment.request.PubPaymentDetailCreateRequest;
import com.reklamstore.rexmob.payment.response.PubPaymentDetailResponseDTO;
import com.reklamstore.rexmob.payment.response.PubPaymentResponseDTO;
import com.reklamstore.rexmob.payment.response.PubPaymentUserResponseDTO;
import com.reklamstore.rexmob.user.SimpleUser;
import com.reklamstore.rexmob.user.User;
import com.reklamstore.rexmob.user.UserService;
import com.reklamstore.rexmob.user.exception.UserNotFoundException;
import com.reklamstore.rexmob.util.Constants;
import com.reklamstore.rexmob.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PaymentService {

    @Value("${oners.paymentApi.url}")
    private String paymentApiUrl;

    @Value("${oners.paymentApi.secureUrl}")
    private String paymentApiSecureUrl;

    private PubPaymentRepository pubPaymentRepository;
    private PubPaymentDetailRepository pubPaymentDetailRepository;

    private UserService userService;
    private Logger logger = LogManager.getLogger(PaymentService.class);

    final static String AUTHORIZATION_KEY="Bearer 3aMlgwQUpHzBG2nPpho7tDYQ9TKLHo";

    @Autowired
    public PaymentService(PubPaymentRepository pubPaymentRepository,
                          PubPaymentDetailRepository pubPaymentDetailRepository,
                          UserService userService) {

        this.pubPaymentRepository = pubPaymentRepository;
        this.pubPaymentDetailRepository = pubPaymentDetailRepository;

        this.userService = userService;
    }

    @Transactional
    public PubPaymentUserResponseDTO createPubPayment(PubPaymentCreateRequest createDTO) throws BaseException {
        PubPayment pubPayment = pubPaymentRepository.findOneByUserIdAndPaymentDate(createDTO.getUserId(),
                DateUtil.getMinimumDateOfMountByDate(createDTO.getPaymentDate()),
                DateUtil.getMaximumDateOfMountByDate(createDTO.getPaymentDate()));

        if(pubPayment == null)
            pubPayment = new PubPayment();

        pubPayment.setUserId(createDTO.getUserId());
        pubPayment.setAmount(createDTO.getAmount());
        pubPayment.setAmountUsd(createDTO.getAmountUsd());
        pubPayment.setPaymentDate(createDTO.getPaymentDate());
        pubPayment.setStatus(createDTO.getStatus().getCode());

        SimpleUser user = userService.getSimple(createDTO.getUserId());
        if(user == null)
            return new PubPaymentUserResponseDTO(savePubPayment(pubPayment));
        else
            return new PubPaymentUserResponseDTO(savePubPayment(pubPayment), user.getFullName());
    }

    @Transactional
    public PubPaymentUserResponseDTO changePubPaymentStatus(Integer id, PubPaymentStatusType statusType) throws BaseException {
        PubPayment pubPayment = getOnePubPaymentOrThrow(id);
        pubPayment.setStatus(statusType.getCode());

        SimpleUser user = userService.getSimple(pubPayment.getUserId());
        if(user == null)
            return new PubPaymentUserResponseDTO(savePubPayment(pubPayment));
        else
            return new PubPaymentUserResponseDTO(savePubPayment(pubPayment), user.getFullName());
    }

    public void deletePubPaymentById(Integer id){
        pubPaymentRepository.deleteOneById(id);
    }

    public PubPaymentUserResponseDTO getPubPaymentById(Integer id) throws BaseException {
        PubPayment payment = getOnePubPaymentOrThrow(id);

        SimpleUser user = userService.getSimple(payment.getUserId());
        if(user == null)
            return new PubPaymentUserResponseDTO(payment);
        else
            return new PubPaymentUserResponseDTO(payment, user.getFullName());
    }

    public List<PubPaymentResponseDTO> getPubPaymentByUser(Integer userId) {
        List<PubPayment> payments = pubPaymentRepository.findByUserIdOrderByPaymentDateDesc(userId);
        List<PubPaymentResponseDTO> responseList = new ArrayList<>();
        for(PubPayment payment: payments)
            responseList.add(new PubPaymentResponseDTO(payment));

        return responseList;
    }

    public List<PubPaymentUserResponseDTO> getPubPaymentUserListByPaymentDate(
            Date paymentDate, PubPaymentStatusType statusType){
        Date startDate = DateUtil.getMinimumDateOfMountByDate(paymentDate);
        Date endDate = DateUtil.getMaximumDateOfMountByDate(paymentDate);

        List<PubPayment> pubPayments;
        if(statusType == null)
            pubPayments = pubPaymentRepository.findByPaymentDateByMonth(startDate, endDate);
        else
            pubPayments = pubPaymentRepository.findByPaymentDateByMonthAndStatus(startDate, endDate, statusType.getCode());

        List<PubPaymentUserResponseDTO> response = new ArrayList<>();
        if(pubPayments.size() == 0)
            return response;

        List<Integer> userIds = new ArrayList<>();
        for(PubPayment payment: pubPayments)
            userIds.add(payment.getUserId());

        List<SimpleUser> users = userService.getSimple(userIds);
        HashMap<Integer, SimpleUser> userMap = new HashMap<>();
        for(SimpleUser user: users)
            userMap.put(user.getId(), user);

        SimpleUser user;
        for(PubPayment pubPayment: pubPayments){
            user = userMap.get(pubPayment.getUserId());
            if(user == null)
                response.add(new PubPaymentUserResponseDTO(pubPayment));
            else
                response.add(new PubPaymentUserResponseDTO(pubPayment, user.getFullName()));
        }
        return response;
    }

    public PubPayment getPubPaymentByUserAndPaymentDate(Integer userId, Date lastPaidDate){
        Date startDate = DateUtil.getMinimumDateOfMountByDate(lastPaidDate);
        Date endDate = DateUtil.getMaximumDateOfMountByDate(lastPaidDate);

        return pubPaymentRepository.findOneByUserIdAndPaymentDate(userId, startDate, endDate);
    }

    @Transactional
    public PubPaymentDetailResponseDTO createPubPaymentDetail(PubPaymentDetailCreateRequest createDTO) {
        PubPaymentDetail paymentDetail = getOnePubPaymentDetailOptionalByUserId(createDTO.getUserId())
                .orElseGet(PubPaymentDetail::new);

        if(paymentDetail.getUserId() == null)
            paymentDetail.setCreateDate(new Date());

        paymentDetail.setUserId(createDTO.getUserId());
        paymentDetail.setIban(createDTO.getIban());
        paymentDetail.setBankName(createDTO.getBankName());
        paymentDetail.setBenchCode(createDTO.getBenchCode());
        paymentDetail.setAccountOwner(createDTO.getAccountOwner());
        paymentDetail.setAccountNumber(createDTO.getAccountNumber());
        paymentDetail.setSwiftCode(createDTO.getSwiftCode());
        paymentDetail.setPaypal(createDTO.getPaypal());
        paymentDetail.setTaxId(createDTO.getTaxId());
        paymentDetail.setTaxRate(createDTO.getTaxRate());
        paymentDetail.setVatRate(createDTO.getVatRate());
        paymentDetail.setTckn(createDTO.getTckn());
        paymentDetail.setInvoiceName(createDTO.getInvoiceName());
        paymentDetail.setInvoiceAddress(createDTO.getInvoiceAddress());
        paymentDetail.setPaymentMethod(createDTO.getPaymentMethod());
        paymentDetail.setTaxAdmin(createDTO.getTaxAdmin());
        paymentDetail.setCompanyName(createDTO.getCompanyName());
        paymentDetail.setCompanyType(createDTO.getCompanyType());
        paymentDetail.setUpdateDate(new Date());

        return new PubPaymentDetailResponseDTO(savePubPaymentDetail(paymentDetail));
    }

    public void calculatePaymentsToPaymentApi() {
        errorCount=0;
        Date now = new Date();
        Date paymentStartDate = DateUtils.addMonths(now, -1);

        paymentStartDate = DateUtils.truncate(paymentStartDate, Calendar.MONTH);
        Date paymentEndDate = DateUtils.truncate(now, Calendar.MONTH);

        String query="{ \"aggs\": { \"userId_term\": { \"terms\": { \"field\": \"userId\", \"size\": 20000 }, \"aggs\": "
                + "{ \"pubRevenue_sum\": { \"sum\": { \"field\": \"pubRevenue\" } }, \"pubRevenueUsd_sum\": { \"sum\": { \"field\": \"pubRevenueUsd\" } } } } }, "
                + "\"query\": { \"bool\": { \"filter\": { \"bool\": { \"must\": [ { \"range\": { \"statTime\": "
                + "{ \"gte\": "+paymentStartDate.getTime()+", \"lt\":"+paymentEndDate.getTime()+" } } } ] } } } } }";

        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        HttpEntity<String> entity = new HttpEntity<>(query, headers);
        template.setErrorHandler(new ResponseErrorHandler() {

            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                return response.getRawStatusCode() < 200 || response.getRawStatusCode() > 299;
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {
                //System.out.println(""+ IOUtils.toString(response.getBody()));
            }
        });
        String response = template.postForObject("http://" + Constants.ELASTIC_SEARCH_HOST +
                ":9200/publisher_stats/app_unit_stat/_search?size=0", entity, String.class);

        JSONObject jResponse = new JSONObject(response);
        JSONObject userTerm = jResponse.getJSONObject("aggregations").getJSONObject("userId_term");
        JSONArray buckets = userTerm.getJSONArray("buckets");

        paymentEndDate = DateUtils.addDays(paymentEndDate, -1);
        PubPaymentDetail paymentDetail;
        User user;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        for(int i=0; i<buckets.length(); i++) {
            try {
                JSONObject jBucket = buckets.getJSONObject(i);
                int userId = jBucket.getInt("key");

                JSONObject jRev = jBucket.getJSONObject("pubRevenue_sum");
                double rev = jRev.getDouble("value");

                if(rev < 0.01)
                    continue;

                rev = ((int)(rev*100))/100.0;

                JSONObject jUsdRev = jBucket.getJSONObject("pubRevenueUsd_sum");
                double usdRev = jUsdRev.getDouble("value");
                usdRev = ((int)(usdRev*100))/100.0;
                if(usdRev < 0.01)
                    continue;
                user = userService.getOne(userId);

                if (user == null)
                    continue;
                if (user.getBan() || !user.getActive())
                    continue;
                User finalUser = user;
                paymentDetail = getPubPaymentDetailByUser(userId);
                if(paymentDetail == null)
                    paymentDetail = new PubPaymentDetail();

                PaymentApiUser paymentUser = getPaymentUser(user);
                if (paymentUser == null){
                    logger.error("Payment-Api: user not found and start signup."+user.getEmail());
                    signUpPaymentUser(new PaymentUserSignUpRequest(user, paymentDetail), new ResponseErrorHandler() {
                        @Override
                        public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
                            return clientHttpResponse.getRawStatusCode() < 200 || clientHttpResponse.getRawStatusCode() > 299;
                        }

                        @Override
                        public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
                            throw new RuntimeException("Payment Api: " + finalUser.getFullName() + ": "+ finalUser.getId() + ": SignUp request not working.");
                        }
                    });
                    paymentUser = getPaymentUser(user);
                    if(paymentUser == null)
                        throw new RuntimeException("Payment Api: " + finalUser.getFullName() + ": "+ finalUser.getId() + ": SignUp request return null.");
                }

                final PaymentCreateRequest paymentCreateRequest = new PaymentCreateRequest();
                paymentCreateRequest.setUser(paymentUser.getId());
                paymentCreateRequest.setAccumulated_revenue(0.0);
                paymentCreateRequest.setBranch(1);
                paymentCreateRequest.setDescription("ssp_transfer");
                paymentCreateRequest.setStatus("WaitPaid");
                paymentCreateRequest.setPayment_method(paymentDetail.getApiPaymentMethod());
                paymentCreateRequest.setPayment_period(0);
                paymentCreateRequest.setPlatform(1);

                paymentCreateRequest.setPayment_date(dateFormat.format(new Date()));
                paymentCreateRequest.setProgress_payment_start(dateFormat.format(paymentStartDate));
                paymentCreateRequest.setProgress_payment_end(dateFormat.format(paymentEndDate));
                if (CurrencyCodeType.TRY.equals(CurrencyCodeType.valueOf(user.getCurrencyCode()))) {
                    paymentCreateRequest.setCurrency(1);
                    paymentCreateRequest.setAmount_of_payment(rev);
                } else if (CurrencyCodeType.USD.equals(CurrencyCodeType.valueOf(user.getCurrencyCode()))) {
                    paymentCreateRequest.setCurrency(2);
                    paymentCreateRequest.setAmount_of_payment(usdRev);
                }

                sendPaymentDataToPaymentApi(paymentCreateRequest, new ResponseErrorHandler() {
                    @Override
                    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
                        return clientHttpResponse.getRawStatusCode() < 200 || clientHttpResponse.getRawStatusCode() > 299;
                    }

                    @Override
                    public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
                        String responseString = new String(ByteStreams.toByteArray(clientHttpResponse.getBody()),
                                Charset.forName("UTF-8"));
                        logger.error(responseString);
                        throw new RuntimeException("Payment Api: " + finalUser.getFullName() + ": "+ finalUser.getId() + ": Send Payment request not working.");
                    }
                });
            }catch (Exception e){
                logger.error(e.getMessage());
            }
        }
    }
    public void calculateOnePaymentToPaymentApi(Integer userId) {

        Date now = new Date();
        Date paymentStartDate = DateUtils.addMonths(now, -1);

        paymentStartDate = DateUtils.truncate(paymentStartDate, Calendar.MONTH);
        Date paymentEndDate = DateUtils.truncate(now, Calendar.MONTH);

        String query="{ \"aggs\": { \"userId_term\": { \"terms\": { \"field\": \"userId\", \"size\": 20000 }, \"aggs\": "
                + "{ \"pubRevenue_sum\": { \"sum\": { \"field\": \"pubRevenue\" } }, \"pubRevenueUsd_sum\": { \"sum\": { \"field\": \"pubRevenueUsd\" } } } } }, "
                + "\"query\": { \"bool\": { \"filter\": { \"bool\": { \"must\": [ { \"range\": { \"statTime\": "
                + "{ \"gte\": "+paymentStartDate.getTime()+", \"lt\":"+paymentEndDate.getTime()+" } } }," +
                " { \"bool\": { \"should\": [ { \"term\": { \"userId\": "+userId+" } } ] } } ] } } } } }";

        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        HttpEntity<String> entity = new HttpEntity<>(query, headers);
        template.setErrorHandler(new ResponseErrorHandler() {

            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                return response.getRawStatusCode() < 200 || response.getRawStatusCode() > 299;
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {

            }
        });
        String response = template.postForObject("http://" + Constants.ELASTIC_SEARCH_HOST +
                ":9200/publisher_stats/app_unit_stat/_search?size=0", entity, String.class);

        JSONObject jResponse = new JSONObject(response);
        JSONObject userTerm = jResponse.getJSONObject("aggregations").getJSONObject("userId_term");
        JSONArray buckets = userTerm.getJSONArray("buckets");

        paymentEndDate = DateUtils.addDays(paymentEndDate, -1);
        PubPaymentDetail paymentDetail;
        User user;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            JSONObject jBucket = buckets.getJSONObject(0);
            JSONObject jRev = jBucket.getJSONObject("pubRevenue_sum");
            double rev = jRev.getDouble("value");

            if(rev < 0.01)
                return;

            rev = ((int)(rev*100))/100.0;

            JSONObject jUsdRev = jBucket.getJSONObject("pubRevenueUsd_sum");
            double usdRev = jUsdRev.getDouble("value");
            usdRev = ((int)(usdRev*100))/100.0;

            if(usdRev < 0.01)
                return;

            user = userService.getOne(userId);

            if (user == null)
                return;
            if (user.getBan() || !user.getActive())
                return;
            User finalUser = user;
            paymentDetail = getPubPaymentDetailByUser(userId);
            if(paymentDetail == null)
                paymentDetail = new PubPaymentDetail();

            PaymentApiUser paymentUser = getPaymentUser(user);
            if (paymentUser == null){
                 logger.error("Payment-Api: user not found and start signup."+user.getEmail());
                 signUpPaymentUser(new PaymentUserSignUpRequest(user, paymentDetail), new ResponseErrorHandler() {
                    @Override
                    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
                        return clientHttpResponse.getRawStatusCode() < 200 || clientHttpResponse.getRawStatusCode() > 299;
                    }

                    @Override
                    public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
                        throw new RuntimeException("Payment Api: " + finalUser.getFullName() + ": "+ finalUser.getId() + ": SignUp request not working.");
                    }
                 });
                paymentUser = getPaymentUser(user);
                if(paymentUser == null)
                    throw new RuntimeException("Payment Api: " + finalUser.getFullName() + ": "+ finalUser.getId() + ": SignUp request return null.");
            }

            final PaymentCreateRequest paymentCreateRequest = new PaymentCreateRequest();
            paymentCreateRequest.setUser(paymentUser.getId());
            paymentCreateRequest.setAccumulated_revenue(0.0);
            paymentCreateRequest.setBranch(1);
            paymentCreateRequest.setDescription("ssp_transfer");
            paymentCreateRequest.setStatus("WaitPaid");
            paymentCreateRequest.setPayment_method(paymentDetail.getApiPaymentMethod());
            paymentCreateRequest.setPayment_period(0);
            paymentCreateRequest.setPlatform(1);

            paymentCreateRequest.setPayment_date(dateFormat.format(new Date()));
            paymentCreateRequest.setProgress_payment_start(dateFormat.format(paymentStartDate));
            paymentCreateRequest.setProgress_payment_end(dateFormat.format(paymentEndDate));
            if (CurrencyCodeType.TRY.equals(CurrencyCodeType.valueOf(user.getCurrencyCode()))) {
                paymentCreateRequest.setCurrency(1);
                paymentCreateRequest.setAmount_of_payment(rev);
            } else if (CurrencyCodeType.USD.equals(CurrencyCodeType.valueOf(user.getCurrencyCode()))) {
                paymentCreateRequest.setCurrency(2);
                paymentCreateRequest.setAmount_of_payment(usdRev);
            }

            sendPaymentDataToPaymentApi(paymentCreateRequest, new ResponseErrorHandler() {
                @Override
                public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
                    return clientHttpResponse.getRawStatusCode() < 200 || clientHttpResponse.getRawStatusCode() > 299;
                }

                @Override
                public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
                    String responseString = new String(ByteStreams.toByteArray(clientHttpResponse.getBody()),
                            Charset.forName("UTF-8"));
                    logger.error(responseString);
                    throw new RuntimeException("Payment Api: " + finalUser.getFullName() + ": "+ finalUser.getId() + ": Send Payment request not working.");
                }
            });
        }catch (Exception e){
            logger.error(e.getMessage());
        }
    }

    public void comparePayments(String progressPaymentEnd,Integer prevMonth) {
        errorCount = 0;
        Date now = new Date();
        Date paymentStartDate = DateUtils.addMonths(now, prevMonth);
        Date paymentEndDate = DateUtils.truncate(DateUtils.addMonths(
                paymentStartDate, 1), Calendar.MONTH);

        paymentStartDate = DateUtils.truncate(paymentStartDate, Calendar.MONTH);

        String query = "{ \"aggs\": { \"userId_term\": { \"terms\": { \"field\": \"userId\", \"size\": 20000 }, \"aggs\": "
                + "{ \"pubRevenue_sum\": { \"sum\": { \"field\": \"pubRevenue\" } }, \"pubRevenueUsd_sum\": { \"sum\": { \"field\": \"pubRevenueUsd\" } } } } }, "
                + "\"query\": { \"bool\": { \"filter\": { \"bool\": { \"must\": [ { \"range\": { \"statTime\": "
                + "{ \"gte\": " + paymentStartDate.getTime() + ", \"lt\":" + paymentEndDate.getTime() + " } } } ] } } } } }";

        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        HttpEntity<String> entity = new HttpEntity<>(query, headers);
        template.setErrorHandler(new ResponseErrorHandler() {

            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                return response.getRawStatusCode() < 200 || response.getRawStatusCode() > 299;
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {
                //System.out.println(""+ IOUtils.toString(response.getBody()));
            }
        });
        String response = template.postForObject("http://" + Constants.ELASTIC_SEARCH_HOST +
               ":9200/publisher_stats/app_unit_stat/_search?size=0", entity, String.class);
        JSONObject jResponse = new JSONObject(response);
        JSONObject userTerm = jResponse.getJSONObject("aggregations").getJSONObject("userId_term");
        JSONArray buckets = userTerm.getJSONArray("buckets");

        User user;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        ArrayList<HashMap> earnedUsers=new ArrayList<HashMap>();
        for (int i = 0; i < buckets.length(); i++) {
            try {
                JSONObject jBucket = buckets.getJSONObject(i);
                int userId = jBucket.getInt("key");

                JSONObject jRev = jBucket.getJSONObject("pubRevenue_sum");
                double rev = jRev.getDouble("value");

                if (rev < 0.01)
                    continue;
                rev = ((int)(rev*100))/100.0;

                JSONObject jUsdRev = jBucket.getJSONObject("pubRevenueUsd_sum");
                double usdRev = jUsdRev.getDouble("value");
                usdRev = ((int)(usdRev*100))/100.0;
                user = userService.getOne(userId);

                if (user == null)
                    continue;
                if (user.getBan() || !user.getActive())
                    continue;
                if(user.getIsTR()==1)
                    continue;
                HashMap<String,String> userMap=new HashMap<String,String>();
                userMap.put("email",user.getEmail());
                userMap.put("rev",rev+"");
                userMap.put("usdRev",usdRev+"");
                earnedUsers.add(userMap);
            }catch (Exception e){
                logger.error(e.getMessage());
            }
        }

        try {
            HashMap responseMap=(HashMap)getPaymentReportsAll(progressPaymentEnd);
            ArrayList<HashMap> apiPayments=(ArrayList<HashMap>)responseMap.get("results");
            HashMap<String,HashMap> apiPaymentsMap=new HashMap<String,HashMap>();
            for(HashMap apiPayment : apiPayments){
                HashMap userMap=(HashMap) apiPayment.get("user");
                apiPaymentsMap.put(userMap.get("email").toString(),userMap);
            }
            for(HashMap<String,String> userMap2: earnedUsers){
                if(apiPaymentsMap.get(userMap2.get("email"))==null){
                    logger.error(userMap2.get("email").toString()+" = "+ userMap2.get("usdRev").toString());
                }
            }

        } catch (BaseException e) {
            e.printStackTrace();
        }
    }

    static int errorCount=0;
    public PaymentApiUser getPaymentUser(final User user){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json;charset=UTF-8");
        headers.set("Authorization", AUTHORIZATION_KEY);

        HttpEntity<HashMap<String, Object>> request = new HttpEntity<>(headers);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(paymentApiUrl+"/api/users");
        builder.queryParam("user__email", user.getEmail());

        restTemplate.setErrorHandler(new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
                return clientHttpResponse.getRawStatusCode() < 200 || clientHttpResponse.getRawStatusCode() > 299;
            }

            @Override
            public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
                throw new RuntimeException("Payment Api: " + user.getFullName() + ": "+ user.getId() + ": exist request not working.");
            }
        });

        PaymentApiUserResponse response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, request, PaymentApiUserResponse.class).getBody();
        if(response == null || response.getCount() == 0)
            return null;
        return response.getResults().get(0);
    }

    public void syncUserForPayment(User user) {
        try {
            PaymentApiUser paymentUser = getPaymentUser(user);
            final User finalUser=user;
            PubPaymentDetail paymentDetail = getPubPaymentDetailByUser(user.getId());
            if(paymentDetail == null)
                paymentDetail = new PubPaymentDetail();
            if (paymentUser == null){

                signUpPaymentUser(new PaymentUserSignUpRequest(user, paymentDetail), new ResponseErrorHandler() {
                    @Override
                    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
                        return clientHttpResponse.getRawStatusCode() < 200 || clientHttpResponse.getRawStatusCode() > 299;
                    }

                    @Override
                    public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
                       // throw new RuntimeException("Payment Api: " + finalUser.getFullName() + ": "+ finalUser.getId() + ": SignUp request not working.");
                    }
                });
            }else{
                updatePaymentUser(
                        new PaymentUserUpdateRequest(user, paymentDetail),paymentUser.getId(),new ResponseErrorHandler() {
                            @Override
                            public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
                                return clientHttpResponse.getRawStatusCode() < 200 || clientHttpResponse.getRawStatusCode() > 299;
                            }

                            @Override
                            public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
                              //  throw new RuntimeException("Payment Api: " + finalUser.getFullName() + ": "+ finalUser.getId() + ": SignUp request not working.");
                            }
                        });
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }
    private PaymentApiUser updatePaymentUser(PaymentUserUpdateRequest updateRequest,
                                             Integer paymentUserId,
                                             ResponseErrorHandler errorHandler){
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setConnectTimeout(20000);
        requestFactory.setReadTimeout(20000);


        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(requestFactory);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json;charset=UTF-8");
        headers.set("Authorization", AUTHORIZATION_KEY);
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");

        HttpEntity<PaymentUserUpdateRequest> request = new HttpEntity<>(updateRequest, headers);

        if(errorHandler != null)
            restTemplate.setErrorHandler(errorHandler);

        return restTemplate.patchForObject(paymentApiUrl+"/api/users/"+paymentUserId, request, PaymentApiUser.class);
    }

    private HashMap signUpPaymentUser(PaymentUserSignUpRequest signUpRequest, ResponseErrorHandler errorHandler){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json;charset=UTF-8");
        headers.set("Authorization",AUTHORIZATION_KEY);

        HttpEntity<PaymentUserSignUpRequest> request = new HttpEntity<>(signUpRequest, headers);
        if(errorHandler != null)
            restTemplate.setErrorHandler(errorHandler);

        return restTemplate.postForObject(paymentApiUrl+"/api/signup", request, HashMap.class);
    }

    private void sendPaymentDataToPaymentApi(PaymentCreateRequest createRequest, ResponseErrorHandler errorHandler){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json;charset=UTF-8");
        headers.set("Authorization", AUTHORIZATION_KEY);

        HttpEntity<PaymentCreateRequest> request = new HttpEntity<>(createRequest, headers);
        if(errorHandler != null)
            restTemplate.setErrorHandler(errorHandler);

        restTemplate.postForObject(paymentApiUrl+"/api/payment-transaction?platform=1", request, HashMap.class);
    }

    public void savePubPayment(List<PubPayment> payments){
        for(PubPayment payment: payments)
            savePubPayment(payment);
    }

    public PubPayment savePubPayment(PubPayment payment){
        return pubPaymentRepository.save(payment);
    }

    private PubPayment getOnePubPaymentOrThrow(Integer id) throws BaseException {
        Optional<PubPayment> pubPaymentVO = getOnePubPaymentOptional(id);
        if(!pubPaymentVO.isPresent())
            throw new BaseException(400, "Not found.");

        return pubPaymentVO.get();
    }

    private PubPayment getOnePubPayment(Integer id){
        return getOnePubPaymentOptional(id).orElse(null);
    }

    private Optional<PubPayment> getOnePubPaymentOptional(Integer id){
        return pubPaymentRepository.findOneById(id);
    }

    private void isPresentPubPaymentDetailByUserIdOrThrow(Integer userId) throws BaseException {
        if(!isPresentPubPaymentDetailByUserId(userId))
            throw new BaseException(400, "Pub payment detail not found!");
    }

    private Boolean isPresentPubPaymentDetailByUserId(Integer userId){
        return getOnePubPaymentDetailOptionalByUserId(userId).isPresent();
    }

    public PubPaymentDetail savePubPaymentDetail(PubPaymentDetail paymentDetail){
        return pubPaymentDetailRepository.save(paymentDetail);
    }

    public List<PubPaymentDetail> savePubPaymentDetail(List<PubPaymentDetail> paymentDetails){
        return pubPaymentDetailRepository.saveAll(paymentDetails);
    }

    public PubPaymentDetailResponseDTO getPubPaymentDetailByUserOrThrow(Integer userId) throws BaseException {
        Optional<PubPaymentDetail> paymentDetailVO = getOnePubPaymentDetailOptionalByUserId(userId);
        if(!paymentDetailVO.isPresent())
            throw new BaseException(400, "Pub payment detail not found!");

        return new PubPaymentDetailResponseDTO(paymentDetailVO.get());
    }

    public PubPaymentDetail getPubPaymentDetailByUser(Integer userId) {
        return getOnePubPaymentDetailOptionalByUserId(userId).orElse(null);
    }

    private Optional<PubPaymentDetail> getOnePubPaymentDetailOptionalByUserId(Integer userId){
        return pubPaymentDetailRepository.findOneByUserId(userId);
    }
    public void syncUsersToPaymentApi(ArrayList<Integer> userIds){
        User user;
        for(Integer userId : userIds){
            user=userService.getOne(userId);
            if(user!=null){
                syncUserForPayment(user);
            }
        }
    }
    public Object getPaymentReportsByUser(Integer userId,Integer limit, Boolean asc) throws BaseException{

        User user=userService.getOne(userId);
        if(user==null){
            throw new BaseException(400, "User not found!");
        }
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json;charset=UTF-8");
        headers.set("Authorization",AUTHORIZATION_KEY);
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");

        HttpEntity<HashMap<String, Object>> request = new HttpEntity<>(headers);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(paymentApiUrl+"/api/closing-of-account");
        builder.queryParam("search", user.getEmail());
        if(limit != null)
            builder.queryParam("limit", String.valueOf(limit));

        if(asc == null || asc)
            builder.queryParam("ordering", "progress_payment_start");
        else
            builder.queryParam("ordering", "-progress_payment_start");

        builder.queryParam("platform", "1");

        return restTemplate.exchange(builder.toUriString(), HttpMethod.GET, request, Object.class).getBody();
    }

    public Object getPaymentReportsAll(String progressPaymentEnd) throws BaseException{

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json;charset=UTF-8");
        headers.set("Authorization",AUTHORIZATION_KEY);
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");

        HttpEntity<HashMap<String, Object>> request = new HttpEntity<>(headers);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(paymentApiUrl+"/api/closing-of-account");
        builder.queryParam("ordering", "-progress_payment_start");
        builder.queryParam("platform", "1");
        builder.queryParam("limit", 3000);
        builder.queryParam("progress_payment_end", progressPaymentEnd);

        return restTemplate.exchange(builder.toUriString(), HttpMethod.GET, request, Object.class).getBody();
    }
    public Object getUserMonthTransaction(String progressPaymentEnd) throws BaseException{

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json;charset=UTF-8");
        headers.set("Authorization",AUTHORIZATION_KEY);
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");

        HttpEntity<HashMap<String, Object>> request = new HttpEntity<>(headers);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(paymentApiUrl+"/api/payment-transaction");
        builder.queryParam("ordering", "-progress_payment_start");
        builder.queryParam("platform", "1");
        builder.queryParam("limit", 2000);
        builder.queryParam("progress_payment_end", progressPaymentEnd);

        return restTemplate.exchange(builder.toUriString(), HttpMethod.GET, request, Object.class).getBody();
    }
    public void syncTransactions(String progressPaymentEnd,Integer prevMonth) {
        errorCount = 0;
        Date now = new Date();
        Date paymentStartDate = DateUtils.addMonths(now, prevMonth);
        Date paymentEndDate = DateUtils.truncate(DateUtils.addMonths(
                paymentStartDate, 1), Calendar.MONTH);

        paymentStartDate = DateUtils.truncate(paymentStartDate, Calendar.MONTH);

        String query = "{ \"aggs\": { \"userId_term\": { \"terms\": { \"field\": \"userId\", \"size\": 20000 }, \"aggs\": "
                + "{ \"pubRevenue_sum\": { \"sum\": { \"field\": \"pubRevenue\" } }, \"pubRevenueUsd_sum\": { \"sum\": { \"field\": \"pubRevenueUsd\" } } } } }, "
                + "\"query\": { \"bool\": { \"filter\": { \"bool\": { \"must\": [ { \"range\": { \"statTime\": "
                + "{ \"gte\": " + paymentStartDate.getTime() + ", \"lt\":" + paymentEndDate.getTime() + " } } } ] } } } } }";

        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        HttpEntity<String> entity = new HttpEntity<>(query, headers);
        template.setErrorHandler(new ResponseErrorHandler() {

            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                return response.getRawStatusCode() < 200 || response.getRawStatusCode() > 299;
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {
                //System.out.println(""+ IOUtils.toString(response.getBody()));
            }
        });
        String response = template.postForObject("http://" + Constants.ELASTIC_SEARCH_HOST +
                ":9200/publisher_stats/app_unit_stat/_search?size=0", entity, String.class);
        JSONObject jResponse = new JSONObject(response);
        JSONObject userTerm = jResponse.getJSONObject("aggregations").getJSONObject("userId_term");
        JSONArray buckets = userTerm.getJSONArray("buckets");

        User user;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        ArrayList<HashMap> earnedUsers=new ArrayList<HashMap>();
        for (int i = 0; i < buckets.length(); i++) {
            try {
                JSONObject jBucket = buckets.getJSONObject(i);
                int userId = jBucket.getInt("key");

                JSONObject jRev = jBucket.getJSONObject("pubRevenue_sum");
                double rev = jRev.getDouble("value");

                if (rev < 0.01)
                    continue;
                rev = ((int)(rev*100))/100.0;

                JSONObject jUsdRev = jBucket.getJSONObject("pubRevenueUsd_sum");
                double usdRev = jUsdRev.getDouble("value");
                usdRev = ((int)(usdRev*100))/100.0;
                user = userService.getOne(userId);

                if (user == null)
                    continue;
                if (user.getBan() || !user.getActive())
                    continue;
                if (usdRev < 0.01)
                    continue;
                HashMap<String,String> userMap=new HashMap<String,String>();
                userMap.put("email",user.getEmail());
                userMap.put("rev",rev+"");
                userMap.put("userId",user.getId()+"");
                userMap.put("usdRev",usdRev+"");
                earnedUsers.add(userMap);
            }catch (Exception e){
                logger.error(e.getMessage());
            }
        }

        try {
            HashMap responseMap=(HashMap)getUserMonthTransaction(progressPaymentEnd);
            ArrayList<HashMap> apiTransactions=(ArrayList<HashMap>)responseMap.get("results");
            HashMap<String,HashMap> apiTransactionsMap=new HashMap<String,HashMap>();
            for(HashMap apiPayment : apiTransactions){
                HashMap userMap=(HashMap) apiPayment.get("user");
                apiTransactionsMap.put(userMap.get("email").toString(),userMap);
            }
            for(HashMap<String,String> userMap2: earnedUsers){
                if(apiTransactionsMap.get(userMap2.get("email"))==null){
                    logger.error(" NO TRANSACTION "+userMap2.get("userId").toString()+" "+
                            userMap2.get("usdRev").toString()+" "+userMap2.get("email").toString());
                    calculateOnePaymentToPaymentApi(Integer.parseInt(userMap2.get("userId")));
                }
            }

        } catch (BaseException e) {
            e.printStackTrace();
        }
    }
    public void uploadInvoice(MultipartFile file, Integer paymentId,Integer userId,
                              String invoiceStartDate, String invoiceEndDate) {

        String FTP_ADDRESS = "reklamstore.storage.cubecdn.net";
        String LOGIN = "reklamstore";
        String PSW = "950tGZIcjVGXOt2P";
        FTPClient con = null;
        String invoiceUrl="/file/invoices/"+invoiceEndDate+"-"+paymentId+".pdf";
        try {
            con = new FTPClient();
            con.connect(FTP_ADDRESS);

            if (con.login(LOGIN, PSW)) {
                con.enterLocalPassiveMode();
                con.setFileType(FTP.BINARY_FILE_TYPE);

                boolean result = con.storeFile(invoiceUrl,file.getInputStream());
                con.logout();
                con.disconnect();
                if(result){
                    User user = userService.getOne(userId);
                    if(user==null){
                        throw new Exception("Invoice upload user not found");
                    }
                    PaymentApiUser paymentUser = getPaymentUser(user);
                    if(paymentUser!=null){
                        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
                        requestFactory.setConnectTimeout(20000);
                        requestFactory.setReadTimeout(20000);


                        RestTemplate restTemplate = new RestTemplate();
                        restTemplate.setRequestFactory(requestFactory);
                        HttpHeaders headers = new HttpHeaders();
                        headers.set("Content-Type", "application/json;charset=UTF-8");
                        headers.set("Authorization", AUTHORIZATION_KEY);
                        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
                        PaymentInvoiceUploadRequest invoiceUploadRequest=new PaymentInvoiceUploadRequest();
                        invoiceUploadRequest.setUser(paymentUser.getId());
                        invoiceUploadRequest.setProgress_payment_start(invoiceStartDate);
                        invoiceUploadRequest.setInvoice_file("https://file.reklamstore.com"+invoiceUrl);
                        HttpEntity<PaymentInvoiceUploadRequest> request = new HttpEntity<>(invoiceUploadRequest, headers);
                        restTemplate.setErrorHandler(new ResponseErrorHandler() {
                            @Override
                            public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
                                return clientHttpResponse.getRawStatusCode() < 200 || clientHttpResponse.getRawStatusCode() > 299;
                            }

                            @Override
                            public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
                                //throw new RuntimeException("Error");
                            }
                        });
                        try{
                            restTemplate.postForObject(paymentApiUrl+"/api/invoice-upload", request, String.class);
                            headers = new HttpHeaders();
                            headers.set("Content-Type", "application/json;charset=UTF-8");
                            headers.set("Authorization", AUTHORIZATION_KEY);
                            headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
                            PaymentInvoiceStatusRequest invoiceStatusRequest=new PaymentInvoiceStatusRequest();
                            invoiceStatusRequest.setInvoice_status(true);
                            HttpEntity<PaymentInvoiceStatusRequest> request2 = new HttpEntity<>(invoiceStatusRequest, headers);
                            restTemplate.patchForObject(paymentApiUrl+"/api/closing-of-account/"+paymentId+"?platform=1",request2, String.class);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
