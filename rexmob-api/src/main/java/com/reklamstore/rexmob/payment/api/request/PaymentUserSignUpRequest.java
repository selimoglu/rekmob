package com.reklamstore.rexmob.payment.api.request;

import com.reklamstore.rexmob.payment.api.PaymentApiUserType;
import com.reklamstore.rexmob.payment.detail.PubPaymentDetail;
import com.reklamstore.rexmob.user.User;
import org.springframework.util.StringUtils;

import java.util.HashMap;

public class PaymentUserSignUpRequest {

    private String first_name;
    private String last_name;
    private String email;
    private String username;
    private String password;
    private String phone;
    private String address;
    private String country;
    private String city;
    private String company_name;
    private String identification_number;
    private Integer user_language;
    private Integer user_type;
    private String iban_number;
    private String tax_number;
    private String invoice_name;
    private String bank_name;
    private String extra;
    private String gender;
    private Integer branch;
    private Integer payment_method;
    private Integer platform;
    private String account_manager;

    public PaymentUserSignUpRequest() {
    }

    public PaymentUserSignUpRequest(User user, PubPaymentDetail paymentDetail) {

        if (StringUtils.isEmpty(user.getFullName())) {
            user.setFullName("- -");
        }
        String[] fullName = user.getFullName().split(" ");
        StringBuilder fistName = new StringBuilder();
        for (int i = 0; i < fullName.length - 1; i++)
            fistName.append(fullName[i]).append(" ");

        first_name = fistName.toString().trim();
        if (fullName.length > 1)
            this.last_name = fullName[fullName.length - 1];
        else {
            first_name = user.getFullName();
            last_name = "-";
        }
        email = user.getEmail();
        username = user.getEmail();
        password = user.getPassword();

        if (StringUtils.isEmpty(user.getPhone()))
            phone = "-";
        else
            phone = user.getPhone();

        if (StringUtils.isEmpty(paymentDetail.getInvoiceAddress())) {
            address = !StringUtils.isEmpty(user.getAddress()) ? user.getAddress() : "-";
        } else
            address = paymentDetail.getInvoiceAddress();

        country = "Turkey";
        city = "-";
        company_name = "-";
        identification_number = user.getIsTR() != null && user.getIsTR() == 1 ? user.getTcNo() : "0";

        String lang = StringUtils.isEmpty(user.getLang()) ? "EN" : user.getLang().split("-")[0].toUpperCase();
        if (lang.equals("TR"))
            user_language = 1;
        else if (lang.equals("EN"))
            user_language = 2;

        if (user.getAccType() == 1)
            user_type = 5;
        else user_type = 4;

        iban_number = paymentDetail.getIban();
        if (StringUtils.isEmpty(iban_number))
            iban_number = "-";

        tax_number = paymentDetail.getTaxId();
        if (tax_number != null)
            tax_number = tax_number.trim();
        if (StringUtils.isEmpty(tax_number))
            tax_number = "-";

        invoice_name = paymentDetail.getInvoiceName();
        if (StringUtils.isEmpty(invoice_name))
            invoice_name = "-";

        bank_name = paymentDetail.getAccountOwner();
        if (StringUtils.isEmpty(bank_name))
            bank_name = "-";

        extra = "-";
        gender = "False";
        branch = 1;
        payment_method = 1;
        String paymentMethod = paymentDetail.getPaymentMethod();
        if (!StringUtils.isEmpty(paymentMethod)) {
            payment_method = paymentDetail.getApiPaymentMethod();
            if (paymentMethod.equals("PAYPAL")) {
                iban_number = paymentDetail.getPaypal();
            }
        }
        platform = 1;
        account_manager=user.getAccManager();
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getIdentification_number() {
        return identification_number;
    }

    public void setIdentification_number(String identification_number) {
        this.identification_number = identification_number;
    }

    public Integer getUser_language() {
        return user_language;
    }

    public void setUser_language(Integer user_language) {
        this.user_language = user_language;
    }


    public String getIban_number() {
        return iban_number;
    }

    public void setIban_number(String iban_number) {
        this.iban_number = iban_number;
    }

    public String getTax_number() {
        return tax_number;
    }

    public void setTax_number(String tax_number) {
        this.tax_number = tax_number;
    }

    public String getInvoice_name() {
        return invoice_name;
    }

    public void setInvoice_name(String invoice_name) {
        this.invoice_name = invoice_name;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getUser_type() {
        return user_type;
    }

    public void setUser_type(Integer user_type) {
        this.user_type = user_type;
    }

    public Integer getBranch() {
        return branch;
    }

    public void setBranch(Integer branch) {
        this.branch = branch;
    }

    public Integer getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(Integer payment_method) {
        this.payment_method = payment_method;
    }

    public Integer getPlatform() {
        return platform;
    }

    public void setPlatform(Integer platform) {
        this.platform = platform;
    }

    public String getAccount_manager() {
        return account_manager;
    }

    public void setAccount_manager(String account_manager) {
        this.account_manager = account_manager;
    }
}
