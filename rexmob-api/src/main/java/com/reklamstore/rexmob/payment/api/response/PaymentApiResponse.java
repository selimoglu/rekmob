package com.reklamstore.rexmob.payment.api.response;

import java.util.HashMap;
import java.util.List;

public class PaymentApiResponse {

    private Integer count = 0;
    private String next;
    private String previous;
    private List<HashMap<String, Object>> results;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public List<HashMap<String, Object>> getResults() {
        return results;
    }

    public void setResults(List<HashMap<String, Object>> results) {
        this.results = results;
    }
}
