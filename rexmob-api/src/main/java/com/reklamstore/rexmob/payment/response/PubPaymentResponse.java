package com.reklamstore.rexmob.payment.response;

import com.reklamstore.rexmob.base.BaseException;
import com.reklamstore.rexmob.base.BaseResponse;

import java.util.List;

public class PubPaymentResponse extends BaseResponse {

    private List<PubPaymentResponseDTO> response;

    public PubPaymentResponse() {
    }

    public PubPaymentResponse(List<PubPaymentResponseDTO> response) {
        super(true);
        this.response = response;
    }

    public PubPaymentResponse(BaseException exception) {
        super(exception);
    }

    public List<PubPaymentResponseDTO> getResponse() {
        return response;
    }

    public void setResponse(List<PubPaymentResponseDTO> response) {
        this.response = response;
    }
}
