package com.reklamstore.rexmob.payment.api;

import java.util.Date;
import java.util.HashMap;

public class PaymentApiUser {

    private Integer id;
    private Boolean gender;
    private String address;
    private String phone;
    private String country;
    private String city;
    private String company_name;
    private String identification_number;
    private Integer user_language;
    private HashMap<String, Object> role;
    private HashMap<String, Object> platform;
    private HashMap<String, Object> branch;
    private HashMap<String, Object> payment_method;
    private PaymentApiUserType user_type;
    private String username;
    private String first_name;
    private String last_name;
    private String email;
    private Boolean is_active;
    private Date create_time;
    private Date update_time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getIdentification_number() {
        return identification_number;
    }

    public void setIdentification_number(String identification_number) {
        this.identification_number = identification_number;
    }

    public Integer getUser_language() {
        return user_language;
    }

    public void setUser_language(Integer user_language) {
        this.user_language = user_language;
    }

    public HashMap<String, Object> getRole() {
        return role;
    }

    public void setRole(HashMap<String, Object> role) {
        this.role = role;
    }

    public HashMap<String, Object> getPlatform() {
        return platform;
    }

    public void setPlatform(HashMap<String, Object> platform) {
        this.platform = platform;
    }

    public HashMap<String, Object> getBranch() {
        return branch;
    }

    public void setBranch(HashMap<String, Object> branch) {
        this.branch = branch;
    }

    public HashMap<String, Object> getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(HashMap<String, Object> payment_method) {
        this.payment_method = payment_method;
    }

    public PaymentApiUserType getUser_type() {
        return user_type;
    }

    public void setUser_type(PaymentApiUserType user_type) {
        this.user_type = user_type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIs_active() {
        return is_active;
    }

    public void setIs_active(Boolean is_active) {
        this.is_active = is_active;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }
}
