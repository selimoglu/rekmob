package com.reklamstore.rexmob.payment.api.response;

public class PaymentApiBaseResponse {

    private Integer count = 0;
    private String next;
    private String previous;

    public PaymentApiBaseResponse() {
    }

    public PaymentApiBaseResponse(Integer count, String next, String previous) {
        this.count = count;
        this.next = next;
        this.previous = previous;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }
}
