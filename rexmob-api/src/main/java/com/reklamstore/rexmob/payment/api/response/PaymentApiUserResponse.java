package com.reklamstore.rexmob.payment.api.response;

import com.reklamstore.rexmob.payment.api.PaymentApiUser;

import java.util.List;

public class PaymentApiUserResponse extends PaymentApiBaseResponse{

    private List<PaymentApiUser> results;

    public PaymentApiUserResponse() {
    }

    public PaymentApiUserResponse(Integer count, String next, String previous, List<PaymentApiUser> results) {
        super(count, next, previous);
        this.results = results;
    }

    public List<PaymentApiUser> getResults() {
        return results;
    }

    public void setResults(List<PaymentApiUser> results) {
        this.results = results;
    }
}
