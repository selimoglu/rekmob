package com.reklamstore.rexmob.payment.response;

import com.reklamstore.rexmob.payment.PubPayment;
import com.reklamstore.rexmob.payment.PubPaymentStatusType;

import java.util.Date;

public class PubPaymentUserResponseDTO {
    private Integer id;
    private Integer userId;

    private String userName;

    private Double preIncome;
    private Double postIncome;

    private Double amount;
    private Double amountUsd;
    private String iban;

    private Date paymentDate;
    private Date createDate;

    private int archived;
    private int active;
    private PubPaymentStatusType status;

    private double taxRate;
    private double vatRate;

    public PubPaymentUserResponseDTO() {
    }

    public PubPaymentUserResponseDTO(PubPayment pubPayment, String userName) {
        this(pubPayment);
        this.userName = userName;
    }

    public PubPaymentUserResponseDTO(PubPayment pubPayment) {
        this.id = pubPayment.getId();
        this.userId = pubPayment.getUserId();
        this.preIncome = pubPayment.getPreIncome();
        this.postIncome = pubPayment.getPostIncome();
        this.amount = pubPayment.getAmount();
        this.amountUsd = pubPayment.getAmountUsd();
        this.iban = pubPayment.getIban();
        this.paymentDate = pubPayment.getPaymentDate();
        this.createDate = pubPayment.getCreateDate();
        this.archived = pubPayment.getArchived();
        this.active = pubPayment.getActive();
        this.status = PubPaymentStatusType.valueOf(pubPayment.getStatus());
        this.taxRate = pubPayment.getTaxRate();
        this.vatRate = pubPayment.getVatRate();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Double getPreIncome() {
        return preIncome;
    }

    public void setPreIncome(Double preIncome) {
        this.preIncome = preIncome;
    }

    public Double getPostIncome() {
        return postIncome;
    }

    public void setPostIncome(Double postIncome) {
        this.postIncome = postIncome;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getAmountUsd() {
        return amountUsd;
    }

    public void setAmountUsd(Double amountUsd) {
        this.amountUsd = amountUsd;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getArchived() {
        return archived;
    }

    public void setArchived(int archived) {
        this.archived = archived;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public PubPaymentStatusType getStatus() {
        return status;
    }

    public void setStatus(PubPaymentStatusType status) {
        this.status = status;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public double getVatRate() {
        return vatRate;
    }

    public void setVatRate(double vatRate) {
        this.vatRate = vatRate;
    }
}
