package com.reklamstore.rexmob.payment.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.reklamstore.rexmob.payment.PubPaymentStatusType;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class PubPaymentCreateRequest {

    @NotNull
    private Integer userId;

    @NotNull
    private Double amount;

    @NotNull
    private Double amountUsd;

    @NotNull
    private Date paymentDate;

    @NotNull
    private PubPaymentStatusType status;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Double getAmount() {
        return amount;
    }

    public Double getAmountUsd() {
        return amountUsd;
    }

    public void setAmountUsd(Double amountUsd) {
        this.amountUsd = amountUsd;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date getPaymentDate() {
        return paymentDate;
    }

    @JsonFormat(pattern = "yyyy-MM-dd")
    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public PubPaymentStatusType getStatus() {
        return status;
    }

    public void setStatus(PubPaymentStatusType status) {
        this.status = status;
    }
}
