package com.reklamstore.rexmob.payment.response;

import com.reklamstore.rexmob.base.BaseException;
import com.reklamstore.rexmob.base.BaseResponse;

public class PubPaymentDetailResponse extends BaseResponse {

    private PubPaymentDetailResponseDTO response;

    public PubPaymentDetailResponse() {
    }

    public PubPaymentDetailResponse(BaseException exception) {
        super(exception);
    }

    public PubPaymentDetailResponse(PubPaymentDetailResponseDTO response) {
        super(false);
        this.response = response;
    }

    public PubPaymentDetailResponseDTO getResponse() {
        return response;
    }

    public void setResponse(PubPaymentDetailResponseDTO response) {
        this.response = response;
    }
}
