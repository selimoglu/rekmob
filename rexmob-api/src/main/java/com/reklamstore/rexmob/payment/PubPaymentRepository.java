package com.reklamstore.rexmob.payment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface PubPaymentRepository extends JpaRepository<PubPayment, Integer> {

    @Query("SELECT r FROM PubPayment r WHERE r.userId = :userId AND r.paymentDate >= :startDate AND r.paymentDate <= :endDate")
    PubPayment findOneByUserIdAndPaymentDate(@Param("userId") Integer userId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    Optional<PubPayment> findOneById(Integer id);

    List<PubPayment> findByUserIdOrderByPaymentDateDesc(Integer userId);
    List<PubPayment> findByPaymentDate(Date paymentDate);
    List<PubPayment> findByPaymentDateAndStatus(Date paymentDate, int status);

    @Query("SELECT r FROM PubPayment r WHERE r.paymentDate >= :startDate AND r.paymentDate <= :endDate")
    List<PubPayment> findByPaymentDateByMonth(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

    @Query("SELECT r FROM PubPayment r WHERE r.paymentDate >= :startDate AND r.paymentDate <= :endDate AND status = :status")
    List<PubPayment> findByPaymentDateByMonthAndStatus(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("status") int status);

    void deleteOneById(Integer id);
}
