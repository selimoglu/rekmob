package com.reklamstore.rexmob.payment.response;

import com.reklamstore.rexmob.base.BaseException;
import com.reklamstore.rexmob.base.BaseResponse;

public class PubPaymentCreateResponse extends BaseResponse {

    private PubPaymentUserResponseDTO response;

    public PubPaymentCreateResponse() {
    }

    public PubPaymentCreateResponse(BaseException exception) {
        super(exception);
    }

    public PubPaymentCreateResponse(PubPaymentUserResponseDTO response) {
        super(false);
        this.response = response;
    }

    public PubPaymentUserResponseDTO getResponse() {
        return response;
    }

    public void setResponse(PubPaymentUserResponseDTO response) {
        this.response = response;
    }
}
