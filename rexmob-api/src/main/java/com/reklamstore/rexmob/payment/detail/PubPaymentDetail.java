package com.reklamstore.rexmob.payment.detail;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "pub_payment_detail")
public class PubPaymentDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PAYMENT_DETAIL_ID")
    private Integer id;

    @Column(name = "USER_ID")
    private Integer userId;

    @Column(name = "IBAN")
    private String iban;

    @Column(name = "BANK_NAME")
    private String bankName;

    @Column(name = "BENCH_CODE")
    private String benchCode;

    @Column(name = "ACCOUNT_OWNER")
    private String accountOwner;

    @Column(name = "ACCOUNT_NUMBER")
    private String accountNumber;

    @Column(name = "SWIFT_CODE")
    private String swiftCode;

    @Column(name = "PAYPAL")
    private String paypal;

    @Column(name = "INVOICED")
    private Integer invoiced;

    @Column(name = "TAX_ID")
    private String taxId;

    @Column(name = "TAX_RATE")
    private double taxRate;

    @Column(name = "VAT_RATE")
    private double vatRate;

    @Column(name = "TCKN")
    private String tckn;

    @Column(name = "INVOICE_NAME")
    private String invoiceName;

    @Column(name = "INVOICE_ADDRESS")
    private String invoiceAddress;

    @Column(name = "PAYMENT_METHOD")
    private String paymentMethod;

    @Column(name = "TAX_ADMIN")
    private String taxAdmin;

    @Column(name = "COMPANY_NAME")
    private String companyName;

    @Column(name = "COMPANY_TYPE")
    private Integer companyType;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBenchCode() {
        return benchCode;
    }

    public void setBenchCode(String benchCode) {
        this.benchCode = benchCode;
    }

    public String getAccountOwner() {
        return accountOwner;
    }

    public void setAccountOwner(String accountOwner) {
        this.accountOwner = accountOwner;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    public String getPaypal() {
        return paypal;
    }

    public void setPaypal(String paypal) {
        this.paypal = paypal;
    }

    public Integer getInvoiced() {
        return invoiced;
    }

    public void setInvoiced(Integer invoiced) {
        this.invoiced = invoiced;
    }

    public boolean isInvoiced() {
        return invoiced != null && invoiced == 1;
    }


    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public double getVatRate() {
        return vatRate;
    }

    public void setVatRate(double vatRate) {
        this.vatRate = vatRate;
    }

    public String getTckn() {
        return tckn;
    }

    public void setTckn(String tckn) {
        this.tckn = tckn;
    }

    public String getInvoiceName() {
        return invoiceName;
    }

    public void setInvoiceName(String invoiceName) {
        this.invoiceName = invoiceName;
    }

    public String getInvoiceAddress() {
        return invoiceAddress;
    }

    public void setInvoiceAddress(String invoiceAddress) {
        this.invoiceAddress = invoiceAddress;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public int getApiPaymentMethod() {
        if("PAYPAL".equals(paymentMethod)){
            return 2;
        }else if("BANK".equals(paymentMethod)){
            return 1;
        }else if("PAYONEER".equals(paymentMethod)){
            return 5;
        }
        return 1;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getTaxAdmin() {
        return taxAdmin;
    }

    public void setTaxAdmin(String taxAdmin) {
        this.taxAdmin = taxAdmin;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Integer getCompanyType() {
        return companyType;
    }

    public void setCompanyType(Integer companyType) {
        this.companyType = companyType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}

