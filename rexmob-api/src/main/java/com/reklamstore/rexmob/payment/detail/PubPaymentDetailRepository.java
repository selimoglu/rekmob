package com.reklamstore.rexmob.payment.detail;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PubPaymentDetailRepository extends JpaRepository<PubPaymentDetail, Integer> {

    Optional<PubPaymentDetail> findOneById(Integer id);
    Optional<PubPaymentDetail> findOneByUserId(Integer userId);
}
