package com.reklamstore.rexmob.payment;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "pub_payment")
public class PubPayment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PUB_PAYMENT_ID")
    private Integer id;

    @Column(name = "USER_ID")
    private Integer userId;

    /**
     * user income before payment
     * 
     */
    @Column(name = "PRE_INCOME")
    private Double preIncome;

    /**
     * user income after payment
     * 
     */
    @Column(name = "POST_INCOME")
    private Double postIncome;

    /**
     * payment amount
     * 
     */
    @NotNull
    @Column(name = "AMOUNT")
    private Double amount;

    /**
     * payment usdAmount
     *
     */
    @NotNull
    @Column(name = "AMOUNT_USD")
    private Double amountUsd;

    @Column(name = "IBAN")
    private String iban;

    @NotNull
    @Column(name = "PAYMENT_DATE")
    private Date paymentDate;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "ARCHIVED")
    private int archived;

    @Column(name = "ACTIVE")
    private int active;

    @Column(name = "STATUS")
    private int status;

    @Column(name = "TAX_RATE")
    private double taxRate;

    @Column(name = "VAT_RATE")
    private double vatRate;

    /**
     * @return {@link #name}
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return {@link #name}
     */
    public Integer getUserId() {
	return userId;
    }

    /**
     * @param userId
     *            the userId to set
     */
    public void setUserId(Integer userId) {
	this.userId = userId;
    }

    /**
     * @return {@link #name}
     */
    public Double getPreIncome() {
	return preIncome;
    }

    /**
     * @param preIncome
     *            the preIncome to set
     */
    public void setPreIncome(Double preIncome) {
	this.preIncome = preIncome;
    }

    /**
     * @return {@link #name}
     */
    public Double getPostIncome() {
	return postIncome;
    }

    /**
     * @param postIncome
     *            the postIncome to set
     */
    public void setPostIncome(Double postIncome) {
	this.postIncome = postIncome;
    }

    /**
     * @return {@link #name}
     */
    public Double getAmount() {
	return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(Double amount) {
	this.amount = amount;
    }

    public Double getAmountUsd() {
        return amountUsd;
    }

    public void setAmountUsd(Double amountUsd) {
        this.amountUsd = amountUsd;
    }

    /**
     * @return {@link #name}
     */
    public String getIban() {
	return iban;
    }

    /**
     * @param iban
     *            the iban to set
     */
    public void setIban(String iban) {
	this.iban = iban;
    }

    /**
     * @return {@link #name}
     */
    public Date getPaymentDate() {
	return paymentDate;
    }

    /**
     * @param paymentDate
     *            the paymentDate to set
     */
    public void setPaymentDate(Date paymentDate) {
	this.paymentDate = paymentDate;
    }

    /**
     * @return {@link #name}
     */
    public Date getCreateDate() {
	return createDate;
    }

    /**
     * @param createDate
     *            the createDate to set
     */
    public void setCreateDate(Date createDate) {
	this.createDate = createDate;
    }

    /**
     * @return {@link #name}
     */
    public int getArchived() {
	return archived;
    }

    /**
     * @param archived
     *            the archived to set
     */
    public void setArchived(int archived) {
	this.archived = archived;
    }

    public void setArchived(boolean isArchived) {
	this.archived = isArchived ? 1 : 0;
    }

    public boolean isArchived() {
	return archived == 1;
    }

    /**
     * @return {@link #name}
     */
    public int getActive() {
	return active;
    }

    /**
     * @param active
     *            the active to set
     */
    public void setActive(int active) {
	this.active = active;
    }

    public void setActive(boolean isActive) {
	this.active = isActive ? 1 : 0;
    }

    public boolean isActive() {
	return active == 1;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public double getVatRate() {
        return vatRate;
    }

    public void setVatRate(double vatRate) {
        this.vatRate = vatRate;
    }
}
