package com.reklamstore.rexmob.payment.response;

import com.reklamstore.rexmob.base.BaseException;
import com.reklamstore.rexmob.base.BaseResponse;

import java.util.List;

public class PubPaymentUserResponse extends BaseResponse {

    private List<PubPaymentUserResponseDTO> response;

    public PubPaymentUserResponse() {
    }

    public PubPaymentUserResponse(BaseException exception) {
        super(exception);
    }

    public PubPaymentUserResponse(List<PubPaymentUserResponseDTO> response) {
        super(true);
        this.response = response;
    }

    public List<PubPaymentUserResponseDTO> getResponse() {
        return response;
    }

    public void setResponse(List<PubPaymentUserResponseDTO> response) {
        this.response = response;
    }
}
