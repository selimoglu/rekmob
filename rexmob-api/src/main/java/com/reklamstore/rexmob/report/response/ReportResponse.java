package com.reklamstore.rexmob.report.response;

import com.reklamstore.rexmob.base.BaseResponse;

import java.util.List;

public class ReportResponse extends BaseResponse {

    private List<ReportResponseDTO> response;

    public ReportResponse() {
    }

    public ReportResponse(List<ReportResponseDTO> response) {
        super(true);
        this.response = response;
    }

    public List<ReportResponseDTO> getResponse() {
        return response;
    }

    public void setResponse(List<ReportResponseDTO> response) {
        this.response = response;
    }
}
