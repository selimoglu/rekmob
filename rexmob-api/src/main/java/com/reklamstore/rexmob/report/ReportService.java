package com.reklamstore.rexmob.report;


import com.reklamstore.rexmob.app.unit.AppUnit;
import com.reklamstore.rexmob.app.unit.AppUnitRepository;
import com.reklamstore.rexmob.email.EmailService;
import com.reklamstore.rexmob.report.response.PopResponseDTO;
import com.reklamstore.rexmob.report.response.ReportResponseDTO;
import com.reklamstore.rexmob.user.UserRepository;
import io.swagger.adcash.client.model.ReportRow;
import io.swagger.propellerv5.client.model.InlineResponse2001;
import io.swagger.propellerv5.client.model.StatisticData;
import io.swagger.propellerv5.client.model.StatisticDataInner;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;


import java.text.SimpleDateFormat;
import java.util.*;


@Service
public class ReportService {


    @Autowired
    AdcashService adCashService;
    @Autowired
    PropellerService propellerService;
    @Autowired
    AppUnitRepository appUnitRepository;
    @Autowired
    UserRepository userRepository;

    @Autowired
    EmailService emailService;


    public List<ReportResponseDTO> getReport(String startDate, String endDate) {
        HashMap<String, HashMap<String, Double>> appUnitMap = new HashMap<String, HashMap<String, Double>>();
        HashMap<String, Double> appUniStat = new HashMap<>();

        List<Integer> appUnitIds = new ArrayList<>();

        /*List<ReportRow> rows = getAdcashReport(startDate, endDate);
        for (int i = 0; i < rows.size(); i++) {
            io.swagger.adcash.client.model.ReportRow row = rows.get(i);
            String placementId = row.getSub1();
            if (placementId.equals("n/a"))
                continue;
            if (!appUnitMap.containsKey(placementId)) {
                appUnitMap.put(placementId, new HashMap<String, Double>());
            }
            appUniStat = appUnitMap.get(placementId);
            appUniStat.put("adcashImp", Double.parseDouble(row.getImpressions()));
            appUniStat.put("adcashRev", Double.parseDouble(row.getEarnings()));
            appUniStat.put("adcashFallback", Double.parseDouble(row.getFallback()));
            appUniStat.put("adcashRejected", Double.parseDouble(row.getFallback_rejected_imps()));

        }*/
        StatisticData propRows = getPropellerReport(startDate, endDate);

        for (int i = 0; i < propRows.size(); i++) {
            StatisticDataInner propRow = propRows.get(i);
            String placementId = propRow.getRequestVar();
            if (placementId.contains("_")) {
                String[] st = placementId.split("_");
                if (st.length > 0)
                    placementId = st[0];
            }

            if (placementId.contains("/")) {
                String[] st = placementId.split("/");
                if (st.length > 0)
                    placementId = st[0];
            }
            if (placementId.contains(".")) {
                String[] sts = placementId.split("\\.");
                if (sts.length > 0)
                    placementId = sts[0];
            }
            if (!appUnitMap.containsKey(placementId)) {
                appUnitMap.put(placementId, new HashMap<String, Double>());
            }
            appUniStat = appUnitMap.get(placementId);
            appUniStat.put("propRev", propRow.getMoney());
            appUniStat.put("propImp", propRow.getImpressions().doubleValue());
            appUniStat.put("propConversion", propRow.getConversions().doubleValue());
        }

        StatisticData propRows2 = getPropeller2Report(startDate, endDate);

        for (int i = 0; i < propRows2.size(); i++) {
            StatisticDataInner propRow2 = propRows2.get(i);
            String placementId = propRow2.getRequestVar();
            if (placementId.contains("_")) {
                String[] st = placementId.split("_");
                if (st.length > 0)
                    placementId = st[0];
            }
            if (!appUnitMap.containsKey(placementId)) {
                appUnitMap.put(placementId, new HashMap<String, Double>());
            }
            appUniStat = appUnitMap.get(placementId);
            appUniStat.put("prop2Rev", propRow2.getMoney());
            appUniStat.put("prop2Imp", propRow2.getImpressions().doubleValue());
            appUniStat.put("prop2Conversion", propRow2.getConversions().doubleValue());

        }

        HashMap<String, ReportResponseDTO> reportHashMap = new HashMap<>();
        List<ReportResponseDTO> responseList = new ArrayList<>();
        ReportResponseDTO responseDTO;
        for (String appUnitId : appUnitMap.keySet()) {
            try {
                appUnitIds.add(Integer.parseInt(appUnitId));
            }catch (Exception e){
                continue;
            }
            appUniStat = appUnitMap.get(appUnitId);
            responseDTO = new ReportResponseDTO();
            responseDTO.setAppUnitId(appUnitId);

            /*if (appUniStat.containsKey("adcashImp"))
                responseDTO.setAdcashImp(appUniStat.get("adcashImp"));
            if (appUniStat.containsKey("adcashRev"))
                responseDTO.setAdcashRev(appUniStat.get("adcashRev"));
            if (appUniStat.containsKey("adcashFallback"))
                responseDTO.setAdcashFallback(appUniStat.get("adcashFallback"));
            if (appUniStat.containsKey("adcashRejected"))
                responseDTO.setAdcashRejected(appUniStat.get("adcashRejected"));
            */
            if (appUniStat.containsKey("propRev"))
                responseDTO.setPropRev(appUniStat.get("propRev"));
            if (appUniStat.containsKey("propImp"))
                responseDTO.setPropImp(appUniStat.get("propImp"));
            if (appUniStat.containsKey("propConversion"))
                responseDTO.setPropConversion(appUniStat.get("propConversion"));

            if (appUniStat.containsKey("prop2Rev"))
                responseDTO.setProp2Rev(appUniStat.get("prop2Rev"));
            if (appUniStat.containsKey("prop2Imp"))
                responseDTO.setProp2Imp(appUniStat.get("prop2Imp"));
            if (appUniStat.containsKey("prop2Conversion"))
                responseDTO.setProp2Conversion(appUniStat.get("prop2Conversion"));

            reportHashMap.put(appUnitId, responseDTO);
            responseList.add(responseDTO);

        }

        List<PopResponseDTO> list = appUnitRepository.findAppUnitUser(appUnitIds);

        for (PopResponseDTO nameResponse : list) {
            responseDTO = reportHashMap.get(nameResponse.getId().toString());
            if (responseDTO != null) {
                responseDTO.setUserId(nameResponse.getUserId());
                responseDTO.setFullName(nameResponse.getFullName());
            }
        }

        return responseList;
    }
    @Scheduled(cron = "1 5 6 * * ?")
    public void checkAdcashPropellerFraud(){
        Date now = new Date();
        Date reportStartDate = DateUtils.addDays(now, -1);
        reportStartDate = DateUtils.truncate(reportStartDate, Calendar.DATE);
        Date reportEndDate = DateUtils.truncate(now, Calendar.DATE);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String startDate = dateFormat.format(reportStartDate);
        String endDate = dateFormat.format(reportEndDate);
        List<ReportResponseDTO> popReports=getReport(startDate, endDate);

        String inactived="";
        String tmp="";
        for(ReportResponseDTO report : popReports){

            /*double adcashRejected=report.getAdcashRejected();
            double adcashImp=report.getAdcashImp();
            double adcashFallback=report.getAdcashImp();
            double adcashRev=report.getAdcashRev();
            double totalRequest = adcashRejected+adcashImp+adcashFallback;
            */

            double propConversion=report.getPropConversion();
            double propImp=report.getPropImp();
            double propRev=report.getPropRev();

            double prop2Conversion=report.getProp2Conversion();
            double prop2Imp=report.getProp2Imp();
            double prop2Rev=report.getProp2Rev();

            boolean adCashFraud=false;
            boolean propFraud=false;
            boolean prop2Fraud=false;

            /*if(totalRequest>10000 && (adcashRejected/totalRequest)>0.5){
                tmp=report.getAppUnitId()+" : AdcashFraud rj="+adcashRejected+" fb="+
                        adcashFallback+"  imp="+adcashImp+" rev="+adcashRev;
                inactived+=tmp+"\n";
                adCashFraud=true;
                System.out.println(tmp);
            }*/
            if(propImp>10000 && (propConversion/propImp)<0.00005){
                tmp=report.getAppUnitId()+" : PropFraud conv="+propConversion+" imp="+propImp+" rev="+propRev;;
                inactived+=tmp+"\n";
                System.out.println(tmp);
                propFraud=true;
            }
            if(prop2Imp>10000 && (prop2Conversion/prop2Imp)<0.00005){
                tmp= report.getAppUnitId()+" : Prop2Fraud conv="+prop2Conversion+" imp="+prop2Imp+" rev="+prop2Rev;;
                inactived+=tmp+"\n";
                System.out.println(tmp);
                prop2Fraud=true;
            }
            //adCashFraud ||
            if(propFraud || prop2Fraud){

                Optional<AppUnit> appUnitOptional = appUnitRepository.findOneById(Integer.parseInt(report.getAppUnitId()));
                if(appUnitOptional.isPresent()){
                    AppUnit appUnit=appUnitOptional.get();
                    if(adCashFraud){
                        appUnit.setAdcashId("0");
                    }
                    if(propFraud){
                        appUnit.setPropellerId("0");
                    }
                    if(prop2Fraud){
                        appUnit.setPropeller2Id("0");
                    }
                    appUnitRepository.save(appUnit);
                }

            }
        }
        emailService.send("pub@reklamstore.com","Pop Fraud List "+startDate,inactived);
    }

    private List<ReportRow> getAdcashReport(String startDate, String endDate) {
        io.swagger.adcash.client.model.Report report = adCashService.getReport(startDate, endDate);
        report = adCashService.filterSubIds(report);
        List<ReportRow> adcashRows = report.getRows();
        return adcashRows;
    }


    private StatisticData getPropellerReport(String startDate, String endDate) {
        InlineResponse2001 response = propellerService.getReport(startDate, endDate);
        StatisticData propRows = response.getResult();
        return propRows;
    }

    private StatisticData getPropeller2Report(String startDate, String endDate) {
        InlineResponse2001 response = propellerService.getReport2(startDate, endDate);

        response = propellerService.filterSubIds(response, true);
        StatisticData prop2Rows = response.getResult();
        return prop2Rows;
    }


    public Object getReportByRequest(ReportRequestDto reportRequestDto) {
        Object response = null;
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.set("Content-Type", reportRequestDto.getContentType());
            if (reportRequestDto.getUseOtherApiKey())
                httpHeaders.set("apiKey", "cmVrbGFtcHViIScx");
            else
                httpHeaders.set("apiKey", "rs1xd34452weo");

            HttpEntity<HashMap> httpEntity = new HttpEntity<>(reportRequestDto.getParams(), httpHeaders);
            if (reportRequestDto.getRequestType().toString().equals(RequestMethod.POST.toString()))
                response = restTemplate.postForObject(reportRequestDto.getUrl(), httpEntity, Object.class);
            else if (reportRequestDto.getRequestType().toString().equals(RequestMethod.GET.toString()))
                response = restTemplate.exchange(reportRequestDto.getUrl(), HttpMethod.GET, httpEntity, Object.class);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
        return response;
    }


}
