package com.reklamstore.rexmob.report;


import com.reklamstore.rexmob.exchange.Exchange;
import com.reklamstore.rexmob.exchange.ExchangeService;
import io.swagger.propellerv5.client.ApiClient;
import io.swagger.propellerv5.client.api.AuthorizationApi;
import io.swagger.propellerv5.client.api.StatisticsApi;
import io.swagger.propellerv5.client.model.*;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Service
public class PropellerService implements InitializingBean, DisposableBean {


    @Autowired
    private ExchangeService exchangeService;

    AuthorizationApi authorizationApi = null;
    StatisticsApi statisticsApi = null;
    ApiClient apiClient = null;
    Exchange exchange;

    public static final String USERNAME_PROP = "pub@reklamstore.com";
    public static final String PASSWORD_PROP = "Reklam@1453";

    public static final String USERNAME_PROP2 = "kurtulus@link.tl";
    public static final String PASSWORD_PROP2 = "Krt999**//";

    public static final String NETWORK_PREFIX = "propeller";

    @Override
    public void destroy() throws Exception {

    }

    @Override
    public void afterPropertiesSet() throws Exception {

        authorizationApi = new AuthorizationApi();
        statisticsApi = new StatisticsApi();
        exchange = exchangeService.findLastExchange();

    }

    public InlineResponse2001 getReport(String startDate, String endDate) {

        InlineResponse2001 response = null;
        try {
            Body body = new Body();
            body.setUsername(USERNAME_PROP);
            body.setPassword(PASSWORD_PROP);

            InlineResponse200 authResponse = authorizationApi.pubLogin(body);
            String token = authResponse.getApiToken();
            apiClient = statisticsApi.getApiClient();
            apiClient.setApiKeyPrefix("Bearer");
            apiClient.setApiKey(token);
            ArrayList<String> paramsZone = new ArrayList<String>();
            paramsZone.add("2761083");
            ArrayList<String> groupBy = new ArrayList<String>();
            groupBy.add("request_var");

            response = statisticsApi.pubStatistics(groupBy, startDate, endDate, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public InlineResponse2001 getReport2(String startDate, String endDate) {

        InlineResponse2001 response = null;
        try {
            Body body = new Body();
            body.setUsername(USERNAME_PROP2);
            body.setPassword(PASSWORD_PROP2);

            InlineResponse200 authResponse = authorizationApi.pubLogin(body);
            String token = authResponse.getApiToken();
            apiClient = statisticsApi.getApiClient();
            apiClient.setApiKeyPrefix("Bearer");
            apiClient.setApiKey(token);
            ArrayList<String> paramsZone = new ArrayList<String>();
            paramsZone.add("2761083");
            ArrayList<String> groupBy = new ArrayList<String>();
            groupBy.add("request_var");

            response = statisticsApi.pubStatistics(groupBy, startDate, endDate, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public InlineResponse2001 filterSubIds(InlineResponse2001 response, boolean propeller2) {

        StatisticData rows = response.getResult();
        StatisticData newRows = new StatisticData();
        HashMap<String, StatisticDataInner> rowMap = new HashMap<String, StatisticDataInner>();
        for (int i = 0; i < rows.size(); i++) {
            StatisticDataInner row = rows.get(i);
            String placementId = row.getRequestVar();
            if (placementId == null) {
                continue;
            }
            if (propeller2 && !placementId.endsWith("_1")) {
                continue;
            }
            if (placementId.contains("_")) {
                String[] st = placementId.split("_");
                if (st.length > 0)
                    placementId = st[0];
            }
            row.setRequestVar(placementId);
            if (rowMap.containsKey(placementId)) {
                StatisticDataInner row2 = rowMap.get(placementId);
                row2.setClicks(row2.getClicks().intValue() + row.getClicks().intValue());
                row2.setImpressions(row2.getImpressions() + row.getImpressions().intValue());
                row2.setMoney(row2.getMoney().doubleValue() + row.getMoney().doubleValue());
            } else {
                rowMap.put(placementId, row);
            }
        }
        for (Map.Entry<String, StatisticDataInner> entry : rowMap.entrySet()) {
            StatisticDataInner value = entry.getValue();
            newRows.add(value);
        }
        response.setResult(newRows);
        return response;
    }

}
