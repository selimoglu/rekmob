package com.reklamstore.rexmob.report.response;

public class ReportResponseDTO {

    private String appUnitId;
    private Double adcashImp = 0.0d;
    private Double adcashRev = 0.0d;
    private Double adcashFallback = 0.0d;
    private Double adcashRejected = 0.0d;
    private Double propRev = 0.0d;
    private Double propImp = 0.0d;
    private Double propConversion = 0.0d;
    private Double prop2Rev = 0.0d;
    private Double prop2Imp = 0.0d;
    private Double prop2Conversion = 0.0d;
    private Integer userId = 0;
    private String fullName = "";


    public String getAppUnitId() {
        return appUnitId;
    }

    public void setAppUnitId(String appUnitId) {
        this.appUnitId = appUnitId;
    }

    public Double getAdcashImp() {
        return adcashImp;
    }

    public void setAdcashImp(Double adcashImp) {
        this.adcashImp = adcashImp;
    }

    public Double getAdcashRev() {
        return adcashRev;
    }

    public void setAdcashRev(Double adcashRev) {
        this.adcashRev = adcashRev;
    }

    public Double getAdcashFallback() {
        return adcashFallback;
    }

    public void setAdcashFallback(Double adcashFallback) {
        this.adcashFallback = adcashFallback;
    }

    public Double getAdcashRejected() {
        return adcashRejected;
    }

    public void setAdcashRejected(Double adcashRejected) {
        this.adcashRejected = adcashRejected;
    }

    public Double getPropRev() {
        return propRev;
    }

    public void setPropRev(Double propRev) {
        this.propRev = propRev;
    }

    public Double getPropImp() {
        return propImp;
    }

    public void setPropImp(Double propImp) {
        this.propImp = propImp;
    }

    public Double getPropConversion() {
        return propConversion;
    }

    public void setPropConversion(Double propConversion) {
        this.propConversion = propConversion;
    }

    public Double getProp2Rev() {
        return prop2Rev;
    }

    public void setProp2Rev(Double prop2Rev) {
        this.prop2Rev = prop2Rev;
    }

    public Double getProp2Imp() {
        return prop2Imp;
    }

    public void setProp2Imp(Double prop2Imp) {
        this.prop2Imp = prop2Imp;
    }

    public Double getProp2Conversion() {
        return prop2Conversion;
    }

    public void setProp2Conversion(Double prop2Conversion) {
        this.prop2Conversion = prop2Conversion;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
