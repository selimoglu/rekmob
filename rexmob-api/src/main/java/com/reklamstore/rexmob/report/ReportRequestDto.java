package com.reklamstore.rexmob.report;

import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashMap;

public class ReportRequestDto {

    private String url;
    private RequestMethod requestType = RequestMethod.POST;
    private HashMap<String, Object> params;
    private String contentType;
    private Boolean useOtherApiKey = false;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public RequestMethod getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestMethod requestType) {
        this.requestType = requestType;
    }

    public HashMap<String, Object> getParams() {
        return params;
    }

    public void setParams(HashMap<String, Object> params) {
        this.params = params;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Boolean getUseOtherApiKey() {
        return useOtherApiKey;
    }

    public void setUseOtherApiKey(Boolean useOtherApiKey) {
        this.useOtherApiKey = useOtherApiKey;
    }
}
