package com.reklamstore.rexmob.report;

import com.reklamstore.rexmob.report.response.ReportResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.ok;

@Controller
@RequestMapping("/reports")
public class ReportController {

    @Autowired
    AdcashService adCashService;
    private ReportService reportService;


    @Autowired
    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/request")
    public ResponseEntity getReports(@RequestBody ReportRequestDto reportRequestDto) {
        return ok(reportService.getReportByRequest(reportRequestDto));
    }


    @ResponseBody()
    @GetMapping("/pop-report")
    public ReportResponse getReportAdCashAndPropeller(@RequestParam String startDate, @RequestParam String endDate) {
        return new ReportResponse(reportService.getReport(startDate, endDate));

    }

}
