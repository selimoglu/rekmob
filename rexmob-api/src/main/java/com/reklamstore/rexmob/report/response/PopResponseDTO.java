package com.reklamstore.rexmob.report.response;

public class PopResponseDTO {

    private Integer id;
    private Integer userId;
    private String fullName ;

    public PopResponseDTO() {
    }

    public PopResponseDTO(Integer id, Integer userId, String fullName) {
        this.id = id;
        this.userId = userId;
        this.fullName = fullName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
