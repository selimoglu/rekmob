package com.reklamstore.rexmob.report;

import com.reklamstore.rexmob.exchange.Exchange;
import com.reklamstore.rexmob.exchange.ExchangeService;
import io.swagger.adcash.client.api.DefaultApi;
import io.swagger.adcash.client.model.Report;
import io.swagger.adcash.client.model.ReportRow;
import org.json.JSONObject;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdcashService implements InitializingBean, DisposableBean {

    @Autowired
    private ExchangeService exchangeService;

    DefaultApi adCashApi = null;
    Exchange exchange;

    public static final String EMAIL = "sefa@reklamstore.com";
    public static final String PASSWORD = "reklamstore123";
    public static final String NETWORK_PREFIX = "adcash";

    @Override
    public void destroy() throws Exception {

    }

    @Override
    public void afterPropertiesSet() throws Exception {

        adCashApi = new DefaultApi();
        exchange= exchangeService.findLastExchange();

    }


    public Report getReport(String startDate, String endDate) {

        Report report = null;
        try {

            String res = adCashApi.getToken(EMAIL, PASSWORD);
            JSONObject tokenObject = new JSONObject(res);
            String token = tokenObject.getString("token");

            ArrayList<String> grouping = new ArrayList<String>();
            grouping.add("sub1");
            report = adCashApi.getReport(token, "get_publisher_detailed_statistics", startDate, endDate, grouping);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return report;
    }

    public Double parseDouble(Object str) {
        double v = 0;
        try {
            v = str != null ? Double.parseDouble(str.toString()) : 0;
        } catch (Exception e) {
        }
        return v;
    }

    public Integer parseInt(Object str) {
        int v = 0;
        try {
            v = str != null ? Integer.parseInt(str.toString()) : 0;
        } catch (Exception e) {
        }
        return v;
    }


    public Report filterSubIds(Report report) {

        List<ReportRow> rows = report.getRows();
        List<ReportRow> newRows = new ArrayList<ReportRow>();
        HashMap<String, ReportRow> rowMap = new HashMap<String, ReportRow>();
        for (int i = 0; i < rows.size(); i++) {

            ReportRow row = rows.get(i);
            String placementId = row.getSub1();
            if (placementId == null || placementId.contains(".")) {
                continue;
            }
            if (placementId.contains("_")) {
                String[] st = placementId.split("_");
                if (st.length > 0)
                    placementId = st[0];
            }
            row.setSub1(placementId);
            if (rowMap.containsKey(placementId)) {
                ReportRow row2 = rowMap.get(placementId);
                row2.setClicks((parseInt(row2.getClicks()) + parseInt(row.getClicks())) + "");
                row2.setImpressions((parseInt(row2.getImpressions()) + parseInt(row.getImpressions())) + "");
                row2.setEarnings((parseDouble(row2.getEarnings()) + parseDouble(row.getEarnings())) + "");
                row2.setFallback((parseInt(row2.getFallback()) + parseInt(row.getFallback())) + "");
                row2.setFallback_rejected_imps((parseInt(row2.getFallback_rejected_imps()) + parseInt(row.getFallback_rejected_imps())) + "");

            } else {
                rowMap.put(placementId, row);
            }
        }
        for (Map.Entry<String, ReportRow> entry : rowMap.entrySet()) {
            ReportRow value = entry.getValue();
            newRows.add(value);
        }
        report.setRows(newRows);
        return report;
    }


}
