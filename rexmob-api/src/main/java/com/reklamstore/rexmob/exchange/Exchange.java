package com.reklamstore.rexmob.exchange;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "exchange")
public class Exchange {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private String id;

    @Column(name = "from_currency_name")
    private String fromCurrencyName;

    @Column(name = "to_currency_name")
    private String toCurrencyName;

    @Column(name = "buying_amount")
    private Double buyingAmount;

    @Column(name = "date")
    private Date date;


    public String getFromCurrencyName() {
        return fromCurrencyName;
    }

    public void setFromCurrencyName(String fromCurrencyName) {
        this.fromCurrencyName = fromCurrencyName;
    }

    public String getToCurrencyName() {
        return toCurrencyName;
    }

    public void setToCurrencyName(String toCurrencyName) {
        this.toCurrencyName = toCurrencyName;
    }

    public Double getBuyingAmount() {
        return buyingAmount;
    }

    public void setBuyingAmount(Double buyingAmount) {
        this.buyingAmount = buyingAmount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double toTL(double val) {
        return val * getBuyingAmount();
    }

    public double toUsd(double val) {
        return val / getBuyingAmount();
    }
}
