package com.reklamstore.rexmob.exchange;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("exchangeService")
@Transactional
public class ExchangeService{

  private ExchangeRepository exchangeRepository;

  @Autowired
  public ExchangeService(ExchangeRepository exchangeRepository){
    this.exchangeRepository = exchangeRepository;
  }
  
  public Exchange findLastExchangeWithoutCache(){
    return exchangeRepository.findFirstByOrderByIdDesc();
  }
  
  @Cacheable(value = {"findLastExchange"})
  public Exchange findLastExchange(){
    return exchangeRepository.findFirstByOrderByIdDesc();
  }
  
  @CacheEvict(value = {"findLastExchange"}, allEntries = true)
  public void clearExchangeCache(){
    
  }

}
