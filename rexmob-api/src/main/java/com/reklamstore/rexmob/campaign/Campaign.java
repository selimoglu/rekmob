package com.reklamstore.rexmob.campaign;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.reklamstore.rexmob.ad.Ad;
import com.reklamstore.rexmob.util.TotalStatUtil;
import org.hibernate.annotations.Formula;
import javax.validation.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author mehmet
 *
 */
@Entity
@Table(name = "campaign")
public class Campaign {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CAMPAIGN_ID")
    private Integer id;

    @NotEmpty
    @Column(name = "CAMPAIGN_NAME")
    private String campaignName;

    @Column(name = "TAGS_TEXT")
    private String tagsText;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "START_TIME")
    private String startTime;

    @Column(name = "END_DATE_FLAG")
    private boolean endDateFlag;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "END_DATE")
    private Date endDate;

    @Column(name = "END_TIME")
    private String endTime;

    @Column(name = "ACTIVE", nullable = true)
    private int active;

    @Column(name = "ARCHIVED", nullable = true)
    private int archived;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "COUNTRIES")
    private String countries;

    @Column(name = "HOUSE_AD")
    private boolean houseAd;

    @Column(name = "USER_ID")
    private Integer userId;

    @Column(name = "NETWORK")
    private Integer network;

    /**
     * Status Enum PENDING(0), APPROVED(1), REJECTED(2);
     */
    @Column(name = "STATUS")
    private int status;

    @Column(name = "STATUS_DESC")
    private String statusDesc;

    @Column(name = "START_TIMESTAMP")
    private Long startTimestamp;

    @Column(name = "END_TIMESTAMP")
    private Long endTimestamp;

    @Column(name = "INCLUDED_APPS")
    private String includedApps;

    @Column(name = "EXCLUDED_APPS")
    private String excludedApps;

    @Column(name = "OPERATORS")
    private String operators;

    @Column(name="TARGET_TAGS_TEXT")
    private String targetTagsText;


    @Column(name="BUDGET")
    private Double budget;

    @Column(name="SPENT_BUDGET")
    private Double spentBudget;

    @Formula("COALESCE(BUDGET,0) - COALESCE(SPENT_BUDGET,0)")
    private double remaningBudget;
    /**
     * @return {@link #name}
     */
    public String getTargetTagsText() {
        return targetTagsText;
    }

    /**
     * @param targetTagsText the targetTagsText to set
     */
    public void setTargetTagsText(String targetTagsText) {
        this.targetTagsText = targetTagsText;
    }


    @Transient
    private List<Ad> adList;

    @Transient
    private TotalStatUtil totalStatUtil;

    /**
     * @return {@link #name}
     */
    public List<Ad> getAdList() {
        return adList;
    }

    /**
     * @param adList the adList to set
     */
    public void setAdList(List<Ad> adList) {
        this.adList = adList;
    }

    /**
     * @return {@link #name}
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return {@link #name}
     */
    public String getCampaignName() {
        return campaignName;
    }

    /**
     * @param campaignName
     *            the campaignName to set
     */
    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    /**
     * @return {@link #name}
     */
    public String getTagsText() {
        return tagsText;
    }

    /**
     * @param tagsText
     *            the tagsText to set
     */
    public void setTagsText(String tagsText) {
        this.tagsText = tagsText;
    }

    /**
     * @return {@link #name}
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate
     *            the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return {@link #name}
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * @param startTime
     *            the startTime to set
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * @return {@link #name}
     */
    public boolean isEndDateFlag() {
        return endDateFlag;
    }

    /**
     * @param endDateFlag
     *            the endDateFlag to set
     */
    public void setEndDateFlag(boolean endDateFlag) {
        this.endDateFlag = endDateFlag;
    }

    /**
     * @return {@link #name}
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate
     *            the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return {@link #name}
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * @param endTime
     *            the endTime to set
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * @return {@link #name}
     */
    public int getActive() {
        return active;
    }

    /**
     * @param active
     *            the active to set
     */
    public void setActive(int active) {
        this.active = active;
    }

    /**
     * @return {@link #name}
     */
    public int getArchived() {
        return archived;
    }

    /**
     * @param archived
     *            the archived to set
     */
    public void setArchived(int archived) {
        this.archived = archived;
    }

    /**
     * @return {@link #name}
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     *            the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return {@link #name}
     */
    public String getCountries() {
        return countries;
    }

    /**
     * @param countries
     *            the countries to set
     */
    public void setCountries(String countries) {
        this.countries = countries;
    }

    /**
     * @return {@link #name}
     */
    public boolean isHouseAd() {
        return houseAd;
    }

    public boolean getHouseAd() {
        return houseAd;
    }

    /**
     * @param houseAd
     *            the houseAd to set
     */
    public void setHouseAd(boolean houseAd) {
        this.houseAd = houseAd;
    }

    /**
     * @return {@link #name}
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId
     *            the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return {@link #name}
     */
    public Integer getNetwork() {
        return network;
    }

    /**
     * @param network
     *            the network to set
     */
    public void setNetwork(Integer network) {
        this.network = network;
    }

    /**
     * @return {@link #name}
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return {@link #name}
     */
    public String getStatusDesc() {
        return statusDesc;
    }

    /**
     * @param statusDesc
     *            the statusDesc to set
     */
    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    /**
     * @return {@link #name}
     */
    public Long getStartTimestamp() {
        return startTimestamp;
    }

    /**
     * @param startTimestamp
     *            the startTimestamp to set
     */
    public void setStartTimestamp(Long startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    /**
     * @return {@link #name}
     */
    public Long getEndTimestamp() {
        return endTimestamp;
    }

    /**
     * @param endTimestamp
     *            the endTimestamp to set
     */
    public void setEndTimestamp(Long endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    /**
     * @return {@link #name}
     */
    public String getIncludedApps() {
        return includedApps;
    }

    /**
     * @param includedApps
     *            the includedApps to set
     */
    public void setIncludedApps(String includedApps) {
        this.includedApps = includedApps;
    }

    /**
     * @return {@link #name}
     */
    public String getExcludedApps() {
        return excludedApps;
    }

    /**
     * @param excludedApps
     *            the excludedApps to set
     */
    public void setExcludedApps(String excludedApps) {
        this.excludedApps = excludedApps;
    }

    /**
     * @return {@link #name}
     */
    public String getOperators() {
        return operators;
    }

    /**
     * @param operators
     *            the operators to set
     */
    public void setOperators(String operators) {
        this.operators = operators;
    }

    /**
     * @return {@link #name}
     */
    public TotalStatUtil getTotalStatUtil() {
        return totalStatUtil;
    }

    /**
     * @param totalStatUtil the totalStatUtil to set
     */
    public void setTotalStatUtil(TotalStatUtil totalStatUtil) {
        this.totalStatUtil = totalStatUtil;
    }

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public Double getSpentBudget() {
        return spentBudget;
    }

    public void setSpentBudget(Double spendBudget) {
        this.spentBudget = spendBudget;
    }

    public double getRemaningBudget() {
        return remaningBudget;
    }

    public void setRemaningBudget(double remaningBudget) {
        this.remaningBudget = remaningBudget;
    }

}
