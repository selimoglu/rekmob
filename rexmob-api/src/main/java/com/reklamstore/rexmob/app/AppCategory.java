package com.reklamstore.rexmob.app;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import com.reklamstore.rexmob.util.ApiJsonFilter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "app_category")
public class AppCategory {

	@JsonView(ApiJsonFilter.PublicView.class)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cat_id")
	private Integer catId;

	@JsonView(ApiJsonFilter.PublicView.class)
	@NotEmpty
	@Column(name = "cat_name")
	private String catName;

	@JsonIgnore
	@Column(name = "anx_cat_id")
	private Integer anxCatId;

	@JsonIgnore
	@Column(name = "imd_cat_id")
	private Integer imdCatId;

	@JsonIgnore
	@Column(name = "adf_cat_id")
	private Integer adfCatId;

	@JsonIgnore
	@Column(name = "iab_cat_id")
	private String iabCatId;

	public Integer getAdfCatId() {
		return adfCatId;
	}

	public void setAdfCatId(Integer adfCatId) {
		this.adfCatId = adfCatId;
	}

	public Integer getCatId() {
		return catId;
	}

	public void setCatId(Integer catId) {
		this.catId = catId;
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public Integer getAnxCatId() {
		return anxCatId;
	}

	public void setAnxCatId(Integer anxCatId) {
		this.anxCatId = anxCatId;
	}

	public String getIabCatId() {
		return iabCatId;
	}

	public void setIabCatId(String iabCatId) {
		this.iabCatId = iabCatId;
	}

	public Integer getImdCatId() {
		return imdCatId;
	}

	public void setImdCatId(Integer imdCatId) {
		this.imdCatId = imdCatId;
	}


}