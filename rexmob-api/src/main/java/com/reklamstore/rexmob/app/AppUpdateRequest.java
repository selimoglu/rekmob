package com.reklamstore.rexmob.app;

import com.reklamstore.rexmob.util.Status;

public class AppUpdateRequest {

    private String appUrl;
    private Integer appType;
    private Integer catId;
    private Status status;
    private RejectReasonType rejectReasonType;

    private String accManager;

    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public Integer getAppType() {
        return appType;
    }

    public void setAppType(Integer appType) {
        this.appType = appType;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public RejectReasonType getRejectReasonType() {
        return rejectReasonType;
    }

    public void setRejectReasonType(RejectReasonType rejectReasonType) {
        this.rejectReasonType = rejectReasonType;
    }

    public String getAccManager() {
        return accManager;
    }

    public void setAccManager(String accManager) {
        this.accManager = accManager;
    }
}
