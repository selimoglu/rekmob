package com.reklamstore.rexmob.app;

public enum RejectReasonType {
    INAPPROPRIATE_CONTENT(0), ADULT_CONTENT(1), ILLEGAL_CONTENT(2),
    MAJORITY_OF_SIMILAR_SITES(3), SITE_CONTAINS_TOO_MANY_ADS(4), DETAILED_REFUSAL(5),
    INCENTIVE_TO_CLICK(6), INSUFFICIENT_CONTENT(7), FREE_DOMAIN_NAME(8), NO_REASON(9);

    private int type;

    RejectReasonType(int type) {
        this.type = type;
    }

    public int getType(){
        return type;
    }

    public boolean equals(RejectReasonType rejectType){
        return rejectType != null && type == rejectType.getType();
    }

    public boolean equals(Integer rejectType){
        return rejectType != null && type == rejectType;
    }
}
