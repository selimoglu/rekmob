package com.reklamstore.rexmob.app.unit;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import com.reklamstore.rexmob.app.App;
import com.reklamstore.rexmob.app.AppUnitStat;
import com.reklamstore.rexmob.util.*;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "app_unit")
public class AppUnit {

	@JsonView(ApiJsonFilter.PublicView.class)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "UNIT_ID")
	private Integer id;

	@JsonView(ApiJsonFilter.PublicView.class)
	@NotEmpty
	@Column(name = "UNIT_NAME")
	private String unitName;

	@JsonView(ApiJsonFilter.PublicView.class)
	@Column(name = "UNIT_KEY", updatable = false)
	private String unitKey;

	@JsonView(ApiJsonFilter.AdminView.class)
	@JsonIgnore
	@Column(name = "ADMOB_KEY")
	private String admobKey;

	@JsonIgnore
	@Column(name = "ADSENSE_PUBLISHER_ID")
	private String adsensePublisherId;

	@JsonIgnore
	@Column(name = "ADSENSE_CHANNEL_ID")
	private String adsenseChannelId;

	@JsonView(ApiJsonFilter.PublicView.class)
	@Column(name = "UNIT_DESC")
	private String unitDesc;

	@JsonIgnore
	@Column(name = "REFRESH_TIME")
	private Integer refreshTime;

	@JsonView(ApiJsonFilter.PublicView.class)
	@Column(name = "APP_ID")
	private Integer appId;

	@JsonView(ApiJsonFilter.PublicView.class)
	@Column(name = "HOUSE_AD_RATE")
	private int houseAdRate;

	@JsonView(ApiJsonFilter.PublicView.class)
	@Column(name = "ACTIVE", nullable = true)
	private Boolean active;

	@JsonIgnore
	@Column(name = "ARCHIVED", nullable = true)
	private int archived;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "AD_SOURCE", nullable = true)
	private int adSource;

	@JsonView(ApiJsonFilter.PublicView.class)
	@Column(name = "CREATE_DATE", updatable = false)
	private Date createDate;

	@JsonView(ApiJsonFilter.PublicView.class)
	@Column(name = "UPDATE_DATE")
	private Date updateDate;

	@JsonView(ApiJsonFilter.PublicView.class)
	@Column(name = "CREATIVE_FORMAT")
	private int creativeFormat;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "ADX_ACCOUNT", nullable = true)
	private int adxAccount;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "ADX_PERCENT")
	private Double adxPercent;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "anx_placement_id")
	private Integer anxPlacementId;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "adf_placement_id")
	private String adfPlacementId;

	@JsonView(ApiJsonFilter.NetworkView.class)
	@Column(name = "brt_placement_id")
	private Integer brtPlacementId;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "FIXED_CPM")
	private Double fixedCpm;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "FIXED_CPM_USD")
	private Double fixedCpmUsd;

	@JsonView(ApiJsonFilter.NetworkView.class)
	@Column(name = "TOP_CPM")
	private Double topCpm;

	@JsonView(ApiJsonFilter.NetworkView.class)
	@Column(name = "rekmob_percent")
	private Double rekmobPercent;

	@JsonView(ApiJsonFilter.NetworkView.class)
	@Column(name = "anx_payment_rule_id")
	private Integer anxPaymentRuleId;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "sr_placement_id")
	private String srPlacementId;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "native_placement_id")
	private String nativePlacementId;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "pm_placement_id")
	private String pmPlacementId;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "ss_placement_id")
	private String ssPlacementId;

	@Column(name = "rubi_placement_id")
	private String rubiPlacementId;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "imdi_placement_id")
	private String imdiPlacementId;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "fraud_ratio")
	private Integer fraudRatio;

	@JsonView(ApiJsonFilter.PublicView.class)
	@Column(name = "UNIT_SIZE")
	private Integer unitSize;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "region_id")
	private String regionId;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "propeller_id")
	private String propellerId;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "propeller2_id")
	private String propeller2Id;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "adcash_id")
	private String adcashId;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "adsterra_id")
	private String adsterraId;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "rsdsp_id")
	private String rsdspId;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "admaven_id")
	private String admavenId;

	@JsonView(ApiJsonFilter.AdminView.class)
	@Column(name = "native_type")
	private Integer nativeType;

	@JsonView(ApiJsonFilter.PublicView.class)
	@Transient
	private String tagCode;

	@JsonView(ApiJsonFilter.PublicView.class)
	@Transient
	private Integer userId;

	@Transient
	private String pmTagId;

	@Transient
	private Double fixedCpmDollar;

	@Transient
	private String subId;

	@JsonView(ApiJsonFilter.PublicView.class)
	@Transient
	private String videoEnvironment;

	public Integer getAnxPlacementId() {
		return anxPlacementId;
	}

	@JsonView(ApiJsonFilter.PublicView.class)
	public String getNetworkIds() {
		List<String> s = new ArrayList<String>();

		if(app!=null && app.getAppType()!=null) {
			if(app.getAppType().intValue()==AppType.BRANDSAFE.getType()) {
				if (anxPlacementId != null) {
					s.add("anx_placement_id=" + anxPlacementId);
				}
				if (!StringUtils.isEmpty(adfPlacementId)) {
					s.add("adf_placement_id=" + adfPlacementId);
				}
				if (brtPlacementId != null) {
					s.add("brt_placement_id=" + brtPlacementId);
				}
				if (!StringUtils.isEmpty(pmPlacementId)) {
					s.add("pm_placement_id=" + pmPlacementId);
				}
			}
			if (!StringUtils.isEmpty(imdiPlacementId)) {
				s.add("imdi_placement_id=" + imdiPlacementId);
			}
		}

		if(app!=null && app.getCrtId()!=null) {
			s.add("crt_id=" + app.getCrtId());
		}
		if(app!=null && app.getRtbhId()!=null) {
			s.add("rtbh_id=" + app.getRtbhId());
		}

		return s.isEmpty() ? null : StringUtils.join(s, ";");
	}

	public void setAnxPlacementId(Integer anxPlacementId) {
		this.anxPlacementId = anxPlacementId;
	}

	public Double getAdxPercent() {
		return adxPercent;
	}

	public void setAdxPercent(Double adxPercent) {
		this.adxPercent = adxPercent;
	}

	@JsonView(ApiJsonFilter.PublicView.class)
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "APP_ID", insertable = false, updatable = false)
	private App app;

	@JsonIgnore
	@Transient
	private List<AppUnitStat> appUnitStatList;

	@Transient
	private TotalStatUtil totalStat;

	/**
	 * @return {@link #name}
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return {@link #name}
	 */
	public String getUnitName() {
		return unitName;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	/**
	 * @param unitName
	 *            the unitName to set
	 */
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	/**
	 * @return {@link #name}
	 */
	public String getUnitKey() {
		return unitKey;
	}

	/**
	 * @param unitKey
	 *            the unitKey to set
	 */
	public void setUnitKey(String unitKey) {
		this.unitKey = unitKey;
	}

	/**
	 * @return {@link #name}
	 */
	public String getAdmobKey() {
		return admobKey;
	}

	/**
	 * @param admobKey
	 *            the admobKey to set
	 */
	public void setAdmobKey(String admobKey) {
		this.admobKey = admobKey;
	}

	public String getAdsensePublisherId() {
		return adsensePublisherId;
	}

	public void setAdsensePublisherId(String adsensePublisherId) {
		this.adsensePublisherId = adsensePublisherId;
	}

	public String getAdsenseChannelId() {
		return adsenseChannelId;
	}

	public void setAdsenseChannelId(String adsenseChannelId) {
		this.adsenseChannelId = adsenseChannelId;
	}

	/**
	 * @return {@link #name}
	 */
	public String getUnitDesc() {
		return unitDesc;
	}

	/**
	 * @param unitDesc
	 *            the unitDesc to set
	 */
	public void setUnitDesc(String unitDesc) {
		this.unitDesc = unitDesc;
	}

	/**
	 * @return {@link #name}
	 */
	public Integer getRefreshTime() {
		return refreshTime;
	}

	/**
	 * @param refreshTime
	 *            the refreshTime to set
	 */
	public void setRefreshTime(Integer refreshTime) {
		this.refreshTime = refreshTime;
	}

	/**
	 * @return {@link #name}
	 */
	public Integer getAppId() {
		return appId;
	}

	/**
	 * @param appId
	 *            the appId to set
	 */
	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	/**
	 * @return {@link #name}
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * @return {@link #name}
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate
	 *            the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * @return the creativeFormat
	 */
	public int getCreativeFormat() {
		return creativeFormat;
	}

	/**
	 * @param creativeFormat
	 *            the creativeFormat to set
	 */
	public void setCreativeFormat(int creativeFormat) {
		this.creativeFormat = creativeFormat;
	}

	/**
	 * @return {@link #name}
	 */
	public List<AppUnitStat> getAppUnitStatList() {
		return appUnitStatList;
	}

	/**
	 * @param appUnitStatList
	 *            the appUnitStatList to set
	 */
	public void setAppUnitStatList(List<AppUnitStat> appUnitStatList) {
		this.appUnitStatList = appUnitStatList;
	}

	/**
	 * @return {@link #name}
	 */
	public App getApp() {
		return app;
	}

	/**
	 * @param app
	 *            the app to set
	 */
	public void setApp(App app) {
		this.app = app;
	}

	/**
	 * @return {@link #name}
	 */
	public boolean isArchived() {
		return archived > 0 ? true : false;
	}

	/**
	 * @return {@link #name}
	 */
	public int getArchived() {
		return archived;
	}

	/**
	 * @param archived
	 *            the archived to set
	 */
	public void setArchived(int archived) {
		this.archived = archived;
	}

	/**
	 * @param archived
	 *            the archived to set
	 */
	// @JsonProperty("archived")
	public void setArchived(boolean isArchived) {
		this.archived = isArchived ? 1 : 0;
	}

	public int getAdSource() {
		return adSource;
	}

	public void setAdSource(int adSource) {
		this.adSource = adSource;
	}

	/**
	 * @return {@link #name}
	 */
	public int getHouseAdRate() {
		return houseAdRate;
	}

	/**
	 * @param houseAdRate
	 *            the houseAdRate to set
	 */
	public void setHouseAdRate(int houseAdRate) {
		this.houseAdRate = houseAdRate;
	}

	public int getAdxAccount() {
		return adxAccount;
	}

	public void setAdxAccount(int adxAccount) {
		this.adxAccount = adxAccount;
	}

	public boolean isAdxAccount() {
		return this.adxAccount > 0 ? true : false;
	}

	public Double getFixedCpm() {
		return fixedCpm;
	}

	/* use for edit fixedcpm */
	public String getFixedCpmStr() {
		return isFixedCpm() ? String.valueOf(getFixedCpm()) : "";
	}

	public String getTopCpmStr() {
		return topCpm != null ? String.valueOf(getTopCpm()) : "";
	}

	public void setFixedCpm(Double fixedCpm) {
		this.fixedCpm = fixedCpm;
	}

	public boolean isFixedCpm() {
		if (this.fixedCpm == null) {
			return false;
		}
		return true;
	}

	public String getLabelString() {
		if (unitSize == null) {
			return "";
		}
		int unitSizeVal = unitSize.intValue();
		return AdSize.getLabelString(unitSizeVal);

	}

	public Integer getUnitSize() {
		return unitSize;
	}

	public void setUnitSize(Integer unitSize) {
		this.unitSize = unitSize;
	}

	/**
	 * @return {@link #name}
	 */
	public TotalStatUtil getTotalStat() {
		return totalStat;
	}

	/**
	 * @param totalStat
	 *            the totalStat to set
	 */
	public void setTotalStat(TotalStatUtil totalStat) {
		this.totalStat = totalStat;
	}

	public JSONObject getJson(String timezone) {
		JSONObject unitObj = new JSONObject();
		unitObj.put("unitName", getUnitName());
		unitObj.put("unitId", getId());

		unitObj.put("totalReq", getTotalStat().getTotalReq());
		unitObj.put("totalImp", getTotalStat().getTotalImp());
		unitObj.put("totalClick", getTotalStat().getTotalClick());
		unitObj.put("totalIncome", getTotalStat().getTotalIncome());

		unitObj.put("totalHReq", getTotalStat().getTotalHreq());
		unitObj.put("totalHImp", getTotalStat().getTotalHimp());
		unitObj.put("totalHClick", getTotalStat().getTotalHclick());

		// unitObj.put("totalAReq", getTotalStat().getTotalAreq());
		// unitObj.put("totalAImp", getTotalStat().getTotalAimp());
		// unitObj.put("totalAClick", getTotalStat().getTotalAclick());

		JSONArray clickStats = new JSONArray();
		JSONArray impStats = new JSONArray();
		JSONArray reqStats = new JSONArray();
		JSONArray incomeStats = new JSONArray();
		JSONArray hclickStats = new JSONArray();
		JSONArray himpStats = new JSONArray();
		JSONArray hreqStats = new JSONArray();
		// JSONArray aclickStats = new JSONArray();
		// JSONArray aimpStats = new JSONArray();
		// JSONArray areqStats = new JSONArray();
		for (AppUnitStat stat : getAppUnitStatList()) {
			long unixTime = DateUtil.sameDateNewTimezone(stat.getStatTime(), timezone).getTime();

			reqStats.put(getFieldJSON(unixTime, stat.getReq()));
			impStats.put(getFieldJSON(unixTime, stat.getImp()));
			clickStats.put(getFieldJSON(unixTime, stat.getClick()));
			incomeStats.put(getFieldJSON(unixTime, stat.getIncome()));

			/*
			 * hreqStats.put(getFieldJSON(unixTime, stat.getHreq()));
			 * himpStats.put(getFieldJSON(unixTime, stat.getHimp()));
			 * hclickStats.put(getFieldJSON(unixTime, stat.getHclick()));
			 */

			hreqStats.put(getFieldJSON(unixTime, 0));
			himpStats.put(getFieldJSON(unixTime, 0));
			hclickStats.put(getFieldJSON(unixTime, 0));

			// areqStats.put(getFieldJSON(unixTime, stat.getAreq()));
			// aimpStats.put(getFieldJSON(unixTime, stat.getAimp()));
			// aclickStats.put(getFieldJSON(unixTime, stat.getAclick()));

		}
		unitObj.put("reqs", reqStats);
		unitObj.put("imps", impStats);
		unitObj.put("clicks", clickStats);
		unitObj.put("incomes", incomeStats);

		unitObj.put("hreqs", hreqStats);
		unitObj.put("himps", himpStats);
		unitObj.put("hclicks", hclickStats);

		// unitObj.put("areqs", areqStats);
		// unitObj.put("aimps", aimpStats);
		// unitObj.put("aclicks", aclickStats);

		return unitObj;
	}

	private JSONArray getFieldJSON(long unixTime, Object count) {
		JSONArray ja = new JSONArray();
		ja.put(unixTime);
		ja.put(count);
		return ja;
	}

	@JsonIgnore
	@Transient
	private AppUnitJsonHelper unitJsonHelper;

	public AppUnitJsonHelper getUnitJsonHelper() {
		return unitJsonHelper;
	}

	public void setUnitJsonHelper(AppUnitJsonHelper unitJsonHelper) {
		this.unitJsonHelper = unitJsonHelper;
	}

	public String getTagCode() {
		return tagCode;
	}

	public void setTagCode(String tagCode) {
		this.tagCode = tagCode;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getAdfPlacementId() {
		return adfPlacementId;
	}

	public void setAdfPlacementId(String adfPlacementId) {
		this.adfPlacementId = adfPlacementId;
	}

	public Integer getBrtPlacementId() {
		return brtPlacementId;
	}

	public void setBrtPlacementId(Integer brtPlacementId) {
		this.brtPlacementId = brtPlacementId;
	}

	public Double getRekmobPercent() {
		return rekmobPercent;
	}

	public void setRekmobPercent(Double rekmobPercent) {
		this.rekmobPercent = rekmobPercent;
	}

	public Double getTopCpm() {
		return topCpm;
	}

	public void setTopCpm(Double topCpm) {
		this.topCpm = topCpm;
	}

	public Integer getAnxPaymentRuleId() {
		return anxPaymentRuleId;
	}

	public void setAnxPaymentRuleId(Integer anxPaymentRuleId) {
		this.anxPaymentRuleId = anxPaymentRuleId;
	}

	public String getSrPlacementId() {
		return srPlacementId;
	}

	public void setSrPlacementId(String srPlacementId) {
		this.srPlacementId = srPlacementId;
	}

	public String getNativePlacementId() {
		return nativePlacementId;
	}

	public void setNativePlacementId(String nativePlacementId) {
		this.nativePlacementId = nativePlacementId;
	}

	public String getPmPlacementId() {
		return pmPlacementId;
	}

	public void setPmPlacementId(String pmPlacementId) {
		this.pmPlacementId = pmPlacementId;
	}

	public String getRubiPlacementId() {
		return rubiPlacementId;
	}

	public int getRubiSiteId() {
		int siteId=0;
		if(!StringUtils.isEmpty(rubiPlacementId) && rubiPlacementId.contains(",")) {
			try {
				siteId= Integer.parseInt(rubiPlacementId.split(",")[0]);
			} catch (Exception e) {
			}
		}
		return siteId;
	}
	public int getRubiZoneId() {
		int zoneId=0;
		if(!StringUtils.isEmpty(rubiPlacementId) && rubiPlacementId.contains(",")) {
			try {
				zoneId= Integer.parseInt(rubiPlacementId.split(",")[1]);
			} catch (Exception e) {
			}
		}
		return zoneId;
	}

	public void setRubiPlacementId(String rubiPlacementId) {
		this.rubiPlacementId = rubiPlacementId;
	}

	public String getPmTagId() {
		if(!StringUtils.isEmpty(pmPlacementId) && pmPlacementId.contains("@")) {
			return pmPlacementId.substring(0, pmPlacementId.indexOf("@"));
		}else {
			return pmPlacementId;
		}

	}

	public Double getFixedCpmDollar() {
		return fixedCpmDollar;
	}

	public void setFixedCpmDollar(Double fixedCpmDollar) {
		this.fixedCpmDollar = fixedCpmDollar;
	}

	public String getSsPlacementId() {
		return ssPlacementId;
	}

	public void setSsPlacementId(String ssPlacementId) {
		this.ssPlacementId = ssPlacementId;
	}

	public String getVideoEnvironment() {
		return videoEnvironment;
	}

	public void setVideoEnvironment(String videoEnvironment) {
		this.videoEnvironment = videoEnvironment;
	}

	public Integer getNativeType() {
		return nativeType;
	}

	public void setNativeType(Integer nativeType) {
		this.nativeType = nativeType;
	}

	public String getImdiPlacementId() {
		return imdiPlacementId;
	}

	public void setImdiPlacementId(String imdiPlacementId) {
		this.imdiPlacementId = imdiPlacementId;
	}

	public String getSubId() {
		if(subId==null)
			return id+"";
		return subId;
	}

	public void setSubId(String subId) {
		this.subId = subId;
	}

	public Integer getFraudRatio() {
		return fraudRatio;
	}

	public void setFraudRatio(Integer fraudRatio) {
		this.fraudRatio = fraudRatio;
	}

	public String getPropellerId() {
		return propellerId;
	}

	public void setPropellerId(String propellerId) {
		this.propellerId = propellerId;
	}

	public String getAdcashId() {
		return adcashId;
	}

	public void setAdcashId(String adcashId) {
		this.adcashId = adcashId;
	}

	public String getAdsterraId() {
		return adsterraId;
	}

	public void setAdsterraId(String adsterraId) {
		this.adsterraId = adsterraId;
	}

	public String getRsdspId() {
		return rsdspId;
	}

	public void setRsdspId(String rsdspId) {
		this.rsdspId = rsdspId;
	}

	public String getAdmavenId() {
		return admavenId;
	}

	public void setAdmavenId(String admavenId) {
		this.admavenId = admavenId;
	}

	public String getPropeller2Id() {
		return propeller2Id;
	}

	public void setPropeller2Id(String propeller2Id) {
		this.propeller2Id = propeller2Id;
	}


	public Double getFixedCpmUsd() {
		return fixedCpmUsd;
	}

	public void setFixedCpmUsd(Double fixedCpmUsd) {
		this.fixedCpmUsd = fixedCpmUsd;
	}
}
