package com.reklamstore.rexmob.app.response;

import com.reklamstore.rexmob.base.BaseException;
import com.reklamstore.rexmob.base.BaseResponse;

import java.util.List;

public class AppListResponse extends BaseResponse {

    private List<AppResponseDTO> response;

    public AppListResponse() {
    }

    public AppListResponse(BaseException exception) {
        super(exception);
    }

    public AppListResponse(List<AppResponseDTO> response) {
        super(true);
        this.response = response;
    }

    public List<AppResponseDTO> getResponse() {
        return response;
    }

    public void setResponse(List<AppResponseDTO> response) {
        this.response = response;
    }
}
