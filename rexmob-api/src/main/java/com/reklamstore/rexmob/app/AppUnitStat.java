package com.reklamstore.rexmob.app;

import java.math.BigInteger;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

import javax.persistence.*;

/*
 * if any field is added AppUnitStat class , please check JobSchedulerService.java for reducedStats jobs
 */
@Entity
@Table(name = "app_unit_nstat")
public class AppUnitStat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "STAT_ID")
    private BigInteger id;

    @Column(name = "REQ")
    private long req;

    @Column(name = "IMP")
    private long imp;

    @Column(name = "CLICK")
    private long click;

    @Column(name="CONVERSION")
    private long conversion;

    @Column(name="AD_SOURCE")
    private int adSource;

    public BigInteger getId() {
        return id;
    }



    public void setId(BigInteger id) {
        this.id = id;
    }



    public long getReq() {
        return req;
    }



    public void setReq(long req) {
        this.req = req;
    }



    public long getImp() {
        return imp;
    }



    public void setImp(long imp) {
        this.imp = imp;
    }



    public long getClick() {
        return click;
    }



    public void setClick(long click) {
        this.click = click;
    }



    public long getConversion() {
        return conversion;
    }



    public void setConversion(long conversion) {
        this.conversion = conversion;
    }



    public int getAdSource() {
        return adSource;
    }



    public void setAdSource(int adSource) {
        this.adSource = adSource;
    }



    public double getIncome() {
        return income;
    }



    public void setIncome(double income) {
        this.income = income;
    }



    public double getNetIncome() {
        return netIncome;
    }



    public void setNetIncome(double netIncome) {
        this.netIncome = netIncome;
    }



    public Integer getAppUnitId() {
        return appUnitId;
    }



    public void setAppUnitId(Integer appUnitId) {
        this.appUnitId = appUnitId;
    }



    public Date getStatTime() {
        return statTime;
    }



    public void setStatTime(Date statTime) {
        this.statTime = statTime;
    }



    public Date getCreateDate() {
        return createDate;
    }



    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }



    public String getTag() {
        return tag;
    }



    public void setTag(String tag) {
        this.tag = tag;
    }



    public int getValidated() {
        return validated;
    }



    public void setValidated(int validated) {
        this.validated = validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated?1:0;
    }

    public boolean isValidated(){
        return this.validated==1;
    }

    public Integer getUserId() {
        return UserId;
    }



    public void setUserId(Integer userId) {
        UserId = userId;
    }



    @Column(name = "INCOME")
    private double income;

    @Column(name = "NET_INCOME")
    private double netIncome;

    @Column(name = "INCOME_USD")
    private double incomeUsd;

    @Column(name = "NET_INCOME_USD")
    private double netIncomeUsd;


    @Column(name = "APP_UNIT_ID")
    private Integer appUnitId;

    @Column(name = "STAT_TIME")
    private Date statTime;

    @Column(name = "CREATE_DATE")
    private Date createDate;


    @Column(name="TAG")
    private String tag;

    @Column(name="VALIDATED")
    private int validated;



    @Transient
    private Integer UserId;

    public double getIncomeUsd() {
        return incomeUsd;
    }



    public void setIncomeUsd(double incomeUsd) {
        this.incomeUsd = incomeUsd;
    }



    public double getNetIncomeUsd() {
        return netIncomeUsd;
    }



    public void setNetIncomeUsd(double netIncomeUsd) {
        this.netIncomeUsd = netIncomeUsd;
    }



}
