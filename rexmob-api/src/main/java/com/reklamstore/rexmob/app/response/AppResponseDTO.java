package com.reklamstore.rexmob.app.response;

import com.reklamstore.rexmob.app.App;
import com.reklamstore.rexmob.user.User;
import java.util.Date;

public class AppResponseDTO {

    private Integer id;

    private String appName;
    private String appUrl;
    private String appKey;
    private Integer appType;
    private String tagsText;
    private String appDesc;

    private Integer userId;
    private String fullName;
    private String email;
    private String accManager;
    private String utmSource;
    private Integer isTR;
    private Date userCreateDate;

    private Integer catId;
    private int active;
    private int archived;

    private Integer platform;

    private Double rekmobPercent;

    private int status;
    private Integer rejectReason;
    private String statusDesc;
    private String catText;
    private String appIconUrl;

    private Integer anxPlGroupId;
    private Integer pmSiteId;
    private Integer imdSiteId;

    private String adsTxt;

    private Date createDate;

    public AppResponseDTO() {
    }

    public AppResponseDTO(App app, User user) {
        this(app);
        if(user == null)
            return;

        this.fullName = user.getFullName();
        this.email = user.getEmail();
        this.accManager = user.getAccManager();
        this.utmSource = user.getUtmSource();
        this.isTR = user.getIsTR();
        this.userCreateDate = user.getCreateDate();
    }

    public AppResponseDTO(App app) {
        if(app == null)
            return;

        this.id = app.getId();
        this.appName = app.getAppName();
        this.appUrl = app.getAppUrl();
        this.appKey = app.getAppKey();
        this.appType = app.getAppType();
        this.tagsText = app.getTagsText();
        this.appDesc = app.getAppDesc();
        this.userId = app.getUserId();

        this.catId = app.getCatId();
        this.active = app.getActive();
        this.archived = app.getArchived();
        this.platform = app.getPlatform();
        this.rekmobPercent = app.getRekmobPercent();
        this.status = app.getStatus();
        this.rejectReason = app.getRejectReason();
        this.statusDesc = app.getStatusDesc();
        this.catText = app.getCatText();
        this.appIconUrl = app.getAppIconUrl();
        this.anxPlGroupId = app.getAnxPlGroupId();

        this.pmSiteId = app.getPmSiteId();
        this.imdSiteId = app.getImdSiteId();
        this.adsTxt = app.getAdsTxt();
        this.createDate = app.getCreateDate();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public Integer getAppType() {
        return appType;
    }

    public void setAppType(Integer appType) {
        this.appType = appType;
    }

    public String getTagsText() {
        return tagsText;
    }

    public void setTagsText(String tagsText) {
        this.tagsText = tagsText;
    }

    public String getAppDesc() {
        return appDesc;
    }

    public void setAppDesc(String appDesc) {
        this.appDesc = appDesc;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccManager() {
        return accManager;
    }

    public void setAccManager(String accManager) {
        this.accManager = accManager;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public Integer getIsTR() {
        return isTR;
    }

    public void setIsTR(Integer isTR) {
        this.isTR = isTR;
    }

    public Date getUserCreateDate() {
        return userCreateDate;
    }

    public void setUserCreateDate(Date userCreateDate) {
        this.userCreateDate = userCreateDate;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getArchived() {
        return archived;
    }

    public void setArchived(int archived) {
        this.archived = archived;
    }

    public Integer getPlatform() {
        return platform;
    }

    public void setPlatform(Integer platform) {
        this.platform = platform;
    }

    public Double getRekmobPercent() {
        return rekmobPercent;
    }

    public void setRekmobPercent(Double rekmobPercent) {
        this.rekmobPercent = rekmobPercent;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Integer getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(Integer rejectReason) {
        this.rejectReason = rejectReason;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getCatText() {
        return catText;
    }

    public void setCatText(String catText) {
        this.catText = catText;
    }

    public String getAppIconUrl() {
        return appIconUrl;
    }

    public void setAppIconUrl(String appIconUrl) {
        this.appIconUrl = appIconUrl;
    }

    public Integer getAnxPlGroupId() {
        return anxPlGroupId;
    }

    public void setAnxPlGroupId(Integer anxPlGroupId) {
        this.anxPlGroupId = anxPlGroupId;
    }

    public Integer getPmSiteId() {
        return pmSiteId;
    }

    public void setPmSiteId(Integer pmSiteId) {
        this.pmSiteId = pmSiteId;
    }

    public Integer getImdSiteId() {
        return imdSiteId;
    }

    public void setImdSiteId(Integer imdSiteId) {
        this.imdSiteId = imdSiteId;
    }

    public String getAdsTxt() {
        return adsTxt;
    }

    public void setAdsTxt(String adsTxt) {
        this.adsTxt = adsTxt;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
