package com.reklamstore.rexmob.app;

import com.reklamstore.rexmob.app.response.AppResponseDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface AppRepository extends JpaRepository<App, Integer> {

    Optional<App> findOneById(Integer id);

    List<App> findByUserId(Integer userId);

    List<App> findByStatus(int status);

    @Query("SELECT a FROM App a WHERE a.status = :status AND a.createDate >= :date ORDER BY a.createDate DESC")
    List<App> findByStatus(@Param("status") int status, @Param("date") Date date);

    @Query("SELECT new com.reklamstore.rexmob.app.response.AppResponseDTO(a, (SELECT u" +
            " FROM User u WHERE u.id = a.userId))" +
            " FROM App a WHERE a.status = :status AND a.createDate >= :date ORDER BY a.createDate DESC")
    List<AppResponseDTO> findAppByStatusAndDate(@Param("status") Integer status, @Param("date") Date date);
}
