package com.reklamstore.rexmob.app;

import com.reklamstore.rexmob.app.request.AppUnitIdsRequestDTO;
import com.reklamstore.rexmob.app.response.AppResponseDTO;
import com.reklamstore.rexmob.app.response.AppUnitResponseDTO;
import com.reklamstore.rexmob.app.unit.AppUnit;
import com.reklamstore.rexmob.app.unit.AppUnitRepository;
import com.reklamstore.rexmob.base.BaseException;
import com.reklamstore.rexmob.base.BaseResponse;
import com.reklamstore.rexmob.email.EmailService;
import com.reklamstore.rexmob.exchange.Exchange;
import com.reklamstore.rexmob.exchange.ExchangeService;
import com.reklamstore.rexmob.user.User;
import com.reklamstore.rexmob.user.UserService;
import com.reklamstore.rexmob.util.GlobalAccManager;
import com.reklamstore.rexmob.util.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class AppService {

    private AppRepository appRepository;
    private AppUnitRepository appUnitRepository;
    private UserService userService;
    private EmailService emailService;
    private ExchangeService exchangeService;

    @Autowired
    public AppService(AppRepository appRepository,
                      AppUnitRepository appUnitRepository,
                      UserService userService,
                      EmailService emailService,
                      ExchangeService exchangeService) {
        this.appRepository = appRepository;
        this.appUnitRepository = appUnitRepository;
        this.userService = userService;
        this.emailService = emailService;
        this.exchangeService = exchangeService;
    }

    public App get(Integer id) {
        return appRepository.findOneById(id).orElse(null);
    }

    public List<AppResponseDTO> getAppByStatus(Status status) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -7);
        return appRepository.findAppByStatusAndDate(status.getStatus(), cal.getTime());
    }

    @Transactional
    public AppResponseDTO changeAppStatusById(Integer id, Status status, RejectReasonType rejectType) throws BaseException {
        App app = appRepository.findOneById(id).orElseThrow(() -> new BaseException("Site not found."));
        User user = userService.getOne(app.getUserId());
        final boolean onChange = !status.equals(app.getStatus()) || (rejectType != null && !rejectType.equals(app.getRejectReason()));
        app.setStatus(status.getStatus());
        if (rejectType != null)
            app.setRejectReason(rejectType.getType());
        else
            app.setRejectReason(null);
        appRepository.save(app);

        if (onChange)
            emailService.sendOnChangeAppStatus(user, app);
        return new AppResponseDTO(app);
    }

    @Transactional
    public AppResponseDTO updateApp(Integer id, AppUpdateRequest request) throws BaseException {
        App app = appRepository.findOneById(id).orElseThrow(() -> new BaseException("Site not found."));

        app.setAppType(request.getAppType());
        app.setCatId(request.getCatId());
        app.setAppUrl(request.getAppUrl());

        RejectReasonType rejectType = request.getRejectReasonType();
        final boolean onChangeStatus = !request.getStatus().equals(app.getStatus()) || (rejectType != null && !rejectType.equals(app.getRejectReason()));
        app.setStatus(request.getStatus().getStatus());
        if (rejectType != null)
            app.setRejectReason(rejectType.getType());
        else
            app.setRejectReason(null);
        appRepository.save(app);

        User user = userService.getOneOptional(app.getUserId()).orElseThrow(() -> new BaseException("User not found."));
        if ("NextGlobal".equals(request.getAccManager()))
            user.setAccManager(GlobalAccManager.getNext());
        else
            user.setAccManager(request.getAccManager());
        userService.save(user);

        if (onChangeStatus)
            emailService.sendOnChangeAppStatus(user, app);
        return new AppResponseDTO(app, user);
    }

    public List<AppUnitResponseDTO> getAppUnitByUser(Integer userId) {
        return appUnitRepository.findByUser(userId);
    }

    public List<AppUnitResponseDTO> getAppUnitByApp(Integer appId) {
        return appUnitRepository.findByAppId(appId);
    }

    @Transactional
    public BaseResponse banByUserId(Integer userId) throws BaseException {
        List<App> apps = appRepository.findByUserId(userId);
        List<AppUnit> updateAppUnits = new ArrayList<>();

        for (App app : apps)
            updateAppUnits.addAll(banUnitByApp(app));

        if (updateAppUnits.size() != 0)
            appUnitRepository.saveAll(updateAppUnits);
        return new BaseResponse();
    }
    @Transactional
    public BaseResponse reactivateByUserId(Integer userId) throws BaseException {
        List<App> apps = appRepository.findByUserId(userId);
        List<AppUnit> updateAppUnits = new ArrayList<>();

        for (App app : apps)
            updateAppUnits.addAll(reactivateUnitByApp(app));

        if (updateAppUnits.size() != 0)
            appUnitRepository.saveAll(updateAppUnits);
        return new BaseResponse();
    }


    @Transactional
    public BaseResponse ban(Integer appId) throws BaseException {
        Optional<App> appVO = appRepository.findOneById(appId);
        if (!appVO.isPresent())
            throw new BaseException(400, "Not found!");

        List<AppUnit> updateAppUnits = banUnitByApp(appVO.get());
        if (updateAppUnits.size() != 0)
            appUnitRepository.saveAll(updateAppUnits);
        return new BaseResponse();
    }

    @Transactional
    public BaseResponse banUnit(Integer appUnitId) throws BaseException {
        Optional<AppUnit> unitVO = appUnitRepository.findOneById(appUnitId);
        if (!unitVO.isPresent())
            throw new BaseException(400, "Not found!");
        AppUnit unit = unitVO.get();
        if (unit.getActive() == null || unit.getActive())
            appUnitRepository.save(banUnit(unitVO.get()));

        return new BaseResponse();
    }

    private List<AppUnit> banUnitByApp(App app) {
        List<AppUnit> updateAppUnits = new ArrayList<>();
        for (AppUnit appUnit : app.getAppUnitList()) {
            if (appUnit.getActive() != null && !appUnit.getActive())
                continue;
            updateAppUnits.add(banUnit(appUnit));
        }
        return updateAppUnits;
    }
    private List<AppUnit> reactivateUnitByApp(App app) {
        List<AppUnit> updateAppUnits = new ArrayList<>();
        for (AppUnit appUnit : app.getAppUnitList()) {
            if (appUnit.getActive() != null && appUnit.getActive())
                continue;
            updateAppUnits.add(reactivateUnit(appUnit));
        }
        return updateAppUnits;
    }

    private AppUnit banUnit(AppUnit unit) {
        unit.setRegionId("-" + unit.getRegionId());
        unit.setActive(false);
        return unit;
    }
    private AppUnit reactivateUnit(AppUnit unit) {
        unit.setRegionId(unit.getRegionId().replaceAll("-",""));
        unit.setActive(true);
        return unit;
    }

    public List<AppResponseDTO> getAppByUser(Integer userId) {
        List<App> apps = appRepository.findByUserId(userId);
        List<AppResponseDTO> responseDTOS = new ArrayList<>();
        for (App app : apps) {
            responseDTOS.add(new AppResponseDTO(app));
        }
        return responseDTOS;
    }

    public BaseResponse changeAppUnitPrices(Integer appUnitId,
                                            Double topCpm,
                                            Double fixCpm,
                                            Double rekmobPercent) throws BaseException {
        Optional<AppUnit> unitVO = appUnitRepository.findOneById(appUnitId);
        if (!unitVO.isPresent())
            throw new BaseException(400, "Appunit Not found!");

        AppUnit unit = unitVO.get();
        unit.setTopCpm(topCpm);
        unit.setRekmobPercent(rekmobPercent);
        App app = unit.getApp();
        User user = userService.getOne(app.getUserId());
        if (user.getCurrencyCode() != null && user.getCurrencyCode().equals("TRY")) {
            unit.setFixedCpm(fixCpm);
        } else {
            Exchange exchange = exchangeService.findLastExchangeWithoutCache();
            if (fixCpm != null) {
                unit.setFixedCpm(exchange.toTL(fixCpm));
            } else {
                unit.setFixedCpm(fixCpm);
            }
            unit.setFixedCpmUsd(fixCpm);
        }
        appUnitRepository.save(unit);
        return new BaseResponse();
    }

    public BaseResponse changeAppUnitNetworkIds(AppUnitIdsRequestDTO request) throws BaseException {
        Optional<AppUnit> unitVO = appUnitRepository.findOneById(request.getId());
        if (!unitVO.isPresent())
            throw new BaseException(400, "Appunit Not found!");

        AppUnit unit = unitVO.get();
        unit.setAnxPlacementId(request.getAnxPlacementId());
        unit.setAdfPlacementId(request.getAdfPlacementId());
        unit.setBrtPlacementId(request.getBrtPlacementId());
        unit.setSrPlacementId(request.getSrPlacementId());
        unit.setNativePlacementId(request.getNativePlacementId());
        unit.setPmPlacementId(request.getPmPlacementId());
        unit.setSsPlacementId(request.getSsPlacementId());
        unit.setRubiPlacementId(request.getRubiPlacementId());
        unit.setImdiPlacementId(request.getImdiPlacementId());
        unit.setPropellerId(request.getPropellerId());
        unit.setPropeller2Id(request.getPropeller2Id());
        unit.setAdcashId(request.getAdcashId());
        unit.setAdsterraId(request.getAdsterraId());
        unit.setRsdspId(request.getRsdspId());
        unit.setAdmavenId(request.getAdmavenId());
        appUnitRepository.save(unit);
        return new BaseResponse();
    }
}
