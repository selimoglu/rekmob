package com.reklamstore.rexmob.app.request;

import com.reklamstore.rexmob.app.App;
import com.reklamstore.rexmob.app.unit.AppUnit;

import java.util.Date;

public class AppUnitIdsRequestDTO {

    private Integer id;

    private Integer anxPlacementId;
    private String adfPlacementId;
    private Integer brtPlacementId;
    private String srPlacementId;
    private String nativePlacementId;
    private String pmPlacementId;
    private String ssPlacementId;
    private String rubiPlacementId;
    private String imdiPlacementId;
    private String propellerId;
    private String propeller2Id;
    private String adcashId;
    private String adsterraId;
    private String rsdspId;
    private String admavenId;
    private String pmTagId;

    public AppUnitIdsRequestDTO() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAnxPlacementId() {
        return anxPlacementId;
    }

    public void setAnxPlacementId(Integer anxPlacementId) {
        this.anxPlacementId = anxPlacementId;
    }

    public String getAdfPlacementId() {
        return adfPlacementId;
    }

    public void setAdfPlacementId(String adfPlacementId) {
        this.adfPlacementId = adfPlacementId;
    }

    public Integer getBrtPlacementId() {
        return brtPlacementId;
    }

    public void setBrtPlacementId(Integer brtPlacementId) {
        this.brtPlacementId = brtPlacementId;
    }

    public String getSrPlacementId() {
        return srPlacementId;
    }

    public void setSrPlacementId(String srPlacementId) {
        this.srPlacementId = srPlacementId;
    }

    public String getNativePlacementId() {
        return nativePlacementId;
    }

    public void setNativePlacementId(String nativePlacementId) {
        this.nativePlacementId = nativePlacementId;
    }

    public String getPmPlacementId() {
        return pmPlacementId;
    }

    public void setPmPlacementId(String pmPlacementId) {
        this.pmPlacementId = pmPlacementId;
    }

    public String getSsPlacementId() {
        return ssPlacementId;
    }

    public void setSsPlacementId(String ssPlacementId) {
        this.ssPlacementId = ssPlacementId;
    }

    public String getRubiPlacementId() {
        return rubiPlacementId;
    }

    public void setRubiPlacementId(String rubiPlacementId) {
        this.rubiPlacementId = rubiPlacementId;
    }

    public String getImdiPlacementId() {
        return imdiPlacementId;
    }

    public void setImdiPlacementId(String imdiPlacementId) {
        this.imdiPlacementId = imdiPlacementId;
    }

    public String getPropellerId() {
        return propellerId;
    }

    public void setPropellerId(String propellerId) {
        this.propellerId = propellerId;
    }

    public String getPropeller2Id() {
        return propeller2Id;
    }

    public void setPropeller2Id(String propeller2Id) {
        this.propeller2Id = propeller2Id;
    }

    public String getAdcashId() {
        return adcashId;
    }

    public void setAdcashId(String adcashId) {
        this.adcashId = adcashId;
    }

    public String getAdsterraId() {
        return adsterraId;
    }

    public void setAdsterraId(String adsterraId) {
        this.adsterraId = adsterraId;
    }

    public String getRsdspId() {
        return rsdspId;
    }

    public void setRsdspId(String rsdspId) {
        this.rsdspId = rsdspId;
    }

    public String getAdmavenId() {
        return admavenId;
    }

    public void setAdmavenId(String admavenId) {
        this.admavenId = admavenId;
    }

    public String getPmTagId() {
        return pmTagId;
    }

    public void setPmTagId(String pmTagId) {
        this.pmTagId = pmTagId;
    }

}
