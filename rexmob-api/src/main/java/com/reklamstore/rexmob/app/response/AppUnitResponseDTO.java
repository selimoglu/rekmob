package com.reklamstore.rexmob.app.response;

import com.reklamstore.rexmob.app.App;
import com.reklamstore.rexmob.app.unit.AppUnit;

import java.util.Date;

public class AppUnitResponseDTO {

    private Integer id;

    private String unitName;
    private String unitKey;

    private Integer appId;
    private String appName;
    private String appUrl;
    private Integer appType;
    private int appStatus;

    private int houseAdRate;
    private Boolean active;

    private int adSource;

    private Date createDate;
    private Date updateDate;

    private int creativeFormat;
    //private int adxAccount;

    //private Double adxPercent;
    private Integer anxPlacementId;

    private String adfPlacementId;

    private Integer brtPlacementId;

    private Double fixedCpm;
    private Double fixedCpmUsd;
    private Double topCpm;
    private Double rekmobPercent;

    private Integer anxPaymentRuleId;

    private String srPlacementId;
    private String nativePlacementId;
    private String pmPlacementId;
    private String ssPlacementId;
    private String rubiPlacementId;
    private String imdiPlacementId;

    private Integer fraudRatio;
    private Integer unitSize;

    private String regionId;

    private String propellerId;
    private String propeller2Id;
    private String adcashId;
    private String adsterraId;
    private String rsdspId;
    private String admavenId;

    private Integer nativeType;

    private String tagCode;

    private Integer userId;

    private String pmTagId;

    private Double fixedCpmDollar;

    private String subId;

    private String videoEnvironment;



    public AppUnitResponseDTO() {
    }

    public AppUnitResponseDTO(AppUnit appUnit,
                              String appName, String appUrl){
        this(appUnit);
        this.appName = appName;
        this.appUrl=appUrl;
    }
    public AppUnitResponseDTO(AppUnit appUnit, App app){
        this(appUnit);
        this.appName = app.getAppName();
        this.appUrl=app.getAppUrl();
        this.appStatus=app.getStatus();
        this.appType=app.getAppType();
    }

    public AppUnitResponseDTO(AppUnit appUnit) {
        this.id = appUnit.getId();
        this.unitName = appUnit.getUnitName();
        this.unitKey = appUnit.getUnitKey();
        this.appId = appUnit.getAppId();
        this.houseAdRate = appUnit.getHouseAdRate();
        this.active = appUnit.getActive();
        this.adSource = appUnit.getAdSource();
        this.createDate = appUnit.getCreateDate();
        this.updateDate = appUnit.getUpdateDate();
        this.creativeFormat = appUnit.getCreativeFormat();
        this.anxPlacementId = appUnit.getAnxPlacementId();
        this.adfPlacementId = appUnit.getAdfPlacementId();
        this.brtPlacementId = appUnit.getBrtPlacementId();
        this.fixedCpm = appUnit.getFixedCpm();
        this.fixedCpmUsd = appUnit.getFixedCpmUsd();
        this.rekmobPercent = appUnit.getRekmobPercent();
        this.anxPaymentRuleId = appUnit.getAnxPaymentRuleId();
        this.srPlacementId = appUnit.getSrPlacementId();
        this.nativePlacementId = appUnit.getNativePlacementId();
        this.pmPlacementId = appUnit.getPmPlacementId();
        this.ssPlacementId = appUnit.getSsPlacementId();
        this.rubiPlacementId = appUnit.getRubiPlacementId();
        this.imdiPlacementId = appUnit.getImdiPlacementId();
        this.fraudRatio = appUnit.getFraudRatio();
        this.unitSize = appUnit.getUnitSize();
        this.regionId = appUnit.getRegionId();
        this.propellerId = appUnit.getPropellerId();
        this.propeller2Id = appUnit.getPropeller2Id();
        this.adcashId = appUnit.getAdcashId();
        this.adsterraId = appUnit.getAdsterraId();
        this.rsdspId = appUnit.getRsdspId();
        this.admavenId = appUnit.getAdmavenId();
        this.nativeType = appUnit.getNativeType();
        this.tagCode = appUnit.getTagCode();
        this.userId = appUnit.getUserId();
        this.pmTagId = appUnit.getPmTagId();
        this.fixedCpmDollar = appUnit.getFixedCpmDollar();
        this.subId = appUnit.getSubId();
        this.videoEnvironment = appUnit.getVideoEnvironment();
        this.topCpm=appUnit.getTopCpm();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitKey() {
        return unitKey;
    }

    public void setUnitKey(String unitKey) {
        this.unitKey = unitKey;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public int getHouseAdRate() {
        return houseAdRate;
    }

    public void setHouseAdRate(int houseAdRate) {
        this.houseAdRate = houseAdRate;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public int getAdSource() {
        return adSource;
    }

    public void setAdSource(int adSource) {
        this.adSource = adSource;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public int getCreativeFormat() {
        return creativeFormat;
    }

    public void setCreativeFormat(int creativeFormat) {
        this.creativeFormat = creativeFormat;
    }

    public Integer getAnxPlacementId() {
        return anxPlacementId;
    }

    public void setAnxPlacementId(Integer anxPlacementId) {
        this.anxPlacementId = anxPlacementId;
    }

    public String getAdfPlacementId() {
        return adfPlacementId;
    }

    public void setAdfPlacementId(String adfPlacementId) {
        this.adfPlacementId = adfPlacementId;
    }

    public Integer getBrtPlacementId() {
        return brtPlacementId;
    }

    public void setBrtPlacementId(Integer brtPlacementId) {
        this.brtPlacementId = brtPlacementId;
    }

    public Double getFixedCpm() {
        return fixedCpm;
    }

    public void setFixedCpm(Double fixedCpm) {
        this.fixedCpm = fixedCpm;
    }

    public Double getRekmobPercent() {
        return rekmobPercent;
    }

    public void setRekmobPercent(Double rekmobPercent) {
        this.rekmobPercent = rekmobPercent;
    }

    public Integer getAnxPaymentRuleId() {
        return anxPaymentRuleId;
    }

    public void setAnxPaymentRuleId(Integer anxPaymentRuleId) {
        this.anxPaymentRuleId = anxPaymentRuleId;
    }

    public String getSrPlacementId() {
        return srPlacementId;
    }

    public void setSrPlacementId(String srPlacementId) {
        this.srPlacementId = srPlacementId;
    }

    public String getNativePlacementId() {
        return nativePlacementId;
    }

    public void setNativePlacementId(String nativePlacementId) {
        this.nativePlacementId = nativePlacementId;
    }

    public String getPmPlacementId() {
        return pmPlacementId;
    }

    public void setPmPlacementId(String pmPlacementId) {
        this.pmPlacementId = pmPlacementId;
    }

    public String getSsPlacementId() {
        return ssPlacementId;
    }

    public void setSsPlacementId(String ssPlacementId) {
        this.ssPlacementId = ssPlacementId;
    }

    public String getRubiPlacementId() {
        return rubiPlacementId;
    }

    public void setRubiPlacementId(String rubiPlacementId) {
        this.rubiPlacementId = rubiPlacementId;
    }

    public String getImdiPlacementId() {
        return imdiPlacementId;
    }

    public void setImdiPlacementId(String imdiPlacementId) {
        this.imdiPlacementId = imdiPlacementId;
    }

    public Integer getFraudRatio() {
        return fraudRatio;
    }

    public void setFraudRatio(Integer fraudRatio) {
        this.fraudRatio = fraudRatio;
    }

    public Integer getUnitSize() {
        return unitSize;
    }

    public void setUnitSize(Integer unitSize) {
        this.unitSize = unitSize;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getPropellerId() {
        return propellerId;
    }

    public void setPropellerId(String propellerId) {
        this.propellerId = propellerId;
    }

    public String getPropeller2Id() {
        return propeller2Id;
    }

    public void setPropeller2Id(String propeller2Id) {
        this.propeller2Id = propeller2Id;
    }

    public String getAdcashId() {
        return adcashId;
    }

    public void setAdcashId(String adcashId) {
        this.adcashId = adcashId;
    }

    public String getAdsterraId() {
        return adsterraId;
    }

    public void setAdsterraId(String adsterraId) {
        this.adsterraId = adsterraId;
    }

    public String getRsdspId() {
        return rsdspId;
    }

    public void setRsdspId(String rsdspId) {
        this.rsdspId = rsdspId;
    }

    public String getAdmavenId() {
        return admavenId;
    }

    public void setAdmavenId(String admavenId) {
        this.admavenId = admavenId;
    }

    public Integer getNativeType() {
        return nativeType;
    }

    public void setNativeType(Integer nativeType) {
        this.nativeType = nativeType;
    }

    public String getTagCode() {
        return tagCode;
    }

    public void setTagCode(String tagCode) {
        this.tagCode = tagCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getPmTagId() {
        return pmTagId;
    }

    public void setPmTagId(String pmTagId) {
        this.pmTagId = pmTagId;
    }

    public Double getFixedCpmDollar() {
        return fixedCpmDollar;
    }

    public void setFixedCpmDollar(Double fixedCpmDollar) {
        this.fixedCpmDollar = fixedCpmDollar;
    }

    public String getSubId() {
        return subId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

    public String getVideoEnvironment() {
        return videoEnvironment;
    }

    public void setVideoEnvironment(String videoEnvironment) {
        this.videoEnvironment = videoEnvironment;
    }
    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public Double getTopCpm() {
        return topCpm;
    }

    public void setTopCpm(Double topCpm) {
        this.topCpm = topCpm;
    }

    public int getAppStatus() {
        return appStatus;
    }

    public void setAppStatus(int appStatus) {
        this.appStatus = appStatus;
    }

    public Integer getAppType() {
        return appType;
    }

    public void setAppType(Integer appType) {
        this.appType = appType;
    }

    public Double getFixedCpmUsd() {
        return fixedCpmUsd;
    }

    public void setFixedCpmUsd(Double fixedCpmUsd) {
        this.fixedCpmUsd = fixedCpmUsd;
    }
}
