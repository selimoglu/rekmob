package com.reklamstore.rexmob.app.response;

import com.reklamstore.rexmob.base.BaseException;
import com.reklamstore.rexmob.base.BaseResponse;

public class AppResponse extends BaseResponse {

    private AppResponseDTO response;

    public AppResponse() {
    }

    public AppResponse(BaseException exception) {
        super(exception);
    }

    public AppResponse(AppResponseDTO response) {
        super(false);
        this.response = response;
    }

    public AppResponseDTO getResponse() {
        return response;
    }

    public void setResponse(AppResponseDTO response) {
        this.response = response;
    }
}
