package com.reklamstore.rexmob.app;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.reklamstore.rexmob.app.unit.AppUnit;
import com.reklamstore.rexmob.tag.Tag;
import com.reklamstore.rexmob.user.User;
import com.reklamstore.rexmob.util.ApiJsonFilter;
import com.reklamstore.rexmob.util.TotalStatUtil;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

/**
 * @author fti
 *
 */

@Entity
@Table(name = "app")
@DynamicUpdate
public class App{

    @JsonView(ApiJsonFilter.PublicView.class)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "APP_ID")
    private Integer id;

    @JsonView(ApiJsonFilter.PublicView.class)
    @NotEmpty
    @Column(name = "APP_NAME")
    private String appName;

    @JsonView(ApiJsonFilter.PublicView.class)
    @NotEmpty
    @Column(name = "APP_URL")
    private String appUrl;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name = "APP_KEY", updatable = false)
    private String appKey;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name = "TAGS_TEXT")
    private String tagsText;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name = "APP_DESC")
    private String appDesc;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name = "CAT_ID")
    private Integer catId;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name = "ACTIVE")
    private int active;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name = "ARCHIVED", nullable = true)
    private int archived;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name = "CREATE_DATE", updatable = false)
    private Date createDate;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name = "USER_ID")
    private Integer userId;

    @JsonView(ApiJsonFilter.PublicView.class)
    @NotNull
    @Column(name = "PLATFORM")
    private Integer platform;

    /**
     * Is other networks available for this app
     */
    @JsonIgnore
    @Column(name = "MARIMEDIA")
    private int marimedia;


    @JsonView(ApiJsonFilter.AdminView.class)
    // @NotNull
    // @Size(min = 0, max = 100)
    @Column(name="REKMOB_PERCENT")
    private Double rekmobPercent;


    /**
     * Status Enum PENDING(0), APPROVED(1), REJECTED(2);
     */

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name = "STATUS")
    private int status;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name = "REJECT_REASON")
    private Integer rejectReason;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name = "STATUS_DESC")
    private String statusDesc;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name="CAT_TEXT")
    private String catText;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name="APP_ICON_URL")
    private String appIconUrl;

    @JsonIgnore
    @Column(name="ON_STORE")
    private int onStore;

    @JsonView(ApiJsonFilter.AdminView.class)
    @Column(name="anx_plgroup_id")
    private Integer anxPlGroupId;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "app")
    private List<AppUnit> appUnitList;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "app_tag", joinColumns = { @JoinColumn(name = "app_id", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "tag_id", nullable = false, updatable = false) })
    private Set<Tag> tags;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Transient
    private User user;

    @JsonIgnore
    @Transient
    private AppCategory category;

    @Transient
    private TotalStatUtil totalStat;

    @JsonIgnore
    @Column(name="CRT_ID")
    private Integer crtId;

    @JsonIgnore
    @Column(name="RTBH_ID")
    private Integer rtbhId;

    @JsonIgnore
    @Column(name="BS_ID")
    private Integer bsId;

    @JsonIgnore
    @Column(name="ALLOW_REKMOB")
    private Integer allowRekmob;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name="APP_TYPE")
    private Integer appType;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name="PM_SITE_ID")
    private Integer pmSiteId;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name="IMD_SITE_ID")
    private Integer imdSiteId;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name="ADS_TXT")
    private String adsTxt;

    @JsonIgnore
    @Column(name="IN_IFRAME")
    private Integer inIframe;

    // GETTERS SETTERS

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public int getActive() {
        return active;
    }


    public void setActive(int active) {
        this.active = active;
    }

    @JsonProperty("active")
    public void setActive(boolean isActive){
        this.active=isActive?1:0;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public AppCategory getCategory() {
        return category;
    }

    public void setCategory(AppCategory category) {
        this.category = category;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public String getAppDesc() {
        return appDesc;
    }

    public void setAppDesc(String appDesc) {
        this.appDesc = appDesc;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId
     *            the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPlatform() {
        return platform;
    }

    public void setPlatform(Integer platform) {
        this.platform = platform;
    }

    public Double getRekmobPercent() {
        return rekmobPercent;
    }

    public void setRekmobPercent(Double rekmobPercent) {
        this.rekmobPercent = rekmobPercent;
    }

    /**
     * @return {@link #name}
     */
    public List<AppUnit> getAppUnitList() {
        return appUnitList;
    }

    /**
     * @param appUnitList
     *            the appUnitList to set
     */
    public void setAppUnitList(List<AppUnit> appUnitList) {
        this.appUnitList = appUnitList;
    }



    /**
     * @return {@link #name}
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * @param tags
     *            the tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     * @return {@link #name}
     */
    public String getTagsText() {
        return tagsText;
    }

    /**
     * @param tagsText
     *            the tagsText to set
     */
    public void setTagsText(String tagsText) {
        this.tagsText = tagsText;
    }

    /**
     * @return {@link #name}
     */
    public boolean isArchived() {
        return archived > 0 ? true : false;
    }

    /**
     * @return {@link #name}
     */
    public int getArchived() {
        return archived;
    }

    /**
     * @param archived
     *            the archived to set
     */
    public void setArchived(int archived) {
        this.archived = archived;
    }

    /**
     * @param archived
     *            the archived to set
     */
    @JsonProperty("archived")
    public void setArchived(boolean isArchived) {
        this.archived = isArchived ? 1 : 0;
    }

    /**
     * @return {@link #name}
     */
    public int getMarimedia() {
        return marimedia;
    }

    /**
     * @param mediation the mediation to set
     */
    public void setMarimedia(int marimedia) {
        this.marimedia = marimedia;
    }

    /**
     * @param mediation the mediation to set
     */
    public void setMarimedia(boolean isMarimedia) {
        this.marimedia = isMarimedia ? 1 : 0;
    }

    /**
     * @return {@link #marimedia}
     */
    public boolean isMarimedia() {
        return marimedia > 0 ? true : false;
    }

    /**
     * @return {@link #name}
     */
    public TotalStatUtil getTotalStat() {
        return totalStat;
    }

    /**
     * @param totalStat the totalStat to set
     */
    public void setTotalStat(TotalStatUtil totalStat) {
        this.totalStat = totalStat;
    }

    /**
     * @return {@link #name}
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    public Integer getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(Integer rejectReason) {
        this.rejectReason = rejectReason;
    }

    /**
     * @return {@link #name}
     */
    public String getStatusDesc() {
        return statusDesc;
    }

    /**
     * @param statusDesc the statusDesc to set
     */
    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    /**
     * @return {@link #name}
     */
    public String getCatText() {
        return catText;
    }

    /**
     * @param catText the catText to set
     */
    public void setCatText(String catText) {
        this.catText = catText;
    }

    /**
     * @return {@link #name}
     */
    public String getAppIconUrl() {
        return appIconUrl;
    }

    /**
     * @param appIconUrl the appIconUrl to set
     */
    public void setAppIconUrl(String appIconUrl) {
        this.appIconUrl = appIconUrl;
    }


    /**
     * @param onStore the onStore to set
     */

    public void setOnStore(boolean onStore) {
        this.onStore = onStore? 1: 0;
    }

    /**
     * @return {@link #name}
     */
    public int getOnStore() {
        return onStore;
    }

    /**
     * @param onStore the onStore to set
     */
    public void setOnStore(int onStore) {
        this.onStore = onStore;
    }

    public Integer getAnxPlGroupId() {
        return anxPlGroupId;
    }

    public void setAnxPlGroupId(Integer anxPlGroupId) {
        this.anxPlGroupId = anxPlGroupId;
    }

    public Integer getCrtId() {
        return crtId;
    }

    public void setCrtId(Integer crtId) {
        this.crtId = crtId;
    }

    public Integer getAppType() {
        return appType;
    }

    public void setAppType(Integer appType) {
        this.appType = appType;
    }

    public Integer getPmSiteId() {
        return pmSiteId;
    }

    public void setPmSiteId(Integer pmSiteId) {
        this.pmSiteId = pmSiteId;
    }

    public String getAdsTxt() {
        return adsTxt;
    }
    public void setAdsTxt(String adsTxt) {
        this.adsTxt = adsTxt;
    }

    public Integer getBsId() {
        return bsId;
    }

    public void setBsId(Integer bsId) {
        this.bsId = bsId;
    }

    public Integer getImdSiteId() {
        return imdSiteId;
    }

    public void setImdSiteId(Integer imdSiteId) {
        this.imdSiteId = imdSiteId;
    }

    public Integer getRtbhId() {
        return rtbhId;
    }

    public void setRtbhId(Integer rtbhId) {
        this.rtbhId = rtbhId;
    }

    public Integer getInIframe() {
        return inIframe;
    }

    public void setInIframe(Integer inIframe) {
        this.inIframe = inIframe;
    }

    public Integer getAllowRekmob() {
        return allowRekmob;
    }

    public void setAllowRekmob(Integer allowRekmob) {
        this.allowRekmob = allowRekmob;
    }

}