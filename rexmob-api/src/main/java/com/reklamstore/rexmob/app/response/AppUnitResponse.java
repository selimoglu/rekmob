package com.reklamstore.rexmob.app.response;

import com.reklamstore.rexmob.base.BaseResponse;

import java.util.List;

public class AppUnitResponse extends BaseResponse {

    private List<AppUnitResponseDTO> response;

    public AppUnitResponse() {
    }

    public AppUnitResponse(List<AppUnitResponseDTO> response) {
        super(true);
        this.response = response;
    }

    public List<AppUnitResponseDTO> getResponse() {
        return response;
    }

    public void setResponse(List<AppUnitResponseDTO> response) {
        this.response = response;
    }
}
