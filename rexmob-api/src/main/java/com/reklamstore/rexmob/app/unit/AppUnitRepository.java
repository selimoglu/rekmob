package com.reklamstore.rexmob.app.unit;

import com.reklamstore.rexmob.app.response.AppUnitResponseDTO;
import com.reklamstore.rexmob.report.response.PopResponseDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AppUnitRepository extends JpaRepository<AppUnit, Integer> {

    Optional<AppUnit> findOneById(Integer id);
    List<AppUnitResponseDTO> findByAppId(Integer appId);

    @Query("SELECT new com.reklamstore.rexmob.app.response.AppUnitResponseDTO(au,a) " +
            "FROM AppUnit au, User u, App a WHERE a.userId = u.id AND au.appId = a.id " +
            "AND u.id =:userId ORDER BY au.createDate DESC")
    List<AppUnitResponseDTO> findByUser(@Param("userId") Integer userId);

    @Query("SELECT new com.reklamstore.rexmob.report.response.PopResponseDTO(au.id, a.userId, u.fullName) FROM AppUnit au, User u, App a WHERE  au.appId = a.id  AND a.userId = u.id AND au.id IN (:appUnitIds)")
    List<PopResponseDTO> findAppUnitUser(@Param("appUnitIds") List<Integer> appUnitIds);



}
