package com.reklamstore.rexmob.user.response;

import com.reklamstore.rexmob.base.BaseException;
import com.reklamstore.rexmob.base.BaseResponse;

public class UserResponse extends BaseResponse {

    private UserResponseDTO response;

    public UserResponse() {
    }

    public UserResponse(BaseException exception) {
        super(exception);
    }

    public UserResponse(UserResponseDTO response) {
        super(false);
        this.response = response;
    }

    public UserResponseDTO getResponse() {
        return response;
    }

    public void setResponse(UserResponseDTO response) {
        this.response = response;
    }
}
