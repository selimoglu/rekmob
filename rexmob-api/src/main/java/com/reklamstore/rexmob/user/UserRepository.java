package com.reklamstore.rexmob.user;

import com.reklamstore.rexmob.user.response.UserResponseDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findOneById(Integer id);
    Optional<User> findOneByApiKey(String apiKey);

    Optional<User> findByEmail(String email);

    @Query("SELECT new com.reklamstore.rexmob.user.SimpleUser(u.id, u.fullName) FROM User u WHERE u.id = :id")
    SimpleUser findSimple(@Param("id") Integer id);

    @Query("SELECT new com.reklamstore.rexmob.user.SimpleUser(u.id, u.fullName) FROM User u WHERE u.id IN :ids")
    List<SimpleUser> findSimple(@Param("ids") List<Integer> ids);

    List<User> findByIdIn(List<Integer> ids);
    List<UserResponseDTO> findByCreateDateGreaterThanOrderByIdDesc(Date createDate);
    List<User> findByFullNameContains(String fullName);

}
