/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

package com.reklamstore.rexmob.user;

import com.reklamstore.rexmob.authentication.SignUpDTO;
import com.reklamstore.rexmob.authentication.exception.PasswordsDontMatchException;
import com.reklamstore.rexmob.authentication.exception.UserNotFoundHttpException;
import com.reklamstore.rexmob.base.BaseException;
import com.reklamstore.rexmob.base.BaseResponse;
import com.reklamstore.rexmob.user.exception.UserAlreadyExistsException;
import com.reklamstore.rexmob.user.exception.UserNotFoundException;
import com.reklamstore.rexmob.user.request.UserUpdateRequest;
import com.reklamstore.rexmob.user.response.UserResponse;
import com.reklamstore.rexmob.user.response.UserResponseDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class UserService {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private ModelMapper modelMapper;

    @Autowired
    public UserService(UserRepository userRepository,
                       PasswordEncoder passwordEncoder,
                       ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.modelMapper = modelMapper;
    }

    public User getByApiKey(String apiKey) {
        if(StringUtils.isEmpty(apiKey))
            return null;

        return userRepository.findOneByApiKey(apiKey).orElse(null);
    }

    public User getByEmail(String email) throws UserNotFoundException {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException("User with email: " + email + " not found"));
    }

    public List<User> getByNameContains(String name){
        return userRepository.findByFullNameContains(name);
    }
    public List<UserResponseDTO> getLastUsers(){
        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(today);
        cal.add(Calendar.DAY_OF_MONTH, -30);
        return userRepository.findByCreateDateGreaterThanOrderByIdDesc(cal.getTime());
    }

    public void isPresentOrThrow(Integer id) throws BaseException {
        if(!isPresent(id))
            throw new BaseException(400, "User not found");
    }

    public Boolean isPresent(Integer id){
        return getOneOptional(id).isPresent();
    }

    public User getOne(Integer id){
        return getOneOptional(id).orElse(null);
    }

    public User getOneOrThrow(Integer id) throws BaseException {
        return getOneOptional(id).orElseThrow(() -> new BaseException("User not found."));
    }

    public Optional<User> getOneOptional(Integer id){
        return userRepository.findOneById(id);
    }

    @Transactional
    public User register(SignUpDTO signUpDTO) throws UserAlreadyExistsException {
        if (!signUpDTO.getPassword().equals(signUpDTO.getConfirmPassword())) {
            throw new PasswordsDontMatchException();
        }

        String email = signUpDTO.getEmail();

        if (userRepository.findByEmail(email).isPresent()) {
            throw new UserAlreadyExistsException(email);
        }

        User user = signUpUser(signUpDTO);

        return userRepository.save(user);
    }

    @Transactional
    public void changePassword(ChangePasswordRequest changePasswordRequest) {
        User user = changePasswordRequest.getUser();

        String encodedPassword = encodePassword(changePasswordRequest.getPassword());
        user.setPassword(encodedPassword);

        userRepository.save(user);
    }

    public User ban(Integer id) throws BaseException {
        User user = getOneOrThrow(id);
        if(!user.getBan()){
            user.setBan(true);
            save(user);
        }
        return user;
    }
    public User reactivateUser(Integer id) throws BaseException {
        User user = getOneOrThrow(id);
        if(user.getBan()){
            user.setBan(false);
            save(user);
        }
        return user;
    }

    public List<SimpleUser> getSimple(List<Integer> ids){
        return userRepository.findSimple(ids);
    }

    public SimpleUser getSimple(Integer id){
        return userRepository.findSimple(id);
    }

    public UserDTO getDTOByIdOrThrow(Integer id) {
        Optional<User> userOptional = userRepository.findById(id);

        if (!userOptional.isPresent())
            throw new UserNotFoundHttpException("User with id: " + id + " not found", HttpStatus.NOT_FOUND);

        return modelMapper.map(userOptional.get(), UserDTO.class);
    }

    public UserResponseDTO get(Integer userId) throws BaseException {
        User user = getOne(userId);
        if(user == null)
            throw new BaseException("User not found!");

        return new UserResponseDTO(user);
    }

    public List<User> get(List<Integer> ids) {
        return userRepository.findByIdIn(ids);
    }

    @Transactional
    public UserDTO updateUserById(Integer userId, UserDTO userDTO) {
        return updateUser(userId, userDTO);
    }

    @Transactional
    public UserResponseDTO update(Integer userId, UserUpdateRequest updateDTO) throws BaseException {
        User user = getOne(userId);
        if(user == null)
            throw new BaseException("User not found!");

        user.setAccManager(updateDTO.getAccountManager());
        user.setRekmobPercent(updateDTO.getRekmobPercent());
        return new UserResponseDTO(userRepository.save(user));
    }

    @Transactional
    public User save(User user) throws BaseException {
        return userRepository.save(user);
    }

    @Transactional
    public boolean deleteUser(Integer id) {
        try {
            userRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            throw new UserNotFoundHttpException("User with id: " + id + " not found", HttpStatus.NOT_FOUND);
        }
    }

    public UserDTO getCurrentUser() {
        User user = UserContextHolder.getUser();
        return modelMapper.map(user, UserDTO.class);
    }

    public UserDTO updateCurrentUser(UserDTO userDTO) {
        User user = UserContextHolder.getUser();
        Integer id = user.getId();

        return updateUser(id, userDTO);
    }

    private UserDTO updateUser(Integer id, UserDTO userDTO) {
        Optional<User> userOptional = userRepository.findById(id);

        if (!userOptional.isPresent()) {
            throw new UserNotFoundHttpException("User with id: " + id + " not found", HttpStatus.NOT_FOUND);
        }

        User existingUser = userOptional.get();

        User updatedUser = modelMapper.map(userDTO, User.class);
        updatedUser.setId(id);
        updatedUser.setPassword(existingUser.getPassword());
        // Current version doesn't update roles
        updatedUser.setRole(existingUser.getRole());

        userRepository.save(updatedUser);

        return modelMapper.map(updatedUser, UserDTO.class);
    }

    private User signUpUser(SignUpDTO signUpDTO) {
        User user = new User();
        user.setEmail(signUpDTO.getEmail());
        user.setFullName(signUpDTO.getFullName());

        String encodedPassword = encodePassword(signUpDTO.getPassword());
        user.setPassword(encodedPassword);
        user.setRole(2);

        return user;
    }

    private String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }

}
