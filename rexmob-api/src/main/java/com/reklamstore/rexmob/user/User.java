package com.reklamstore.rexmob.user;

import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.reklamstore.rexmob.ad.Ad;
import com.reklamstore.rexmob.address.Address;
import com.reklamstore.rexmob.app.App;
import com.reklamstore.rexmob.util.ApiJsonFilter;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

/**
 * @author fti
 *
 */
@Entity
@Table(name = "user", uniqueConstraints = { @UniqueConstraint(columnNames = "EMAIL") })
public class User {

    public static int ROLE_ADMIN = 1;
    public static int ROLE_USER = 2;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID")
    private Integer id;

    @JsonView(ApiJsonFilter.PublicView.class)
    @NotEmpty
    @Size(min = 1, max = 250)
    @Column(name = "FULL_NAME")
    private String fullName;

    @JsonView(ApiJsonFilter.PublicView.class)
    @NotEmpty
    @Email
    @Size(min = 3, max = 100)
    @Column(name = "EMAIL")
    private String email;

    @JsonIgnore
    @NotEmpty
    @Size(min = 6, max = 100)
    @Column(name = "PASSWORD")
    private String password;

    @JsonIgnore
    @Transient
    private String rePassword;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Size(min = 1, max = 25)
    @Column(name = "PHONE")
    private String phone;

    /** AccType.java INDIVIDUAL(0),CORPORATE(1); */

    @JsonView(ApiJsonFilter.PublicView.class)
    @NotNull
    @Column(name = "ACC_TYPE")
    private Integer accType;

    /** UserType.java PUBLISHER(0), ADVERTISER(1), BOTH(2); */
    @JsonView(ApiJsonFilter.PublicView.class)
    @NotNull
    @Column(name = "USER_TYPE")
    private Integer userType;

    @JsonIgnore
    @Column(name = "TC_NO")
    private String tcNo;

    @JsonIgnore
    @Column(name = "TAX_NO")
    private String taxNo;

    @JsonIgnore
    @Column(name = "TAX_OFFICE")
    private String taxOffice;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name = "ACTIVE", nullable = true)
    private Boolean active;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name = "BAN", nullable = true)
    private Boolean ban = false;

    /** roles on MyUserDetailsService 1:ROLE_ADMIN 2:ROLE_USER */
    @JsonView(ApiJsonFilter.AdminView.class)
    @Column(name = "role", nullable = true)
    private Integer role;

    @Column(name = "CURRENCY_CODE")
    private String currencyCode;

    @Column(name = "LANG")
    private String lang;

    @Column(name = "IS_TR")
    private Integer isTR;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name = "CREATE_DATE")
    private Date createDate;

    @JsonIgnore
    @Column(name = "ACTIVATION_KEY")
    private String activationKey;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name = "TIME_ZONE")
    private String timeZone;

    @JsonView(ApiJsonFilter.AdminView.class)
    @Column(name = "CREDIT")
    private Double credit;

    @JsonIgnore
    @Column(name = "CAN_USE_ANALYTIC")
    private int canUseAnalytic;

    @JsonView(ApiJsonFilter.PublicView.class)
    @Column(name="ADDITIONAL_INFO")
    private String additionalInfo;

    @JsonView(ApiJsonFilter.AdminView.class)
    @Column(name="INCOME")
    private Double income;

    //NotNull
    //@Size(min =0, max=100)
    @JsonView(ApiJsonFilter.AdminView.class)
    @Column(name="REKMOB_PERCENT")
    private Double rekmobPercent;

    @JsonView(ApiJsonFilter.AdminView.class)
    @Column(name="IBAN")
    private String iban;


    /*
     *  if user is agency value must be 1
     */
    @JsonView(ApiJsonFilter.AdminView.class)
    @Column(name="AGENCY")
    private int agency;

    @JsonView(ApiJsonFilter.AdminView.class)
    @Column(name="ADVANCED_USER")
    private int advancedUser;

    @JsonIgnore
    @Column(name="USER_KEY")
    private String userKey;

    @JsonIgnore
    @Column(name="CREDIT_TEMP")
    private Double creditTemp;

    @JsonView(ApiJsonFilter.AdminView.class)
    @Column(name="anx_pub_id")
    private Integer anxPubId;

    @JsonView(ApiJsonFilter.AdminView.class)
    @Column(name="adf_pub_id")
    private Integer adfPubId;

    @JsonView(ApiJsonFilter.AdminView.class)
    @Column(name="acc_manager")
    private String accManager;

    @JsonView(ApiJsonFilter.AdminView.class)
    @Column(name="utm_source")
    private String utmSource;

    @JsonIgnore
    @Column(name="api_key")
    private String apiKey;

    @JsonIgnore
    @Column(name="master_user_id")
    private Integer masterUserId;

    @JsonView(ApiJsonFilter.AdminView.class)
    @Column(name="pub_type")
    private Integer pubType;

    @JsonView(ApiJsonFilter.AdminView.class)
    @Column(name="sr_pub_id")
    private String srPubId;

    @JsonView(ApiJsonFilter.AdminView.class)
    @Column(name="rs_user_id")
    private Integer rsUserId;

    @JsonView(ApiJsonFilter.AdminView.class)
    @Column(name="ref_user_id")
    private Integer refUserId;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "COUNTRY_CODE")
    private String countryCode;

    @JsonIgnore
    @Transient
    private List<Address> adrList;

    @JsonIgnore
    @Transient
    private List<App> appList;

    @JsonIgnore
    @Transient
    private List<Ad> adList;

    /*
     * Gets -- sets
     */

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRePassword() {
        return rePassword;
    }

    public void setRePassword(String rePassword) {
        this.rePassword = rePassword;
    }


    @JsonIgnore
    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Integer getIsTR() {
        return isTR;
    }

    public void setIsTR(Integer TR) {
        isTR = TR;
    }

    public List<App> getAppList() {
        return appList;
    }

    public void setAppList(List<App> appList) {
        this.appList = appList;
    }

    public List<Ad> getAdList() {
        return adList;
    }

    public void setAdList(List<Ad> adList) {
        this.adList = adList;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getAccType() {
        return accType;
    }

    public void setAccType(Integer accType) {
        this.accType = accType;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    @JsonIgnore
    public String getTcNo() {
        return tcNo;
    }

    public void setTcNo(String tcNo) {
        this.tcNo = tcNo;
    }

    @JsonIgnore
    public String getTaxNo() {
        return taxNo;
    }

    @JsonIgnore
    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    @JsonIgnore
    public String getTaxOffice() {
        return taxOffice;
    }

    public void setTaxOffice(String taxOffice) {
        this.taxOffice = taxOffice;
    }

    public Boolean getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getBan() {
        return ban;
    }

    public void setBan(Boolean ban) {
        this.ban = ban;
    }

    public List<Address> getAdrList() {
        return adrList;
    }

    public void setAdrList(List<Address> adrList) {
        this.adrList = adrList;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return {@link #name}
     */
    @JsonIgnore
    public String getActivationKey() {
        return activationKey;
    }

    /**
     * @param activationKey
     *            the activationKey to set
     */
    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    /**
     * @return {@link #name}
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * @param timeZone
     *            the timeZone to set
     */
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    @JsonIgnore
    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    @JsonIgnore
    public int getCanUseAnalytic() {
        return canUseAnalytic;
    }

    public void setCanUseAnalytic(int canUseAnalytic) {
        this.canUseAnalytic = canUseAnalytic;
    }

    @JsonProperty("canUseAnalytic")
    public void setCanUseAnalytic(boolean canUseAnalytic) {
        this.canUseAnalytic = canUseAnalytic ? 1 : 0;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    @JsonIgnore
    public Double getIncome() {
        return income;
    }

    public void setIncome(Double income) {
        this.income = income;
    }

    @JsonIgnore
    public Double getRekmobPercent() {
        return rekmobPercent;
    }

    public void setRekmobPercent(Double rekmobPercent) {
        this.rekmobPercent = rekmobPercent;
    }

    /**
     * @return {@link #name}
     */
    @JsonIgnore
    public String getIban() {
        return iban;
    }

    /**
     * @param iban the iban to set
     */
    public void setIban(String iban) {
        this.iban = iban;
    }

    /**
     * @return {@link #name}
     */
    public int getAgency() {
        return agency;
    }

    /**
     * @param agency the agency to set
     */

    public void setAgency(int agency) {
        this.agency = agency;
    }

    public boolean isAgency(){
        return this.agency ==1;
    }

    @JsonProperty("agency")
    public void setAgency(boolean agency){
        this.setAgency(agency ? 1 :0);
    }

    /**
     * @return {@link #name}
     */
    public int getAdvancedUser() {
        return advancedUser;
    }

    /**
     * @param advancedUser the advancedUser to set
     */
    public void setAdvancedUser(int advancedUser) {
        this.advancedUser = advancedUser;
    }

    @JsonProperty("advancedUser")
    public void setAdvancedUser(boolean advancedUser) {
        this.advancedUser = advancedUser? 1: 0;
    }

    public boolean isAdvancedUser(){
        return this.advancedUser == 1;
    }

    /**
     * @return {@link #name}
     */
    @JsonIgnore
    public String getUserKey() {
        return userKey;
    }

    /**
     * @param userKey the userKey to set
     */
    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    @JsonIgnore
    public Double getCreditTemp() {
        return creditTemp;
    }

    public void setCreditTemp(Double creditTemp) {
        this.creditTemp = creditTemp;
    }

    public Integer getAnxPubId() {
        return anxPubId;
    }
    @JsonIgnore
    public boolean isGlobal() {
        String accManager=getAccManager();
        return accManager.equals("Global")
                || accManager.equals("Melis")
                || accManager.equals("Esra");
    }

    public void setAnxPubId(Integer anxPubId) {
        this.anxPubId = anxPubId;
    }

    public Integer getAdfPubId() {
        return adfPubId;
    }

    public void setAdfPubId(Integer adfPubId) {
        this.adfPubId = adfPubId;
    }

    @JsonIgnore
    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getAccManager() {
        return accManager;
    }

    public void setAccManager(String accManager) {
        this.accManager = accManager;
    }

    public Integer getMasterUserId() {
        return masterUserId;
    }


    public Integer getPubType() {
        return pubType;
    }

    public void setPubType(Integer pubType) {
        this.pubType = pubType;
    }

    public String getSrPubId() {
        return srPubId;
    }

    public void setSrPubId(String srPubId) {
        this.srPubId = srPubId;
    }

    @JsonIgnore
    public void setMasterUserId(Integer masterUserId) {
        this.masterUserId = masterUserId;
    }

    public Integer getRsUserId() {
        return rsUserId;
    }

    public void setRsUserId(Integer rsUserId) {
        this.rsUserId = rsUserId;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public Integer getRefUserId() {
        return refUserId;
    }

    public void setRefUserId(Integer refUserId) {
        this.refUserId = refUserId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
