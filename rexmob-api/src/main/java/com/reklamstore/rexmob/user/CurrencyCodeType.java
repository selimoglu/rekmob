package com.reklamstore.rexmob.user;

public enum CurrencyCodeType {
    TRY(0), USD(1), EUR(2);

    private int type;

    CurrencyCodeType(int type){
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public boolean equals(CurrencyCodeType currencyCode){
        if(currencyCode == null)
            return false;
        return this.type == currencyCode.getType();
    }
}
