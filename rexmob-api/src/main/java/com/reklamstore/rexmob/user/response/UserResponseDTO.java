package com.reklamstore.rexmob.user.response;
import com.reklamstore.rexmob.user.User;

import java.util.Date;

public class UserResponseDTO {

    private Integer id;
    private String fullName;
    private String email;
    private String phone;
    private Integer accType;
    private Integer userType;

    private String tcNo;
    private String taxNo;
    private String taxOffice;
    private Boolean active;
    private Boolean ban;

    private Integer role;
    private String currencyCode;
    private String lang;
    private Integer isTR;
    private Date createDate;
    private String timeZone;
    private Double credit;
    private String additionalInfo;
    private Double income;
    private Double rekmobPercent;
    private String iban;

    private int agency;
    private int advancedUser;

    private Double creditTemp;
    private Integer anxPubId;
    private Integer adfPubId;
    private String accManager;
    private String utmSource;

    private Integer pubType;
    private String srPubId;
    private Integer rsUserId;
    private Integer refUserId;
    private String address;

    public UserResponseDTO() {
    }

    public UserResponseDTO(User user) {
        this.id = user.getId();
        this.fullName = user.getFullName();
        this.email = user.getEmail();
        this.phone = user.getPhone();
        this.accType = user.getAccType();
        this.userType = user.getUserType();
        this.tcNo = user.getTcNo();
        this.taxNo = user.getTaxNo();
        this.taxOffice = user.getTaxOffice();
        this.active = user.getActive();
        this.ban = user.getBan();
        this.role = user.getRole();
        this.currencyCode = user.getCurrencyCode();
        this.lang = user.getLang();
        this.isTR = user.getIsTR();
        this.createDate = user.getCreateDate();
        this.timeZone = user.getTimeZone();
        this.credit = user.getCredit();
        this.additionalInfo = user.getAdditionalInfo();
        this.income = user.getIncome();
        this.rekmobPercent = user.getRekmobPercent();
        this.iban = user.getIban();
        this.agency = user.getAgency();
        this.advancedUser = user.getAdvancedUser();
        this.creditTemp = user.getCreditTemp();
        this.anxPubId = user.getAnxPubId();
        this.adfPubId = user.getAdfPubId();
        this.accManager = user.getAccManager();
        this.utmSource = user.getUtmSource();
        this.pubType = user.getPubType();
        this.srPubId = user.getSrPubId();
        this.rsUserId = user.getRsUserId();
        this.refUserId = user.getRefUserId();
        this.address = user.getAddress();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getAccType() {
        return accType;
    }

    public void setAccType(Integer accType) {
        this.accType = accType;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getTcNo() {
        return tcNo;
    }

    public void setTcNo(String tcNo) {
        this.tcNo = tcNo;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getTaxOffice() {
        return taxOffice;
    }

    public void setTaxOffice(String taxOffice) {
        this.taxOffice = taxOffice;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getBan() {
        return ban;
    }

    public void setBan(Boolean ban) {
        this.ban = ban;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Integer getIsTR() {
        return isTR;
    }

    public void setIsTR(Integer isTR) {
        this.isTR = isTR;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Double getIncome() {
        return income;
    }

    public void setIncome(Double income) {
        this.income = income;
    }

    public Double getRekmobPercent() {
        return rekmobPercent;
    }

    public void setRekmobPercent(Double rekmobPercent) {
        this.rekmobPercent = rekmobPercent;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public int getAgency() {
        return agency;
    }

    public void setAgency(int agency) {
        this.agency = agency;
    }

    public int getAdvancedUser() {
        return advancedUser;
    }

    public void setAdvancedUser(int advancedUser) {
        this.advancedUser = advancedUser;
    }

    public Double getCreditTemp() {
        return creditTemp;
    }

    public void setCreditTemp(Double creditTemp) {
        this.creditTemp = creditTemp;
    }

    public Integer getAnxPubId() {
        return anxPubId;
    }

    public void setAnxPubId(Integer anxPubId) {
        this.anxPubId = anxPubId;
    }

    public Integer getAdfPubId() {
        return adfPubId;
    }

    public void setAdfPubId(Integer adfPubId) {
        this.adfPubId = adfPubId;
    }

    public String getAccManager() {
        return accManager;
    }

    public void setAccManager(String accManager) {
        this.accManager = accManager;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public Integer getPubType() {
        return pubType;
    }

    public void setPubType(Integer pubType) {
        this.pubType = pubType;
    }

    public String getSrPubId() {
        return srPubId;
    }

    public void setSrPubId(String srPubId) {
        this.srPubId = srPubId;
    }

    public Integer getRsUserId() {
        return rsUserId;
    }

    public void setRsUserId(Integer rsUserId) {
        this.rsUserId = rsUserId;
    }

    public Integer getRefUserId() {
        return refUserId;
    }

    public void setRefUserId(Integer refUserId) {
        this.refUserId = refUserId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
