package com.reklamstore.rexmob.user.request;

public class UserUpdateRequest {

    private String accountManager;
    private Double rekmobPercent;
    private Integer isTR;

    public String getAccountManager() {
        return accountManager;
    }

    public void setAccountManager(String accountManager) {
        this.accountManager = accountManager;
    }

    public Double getRekmobPercent() {
        return rekmobPercent;
    }

    public void setRekmobPercent(Double rekmobPercent) {
        this.rekmobPercent = rekmobPercent;
    }

    public Integer getIsTR() {
        return isTR;
    }

    public void setIsTR(Integer isTR) {
        this.isTR = isTR;
    }
}