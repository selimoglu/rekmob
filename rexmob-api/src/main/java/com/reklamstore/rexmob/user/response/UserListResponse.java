package com.reklamstore.rexmob.user.response;

import com.reklamstore.rexmob.app.response.AppResponseDTO;
import com.reklamstore.rexmob.base.BaseException;
import com.reklamstore.rexmob.base.BaseResponse;

import java.util.List;

public class UserListResponse extends BaseResponse {

    private List<UserResponseDTO> response;

    public UserListResponse() {
    }

    public UserListResponse(BaseException exception) {
        super(exception);
    }

    public UserListResponse(List<UserResponseDTO> response) {
        super(true);
        this.response = response;
    }

    public List<UserResponseDTO> getResponse() {
        return response;
    }

    public void setResponse(List<UserResponseDTO> response) {
        this.response = response;
    }
}
