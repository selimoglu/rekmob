/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

package com.reklamstore.rexmob.user;

import com.reklamstore.rexmob.authentication.RekmobUserDetailsService;
import org.springframework.security.core.context.SecurityContextHolder;

public class UserContextHolder {

    private UserContextHolder() {
    }

    public static User getUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        RekmobUserDetailsService.RekmobUserDetails userDetails = (RekmobUserDetailsService.RekmobUserDetails) principal;
        return userDetails.getUser();
    }
}
