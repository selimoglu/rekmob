package com.reklamstore.rexmob.email;


import com.reklamstore.rexmob.app.App;
import com.reklamstore.rexmob.app.RejectReasonType;
import com.reklamstore.rexmob.user.User;
import com.reklamstore.rexmob.util.LanguageData;
import com.reklamstore.rexmob.util.Status;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

@Service
public class EmailService {

    @Value("${frontend.publisher.url}")
    private String publisherUrl;

    private JavaMailSender emailSender;

    @Autowired
    public EmailService(JavaMailSender emailSender){
        this.emailSender = emailSender;
    }

    public void send(String to, String subject, String text) {
        send(to, subject, text, false);
    }

    public void send(String to, String subject, String text, boolean isHtml) {

        try {
            MimeMessage message = emailSender.createMimeMessage();
            message.setSubject(subject);

            MimeMessageHelper helper = new MimeMessageHelper(message, true, StandardCharsets.UTF_8.name());
            helper.setTo(to);
            helper.setText(text, isHtml);

            emailSender.send(message);
        } catch (MessagingException ignored) {}
    }

    public void sendOnChangeAppStatus(User user, App app){
        if(Status.APPROVED.equals(app.getStatus()))
            sendApprovedApp(user, app);
        else if(Status.REJECTED.equals(app.getStatus())) {
            if(RejectReasonType.INSUFFICIENT_CONTENT.equals(app.getRejectReason()))
                sendRejectAppByInsufficientContentOrTraffic(user, app);
            else if(RejectReasonType.FREE_DOMAIN_NAME.equals(app.getRejectReason()))
                sendRejectAppByFreeDomainName(user, app);
            else if(RejectReasonType.INCENTIVE_TO_CLICK.equals(app.getRejectReason()))
                sendRejectAppByIncentiveToClick(user, app);
            else if(RejectReasonType.ADULT_CONTENT.equals(app.getRejectReason()))
                sendRejectAppByAdultContent(user, app);
            else
                sendRejectAppByNoReason(user, app);
        }
    }

    public void sendApprovedApp(User user, App app){
        final Properties props = new Properties();
        props.setProperty("resource.loader", "class");
        props.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

        VelocityEngine velocityEngine = new VelocityEngine(props);
        velocityEngine.init();
        Template t = velocityEngine.getTemplate("templates/approved_app_mail.vm", StandardCharsets.UTF_8.name());

        VelocityContext context = new VelocityContext();
        context.put("title", LanguageData.get(user.getLang(), LanguageData.APPROVED_APP_MAIL_TITLE_KEY) +" "+ user.getFullName());
        context.put("description1", LanguageData.get(user.getLang(), LanguageData.APPROVED_APP_MAIL_DESCRIPTION_1_1_KEY)
                +" "+ app.getAppUrl() +" "+ LanguageData.get(user.getLang(), LanguageData.APPROVED_APP_MAIL_DESCRIPTION_1_2_KEY));
        context.put("description2", LanguageData.get(user.getLang(), LanguageData.APPROVED_APP_MAIL_DESCRIPTION_2_KEY));
        context.put("description3", LanguageData.get(user.getLang(), LanguageData.APPROVED_APP_MAIL_DESCRIPTION_3_KEY));
        context.put("description4", LanguageData.get(user.getLang(), LanguageData.APPROVED_APP_MAIL_DESCRIPTION_4_KEY));
        //context.put("description5", LanguageData.get(user.getLang(), LanguageData.APPROVED_APP_MAIL_DESCRIPTION_5_KEY));
        context.put("buttonText", LanguageData.get(user.getLang(), LanguageData.APPROVED_APP_MAIL_BUTTON_TEXT_KEY));
        context.put("redirectUrl", publisherUrl);

        StringWriter writer = new StringWriter();
        t.merge(context, writer);

        final String inBody = writer.toString();
        send(user.getEmail(), LanguageData.get(user.getLang(), LanguageData.APPROVED_APP_MAIL_SUBJECT_KEY), inBody, true);
    }

    public void sendRejectAppByNoReason(User user, App app){
        final Properties props = new Properties();
        props.setProperty("resource.loader", "class");
        props.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

        VelocityEngine velocityEngine = new VelocityEngine(props);
        velocityEngine.init();
        Template t = velocityEngine.getTemplate("templates/reject_app_no_reason_mail.vm", StandardCharsets.UTF_8.name());

        VelocityContext context = new VelocityContext();
        context.put("title", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_NO_REASON_MAIL_TITLE_KEY) +" "+ user.getFullName());
        context.put("description1", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_NO_REASON_MAIL_DESCRIPTION_1_1_KEY)
                +" "+ app.getAppUrl() +" "+ LanguageData.get(user.getLang(), LanguageData.REJECT_APP_NO_REASON_MAIL_DESCRIPTION_1_2_KEY));
        context.put("description2", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_NO_REASON_MAIL_DESCRIPTION_2_KEY));
        context.put("description3", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_NO_REASON_MAIL_DESCRIPTION_3_KEY));

        StringWriter writer = new StringWriter();
        t.merge(context, writer);

        final String inBody = writer.toString();
        send(user.getEmail(), LanguageData.get(user.getLang(), LanguageData.REJECT_APP_NO_REASON_MAIL_SUBJECT_KEY), inBody, true);
    }

    public void sendRejectAppByInsufficientContentOrTraffic(User user, App app){
        final Properties props = new Properties();
        props.setProperty("resource.loader", "class");
        props.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

        VelocityEngine velocityEngine = new VelocityEngine(props);
        velocityEngine.init();
        Template t = velocityEngine.getTemplate("templates/reject_app_insufficient_content_or_traffic_mail.vm", StandardCharsets.UTF_8.name());

        VelocityContext context = new VelocityContext();
        context.put("title", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_TITLE_KEY) +" "+ user.getFullName());
        context.put("description1", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_1_1_KEY)
                +" "+ app.getAppUrl() +" "+ LanguageData.get(user.getLang(), LanguageData.REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_1_2_KEY));
        context.put("description2", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_2_KEY));
        context.put("description3", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_3_KEY));
        context.put("description4", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_4_KEY));
        context.put("description5", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_DESCRIPTION_5_KEY));

        StringWriter writer = new StringWriter();
        t.merge(context, writer);

        final String inBody = writer.toString();
        send(user.getEmail(), LanguageData.get(user.getLang(), LanguageData.REJECT_APP_INSUFFICIENT_CONTENT_OR_TRAFFIC_MAIL_SUBJECT_KEY), inBody, true);
    }

    public void sendRejectAppByFreeDomainName(User user, App app){
        final Properties props = new Properties();
        props.setProperty("resource.loader", "class");
        props.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

        VelocityEngine velocityEngine = new VelocityEngine(props);
        velocityEngine.init();
        Template t = velocityEngine.getTemplate("templates/reject_app_free_domain_name_mail.vm", StandardCharsets.UTF_8.name());

        VelocityContext context = new VelocityContext();
        context.put("title", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_FREE_DOMAIN_NAME_MAIL_TITLE_KEY) +" "+ user.getFullName());
        context.put("description1", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_1_1_KEY)
                +" "+ app.getAppUrl() +" "+ LanguageData.get(user.getLang(), LanguageData.REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_1_2_KEY));
        context.put("description2", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_2_KEY));
        context.put("description3", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_3_KEY));
        context.put("description4", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_FREE_DOMAIN_NAME_MAIL_DESCRIPTION_4_KEY));

        StringWriter writer = new StringWriter();
        t.merge(context, writer);

        final String inBody = writer.toString();
        send(user.getEmail(), LanguageData.get(user.getLang(), LanguageData.REJECT_APP_FREE_DOMAIN_NAME_MAIL_SUBJECT_KEY), inBody, true);
    }

    public void sendRejectAppByIncentiveToClick(User user, App app){
        final Properties props = new Properties();
        props.setProperty("resource.loader", "class");
        props.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

        VelocityEngine velocityEngine = new VelocityEngine(props);
        velocityEngine.init();
        Template t = velocityEngine.getTemplate("templates/reject_app_incentive_to_click_mail.vm", StandardCharsets.UTF_8.name());

        VelocityContext context = new VelocityContext();
        context.put("title", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_INCENTIVE_TO_CLICK_MAIL_TITLE_KEY) +" "+ user.getFullName());
        context.put("description1", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_1_1_KEY)
                +" "+ app.getAppUrl() +" "+ LanguageData.get(user.getLang(), LanguageData.REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_1_2_KEY));
        context.put("description2", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_2_KEY));
        context.put("description3", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_3_KEY));
        context.put("description4", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_INCENTIVE_TO_CLICK_MAIL_DESCRIPTION_4_KEY));

        StringWriter writer = new StringWriter();
        t.merge(context, writer);

        final String inBody = writer.toString();
        send(user.getEmail(), LanguageData.get(user.getLang(), LanguageData.REJECT_APP_INCENTIVE_TO_CLICK_MAIL_SUBJECT_KEY), inBody, true);
    }

    public void sendRejectAppByAdultContent(User user, App app){
        final Properties props = new Properties();
        props.setProperty("resource.loader", "class");
        props.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

        VelocityEngine velocityEngine = new VelocityEngine(props);
        velocityEngine.init();
        Template t = velocityEngine.getTemplate("templates/reject_app_adult_content_mail.vm", StandardCharsets.UTF_8.name());

        VelocityContext context = new VelocityContext();
        context.put("title", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_ADULT_CONTENT_MAIL_TITLE_KEY) +" "+ user.getFullName());
        context.put("description1", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_1_1_KEY)
                +" "+ app.getAppUrl() +" "+ LanguageData.get(user.getLang(), LanguageData.REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_1_2_KEY));
        context.put("description2", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_2_KEY));
        context.put("description3", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_3_KEY));
        context.put("description4", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_4_KEY));
        context.put("description5", LanguageData.get(user.getLang(), LanguageData.REJECT_APP_ADULT_CONTENT_MAIL_DESCRIPTION_5_KEY));

        StringWriter writer = new StringWriter();
        t.merge(context, writer);

        final String inBody = writer.toString();
        send(user.getEmail(), LanguageData.get(user.getLang(), LanguageData.REJECT_APP_ADULT_CONTENT_MAIL_SUBJECT_KEY), inBody, true);
    }
}
