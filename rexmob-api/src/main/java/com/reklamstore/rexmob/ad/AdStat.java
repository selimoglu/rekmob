package com.reklamstore.rexmob.ad;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.*;

/*
 * if any field is added AdStat class , please check JobSchedulerService.java for reducedStats jobs
 */
@Entity
@Table(name = "ad_stat")
public class AdStat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "STAT_ID")
    private BigInteger id;

    @Column(name = "REQ")
    private long req;

    @Column(name = "IMP")
    private long imp;

    @Column(name = "CLICK")
    private long click;

    @Column(name = "AD_ID")
    private Integer adId;

    @Column(name = "COST")
    private double cost;

    @Column(name="RCOST")
    private double rcost;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "STAT_TIME")
    private Date statTime;

    @Column(name="CLICK_V")
    private long clickV;

    @Column(name="INS")
    private long ins;

    @Transient
    private double ctr;

    @Transient
    private Integer userId;

    @Transient
    private Integer campaignId;

    public AdStat() {
    }

    public AdStat(Integer adId, Date createDate, Date statTime, Long req, Long imp, Long click, Double cost,Double rcost,Long clickV,Long install) {
        this.adId = adId;
        this.createDate = createDate;
        this.req = req == null ? 0 : req;
        this.imp = imp == null ? 0 : imp;
        this.click = click == null ? 0 : click;
        this.cost = cost == null ? 0 : cost;
        this.statTime = statTime;
        this.clickV = clickV==null ? 0 : clickV;
        this.ins = install==null ? 0 : install;
        this.rcost= rcost==null ? 0 : rcost;
    }

    public void incClick() {
        this.click += 1;
    }

    public void incReq() {
        this.req += 1;
    }

    public void incImp() {
        this.imp += 1;
    }

    public void incIns(){
        this.ins += 1;
    }

    public void incClickV(){
        this.clickV += 1;
    }

    /**
     * @return {@link #name}
     */
    public Integer getAdId() {
        return adId;
    }

    /**
     * @param adId
     *            the adId to set
     */
    public void setAdId(Integer adId) {
        this.adId = adId;
    }

    /**
     * @return {@link #name}
     */
    public double getCost() {
        return cost;
    }

    /**
     * @param cost
     *            the cost to set
     */
    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getRcost() {
        return rcost;
    }

    public void setRcost(double rcost) {
        this.rcost = rcost;
    }

    /**
     * @return {@link #name}
     */
    public BigInteger getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(BigInteger id) {
        this.id = id;
    }

    /**
     * @return {@link #name}
     */
    public long getReq() {
        return req;
    }

    /**
     * @param req
     *            the req to set
     */
    public void setReq(long req) {
        this.req = req;
    }

    /**
     * @return {@link #name}
     */
    public long getImp() {
        return imp;
    }

    /**
     * @param imp
     *            the imp to set
     */
    public void setImp(long imp) {
        this.imp = imp;
    }

    /**
     * @return {@link #name}
     */
    public long getClick() {
        return click;
    }

    /**
     * @param click
     *            the click to set
     */
    public void setClick(long click) {
        this.click = click;
    }

    /**
     * @return {@link #name}
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     *            the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return {@link #name}
     */
    public Date getStatTime() {
        return statTime;
    }

    /**
     * @param statTime the statTime to set
     */
    public void setStatTime(Date statTime) {
        this.statTime = statTime;
    }

    /**
     * @return the clickV
     */
    public long getClickV() {
        return clickV;
    }

    /**
     * @param clickV the clickV to set
     */
    public void setClickV(long clickV) {
        this.clickV = clickV;
    }

    /**
     * @return the ins
     */
    public long getIns() {
        return ins;
    }

    /**
     * @param ins the ins to set
     */
    public void setIns(long ins) {
        this.ins = ins;
    }

    /**
     * @return {@link #name}
     */
    public double getCtr() {
        return ctr;
    }

    /**
     * @param ctr the ctr to set
     */
    public void setCtr(double ctr) {
        this.ctr = ctr;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }


}
