/**
 * Copyright rekmob bilisim
 */
package com.reklamstore.rexmob.ad;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.reklamstore.rexmob.campaign.Campaign;
import com.reklamstore.rexmob.tag.Tag;
import com.reklamstore.rexmob.user.User;
import com.reklamstore.rexmob.util.DateUtil;
import com.reklamstore.rexmob.util.TotalStatUtil;
import org.hibernate.annotations.Formula;

import org.hibernate.validator.constraints.Range;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.format.annotation.DateTimeFormat;

import org.springframework.util.StringUtils;

/**
 * @author fti
 * 
 */

@Entity
@Table(name = "ad")
public class Ad {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "AD_ID")
    private Integer id;

    @NotEmpty
    @Column(name = "AD_NAME")
    private String adName;

    /** AdvertType.java TEXT(0), BANNER(1), INTERSISTIAL(2) */
    @NotNull
    @Range(min = 0, max = 2)
    @Column(name = "CREATIVE_TYPE")
    private Integer creativeType;

    @Column(name = "CREATIVE_IMAGE")
    private String creativeImage;

    @Column(name = "CREATIVE_IMAGE_PROP")
    private String creativeImageProp;

    @Column(name = "CREATIVE_TEXT")
    private String creativeText;

    @Column(name = "CREATIVE_HTML")
    private String creativeHtml;

    /** CreativeFormat.java BANNER(0), FULLSCREEN(1); **/
    @Column(name = "CREATIVE_FORMAT")
    private String creativeFormat;

    @NotEmpty
    @Column(name = "AD_LINK")
    private String adLink;

    @Column(name = "BID_MIN")
    private Double bidMin;

    @Column(name = "BID_DEF")
    private Double bidDef;

    @Column(name = "BID_MAX")
    private Double bidMax;

    @Column(name="BID_REF")
    private Double bidRef;

    @Column(name = "TAGS_TEXT")
    private String tagsText;

    @Column(name = "DAILY_BUDGET")
    private Double dailyBudget;

    @Column(name = "DAILY_SPENT")
    private Double dailySpent;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "START_TIME")
    private String startTime;

    @Column(name = "END_DATE_FLAG")
    private boolean endDateFlag;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "END_DATE")
    private Date endDate;

    @Column(name = "END_TIME")
    private String endTime;

    @Column(name = "ACTIVE", nullable = true)
    private int active;

    @Column(name = "ARCHIVED", nullable = true)
    private int archived;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "COUNTRIES")
    private String countries;

    @Column(name = "NOT_ALLOWED_COUNTRIES")
    private String notAllowedCountries;

    @Column(name = "HOUSE_AD")
    private boolean houseAd;

    @Column(name = "USER_ID")
    private Integer userId;

    @Column(name = "NETWORK")
    private Integer network;

    @Column(name="SELL_TYPE")
    private Integer sellType;

    @Column(name="BATTERY")
    private String battery;

    @Column(name="SIGNAL_STRENGTH")
    private String signalStrength;

    @Column(name="AMBIENT_LIGHT")
    private String ambientLight;

    @Column(name="AMBIENT_TEMPERATURE")
    private String ambientTemperature;

    @Column(name="CITIES")
    private String cities;

    @Column(name="NETWORK_SPEED")
    private String networkSpeed;

    @Column(name="VISITED_COUNTRIES")
    private String visitedCountries;

    @Column(name="KEYWORDS")
    private String keywords;

    @Column(name="DEVICE_NAMES")
    private String deviceNames;



    //  @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    // @JoinTable(name = "ad_tag", joinColumns = { @JoinColumn(name = "ad_id", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "tag_id", nullable = false, updatable = false) })
    @Transient
    private Set<Tag> tags;

    /**
     * ClickType Enum DIRECT(0), DOUBLECLICK(1);
     */
    @Column(name = "CLICK_TYPE")
    private int clickType;

    @NotNull
    @Column(name = "PLATFORM")
    private Integer platform;

    /**
     * Status Enum PENDING(0), APPROVED(1), REJECTED(2);
     */
    @Column(name = "STATUS")
    private int status;

    @Column(name = "STATUS_DESC")
    private String statusDesc;

    @Formula("COALESCE(DAILY_BUDGET,0) - COALESCE(DAILY_SPENT,0)")
    private double remaningBudget;

    @Column(name = "START_TIMESTAMP")
    private Long startTimestamp;

    @Column(name = "END_TIMESTAMP")
    private Long endTimestamp;

    @Column(name = "LANDSCAPE")
    private int landscape;

    @Column(name = "INCLUDED_APPS")
    private String includedApps;

    @Column(name = "EXCLUDED_APPS")
    private String excludedApps;


    @Column(name = "OPERATORS")
    private String operators;

    @Column(name="CAMPAIGN_ID")
    private Integer campaignId;

    @Column(name="DEVICE_TYPES")
    private String deviceTypes;

    @Column(name="BOOST_SCORE")
    private float boostScore;

    @Column(name="IMPR_TRACK_LINK")
    private String imprTrackLink;

    @Column(name="IMPR_TRACK_CODE")
    private String imprTrackCode;

    @Column(name="TARGET_TAGS_TEXT")
    private String targetTagsText;

    @Column(name="REKMOB_APPS")
    private String rekmobApps;

    @Column(name="REKMOB_NOT_ALLOWED_APPS")
    private String rekmobNotAllowedApps;

    @Column(name="RETARGET_URL")
    private String retargetUrl;


    @Column(name="FREQUENCY_CAP")
    private Integer frequencyCap;

    @Column(name="IMP_COST")
    private Double impCost;

    @Column(name="TRAFFIC_SOURCE")
    private String trafficSource;

    @Column(name="APP_TYPES")
    private String appTypes;

    /**
     * @return {@link #name}
     */
    public String getTargetTagsText() {
        return targetTagsText;
    }

    /**
     * @param targetTagsText the targetTagsText to set
     */
    public void setTargetTagsText(String targetTagsText) {
        this.targetTagsText = targetTagsText;
    }

    @Transient
    private User user;

    @Transient
    private List<AdStat> stats;

    @Transient
    private Campaign campaign;

    @Transient
    private TotalStatUtil totalStatUtil;

    @Transient
    private float searchScore;

    /** GETTER SETTERS */

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getActive() {
        return active;
    }

    public boolean isActive() {
        return active == 1;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public void setActive(boolean var) {
        this.active = var ? 1 : 0;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.userId = user != null ? user.getId() : null;
        this.user = user;
    }

    public String getAdName() {
        return adName;
    }

    /**
     * @return {@link #name}
     */
    public Integer getCreativeType() {
        return creativeType;
    }

    /**
     * @param creativeType
     *            the creativeType to set
     */
    public void setCreativeType(Integer creativeType) {
        this.creativeType = creativeType;
    }

    /**
     * @return {@link #name}
     */
    public String getCreativeImage() {
        return creativeImage;
    }

    /**
     * @param creativePath
     *            the creativePath to set
     */
    public void setCreativeImage(String creativeImage) {
        this.creativeImage = creativeImage;
    }

    /**
     * @return {@link #name}
     */
    public String getCreativeImageProp() {
        return creativeImageProp;
    }

    /**
     * @param creativeImageProp
     *            the creativeImageProp to set
     */
    public void setCreativeImageProp(String creativeImageProp) {
        this.creativeImageProp = creativeImageProp;
    }

    /**
     * @return {@link #name}
     */
    public String getCreativeText() {
        return creativeText;
    }

    /**
     * @param creativeText
     *            the creativeText to set
     */
    public void setCreativeText(String creativeText) {
        this.creativeText = creativeText;
    }

    /**
     * @return {@link #name}
     */
    public String getCreativeHtml() {
        return creativeHtml;
    }

    /**
     * @param creativeHtml
     *            the creativeHtml to set
     */
    public void setCreativeHtml(String creativeHtml) {
        this.creativeHtml = creativeHtml;
    }

    /**
     * @return {@link #name}
     */
    public String getCreativeFormat() {
        return creativeFormat;
    }

    /**
     * @param creativeFormat
     *            the creativeFormat to set
     */
    public void setCreativeFormat(String creativeFormat) {
        this.creativeFormat = creativeFormat;
    }

    public void setAdName(String adName) {
        this.adName = adName;
    }

    public String getAdLink() {
        return adLink;
    }

    public void setAdLink(String adLink) {
        this.adLink = adLink;
    }

    public Double getDailyBudget() {
        return dailyBudget;
    }

    public void setDailyBudget(Double dailyBudget) {
        this.dailyBudget = dailyBudget;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public boolean getEndDateFlag() {
        return endDateFlag;
    }

    public void setEndDateFlag(boolean endDateFlag) {
        this.endDateFlag = endDateFlag;
    }

    /**
     * @return {@link #name}
     */
    public List<AdStat> getStats() {
        return stats;
    }

    /**
     * @param stats
     *            the stats to set
     */
    public void setStats(List<AdStat> stats) {
        this.stats = stats;
    }


    /**
     * @return {@link #name}
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * @param tags
     *            the tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     * @return {@link #name}
     */
    public String getTagsText() {
        return tagsText;
    }

    /**
     * @param tagsText
     *            the tagsText to set
     */
    public void setTagsText(String tagsText) {
        this.tagsText = tagsText;
    }



    /**
     * @return {@link #name}
     */
    public Double getBidMin() {
        return bidMin;
    }

    /**
     * @param bidMin
     *            the bidMin to set
     */
    public void setBidMin(Double bidMin) {
        this.bidMin = bidMin;
    }

    /**
     * @return {@link #name}
     */
    public Double getBidDef() {
        return bidDef;
    }

    /**
     * @param bidDef
     *            the bidDef to set
     */
    public void setBidDef(Double bidDef) {
        this.bidDef = bidDef;
    }

    /**
     * @return {@link #name}
     */
    public Double getBidMax() {
        return bidMax;
    }

    /**
     * @param bidMax
     *            the bidMax to set
     */
    public void setBidMax(Double bidMax) {
        this.bidMax = bidMax;
    }

    public Double getBidRef() {
        return bidRef;
    }

    public void setBidRef(Double bidRef) {
        this.bidRef = bidRef;
    }

    /**
     * @return {@link #name}
     */
    public boolean getHouseAd() {
        return houseAd;
    }

    /**
     * @param houseAd
     *            the houseAd to set
     */
    public void setHouseAd(boolean houseAd) {
        this.houseAd = houseAd;
    }



    /**
     * @return {@link #name}
     */
    public String getCountries() {
        return countries;
    }

    /**
     * @param countries
     *            the countries to set
     */
    public void setCountries(String countries) {
        this.countries = countries;
    }

    /**
     * @return {@link #name}
     */
    public Double getDailySpent() {
        return dailySpent;
    }

    /**
     * @param dailySpent
     *            the dailySpent to set
     */
    public void setDailySpent(Double dailySpent) {
        this.dailySpent = dailySpent;
    }

    /**
     * @return {@link #name}
     */
    public boolean isArchived() {
        return archived > 0 ? true : false;
    }

    /**
     * @return {@link #name}
     */
    public int getArchived() {
        return archived;
    }

    /**
     * @param archived
     *            the archived to set
     */
    public void setArchived(int archived) {
        this.archived = archived;
    }

    /**
     * @param archived
     *            the archived to set
     */
    public void setArchived(boolean isArchived) {
        this.archived = isArchived ? 1 : 0;
    }

    /**
     * @return {@link #name}
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId
     *            the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public int getClickType() {
        return clickType;
    }

    public void setClickType(int clickType) {
        this.clickType = clickType;
    }

    public Integer getPlatform() {
        return platform;
    }

    public void setPlatform(Integer platform) {
        this.platform = platform;
    }

    /**
     * @return {@link #name}
     */
    public Integer getNetwork() {
        return network;
    }

    /**
     * @param network
     *            the network to set
     */
    public void setNetwork(Integer network) {
        this.network = network;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    /**
     * @return {@link #name}
     */
    public Long getStartTimestamp() {
        return startTimestamp;
    }

    /**
     * @param startTimestamp
     *            the startTimestamp to set
     */
    public void setStartTimestamp(Long startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    /**
     * @return {@link #name}
     */
    public Long getEndTimestamp() {
        return endTimestamp;
    }

    /**
     * @param endTimestamp
     *            the endTimestamp to set
     */
    public void setEndTimestamp(Long endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    /**
     * @return {@link #name}
     */
    public double getRemaningBudget() {
        return remaningBudget;
    }

    /**
     * @param remaningBudget
     *            the remaningBudget to set
     */
    public void setRemaningBudget(double remaningBudget) {
        this.remaningBudget = remaningBudget;
    }

    /**
     * @return {@link #name}
     */
    public int getLandscape() {
        return landscape;
    }

    /**
     * @param landscape
     *            the landscape to set
     */
    public void setLandscape(int landscape) {
        this.landscape = landscape;
    }

    public boolean isLandscape() {
        return this.landscape > 0 ? true : false;
    }

    public void setLandscape(boolean isLandscape) {
        this.landscape = isLandscape ? 1 : 0;
    }

    /**
     * @return {@link #name}
     */
    public String getIncludedApps() {
        return includedApps;
    }

    /**
     * @param includedApps
     *            the includedApps to set
     */
    public void setIncludedApps(String includedApps) {
        this.includedApps = includedApps;
    }

    /**
     * @return {@link #name}
     */
    public String getExcludedApps() {
        return excludedApps;
    }

    /**
     * @param exludedApps
     *            the exludedApps to set
     */
    public void setExcludedApps(String exludedApps) {
        this.excludedApps = exludedApps;
    }

    /**
     * @return {@link #name}
     */
    public String getOperators() {
        return operators;
    }

    /**
     * @param operators
     *            the operators to set
     */
    public void setOperators(String operators) {
        this.operators = operators;
    }

    /**
     * @return {@link #name}
     */
    public Integer getCampaignId() {
        return campaignId;
    }

    /**
     * @param campaignId the campaignId to set
     */
    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    /**
     * @return {@link #name}
     */
    public Campaign getCampaign() {
        return campaign;
    }

    /**
     * @param campaign the campaign to set
     */
    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    /**
     * @return {@link #name}
     */
    public String getDeviceTypes() {
        return deviceTypes;
    }

    /**
     * @param deviceType the deviceType to set
     */
    public void setDeviceTypes(String deviceType) {
        this.deviceTypes = deviceType;
    }

    @Transient
    public String getWidth() {
        if (!this.creativeImageProp.isEmpty()) {
            try {
                String imageProps[] = this.creativeImageProp.split("_");
                return imageProps[0];
            } catch (Exception e) {
                return null;
            }
        } else {
            return null;
        }
    }

    @Transient
    public String getHeight() {
        if (!this.creativeImageProp.isEmpty()) {
            try {
                String imageProps[] = this.creativeImageProp.split("_");
                return imageProps[1];
            } catch (Exception e) {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * @return {@link #name}
     */
    public TotalStatUtil getTotalStatUtil() {
        return totalStatUtil;
    }

    /**
     * @param totalStatUtil the totalStatUtil to set
     */
    public void setTotalStatUtil(TotalStatUtil totalStatUtil) {
        this.totalStatUtil = totalStatUtil;
    }


    /**
     * @return the boostScore
     */
    public float getBoostScore() {
        return boostScore;
    }

    /**
     * @param boostScore the boostScore to set
     */
    public void setBoostScore(float boostScore) {
        this.boostScore = boostScore;
    }

    /**
     * @return the searchScore
     */
    public float getSearchScore() {
        return searchScore;
    }

    /**
     * @param searchScore the searchScore to set
     */
    public void setSearchScore(float searchScore) {
        this.searchScore = searchScore;
    }

    /**
     * @return {@link #name}
     */
    public String getImprTrackLink() {
        return imprTrackLink;
    }

    /**
     * @param imprTrackLink the imprTrackLink to set
     */
    public void setImprTrackLink(String imprTrackLink) {
        this.imprTrackLink = imprTrackLink;
    }

    /**
     * @return {@link #name}
     */
    public String getImprTrackCode() {
        return imprTrackCode;
    }

    /**
     * @param imprTrackCode the imprTrackCode to set
     */
    public void setImprTrackCode(String imprTrackCode) {
        this.imprTrackCode = imprTrackCode;
    }

    /**
     * @return {@link #name}
     */
    public String getRekmobApps() {
        return rekmobApps;
    }

    /**
     * @param rekmobApps the rekmobApps to set
     */
    public void setRekmobApps(String rekmobApps) {
        this.rekmobApps = rekmobApps;
    }

    public String getRetargetUrl() {
        return retargetUrl;
    }

    public void setRetargetUrl(String retargetUrl) {
        this.retargetUrl = retargetUrl;
    }

    public Integer getSellType() {
        return sellType;
    }

    public void setSellType(Integer sellType) {
        this.sellType = sellType;
    }

    public String getBattery() {
        return battery;
    }

    public void setBattery(String battery) {
        this.battery = battery;
    }

    public String getSignalStrength() {
        return signalStrength;
    }

    public void setSignalStrength(String signalStrength) {
        this.signalStrength = signalStrength;
    }

    public String getAmbientLight() {
        return ambientLight;
    }

    public void setAmbientLight(String ambientLight) {
        this.ambientLight = ambientLight;
    }

    public String getAmbientTemperature() {
        return ambientTemperature;
    }

    public void setAmbientTemperature(String ambientTemperature) {
        this.ambientTemperature = ambientTemperature;
    }



    public String getNetworkSpeed() {
        return networkSpeed;
    }

    public void setNetworkSpeed(String networkSpeed) {
        this.networkSpeed = networkSpeed;
    }

    public String getVisitedCountries() {
        return visitedCountries;
    }

    public void setVisitedCountries(String visitedCountries) {
        this.visitedCountries = visitedCountries;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDeviceNames() {
        return deviceNames;
    }

    public void setDeviceNames(String deviceNames) {
        this.deviceNames = deviceNames;
    }

    public String getCities() {
        return cities;
    }

    public void setCities(String cities) {
        this.cities = cities;
    }

    public Integer getFrequencyCap() {
        return frequencyCap;
    }

    public void setFrequencyCap(Integer frequencyCap) {
        this.frequencyCap = frequencyCap;
    }



    public Double getImpCost() {
        return impCost;
    }

    public void setImpCost(Double impCost) {
        this.impCost = impCost;
    }

    public String getTrafficSource() {
        return trafficSource;
    }

    public void setTrafficSource(String trafficSource) {
        this.trafficSource = trafficSource;
    }


    public String getNotAllowedCountries() {
        return notAllowedCountries;
    }

    public void setNotAllowedCountries(String notAllowedCountries) {
        this.notAllowedCountries = notAllowedCountries;
    }

    public String getRekmobNotAllowedApps() {
        return rekmobNotAllowedApps;
    }

    public void setRekmobNotAllowedApps(String rekmobNotAllowedApps) {
        this.rekmobNotAllowedApps = rekmobNotAllowedApps;
    }


    public String getAppTypes() {
        return appTypes;
    }

    public void setAppTypes(String appTypes) {
        this.appTypes = appTypes;
    }

    public JSONObject getJson(String timezone ){
        JSONObject unitObj = new JSONObject();
        unitObj.put("adName", getAdName());
        unitObj.put("adId", getId());

        unitObj.put("totalClick", totalStatUtil.getTotalClick());
        unitObj.put("totalImp", totalStatUtil.getTotalImp());
        unitObj.put("totalReq", totalStatUtil.getTotalReq());
        unitObj.put("totalCost", totalStatUtil.getTotalCost());
        unitObj.put("totalCtr", totalStatUtil.getCtr());
        unitObj.put("totalClickV",totalStatUtil.getTotalClickV());
        unitObj.put("totalIns", totalStatUtil.getTotalIns());
        JSONArray clickStats = new JSONArray();
        JSONArray impStats = new JSONArray();
        JSONArray reqStats = new JSONArray();
        JSONArray costStats = new JSONArray();
        JSONArray clickVStats = new JSONArray();
        JSONArray insStats = new JSONArray();

        for(AdStat stat :getStats()){
            long unixTime = DateUtil.sameDateNewTimezone(stat.getStatTime(), timezone).getTime();
            reqStats.put( getFieldJSON(unixTime, stat.getReq()));
            impStats.put(getFieldJSON(unixTime, stat.getImp()));
            clickStats.put(getFieldJSON(unixTime, stat.getClick()));
            costStats.put(getFieldJSON(unixTime, stat.getCost()));
            clickVStats.put(getFieldJSON(unixTime, stat.getClickV()));
            insStats.put(getFieldJSON(unixTime, stat.getIns()));

        }
        unitObj.put("reqs", reqStats);
        unitObj.put("imps", impStats);
        unitObj.put("clicks", clickStats);
        unitObj.put("costs", costStats);
        unitObj.put("clickVs", clickVStats);
        unitObj.put("inss", insStats);
        return unitObj;
    }

    private JSONArray getFieldJSON(long unixTime, Object count){
        JSONArray ja = new JSONArray();
        ja.put(unixTime);
        ja.put(count);
        return ja;
    }

    public boolean containsCreativeFormat(String creativeFormat){
        if(StringUtils.isEmpty(this.creativeFormat)){
            return false;
        }
        String creatives[] = this.creativeFormat.split(",");
        for(String format : creatives){
            if(format.equals(creativeFormat)){
                return true;
            }
        }
        return false;
    }


}