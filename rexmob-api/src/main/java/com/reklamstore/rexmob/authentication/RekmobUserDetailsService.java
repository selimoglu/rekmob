/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

package com.reklamstore.rexmob.authentication;

import com.reklamstore.rexmob.user.User;
import com.reklamstore.rexmob.user.UserService;
import com.reklamstore.rexmob.user.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Collection;
import java.util.ArrayList;

@Service
public class RekmobUserDetailsService implements UserDetailsService {

    private UserService userService;

    @Autowired
    public RekmobUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        try {
            User user = userService.getByEmail(email);
            if(User.ROLE_ADMIN != user.getRole())
                throw new UsernameNotFoundException("Username: " + email + " not found");

            return new RekmobUserDetails(user, getAuthorities(user.getRole()));
        } catch (UserNotFoundException exception) {
            throw new UsernameNotFoundException("Username: " + email + " not found");
        }
    }

    private List<GrantedAuthority> getAuthorities(Integer role) {
        String roleStr;
        switch (role){
            case 1: roleStr = "ADMIN"; break;
            case 2: roleStr = "USER";
            default: roleStr = "USER";
        }
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(roleStr));
        return authorities;
    }

    public static class RekmobUserDetails implements UserDetails {

        private static final long serialVersionUID = -3542337090559589236L;

        private User user;
        private List<GrantedAuthority> authorities;

        RekmobUserDetails(User user, List<GrantedAuthority> authorities) {
            this.user = user;
            this.authorities = authorities;
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return authorities;
        }

        @Override
        public String getPassword() {
            return user.getPassword();
        }

        @Override
        public String getUsername() {
            return user.getEmail();
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }

        public User getUser() {
            return user;
        }
    }
}

