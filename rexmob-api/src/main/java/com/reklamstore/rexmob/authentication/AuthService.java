/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

package com.reklamstore.rexmob.authentication;

import com.reklamstore.rexmob.authentication.exception.InvalidTokenHttpException;
import com.reklamstore.rexmob.authentication.exception.UserAlreadyExistsHttpException;
import com.reklamstore.rexmob.authentication.exception.UserNotFoundHttpException;
import com.reklamstore.rexmob.user.User;
import com.reklamstore.rexmob.user.UserService;
import com.reklamstore.rexmob.user.exception.UserAlreadyExistsException;
import com.reklamstore.rexmob.user.exception.UserNotFoundException;
import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    private UserService userService;
    private AuthenticationManager authenticationManager;
    private TokenService tokenService;

    @Autowired
    public AuthService(UserService userService,
                       AuthenticationManager authenticationManager,
                       TokenService tokenService) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.tokenService = tokenService;
    }

    Token register(SignUpDTO signUpDTO) throws UserAlreadyExistsHttpException {
        try {
            User user = userService.register(signUpDTO);
            return createToken(user);
        } catch (UserAlreadyExistsException exception) {
            throw new UserAlreadyExistsHttpException();
        }
    }

    Token login(LoginDTO loginDTO) throws UserNotFoundHttpException {
        try {
            Authentication authentication = createAuthentication(loginDTO);
            RekmobUserDetailsService.RekmobUserDetails userDetails =
                    (RekmobUserDetailsService.RekmobUserDetails) authenticationManager
                            .authenticate(authentication).getPrincipal();
            User user = userDetails.getUser();
            return createToken(user);
        } catch (AuthenticationException exception) {
            throw new UserNotFoundHttpException("Incorrect email or password", HttpStatus.FORBIDDEN);
        }
    }

    public Token loginAs(Integer userId) throws UserNotFoundHttpException {
        try {

            User user = userService.getOne(userId);
            if(user == null)
                throw new UserNotFoundHttpException("User not found.", HttpStatus.NOT_FOUND);

            return createToken(user);
        } catch (AuthenticationException exception) {
            throw new UserNotFoundHttpException("Incorrect email or password", HttpStatus.FORBIDDEN);
        }
    }

    Token refreshToken(RefreshTokenDTO refreshTokenDTO) throws InvalidTokenHttpException {
        try {
            String email = tokenService.getEmailFromRefreshToken(refreshTokenDTO.getToken().getRefreshToken());
            User user = userService.getByEmail(email);
            return createToken(user);
        } catch (JwtException | UserNotFoundException e) {
            throw new InvalidTokenHttpException();
        }
    }

    private Authentication createAuthentication(LoginDTO loginDTO) {
        return new UsernamePasswordAuthenticationToken(loginDTO.getEmail(), loginDTO.getPassword());
    }

    private Token createToken(User user) {
        return tokenService.createToken(user);
    }

}
