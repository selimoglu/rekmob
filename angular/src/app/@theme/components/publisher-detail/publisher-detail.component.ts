/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */
import { AfterViewInit, Component, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { SmartTableData } from '../../../@core/interfaces/common/smart-table';
import { NbSidebarService } from '@nebular/theme';
import { MessageService } from '../../../@core/mock/common/message-service';
import { Subscription } from 'rxjs';
import { LayoutService } from '../../../@core/utils';
@Component({
  selector: 'publisher-detail',
  template: `
  <div class="row">
    <div class="col-md-12">
        <nb-card size="small" style="height:700px">
            <nb-action class="control-item" (click)="togglePublisher()" icon="fas fa-arrow-right fa-2x" style="width:15px; margin:2px; padding:2px;"></nb-action>
            <nb-tabset>
                <nb-tab tabTitle="Details" style="margin-top:1rem; margin-left:0.5rem ">
                    <nb-card class="list-card" size="small" style="height:700px">
                        <nb-card-header style="font-weight: bold;text-transform:uppercase;">{{name}}</nb-card-header>
                        <nb-list>
                            <nb-list-item><li><a style="font-weight: bold;color:#2a2a2a ">Id : </a>{{id}} </li>
                            </nb-list-item>
                            <nb-list-item><li><a style="font-weight: bold; color:#2a2a2a ">Name : </a>{{name}}</li>
                            </nb-list-item>
                            <nb-list-item><li><a style="font-weight: bold;color:#2a2a2a "> Impression : </a>{{imp}}</li>
                            </nb-list-item>
                            <nb-list-item><li><a style="font-weight: bold;color:#2a2a2a "> Click : </a>{{click}}</li>
                            </nb-list-item>
                            <nb-list-item><li><a style="font-weight: bold;color:#2a2a2a "> pCpm : </a>{{pCpm}}</li>
                            </nb-list-item>
                            <nb-list-item><li><a style="font-weight: bold;color:#2a2a2a "> nCpm : </a>{{nCpm}}</li>
                            </nb-list-item>
                            <nb-list-item><li><a style="font-weight: bold;color:#2a2a2a "> Revenue : </a>{{revenue}}</li>
                            </nb-list-item>
                            <nb-list-item><li><a style="font-weight: bold;color:#2a2a2a "> Cost : </a>{{cost}}</li>
                            </nb-list-item>
                            <nb-list-item><li><a style="font-weight: bold;color:#2a2a2a "> Profit : </a>{{profit}}</li>
                            </nb-list-item>
                        </nb-list>
                    </nb-card>
                </nb-tab>
            </nb-tabset>
        </nb-card>
    </div>
  </div>
  `,
})
export class PublisherDetailComponent implements AfterViewInit, OnDestroy {
  
  
  
  togglePublisher() {

    this.sidebarService.toggle(true, 'menu-sidebar');
    this.sidebarService.toggle(false, 'settings-sidebar');
    this.messageService.setSidebarState(false);

  }
  options: any = {};
  themeSubscription: any;

  results2 = [];
  results3 = [];
  showLegend2 = true;
  showXAxis2 = true;
  showYAxis2 = true;
  xAxisLabel2 = 'Country';
  yAxisLabel2 = 'Population';
  colorScheme: any;
  subscription: Subscription;
  id: number;
  name: string;
  imp: number;
  click: number;
  pCpm: number;
  nCpm: number;
  revenue: number;
  cost: number;
  profit: number;
  constructor(private layoutService: LayoutService, private messageService: MessageService, private service: SmartTableData, private theme: NbThemeService, private sidebarService: NbSidebarService ) {
    const data = this.service.getData();
    this.results2 = [
      { name: data[0].firstName, value: data[0].impression },
      { name: data[1].firstName, value: data[1].impression },
      { name: data[2].firstName, value: data[2].impression },
      { name: data[3].firstName, value: data[3].impression },
      { name: data[4].firstName, value: data[4].impression },
      { name: data[5].firstName, value: data[5].impression },
    ];
    this.results3 = [
      { name: data[0].firstName, value: data[0].click },
      { name: data[1].firstName, value: data[1].click },
      { name: data[2].firstName, value: data[2].click },
      { name: data[3].firstName, value: data[3].click },
      { name: data[4].firstName, value: data[4].click },
      { name: data[5].firstName, value: data[5].click },
    ]
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.colorScheme = {
        domain: [colors.primaryLight, colors.infoLight, colors.successLight, 
          colors.warningLight, colors.dangerLight, colors.black],
      };
    });
    this.subscription = this.messageService.getGeneralData().subscribe(data => {
      this.id = data.text.id;
      this.name = data.text.firstName;
      this.imp = data.text.impression;
      this.click = data.text.click;
      this.pCpm = data.text.pCpm;
      this.nCpm = data.text.nCpm;
      this.revenue = data.text.revenue;
      this.cost = data.text.cost;
      this.profit = data.text.profit;
    });
  }
  ngAfterViewInit() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors = config.variables;
      const echarts: any = config.variables.echarts;

      this.options = {
        backgroundColor: echarts.bg,
        color: [colors.warningLight, colors.infoLight, colors.warningLight, colors.successLight,
           colors.primaryLight, colors.dangerLight],
        tooltip: {
          trigger: 'item',
          formatter: '{a} <br/>{b} : {c} ({d}%)',
        },
        legend: {
          orient: 'vertical',
          left: 'left',
          data: [this.results3[0].name, this.results3[1].name, this.results3[2].name, this.results3[3].name, 
          this.results3[4].name, this.results3[5].name],
          textStyle: {
            color: echarts.textColor,
          },
        },
        series: [
          {
            name: 'Countries',
            type: 'pie',
            radius: '80%',
            center: ['50%', '50%'],
            data: [
              { value: this.results3[0].value, name: this.results3[0].name },
              { value: this.results3[1].value, name: this.results3[1].name },
              { value: this.results3[2].value, name: this.results3[2].name },
              { value: this.results3[3].value, name: this.results3[3].name },
              { value: this.results3[4].value, name: this.results3[4].name },
              { value: this.results3[5].value, name: this.results3[5].name },
            ],
            itemStyle: {
              emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: echarts.itemHoverShadowColor,
              },
            },
            label: {
              normal: {
                textStyle: {
                  color: echarts.textColor,
                },
              },
            },
            labelLine: {
              normal: {
                lineStyle: {
                  color: echarts.axisLineColor,
                },
              },
            },
          },
        ],
      };
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
