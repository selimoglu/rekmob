import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NbWindowService, NbWindowRef, NbWindowConfig, NbCalendarRange, } from '@nebular/theme';
import { SearchData } from '../../../../@core/interfaces/common/searchData';
import { ReportData } from '../../../../@core/interfaces/common/reportData';

import { Router } from '@angular/router';
import { MessageService } from '../../../../@core/mock/common/message-service';
import { UtilDateService } from '../../../../@core/mock/common/util-date.service';
@Component({
  selector: 'header-custom-report',
  templateUrl: './custom-report.component.html',
  styleUrls: ['custom-report.component.scss'],
})
export class CustomReportComponent {

  userDatas = [];
  userLoading = false;
  appDatas = [];
  appUnitDatas = [];
  utmSources = [];
  accountManagers = [
    { name: 'Sefa', id: 'Sefa' },
    { name: 'Ceren', id: 'Ceren' },
    { name: 'Esra', id: 'Esra' },
    { name: 'Melis', id: 'Melis' },
    { name: 'Global', id: 'Global' },
  ];
  appTypes = [
    { name: 'BRANDSAFE', id: 0 },
    { name: 'MEDIUM', id: 1 },
    { name: 'AGGRESSİVE', id: 2 },
  ];
  selectedAppTypes = [this.appTypes[0]];
  sizes = [
    { name: 'BANNER (320x50)', id: 0 },
    { name: 'MEDIUM RECTANGLE (300x250)', id: 1 },
    { name: 'FULL BANNER (468x60)', id: 2 },
    { name: 'LEADERBOARD (728x90)', id: 3 },
    { name: 'MOBILE INTERSTITIAL', id: 9 },
    { name: '(320x568)', id: 10 },
    { name: '(300x600)', id: 11 },
    { name: '(160x600)', id: 12 },
    { name: 'LARGE_RECTANGLE (336x280)', id: 13 },
    { name: 'SCROLL (300x250)', id: 14 },
    { name: '(360x592)', id: 15 },
    { name: '(360x640)', id: 16 },
    { name: 'DESKTOP FULLSCREEN(800x600)', id: 17 },
    { name: 'MOBILE PASS', id: 18 },
    { name: 'SKYSCRAPER (120x600)', id: 19 },
    { name: 'SQUARE (250x250)', id: 20 },
    { name: 'MASTHEAD (970x250) ', id: 21 },
    { name: 'LARGE SKYSCRAPER (300x600)', id: 22 },
    { name: 'WIDE SKYSCRAPER (160x600)', id: 23 },
    { name: 'VIDEO', id: 24 },
    { name: 'NATIVE', id: 25 },
    { name: 'FLIP', id: 26 },
    { name: 'IN_READ', id: 27 },
    { name: 'DIRECTLINK', id: 28 },
    { name: 'POPUP', id: 29 },
    { name: 'POPUNDER', id: 30 },
  ];
  adSources = [
    { name: 'REKMOB', id: 2 },
    { name: 'ANX', id: 3 },
    { name: 'HOUSE_AD', id: 4 },
    { name: 'ADX', id: 5 },
    { name: 'ADF', id: 6 },
    { name: 'BRT', id: 7 },
    { name: 'SR', id: 8 },
    { name: 'NATIVE', id: 9 },
    { name: 'PUBMATIC', id: 10 },
    { name: 'SPRINGSERVE', id: 11 },
    { name: 'RUBICON', id: 12 },
    { name: 'IMD', id: 15 },
    { name: 'PROP', id: 16 },
    { name: 'ADCASH', id: 17 },
    { name: 'ADSTERRA', id: 18 },
    { name: 'ADMAVEN', id: 19 },
  ];

  selectedAdSources = [2];
  filters = [];
  selecteds = {};
  rangeQuery = function(start, end) {
    return { "range": { "statTime": { "gte": start, "lt": end } } };
  };
  selectOptions = {
    "Size": this.sizes,
    "Account Manager": this.accountManagers,
    "App Type": this.appTypes,
    "AdSource": this.adSources
  };
  typeOptions = {
    "User": "dialog",
    "Account Manager": "select",
    "App": "dialog",
    "App Type": "select",
    "AppUnit": "dialog",
    "Size": "select",
    "AdSource": "select",
    "Utm Source": "dialog"
  };

  termQueryWrapper(name) {
    return function(items) {
      if (items && items.length > 0) {
        var q = [];
        items.forEach(function(item) {
          var obj = {};
          obj[name] = item.id;
          q.push({ term: obj });
        });
        return { bool: { should: q } };
      }
      return null;
    }
  }
  queryOptions = {
    "User": this.termQueryWrapper("userId"),
    "Account Manager": this.termQueryWrapper("accountManager"),
    "App": this.termQueryWrapper("appId"),
    "App Type": this.termQueryWrapper("appType"),
    "AppUnit": this.termQueryWrapper("appUnitId"),
    "Size": this.termQueryWrapper("unitSize"),
    "AdSource": this.termQueryWrapper("adSource"),
    "Utm Source": this.termQueryWrapper("utm")
  }
  tableHeaderOptions = {
    "userId_term": "Publisher",
    "accountManager_term": "Account Manager",
    "appUnitId_term": "App Unit",
    "appId_term": "App",
    "appType_term": "App Type",
    "appUrl2_term": "App Url",
    "adSource_term": "Ad Source",
    "unitSize_term": "Size",
    "utm_term": "Utm Source",
    "statTime_term": "Date",
    "cc_term": "Country"
  }

  dimensions = [];
  metrics = [
    { id: "imps" }, { id: "clicks" }, { id: "pubRevenue" }, { id: "netRevenue" },
    { id: "pubRevenueUsd" }, { id: "netRevenueUsd" }];
  subscription: any;
  endDate: number;
  startDate: number;
  range: NbCalendarRange<Date>;
  constructor(
    private utilDateService: UtilDateService,
    private windowService: NbWindowService,
    public windowRef: NbWindowRef,
    public windowConfig: NbWindowConfig,
    private searchService: SearchData,
    public reportService: ReportData,
    private messageService: MessageService,
    private router: Router,
    private changeDetector: ChangeDetectorRef
  ) {

  }
  getQueryBoolFilter(queryArray) {
    return { "bool": { "filter": { "bool": { "must": queryArray } } } }
  }
  dimensionQuery(obj, dimensions) {

    var els = dimensions;
    var current = obj.aggs;
    for (var i = 0; i < els.length + 1; i++) {

      var aggs = current;
      var el = els[i];
      if (el) {
        if (el.id != "statTime")
          aggs[el.id + "_term"] = { terms: { field: el.id, size: el.size }, aggs: {} };
        else
          aggs[el.id + "_term"] = {
            "date_histogram": {
              "field": "statTime",
              "interval": "day"
            }, aggs: {}
          };
      }
      var preEl = els[i - 1];
      if (preEl && preEl.name) {
        aggs[preEl.name + "_tname"] = { terms: { field: preEl.name } }
      }
      if (el) {
        current = aggs[el.id + "_term"].aggs;
      }

    }
    return obj;

  }
  metricQuery(obj, metricList) {

    var lastObj = this.findLastAggs(obj);
    var lastAggObj = lastObj.aggs;
    // lastObj['terms']["order"]={"imps_sum":"desc"};

    for (var i = 0; i < metricList.length; i++) {
      var m = metricList[i];

      var sum_term = lastAggObj[m['id'] + "_sum"] = {};
      sum_term['sum'] = { field: m['id'] };
    }
    return obj;
  }
  findLastAggs(obj) {
    if (obj.aggs) {
      for (var k in obj.aggs) {
        if (k.indexOf("_term") > -1) {
          var n = obj.aggs[k];
          if (n.aggs) {
            return this.findLastAggs(n);
          }
        }
      }
      return obj;
    }
    else {
      return obj;
    }
  };

  pickMetrics(o, b) {
    o.metrics = {};
    o.metrics.imps = b['imps_sum'].value;
    o.metrics.clicks = b['clicks_sum'].value;

    o.metrics.netRevenue = b['netRevenue_sum'].value;
    o.metrics.pubRevenue = b["pubRevenue_sum"].value;
    o.metrics.netRevenueUsd = b["netRevenueUsd_sum"].value;
    o.metrics.pubRevenueUsd = b["pubRevenueUsd_sum"].value;
    o.metrics.profit = b['netRevenue_sum'].value - b["pubRevenue_sum"].value;

    /*  o.metrics.netRevenue=$filter('number')(b['netRevenue_sum'].value,2);
      o.metrics.pubRevenue=$filter('number')(b["pubRevenue_sum"].value,2);
      o.metrics.profit=$filter('number')(b['netRevenue_sum'].value - b["pubRevenue_sum"].value,2);*/
  }
  gl = [];
  totalGl = { metrics: {} };
  addToTotal(o) {
    for (var k in o.metrics) {
      this.totalGl.metrics[k] = this.totalGl.metrics[k] || 0;
      this.totalGl.metrics[k] += o.metrics[k];
    }
  }

  pushDenormalizedItem(o) {
    this.gl.push(o);
    this.addToTotal(o);
  }
  cloneObj(obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  getInverseObj(obj) {
    var inv = {};
    for (var k in obj) {
      var d = obj[k];
      inv[d] = k;
    }
    return inv;
  };
  displayDictionary = {
    "adSource": this.getInverseObj(this.adSources),
    "appType": this.getInverseObj(this.appTypes),
    "unitSize": this.getInverseObj(this.sizes),
    "accountManager": this.getInverseObj(this.accountManagers),
  }
  getKeyAs(bucket, nameTermKey, dimensionId) {
    if (nameTermKey && bucket[nameTermKey]) {
      return bucket[nameTermKey].buckets[0].key;
    }
    var key = null;
    if (this.displayDictionary[dimensionId]) {
      key = this.displayDictionary[dimensionId][bucket.key];
    }
    if (key) {
      return key;
    }
    return null;
  };
  resolveResult(result, obj, index, dimensions) {

    var d = dimensions[index];
    if (!d) {
      if (dimensions.length == 0) {


        var isNoDimensionMetricLevel = false;
        if (result) {
          for (var key in result) {
            if (key.indexOf("_sum") > -1) {
              isNoDimensionMetricLevel = true;
            }
          }
        }
        if (isNoDimensionMetricLevel) {
          o = {};
          o.dimensions = [];

          this.pickMetrics(o, result);
          this.pushDenormalizedItem(o);
        }



        return;
      }
      else {
        console.log("d is not defined", index, obj);
        return;
      }

    }
    var id = d['id'];
    var result = result[id + "_term"];
    if (result && result.buckets) {

      if (result['sum_other_doc_count'] > 0) {
        throw { "status": "size", "size": result['sum_other_doc_count'], index: index };
      }

      for (var i = 0; i < result.buckets.length; i++) {
        var b = result.buckets[i];

        var o = this.cloneObj(obj);
        var objName = this.tableHeaderOptions[id + '_term'] || (id + '_term');
        o['dimensions'] = o['dimensions'] || {};
        o['dimensions'][objName] = b.key;
        var isMetricLevel = false;
        var term = null;
        var nameTermKey = null;
        for (var key in b) {
          if (key.indexOf("_sum") > -1) {
            isMetricLevel = true;
          }
          if (key == "key" || key == "doc_count") {

            continue;
          }
          if (key.indexOf("_tname") > -1) {
            nameTermKey = key;
            continue;
          }
          if (key.indexOf("_term") > -1) {
            term = b;
          }

        }

        var keyAs = this.getKeyAs(b, nameTermKey, id);
        if (keyAs) {
          if (nameTermKey == "statTime_tname") {
            var dateStr = (new Date(parseInt(keyAs))).toISOString();
            o['dimensions'][objName] = dateStr.substring(0, dateStr.indexOf("T"));
          } else {
            o['dimensions'][objName] = keyAs + "(" + o['dimensions'][objName] + ")";
          }
        }

        if (isMetricLevel) {

          this.pickMetrics(o, b);
          this.pushDenormalizedItem(o);
        }
        else {
          this.resolveResult(term, o, index + 1, dimensions);
        }

      }
    }
    else {
      console.log("no bucket found..", result, id);
    }


  }
  createQuery() {

    let filterTypes = ["User", "Account Manager", 'App', 'App Type', 'AppUnit', "Size", "AdSource", "Utm Source"];

    for (let i = 0; i < filterTypes.length; i++) {

      let filterType = filterTypes[i];

      if (this.typeOptions[filterType]) {
        this.filters.push({
          display: filterType, id: filterType, items: [],
          type: this.typeOptions[filterType], selectOptions: this.selectOptions[filterType]
        });
      }

    }

    this.dimensions = [
      { display: "Date", id: "statTime", "name": "statTime", order: 1, selected: this.reportService.customSearchObject.isSelectedDate, size: 2000 },
      { display: "User", id: "userId", "name": "publisherName", order: 2, selected: this.reportService.customSearchObject.isSelectedUser, size: 2000 },
      { display: "App", id: "appId", "name": "appName", order: 3, selected: this.reportService.customSearchObject.isSelectedApp, size: 2000 },
      { display: "AppUnit", id: "appUnitId", "name": "appUnitName", order: 4, selected: this.reportService.customSearchObject.isSelectedAppUnit, size: 2000 },
      { display: "AdSource Type", id: "adSource", order: 5, selected: this.reportService.customSearchObject.isSelectedAddSource, size: 2000 },
      { display: "Size", id: "unitSize", order: 6, selected: this.reportService.customSearchObject.isSelectedSize, size: 2000 },
      {
        display: "Account Manager", id: "accountManager", "name": "accountManager", order: 7, selected:
          this.reportService.customSearchObject.isSelectedAccManager, size: 2000
      },
      { display: "App Type", id: "appType", order: 8, selected: this.reportService.customSearchObject.isSelectedAppType, size: 2000 },
      { display: "App Url", id: "appUrl2", order: 9, selected: this.reportService.customSearchObject.isSelectedAppUrl, size: 2000 },
      { display: "Utm Source", id: "utm", order: 10, selected: this.reportService.customSearchObject.isSelectedUtmSource, size: 2000 },
      { display: "Country", id: "cc", order: 11, selected: this.reportService.customSearchObject.isSelectedCc, size: 2000 }
    ];


    var queryArray = [this.rangeQuery(this.startDate, this.endDate)];

    for (var i = 0; i < this.filters.length; i++) {
      var filter = this.filters[i];
      if (this.reportService.selecteds[filter.id]) {
        filter.items = this.reportService.selecteds[filter.id];
      }
      var queryGetter = this.queryOptions[filter.id];
      if (queryGetter) {
        var q = queryGetter(filter.items);
        if (q) {
          queryArray.push(q);
        }
      }
    }

    var dimensions = this.dimensions.filter(function(item) {
      return item.selected;
    }).sort(function(a, b) {
      return a.order - b.order;
    }).map(function(item) {
      var obj = { id: "", size: 0, name: "" };
      obj.id = item.id;
      obj.size = item.size || 20000;
      if (item.name) {
        obj.name = item.name;
      }
      return obj;
    });

    var query = this.getQueryBoolFilter(queryArray);
    var obj = { aggs: {}, query: {} };
    obj = this.dimensionQuery(obj, dimensions);
    obj.query = query;
    obj = this.metricQuery(obj, this.metrics);
    this.runReport(obj, dimensions);

  }
  metricNameOptions = {
    "clicks": "Click",
    "imps": "Imp",
    "netRevenue": "Revenue",
    "pubRevenue": "Cost",
    "netRevenueUsd": "Revenue(USD)",
    "pubRevenueUsd": "Cost(USD)",
    "profit": "Profit",
    "ecpm": "eCPM",
    "pecpm": "pCPM"
  };
  runReport(obj, dimensions) {
    var onProgress = true;
    if (this.startDate && this.endDate) {
      var data = { startDate: this.startDate, endDate: this.endDate };
      this.messageService.sendCustomTime(data);
    }
    var sendData = [];
    var columns = {};

    this.reportService.getCustomData(obj).subscribe((data: {}) => {
      this.gl = [];
      this.totalGl = { metrics: {} };

      try {
        this.resolveResult(data['aggregations'], {}, 0, dimensions);
      }
      catch (e) {
        /*	var newObj = {aggs:{},query:{}};
          dimensions[e.index]
          obj = dimensionQuery(obj,dimensions);
          console.log(e);*/
        console.log(e);
        if (e && e.status && e.status == "size") {
          //{status: "size", size: 4, index: 0}
          dimensions[e.index].size = e.size;
          var newQuery = { aggs: {}, query: {} };
          newQuery = this.dimensionQuery(newQuery, dimensions);
          obj.aggs = newQuery.aggs;
          newQuery.query = obj.query;
          newQuery = this.metricQuery(newQuery, this.metrics);
          this.runReport(newQuery, dimensions);
          return;
        }
      }



      var result = {};
      var arr = this.gl;
      if (this.gl.length > 0) {
        result['done'] = 1;
        var totalEcpm = this.totalGl['metrics']['imps'] != 0 ? 1000.0 * (this.totalGl['metrics']['netRevenue'] / this.totalGl['metrics']['imps']) : 0;
        var totalPecpm = this.totalGl['metrics']['imps'] != 0 ? 1000.0 * (this.totalGl['metrics']['pubRevenue'] / this.totalGl['metrics']['imps']) : 0;
        this.totalGl['metrics']['ecpm'] = Number(totalEcpm).toFixed(2);
        this.totalGl['metrics']['pecpm'] = Number(totalPecpm).toFixed(2);
        var heads = [];
        for (var k in this.gl[0]) {

          if (k == "metrics") {
            for (var a in this.gl[0]["metrics"]) {
              heads.push({ id: a, name: this.metricNameOptions[a], type: "metrics", "order": "asc" });
            }
            heads.push({ id: "ecpm", name: this.metricNameOptions["ecpm"], type: "metrics", "order": "asc" });
            heads.push({ id: "pecpm", name: this.metricNameOptions["pecpm"], type: "metrics", "order": "asc" });

          }
          else if (k == "dimensions") {
            for (var a in this.gl[0]["dimensions"]) {
              heads.push({ name: a, type: "dimensions" });
            }
          }
          else {
            //heads.push(k);
          }
        }

        result['total'] = this.totalGl;
        result['heads'] = heads;
        result['bodies'] = this.gl.sort((a, b) => {
          return a.clicks - b.clicks;
        });
        result['bodies'].forEach(v => {
          var ecpm = v.metrics.imps != 0 ? 1000.0 * (v.metrics.netRevenue / v.metrics.imps) : 0;
          var pecpm = v.metrics.imps != 0 ? 1000.0 * (v.metrics.pubRevenue / v.metrics.imps) : 0;
          v.metrics.ecpm = Number(ecpm).toFixed(2);;
          v.metrics.pecpm = Number(pecpm).toFixed(2);;
        });

        onProgress = false;
      }
      else {
        onProgress = false;
      }

      for (var i = 0; i < this.dimensions.length; i++) {
        if (this.dimensions[i].selected) {
          columns[this.dimensions[i].id] = {
            title: this.dimensions[i].display,
            type: 'string',
          }
        }

      }
      sendData.push({ data, columns, arr });

      this.messageService.sendCustomColumns(sendData);
    });
    this.windowRef.close();

    this.router.navigateByUrl('/pages/custom-report-table');
  }




  onSearchUser(event) {
    event.term = event.term.toLocaleLowerCase();
    if (event.term.length > 2) {

      this.searchService.getSearchUserData(event.term).subscribe((data: {}) => {
        this.userDatas = [];
        if (data['hits'].total && data['hits'].total > 0) {
          data['hits']['hits'].forEach(hits => {
            this.userDatas.push({ name: hits['_source'].fullName, id: hits['_source'].id + "" });
          });
        }
        else {
          this.userDatas = [];
        }

      });
    }
    else {
      this.userDatas = [];
    }
  }
  onSearchApp(event) {
    event.term = event.term.toLocaleLowerCase();
    if (event.term.length > 2) {

      this.reportService.getSearchAppData(event.term).subscribe((data: {}) => {
        this.appDatas = [];
        if (data['hits'].total && data['hits'].total > 0) {
          data['hits']['hits'].forEach(hits => {
            this.appDatas.push({ name: hits['_source'].appName, id: hits['_source'].id + "" });
          });
        }
        else {
          this.appDatas = [];
        }
      });
    }
    else {
      this.appDatas = [];
    }
  }
  onSearchAppUnit(event) {
    event.term = event.term.toLocaleLowerCase();
    if (event.term.length > 2) {

      this.searchService.getSearchAppUnitData(event.term).subscribe((data: {}) => {
        this.appUnitDatas = [];
        if (data['hits'].total && data['hits'].total > 0) {
          data['hits']['hits'].forEach(hits => {
            this.appUnitDatas.push({ name: hits['_source'].unitName, id: hits['_source'].id + "" });
          });
        }
        else {
          this.appUnitDatas = [];
        }
      });
    }
    else {
      this.appUnitDatas = [];
    }
  }
  onSearchUtmSource(event) {
    event.term = event.term.toLocaleLowerCase();
    if (event.term.length > 0) {

      this.searchService.getSearchUtmSourceData(event.term).subscribe((data: {}) => {
        this.utmSources = [];
        if (data['hits'].total && data['hits'].total > 0) {
          data['hits']['hits'].forEach(hits => {
            this.utmSources.push({ name: hits['_source'].utmSource, id: hits['_source'].utmSource });
          });
        }
        else {
          this.utmSources = [];
        }
      });
    }
    else {
      this.utmSources = [];
    }
  }
  clear() {

  }


  setRange() {
    var date = new Date();
    var timezoneOffset = date.getTimezoneOffset();
    if (this.rangepicker.value.start && this.rangepicker.value.end) {

      this.startDate = new Date(this.rangepicker.value.start.getFullYear(),
        this.rangepicker.value.start.getMonth(), this.rangepicker.value.start.getDate()).getTime();
      this.startDate = this.startDate - timezoneOffset * 60 * 1000;
      this.endDate = new Date(this.rangepicker.value.end.getFullYear(),
        this.rangepicker.value.end.getMonth(), this.rangepicker.value.end.getDate()).getTime();
      this.endDate = this.endDate - timezoneOffset * 60 * 1000 + 24 * 60 * 60 * 1000;
      this.reportService.rangeTime.startDate = this.startDate;
      this.reportService.rangeTime.endDate = this.endDate;
      this.reportService.rangeTime.controlData = 1;
    }

  }
  onClick() { }
  excelReport() { }
  cancel() {
    this.windowRef.close();
  }
  @ViewChild('rangepicker') rangepicker;
  close() {
    this.windowRef.close();
  }
  ngOnInit() {
    console.log(this.reportService.selecteds['Account Manager']);
    this.range = {
      start: new Date(parseInt(this.reportService.rangeTime.startDate + "")),
      end: new Date(parseInt(this.reportService.rangeTime.endDate - 24 * 60 * 60 * 1000 + "")),
    };

    if (this.reportService.rangeTime.controlData == 0) {
      this.startDate = this.reportService.rangeTime.startDate;
      this.endDate = this.reportService.rangeTime.endDate;
    }
    else {
      this.startDate = this.reportService.rangeTime.startDate
      this.endDate = this.reportService.rangeTime.endDate - 24 * 60 * 60 * 1000;
    }
  }
}
