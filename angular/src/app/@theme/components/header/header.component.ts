
import { Component, Input, OnInit } from '@angular/core';
import { NbMenuService, NbSidebarService, NbSearchService, NbWindowService } from '@nebular/theme';
import { AnalyticsService } from '../../../@core/utils';
import { LayoutService } from '../../../@core/utils';
import { CustomReportComponent } from './custom-report/custom-report.component';
import { Router } from '@angular/router';
import { MessageService } from '../../../@core/mock/common/message-service';
import { ReportData } from '../../../@core/interfaces/common/reportData';
import { SearchData } from '../../../@core/interfaces/common/searchData';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {
  userAppDatas: object[] = [];
  subscription: any;
  startDate: number;
  endDate: number;
  constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private analyticsService: AnalyticsService,
    private layoutService: LayoutService,
    private searchService: SearchData,
    private windowService: NbWindowService,

    private router: Router, private messageService: MessageService, public reportService: ReportData) {
  }
  @Input() position = 'normal';
  user: any;
  userMenu = this.getMenuItems();
  getMenuItems() {
    const userLink = this.user ? `/pages/users/edit/${this.user.id}` : '';
    return [
      { title: 'Profile', link: userLink, queryParams: { profile: true } },
      { title: 'Log out', link: '/auth/logout' },
    ];
  }
  selecteds = {};
  onSearchUserApp(event) {
    event.term = event.term.toLocaleLowerCase();
    var i = 0;
    var j = 0;
    if (event.term.length > 2) {

      this.searchService.getSearchUserData(event.term).subscribe((userData: {}) => {

        this.searchService.getSearchAppData(event.term).subscribe((AppData: {}) => {
          this.userAppDatas = [];

          if (userData['hits'].total && userData['hits'].total > 0) {
            userData['hits']['hits'].forEach(hits => {
              i++;
              if (i < 6) {
                this.userAppDatas.push({ group: "Publisher", name: hits['_source'].fullName, id: hits['_source'].id + "" });
              }
            });
            i = 0;
          }

          if (AppData['hits'].total && AppData['hits'].total > 0) {
            AppData['hits']['hits'].forEach(hits => {
              j++;
              if (j < 6) {
                this.userAppDatas.push({ group: "Site", name: hits['_source']['appName'], id: hits['_id'] });
              }
            });
            j = 0;
          }

          if (userData['hits'].total && userData['hits'].total <= 0 && AppData['hits'].total && AppData['hits'].total <= 0) {
            this.userAppDatas = [];
          }
        });
      });
    }
    else {
      this.userAppDatas = [];
    }

  }

  ngOnInit() {
  }
  onChangeUserApp(event) {

    var id: number;

    if (event && event.group == "Site") {
      id = event.id
      this.router.navigateByUrl('/pages/site-detail/' + id);
    }
    else if (event && event.group == "Publisher") {
      id = event.id
      this.router.navigateByUrl('/pages/publisher-detail/' + id);
    }
    this.selecteds = {};
    this.userAppDatas = [];
  }
  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();
    return false;
  }
  goToHome() {
    this.menuService.navigateHome();
  }
  openWindowForm() {

    this.windowService.open(CustomReportComponent, { title: `Report Options` });

  }
}
