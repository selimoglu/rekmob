/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'currencyTl' })
export class CurrencyTlPipe implements PipeTransform {
  
  transform(input: number): string {
    return new Intl.NumberFormat('tr-TR', { style: 'currency', currency: 'TRY' }).format(input);
  }

}
