/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { CampaignDetailsComponent } from './campaign-details.component';
import { StatusCardComponent } from './status-card/status-card.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
@NgModule({
  imports: [
    ThemeModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    CampaignDetailsComponent,
    StatusCardComponent,
  ],
  providers: [
  ],
})
export class CampaignDetailsModule { }
