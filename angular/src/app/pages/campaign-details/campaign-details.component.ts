import { Component, OnDestroy, ViewChild, OnInit } from '@angular/core';
import { ReportData } from '../../@core/interfaces/common/reportData';
import { StatData, campaignStatsData, campaignStatsDataZero, campaignTotal } from '../../@core/interfaces/common/statData';
import { MessageService } from '../../@core/mock/common/message-service';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'campaign-details',
  templateUrl: './campaign-details.component.html',
  styleUrls: ['./campaign-details.component.scss'],
})
export class CampaignDetailsComponent implements OnDestroy, OnInit {
  datas = [];
  dataZeros = [];
  private alive = true;
  campaignStatsDatas: campaignStatsData[];
  campaignStatsDatasZero: campaignStatsDataZero[];
  campaignTotal: campaignTotal[];

  endDate: number;
  startDate: number;

  settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      status: {
        title: 'Durum',
        type: 'string',
      },
      adName: {
        title: 'Advertising Name',
        type: 'string',
      },
      impression: {
        title: 'Impressions',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        },
      },
      click: {
        title: 'Clicks',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        },
      },
      dailyBudget: {
        title: 'Daily Budget',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat('tr-TR', { style: 'currency', currency: 'TRY' }).format(number);
        },
      },
      to: {
        title: 'TO ',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat('tr-TR', { style: 'currency', currency: 'TRY' }).format(number);
        },
      },
      bgbg: {
        title: 'BGBG',
        type: 'number ',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat('tr-TR', { style: 'currency', currency: 'TRY' }).format(number);
        },
      },
      cost: {
        title: 'Cost',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat('tr-TR', { style: 'currency', currency: 'TRY' }).format(number);
        },
      },
      edit: {
        title: ' ',
        type: 'number',
      },
    },
  };
  source: Array<LocalDataSource> = new Array<LocalDataSource>();
  sourceZero: Array<LocalDataSource> = new Array<LocalDataSource>();
  constructor(private reportService: ReportData, private messageService: MessageService, private statService: StatData) {

  }
  @ViewChild('item') accordion;
  campaignDatas = [];
  campaignDatasZero = [];
  tableData = [];
  tableDataZero = [];
  ngOnInit() {
    this.reportService.getCampaigns().subscribe((data: {}) => {
      this.statService.campaignList().pipe().subscribe(campaignTotalData => {
        this.campaignStatsDatas = campaignTotalData.filter(x => x.settings);
      });
      this.statService.campaignListZero().pipe().subscribe(campaignTotalDataZero => {
        this.campaignStatsDatasZero = campaignTotalDataZero.filter(x => x.settings);
      });
      this.statService.campaignTot().pipe().subscribe(campaignTotal => {
        this.campaignTotal = campaignTotal.filter(x => x.settings);
      });
      var totalImp = 0;
      var totalClick = 0;
      var totalCost = 0;
      console.log(data);
      var j = 0;
      var k = 0;
      totalImp = data['body']['list'][0]['totalStatUtil'].totalImp;
      totalClick = data['body']['list'][0]['totalStatUtil'].totalClick;
      totalCost = data['body']['list'][0]['totalStatUtil'].totalCost;
      this.campaignDatas.push([totalImp, totalClick, totalCost]);
      data['body']['list'][0]['campaignList'].forEach(campaignList => {

        totalImp = campaignList['totalStatUtil'].totalImp;
        totalClick = campaignList['totalStatUtil'].totalClick;
        totalCost = campaignList['totalStatUtil'].totalCost;
        if (totalImp == 0 && totalClick == 0 && totalCost == 0) {
          this.campaignStatsDatasZero[0].amount = totalImp;
          this.campaignStatsDatasZero[1].amount = totalClick;
          this.campaignStatsDatasZero[2].amount = totalCost;
          this.campaignDatasZero.push([totalImp, totalClick, totalCost]);
          this.dataZeros.push({ name: campaignList.campaignName });
          this.sourceZero[k] = new LocalDataSource();
        }
        else {
          this.campaignStatsDatas[0].amount = totalImp;
          this.campaignStatsDatas[1].amount = totalClick;
          this.campaignStatsDatas[2].amount = totalCost;
          this.campaignDatas.push([totalImp, totalClick, totalCost]);
          this.datas.push({ name: campaignList.campaignName });
          this.source[j] = new LocalDataSource();
        }
        campaignList['adList'].forEach(adList => {
          if (totalImp == 0 && totalClick == 0 && totalCost == 0) {
            this.tableDataZero.push({
              adName: adList.adName, impression: adList['totalStatUtil'].totalImp,
              click: adList['totalStatUtil'].totalClick, dailyBudget: adList.dailyBudget,
              to: adList['totalStatUtil'].ctr, bgbg: adList['totalStatUtil'].cpm, cost: adList['totalStatUtil'].totalCost,
            });
          }
          else {
            this.tableData.push({
              adName: adList.adName, impression: adList['totalStatUtil'].totalImp,
              click: adList['totalStatUtil'].totalClick, dailyBudget: adList.dailyBudget,
              to: adList['totalStatUtil'].ctr, bgbg: adList['totalStatUtil'].cpm, cost: adList['totalStatUtil'].totalCost,
            });
          }
        });
        if (totalImp == 0 && totalClick == 0 && totalCost == 0) {
          this.sourceZero[k].load(this.tableDataZero);
          this.tableDataZero = [];
          k++;
        }
        else {
          this.source[j].load(this.tableData);
          this.tableData = [];
          j++;
        }
      });
    });
  }
  toggle() {
    this.accordion.toggle();
  }
  ngOnDestroy() {
    this.alive = false;
  }

}
