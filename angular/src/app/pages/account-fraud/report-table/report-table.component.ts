/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableDataTwo } from '../../../@core/interfaces/common/smart-table-two';
import { NbSidebarService } from '@nebular/theme';
import { StateService } from '../../../@core/utils';
import { ReportData } from '../../../@core/interfaces/common/reportData';
import { Router } from '@angular/router';

@Component({
  selector: 'fraud-table',
  templateUrl: './report-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
  styleUrls: ['./report-table.component.scss'],
})
export class FraudTableComponent implements OnInit {
  status = [];
  stat = true;
  settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: 'Name',
        type: 'string',
      },
      imp: {
        title: 'Impression',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        },
      },
      click: {
        title: 'Click',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        },
      },
      revenue: {
        title: 'Revenue',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(number);
        },
      },
      cost: {
        title: 'Cost',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(number);
        },
      },
      profit: {
        title: 'Profit',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(number);
        },
      },
      ctr: {
        title: 'Ctr',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        },
      },
      fraud: {
        title: 'Fraud',
        type: 'html',
        valuePrepareFunction: (cell, row) => {
          if (row.ctr < 1) {
            return ' <i  class="fas fa-stop green" ></i> ';
          }

          else if (row.ctr > 3 && row.imp >= 100) {
            return ' <i  class="fas fa-stop red"></i> ';
          }
          else {
            return ' <i  class="fas fa-stop orange"  ></i> ';
          }
        }
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  sidebarEnd = false;
  constructor(private service: SmartTableDataTwo, private sidebarService: NbSidebarService,
    protected stateService: StateService, private reportService: ReportData) {

  }
  ngOnInit() {
    this.reportService.getAppUnitForFraud().subscribe((data: {}) => {
      const tableData = [];
      if (data['aggregations']['appUnitId_term']['buckets']) {
        data['aggregations']['appUnitId_term']['buckets'].forEach(buckets => {
          var status;
          var name = buckets['appUnitName_tname']['buckets'][0]['key'];
          var imp = Number(buckets['imps_sum']['value']);
          var click = Number(buckets['clicks_sum'].value);
          var revenue = Number(buckets['netRevenue_sum'].value);
          var cost = Number(buckets['pubRevenue_sum'].value);
          var profit = revenue - cost;
          var ctr = imp != 0 ? click / imp : 0;
          tableData.push({
            name: name, imp: imp, click: click,
            revenue: revenue, cost: cost, profit: profit,
            ctr: 100* ctr,
          });

        });

        tableData.sort((a, b) => b.ctr - a.ctr)
        this.source.load(tableData);
      }

    });
  }


}
