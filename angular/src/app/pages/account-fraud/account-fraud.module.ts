/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { AccountFraudComponent } from './account-fraud.component';
import { FraudTableComponent } from './report-table/report-table.component';
@NgModule({
  imports: [
    ThemeModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    AccountFraudComponent,
    FraudTableComponent,
  ],
  providers: [
  ],
})
export class AccountFraudModule { }
