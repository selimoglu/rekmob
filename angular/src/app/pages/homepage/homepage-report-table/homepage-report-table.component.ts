/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { ReportData } from '../../../@core/interfaces/common/reportData';
import { StatData } from '../../../@core/interfaces/common/statData';
import { NbSidebarService } from '@nebular/theme';
import { MessageService } from '../../../@core/mock/common/message-service';
import { LayoutService } from '../../../@core/utils';
import { statsData } from '../../../@core/interfaces/common/statData';
import { Subscription } from 'rxjs';
import { UtilDateService } from '../../../@core/mock/common/util-date.service';
@Component({
  selector: 'homepage-report-table',
  templateUrl: './homepage-report-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
      
    }
  `],
})
export class HomepageReportTableComponent implements OnInit {

  settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'Id',
        type: 'number',
      },
      firstName: {
        title: 'Name',
        type: 'string',
      },
      impression: {
        title: 'Imp ',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        },
      },
      click: {
        title: 'Click',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        },
      },
      pCpm: {
        title: 'pCpm',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat('tr-TR', { style: 'currency', currency: 'TRY' }).format(number);
        },
      },
      nCpm: {
        title: 'nCpm ',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat('tr-TR', { style: 'currency', currency: 'TRY' }).format(number);
        },
      },
      revenue: {
        title: 'Revenue',
        type: 'number ',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat('tr-TR', { style: 'currency', currency: 'TRY' }).format(number);
        },
      },
      cost: {
        title: 'Cost',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat('tr-TR', { style: 'currency', currency: 'TRY' }).format(number);
        },
      },
      profit: {
        title: 'Profit',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat('tr-TR', { style: 'currency', currency: 'TRY' }).format(number);
        },
      },
       ctr: {
        title: 'Ctr',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        },
      },
    },
  };
  type = "today";
  typeTexts = [{ text: 'Today', value: 'today' },
  { text: 'Yesterday', value: 'yesterday' },
  { text: 'Last Seven Days', value: 'lastSevenDays' },
  { text: 'This Month', value: 'thisMonth' },
  { text: 'Last Month', value: 'lastMonth' }];
  currentTheme: string;
  sidebarEnd = false;
  endDate: number;
  startDate: number;
  source: LocalDataSource = new LocalDataSource();
  statsDatas: statsData[];
  subscription: Subscription;
  id: string;
  sidebarState: boolean;
  constructor(private utilDateService: UtilDateService, private reportService: ReportData, private statService: StatData,
    private layoutService: LayoutService, private messageService: MessageService,
    private sidebarService: NbSidebarService) {
  }
  getDateRange(i: string) {
    var date = new Date();
    var timezoneOffset = date.getTimezoneOffset();
    if (!this.startDate || !this.endDate) {
      this.startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() - timezoneOffset * 60 * 1000;
      this.endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() + 24 * 60 * 60 * 1000 - timezoneOffset * 60 * 1000;
    }
    this.utilDateService.rangeTimeData(i).subscribe((data: {}) => {
      this.startDate = data['startDate'];
      this.endDate = data['endDate'];

    });
    this.refreshTableData(this.startDate, this.endDate);
  }
  refreshTableData(startDate, endDate): void {
    this.reportService.getGeneralReportData(startDate, endDate).subscribe((data: {}) => {
      const tableData = [];
      if (data['aggregations']['userId_term']['buckets']) {
        data['aggregations']['userId_term']['buckets'].forEach(buckets => {
          var takeImpression = Number(buckets['imps_sum'].value);
          var takeNetRevenue = Number(buckets['netIncome_sum'].value);
          var takeCost = Number(buckets['income_sum'].value);
          tableData.push({ id: buckets.key, firstName: buckets['publisherName_term']['buckets'][0].key,
           impression: takeImpression, click: Number(buckets['clicks_sum'].value),
            pCpm: (takeImpression != 0 ? (1000 * takeCost) / takeImpression : 0), 
            nCpm: (takeImpression != 0 ? (1000 * takeNetRevenue) / takeImpression : 0),
             revenue: takeNetRevenue, cost: takeCost, profit: buckets['profit_sum'].value,
             ctr:(Number(buckets['clicks_sum'].value))/ takeImpression});
        });
        this.source.load(tableData);
      }

    });
    this.statService.list().pipe().subscribe(totalData => {
      this.statsDatas = totalData.filter(x => x.settings);
    });
    this.reportService.getTotalImpAndClickData(startDate, endDate).subscribe((impClickData: {}) => {

      this.statsDatas[2].amount = impClickData['aggregations'].imps_sum.value;
      this.statsDatas[3].amount = impClickData['aggregations'].clicks_sum.value;

    });
    this.reportService.getTotalRevenueAndProfitData(startDate, endDate).subscribe((revenueProfitData: {}) => {
      var totalRevenue = 0;
      var totalCost = 0;
      var totalProfit = 0;
      if (revenueProfitData['body']['aggregations']['adSource_term']['buckets']) {
        revenueProfitData['body']['aggregations']['adSource_term']['buckets'].forEach(buckets => {
          buckets['accountManager_term']['buckets'].forEach(buckets => {
            totalRevenue += Number(buckets['netRevenue_sum'].value);
            totalCost += Number(buckets['pubRevenue_sum'].value);
            totalProfit = totalRevenue - totalCost;
          });
        });
        this.statsDatas[0].amount = totalRevenue;
        this.statsDatas[1].amount = totalProfit;
      }
      else {
        this.statsDatas[0].amount = 0;
        this.statsDatas[1].amount = 0;
      }
    });

  }
  onUserRowSelect(event): void {
    this.sidebarState = this.messageService.getSidebarState();
    if (this.id == event.data.id || !this.sidebarState) {
      this.sidebarState = !this.sidebarState;
      this.sidebarService.toggle(true, 'menu-sidebar');
      this.layoutService.changeLayoutSize();
      this.messageService.sendGeneralData(event.data);
      this.sidebarService.toggle(false, 'settings-sidebar');
    } else if (this.sidebarState && this.id != event.data.id) {
      this.messageService.sendGeneralData(event.data);
    }
    this.id = event.data.id;
    this.messageService.setSidebarState(this.sidebarState);

  }
  ngOnInit() {

    this.getDateRange("today");

  }
}
