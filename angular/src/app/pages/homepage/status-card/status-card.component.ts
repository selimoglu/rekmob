/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';
@Component({
  selector: 'ngx-status-card',
  styleUrls: ['./status-card.component.scss'],
  template: `
    <nb-card  [ngClass]="{'off': !on}">
      <div class="icon-container">
        <div class="icon {{ type }}">
          <ng-content></ng-content>
        </div>
      </div>

      <div class="details">
        <div class="title">{{ title  }}</div>
        <div class="status">{{ subtitle }}</div>
      </div>
    </nb-card>
  `,
})
export class StatusCardComponent {
  @Input() title: number;
  @Input() type: string;
  @Input() subtitle: string;
  @Input() on = true;
  @Output() statusChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

 
  onClick() {
    this.on = !this.on;
    this.statusChanged.emit(this.on);
  }
}
