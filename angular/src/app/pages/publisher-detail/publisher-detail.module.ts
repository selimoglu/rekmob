import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { PublisherDetailComponent } from './publisher-detail.component';
import { PublisherDetailReportTableComponent,ButtonUserComponent, ButtonUserBanComponent, BanDialogComponent } from './publisher-detail-report-table/publisher-detail-report-table.component';

@NgModule({
  imports: [
    ThemeModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    PublisherDetailComponent,
    PublisherDetailReportTableComponent,
    ButtonUserComponent,
    ButtonUserBanComponent,
    BanDialogComponent
  ],
  providers: [
  ],
  entryComponents: [ButtonUserComponent,ButtonUserBanComponent,BanDialogComponent]
})
export class PublisherDetailModule { }
