/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */
import { Component, OnDestroy } from '@angular/core';

@Component({
  selector: 'publisher-detail',
  templateUrl: './publisher-detail.component.html',
})
export class PublisherDetailComponent implements OnDestroy {

  private alive = true;

  constructor() {
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
