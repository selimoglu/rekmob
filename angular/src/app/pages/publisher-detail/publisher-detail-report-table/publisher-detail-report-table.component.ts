import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LocalDataSource, ViewCell } from 'ng2-smart-table';
import { ReportData } from '../../../@core/interfaces/common/reportData';
import { Router } from '@angular/router';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
@Component({
  selector: 'ban-dialog',
  template: `
  <nb-card>
    <nb-card-header>Do you want to ban {{title}} ?</nb-card-header>
    <nb-card-body style="text-align:center">
    <button (click)="banUserClick()" style="margin-right:15px;text-transform: none;background-color:#db415b" nbButton size="small" shape="semi-round">Ban</button>
    <button (click)="close()" style="text-transform: none;background-color:#a4abb3;cursor:pointer" nbButton size="small" shape="semi-round">Kapat</button>
    </nb-card-body>
</nb-card>

  `,
})
export class BanDialogComponent {
  @Input() value: string | number;
  @Input() rowData: any;

  @Output() save: EventEmitter<any> = new EventEmitter();
  constructor(protected ref: NbDialogRef<BanDialogComponent>, private router: Router, public reportService: ReportData, ) {
  }
  title = this.reportService.rowData['name']
  banUserClick() {
    this.reportService.banUser(this.reportService.rowData['id']).subscribe((data: {}) => {
      this.ref.close();
      window.location.reload();
    });
  }
  close() {
    this.ref.close();
  }
}

@Component({
  selector: 'button-user',
  template: `
  <nb-actions size="medium">
  <nb-action icon="ion-chevron-right"  (click)="onClick()" ></nb-action>
  </nb-actions>
  `,
})
export class ButtonUserComponent {
  @Input() value: string | number;
  @Input() rowData: any;

  @Output() save: EventEmitter<any> = new EventEmitter();
  constructor(private router: Router) {
  }
  onClick() {
    this.save.emit(this.rowData);
    this.router.navigateByUrl('/pages/publisher-detail/' + this.rowData.id);
  }
}
@Component({
  selector: 'button-user-ban',
  template: `
  <button nbButton shape="semi-round" (click)="banControlClick()" status="danger" style="cursor:pointer"  size="xsmall" class="btn-demo">Ban</button>

  `,
})
export class ButtonUserBanComponent {
  @Input() value: string | number;
  @Input() rowData: any;

  @Output() save: EventEmitter<any> = new EventEmitter();
  constructor(private router: Router, private dialogService: NbDialogService, public reportService: ReportData) {
  }
  banControlClick() {
    this.reportService.rowData = this.rowData;
    this.dialogService.open(BanDialogComponent, {
    });
  }

}
@Component({
  selector: 'publisher-detail-report-table',
  templateUrl: './publisher-detail-report-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class PublisherDetailReportTableComponent implements OnInit {

  settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    columns: {
      id: {
        title: 'Id',
        type: 'number',

      },
      name: {
        title: 'Ad-soyad',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
      },
      active: {
        title: 'Active',
        type: 'string',
      },
      credit: {
        title: 'Kredi',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        },
      },
      profit: {
        title: 'Kazanç',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat('tr-TR', { style: 'currency', currency: 'TRY' }).format(number);
        },
      },
      userBan: {
        title: 'Ban',
        type: 'custom',
        renderComponent: ButtonUserBanComponent,
      },
      user: {
        title: 'User',
        type: 'custom',
        renderComponent: ButtonUserComponent,
      },

    },
  };

  source: LocalDataSource = new LocalDataSource();
  sidebarEnd = false;
  constructor(private reportService: ReportData) {
  }
  ngOnInit() {
    this.reportService.getUserData().subscribe((data: {}) => {
      console.log(data);
      const tableData = [];
      data['hits']['hits'].forEach(hits => {
        tableData.push({ id: hits['_source'].id, name: hits['_source'].fullName, email: hits['_source'].email, active: hits['_source'].active, credit: hits['_source'].credit, profit: hits['_source'].income })
      });
      this.source.load(tableData);
    });
  }
}
