/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { CampaignReportsComponent } from './campaign-reports.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { EchartsMultipleXaxisComponent } from './echarts/echarts-multiple-xaxis.component';
import { EchartsAreaStackComponent } from './echarts/echarts-area-stack.component';
import { D3AdvancedPieComponent } from './d3/d3-advanced-pie.component';
@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    NgxChartsModule,
  ],
  declarations: [
    CampaignReportsComponent,
    EchartsMultipleXaxisComponent,
    EchartsAreaStackComponent,
    D3AdvancedPieComponent,
  ],
  providers: [
  ],
})
export class CampaignReportsModule { }
