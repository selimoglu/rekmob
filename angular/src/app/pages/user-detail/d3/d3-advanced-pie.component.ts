/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { Subscription } from 'rxjs';
import { MessageService } from '../../../@core/mock/common/message-service';

@Component({
  selector: 'ngx-d3-advanced-pie',
  styleUrls: ['./d3.component.scss'],
  template: `
    <ngx-charts-advanced-pie-chart
      [scheme]="colorScheme"
      [results]="single">
    </ngx-charts-advanced-pie-chart>
  `,
})
export class D3AdvancedPieComponent implements OnDestroy,OnInit {
  single = [
    {
      name: 'Impression',
      value: 8940000,
    },
    {
      name: 'Clicks',
      value: 5000000,
    },
    {
      name: 'Revenue',
      value: 3200000,
    },
    {
      name: 'Profit',
      value: 3200000,
    },
    {
      name: 'Cost',
      value: 3200000,
    },
  ];
  colorScheme: any;
  themeSubscription: any;
  subscription: Subscription;
  constructor(private theme: NbThemeService, private messageService: MessageService) {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.colorScheme = {
        domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
      };
    });
  }
ngOnInit(){
  this.subscription = this.messageService.getAppData().subscribe(appData => {
    
  
  });
}

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
