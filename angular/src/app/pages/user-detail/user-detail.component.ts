import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DayCellComponent } from './day-cell/day-cell.component';
import { MessageService } from '../../@core/mock/common/message-service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ReportData } from '../../@core/interfaces/common/reportData';
import { switchMap, takeWhile } from 'rxjs/operators';
import { UtilDateService } from '../../@core/mock/common/util-date.service';
import { LocalDataSource } from 'ng2-smart-table';
import { NbThemeService, NbDialogService, NbDialogRef } from '@nebular/theme';
@Component({
  selector: 'payments-detail-dialog',
  styleUrls: ['./user-detail.component.scss'],
  template: `
  <nb-card>
      <nb-card-header>
      </nb-card-header>
      <nb-card-body>
          <ng2-smart-table [settings]="settings" [source]="source " >
          </ng2-smart-table>
      </nb-card-body>
  <nb-card-footer>
  <button (click)="close()" class="payments-header-button" style="float:right"  nbButton size="small" shape="semi-round">Kapat</button>
  </nb-card-footer>
  </nb-card>
  `,
})
export class PaymentsDetailDialogComponent {
  settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      userName: {
        title: 'User Name',
        type: 'string',
      },
      iban: {
        title: 'Iban',
        type: 'number',
      },
      paypal: {
        title: 'Paypal',
        type: 'string',
      },
      taxRate: {
        title: 'Tax Rate',
        type: 'number',
      },
      vatRate: {
        title: 'Vat Rate',
        type: 'number',
      },
      address: {
        title: 'Adress',
        type: 'string',
      },
      tckn: {
        title: 'Tckn',
        type: 'number',
      },
    },
  };
  @Input() value: string | number;
  @Input() rowData: any;

  @Output() save: EventEmitter<any> = new EventEmitter();
  source: LocalDataSource = new LocalDataSource();
  constructor(protected ref: NbDialogRef<BanDialogComponent>, private router: Router, public reportService: ReportData, ) {
    this.reportService.getPubPaymentDetail(this.reportService.userId).subscribe((data: {}) => {
      console.log(data);
      const tableData = [];
      if (data['response']) {



        tableData.push({
          userName: this.reportService.userName, iban: data['response']["iban"], paypal: data['response']["paypal"],
          taxRate: data['response']["taxRate"], vatRate: data['response']["vatRate"], address: data['response']["address"], tckn: data['response']["tckn"]
        });


        this.source.load(tableData);
      }
      console.log(data);
    });
  }

  close() {
    this.ref.close();
  }
}


@Component({
  selector: 'payments-dialog',
  styleUrls: ['./user-detail.component.scss'],
  template: `
  <nb-card>
      <nb-card-header>
      </nb-card-header>
      <nb-card-body>
          <ng2-smart-table [settings]="settings" [source]="source " >
          </ng2-smart-table>
      </nb-card-body>
  <nb-card-footer>
  <button (click)="close()" class="payments-header-button"  style="float:right" nbButton size="small" shape="semi-round">Kapat</button>
  </nb-card-footer>
  </nb-card>
  `,
})
export class PaymentsDialogComponent {
  settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      userName: {
        title: 'User Name',
        type: 'string',
      },
      amount: {
        title: 'Amount',
        type: 'number',
      },
      status: {
        title: 'Status',
        type: 'string',
      },
      taxRate: {
        title: 'Tax Rate',
        type: 'number',
      },
      vatRate: {
        title: 'Vat Rate',
        type: 'number',
      },
      paymentDate: {
        title: 'Payment Date',
        type: 'number',
      },
    },
  };
  @Input() value: string | number;
  @Input() rowData: any;

  @Output() save: EventEmitter<any> = new EventEmitter();
  source: LocalDataSource = new LocalDataSource();
  constructor(protected ref: NbDialogRef<BanDialogComponent>, private router: Router, public reportService: ReportData, ) {
    this.reportService.getPubPayment(this.reportService.userId).subscribe((data: {}) => {
      console.log(data);
      const tableData = [];
      if (data['response']) {

        data['response'].forEach(response => {

          tableData.push({
            userName: this.reportService.userName, amount: response["amount"], status: response["status"],
            taxRate: response["taxRate"], vatRate: response["vatRate"], paymentDate: response["paymentDate"]
          });
        });

        this.source.load(tableData);
      }
      console.log(data);
    });

  }
  close() {
    this.ref.close();
  }

}

@Component({
  selector: 'ban-dialog',
  styleUrls: ['./user-detail.component.scss'],
  template: `
  <nb-card>
    <nb-card-header>Do you want to ban this Unit Name: {{title}} ?</nb-card-header>
    <nb-card-body style="text-align:center">
    <button (click)="banAppUnitClick()" style="margin-right:15px;text-transform: none;background-color:#db415b" nbButton size="small" shape="semi-round">Ban</button>
    <button (click)="close()" class="payments-header-button"  nbButton size="small" shape="semi-round">Kapat</button>
    </nb-card-body>
</nb-card>

  `,
})
export class BanDialogComponent {
  @Input() value: string | number;
  @Input() rowData: any;

  @Output() save: EventEmitter<any> = new EventEmitter();
  constructor(protected ref: NbDialogRef<BanDialogComponent>, private router: Router, public reportService: ReportData, ) {
  }
  title = this.reportService.rowData['unitName']
  banAppUnitClick() {
    this.reportService.banAppUnit(this.reportService.rowData['appUnitId']).subscribe((data: {}) => {
      this.ref.close();
      window.location.reload();
    });

  }
  close() {
    this.ref.close();
  }
}
@Component({
  selector: 'button-user-ban',
  template: `
  <button nbButton shape="semi-round"  (click)="banControlClick()" style="cursor:pointer" status="danger"  size="xsmall" class="btn-demo">Ban</button>

  `,
})
export class ButtonAppUnitBanComponent {
  @Input() value: string | number;
  @Input() rowData: any;

  @Output() save: EventEmitter<any> = new EventEmitter();
  constructor(private router: Router, private dialogService: NbDialogService, public reportService: ReportData) {
  }
  banControlClick() {
    this.reportService.rowData = this.rowData;
    this.dialogService.open(BanDialogComponent, {
    });
  }

}

@Component({
  selector: 'user-detail',
  templateUrl: './user-detail.component.html',
  entryComponents: [DayCellComponent],
  styleUrls: ['./user-detail.component.scss'],

})
export class UserDetailComponent implements OnInit {
  isValid = false;
  options2: any = {};
  options: any = {};
  results = [];
  settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      appName: {
        title: 'App Name',
        type: 'string',
      },
      appUrl: {
        title: 'App Url',
        type: 'string',
      },
      appUnitId: {
        title: 'App Unit Id',
        type: 'string',
      },
      unitName: {
        title: 'Unit Name',
        type: 'string',
      },
      unitSize: {
        title: 'Unit Size',
        type: 'number',
      },
      regionId: {
        title: 'Region Id',
        type: 'number',
      },
      topCpm: {
        title: 'Top Cpm',
        type: 'number',
      },
      rekmobPercent: {
        title: 'Rekmob Percent',
        type: 'number',
      },
      appUnitBan: {
        title: 'Ban',
        type: 'custom',
        renderComponent: ButtonAppUnitBanComponent,
      },
    },
  };

  date = new Date();
  dayCellComponent = DayCellComponent;
  private alive = true;


  startDate: number;
  endDate: number;
  profit: number;
  id: string;
  type = "Today";
  typeTexts = [{ text: 'Today', value: 'today' },
  { text: 'Yesterday', value: 'yesterday' },
  { text: 'Last Seven Days', value: 'lastSevenDays' },
  { text: 'This Month', value: 'thisMonth' },
  { text: 'Last Month', value: 'lastMonth' }];
  source: LocalDataSource = new LocalDataSource();
  colorScheme: any;
  themeSubscription: any;

  labels: any[];
  currency: string;
  chartData: number[];
  appIds = [];
  name: string;
  constructor(private reportService: ReportData,
    private messageService: MessageService,
    private activatedRoute: ActivatedRoute,
    private utilDateService: UtilDateService, private theme: NbThemeService, private dialogService: NbDialogService) {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      const tableData = [];
      this.id = params.get('id');
      this.reportService.userId = this.id;
      var appName, appUrl, appUnitId, unitName, unitSize, regionId, topCpm, rekmobPercent;
      this.reportService.getUserApps(this.id).subscribe((appData: {}) => {
        appData['hits']['hits'].forEach(hits => {

          this.appIds.push(hits['_source']['id']);

        });
        this.reportService.getAppUnitForUser(this.appIds).subscribe((appData: {}) => {
          console.log(appData);
          appData['hits']['hits'].forEach(hits => {
            appName = hits['_source']['app']['appName'];
            appUrl = hits['_source']['app']['appUrl'];
            appUnitId = hits['_id'];
            unitName = hits['_source']['unitName'];
            unitSize = hits['_source']['unitSize'];
            regionId = hits['_source']['regionId'];
            topCpm = hits['_source']['topCpm'];
            rekmobPercent = hits['_source']['app']['rekmobPercent'];
            tableData.push({
              appName: appName, appUrl: appUrl, appUnitId: appUnitId, unitName: unitName, unitSize: unitSize,
              regionId: regionId, topCpm: topCpm, rekmobPercent: rekmobPercent
            })
          });
          this.source.load(tableData);
        });

      });
      this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

        this.reportService.getLast30Days(this.id).subscribe((data: {}) => {


          var dataArr = [];
          var arr = [];
          if (data['aggregations']['statTime_term']['buckets'][29]) {
            this.isValid = true;
            data['aggregations']['statTime_term']['buckets'].forEach(buckets => {

              var day = new Date(buckets['key']).getDate();
              var revenue = Math.round(buckets['userId_term']['buckets'][0]['netRevenue_sum']['value']);
              var cost = Math.round(buckets['userId_term']['buckets'][0]['pubRevenue_sum']['value']);
              var profit = Math.round(revenue - cost);

              var imp = Math.round(buckets['userId_term']['buckets'][0]['imps_sum']['value']);
              var click = Math.round(buckets['userId_term']['buckets'][0]['clicks_sum']['value']);
              dataArr.push({ day: day + "", revenue: revenue, cost: cost, profit: profit });
              arr.push({ day: day + "", imp: imp, click: click });

            });
            const echarts: any = config.variables.echarts;
            const echarts2: any = config.variables.echarts;
            this.options2 = {
              backgroundColor: echarts.bg,
              color: ['#800080', '#AFEEEE', '#469990'],
              tooltip: {
                trigger: 'axis',
                axisPointer: {
                  type: 'cross',
                  label: {
                    backgroundColor: echarts.tooltipBackgroundColor,
                  },
                },
              },
              legend: {
                data: ['Revenue', 'Cost', 'Profit'],
                textStyle: {
                  color: echarts.textColor,
                },
              },
              grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true,
              },
              xAxis: [
                {
                  type: 'category',
                  boundaryGap: false,
                  data: [dataArr[0].day, dataArr[1].day, dataArr[2].day, dataArr[3].day, dataArr[4].day, dataArr[5].day,
                  dataArr[6].day, dataArr[7].day, dataArr[8].day, dataArr[9].day, dataArr[10].day, dataArr[11].day, dataArr[12].day,
                  dataArr[13].day, dataArr[14].day, dataArr[15].day, dataArr[16].day, dataArr[17].day, dataArr[18].day, dataArr[19].day,
                  dataArr[20].day, dataArr[21].day, dataArr[22].day, dataArr[23].day, dataArr[24].day, dataArr[25].day, dataArr[26].day,
                  dataArr[27].day, dataArr[28].day, dataArr[29].day],
                  axisTick: {
                    alignWithLabel: true,
                  },
                  axisLine: {
                    lineStyle: {
                      color: echarts2.axisLineColor,
                    },
                  },
                  axisLabel: {
                    textStyle: {
                      color: echarts2.textColor,
                    },
                  },
                },
              ],
              yAxis: [
                {
                  type: 'value',
                  axisLine: {
                    lineStyle: {
                      color: echarts2.axisLineColor,
                    },
                  },
                  splitLine: {
                    lineStyle: {
                      color: echarts2.splitLineColor,
                    },
                  },
                  axisLabel: {
                    textStyle: {
                      color: echarts2.textColor,
                    },
                  },
                },
              ],
              series: [
                {
                  name: 'Profit',
                  type: 'line',
                  stack: 'Total amount',
                  areaStyle: { normal: { opacity: echarts2.areaOpacity } },
                  data: [dataArr[0].profit, dataArr[1].profit, dataArr[2].profit, dataArr[3].profit, dataArr[4].profit, dataArr[5].profit,
                  dataArr[6].profit, dataArr[7].profit, dataArr[8].profit, dataArr[9].profit, dataArr[10].profit, dataArr[11].profit,
                  dataArr[12].profit, dataArr[13].profit, dataArr[14].profit, dataArr[15].profit, dataArr[16].profit, dataArr[17].profit,
                  dataArr[18].profit, dataArr[19].profit, dataArr[20].profit, dataArr[21].profit, dataArr[22].profit, dataArr[23].profit,
                  dataArr[24].profit, dataArr[25].profit, dataArr[26].profit, dataArr[27].profit, dataArr[28].profit, dataArr[29].profit,
                  ],
                },
                {
                  name: 'Cost',
                  type: 'line',
                  stack: 'Total amount',
                  areaStyle: { normal: { opacity: echarts2.areaOpacity } },
                  data: [dataArr[0].cost, dataArr[1].cost, dataArr[2].cost, dataArr[3].cost, dataArr[4].cost, dataArr[5].cost,
                  dataArr[6].cost, dataArr[7].cost, dataArr[8].cost, dataArr[9].cost, dataArr[10].cost, dataArr[11].cost,
                  dataArr[12].cost, dataArr[13].cost, dataArr[14].cost, dataArr[14].cost, dataArr[15].cost, dataArr[16].cost,
                  dataArr[17].cost, dataArr[18].cost, dataArr[19].cost, dataArr[20].cost, dataArr[21].cost, dataArr[22].cost,
                  dataArr[23].cost, dataArr[24].cost, dataArr[25].cost, dataArr[26].cost, dataArr[27].cost, dataArr[28].cost,
                  dataArr[29].cost],
                },
                {
                  name: 'Revenue',
                  type: 'line',
                  stack: 'Total amount',
                  label: {
                    normal: {
                      show: true,
                      position: 'top',
                      textStyle: {
                        color: echarts2.textColor,
                      },
                    },
                  },
                  areaStyle: { normal: { opacity: echarts2.areaOpacity } },
                  data: [dataArr[0].revenue, dataArr[1].revenue, dataArr[2].revenue, dataArr[3].revenue, dataArr[4].revenue, dataArr[5].revenue,
                  dataArr[6].revenue, dataArr[7].revenue, dataArr[8].revenue, dataArr[9].revenue, dataArr[10].revenue, dataArr[11].revenue, dataArr[12].revenue,
                  dataArr[13].revenue, dataArr[14].revenue, dataArr[15].revenue, dataArr[16].revenue, dataArr[17].revenue, dataArr[18].revenue, dataArr[19].revenue,
                  dataArr[20].revenue, dataArr[21].revenue, dataArr[22].revenue, dataArr[23].revenue, dataArr[24].revenue, dataArr[25].revenue, dataArr[26].revenue,
                  dataArr[27].revenue, dataArr[28].revenue, dataArr[29].revenue],
                },
              ],

            };
            this.options = {
              backgroundColor: echarts.bg,
              color: ['#7FFF00', '#6495ED',],
              tooltip: {
                trigger: 'axis',
                axisPointer: {
                  type: 'cross',
                  label: {
                    backgroundColor: echarts.tooltipBackgroundColor,
                  },
                },
              },
              legend: {
                data: ['İmpression', 'Click'],
                textStyle: {
                  color: echarts.textColor,
                },
              },
              grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true,
              },
              xAxis: [
                {
                  type: 'category',
                  boundaryGap: false,
                  data: [arr[0].day, arr[1].day, arr[2].day, arr[3].day, arr[4].day, arr[5].day,
                  arr[6].day, arr[7].day, arr[8].day, arr[9].day, arr[10].day, arr[11].day, arr[12].day,
                  arr[13].day, arr[14].day, arr[15].day, arr[16].day, arr[17].day, arr[18].day, arr[19].day,
                  arr[20].day, arr[21].day, arr[22].day, arr[23].day, arr[24].day, arr[25].day, arr[26].day,
                  arr[27].day, arr[28].day, arr[29].day],
                  axisTick: {
                    alignWithLabel: true,
                  },
                  axisLine: {
                    lineStyle: {
                      color: echarts.axisLineColor,
                    },
                  },
                  axisLabel: {
                    textStyle: {
                      color: echarts.textColor,
                    },
                  },
                },
              ],
              yAxis: [
                {
                  type: 'value',
                  axisLine: {
                    lineStyle: {
                      color: echarts.axisLineColor,
                    },
                  },
                  splitLine: {
                    lineStyle: {
                      color: echarts.splitLineColor,
                    },
                  },
                  axisLabel: {
                    textStyle: {
                      color: echarts.textColor,
                    },
                  },
                },
              ],
              series: [
                {
                  name: 'İmpression',
                  type: 'line',
                  stack: 'Total amount',
                  areaStyle: { normal: { opacity: echarts.areaOpacity } },
                  data: [arr[0].imp, arr[1].imp, arr[2].imp, arr[3].imp, arr[4].imp, arr[5].imp,
                  arr[6].imp, arr[7].imp, arr[8].imp, arr[9].imp, arr[10].imp, arr[11].imp,
                  arr[12].imp, arr[13].imp, arr[14].imp, arr[15].imp, arr[16].imp, arr[17].imp,
                  arr[18].imp, arr[19].imp, arr[20].imp, arr[21].imp, arr[22].imp, arr[23].imp,
                  arr[24].imp, arr[25].imp, arr[26].imp, arr[27].imp, arr[28].imp, arr[29].imp,
                  ],
                },
                {
                  name: 'Click',
                  type: 'line',
                  stack: 'Total amount',
                  areaStyle: { normal: { opacity: echarts.areaOpacity } },
                  data: [arr[0].click, arr[1].click, arr[2].click, arr[3].click, arr[4].click, arr[5].click,
                  arr[6].click, arr[7].click, arr[8].click, arr[9].click, arr[10].click, arr[11].click,
                  arr[12].click, arr[13].click, arr[14].click, arr[14].click, arr[15].click, arr[16].click,
                  arr[17].click, arr[18].click, arr[19].click, arr[20].click, arr[21].click, arr[22].click,
                  arr[23].click, arr[24].click, arr[25].click, arr[26].click, arr[27].click, arr[28].click,
                  arr[29].click],
                },

              ],

            };
          }
        });

      });
    });
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.colorScheme = {
        domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
      };
    });
  }
  getDateRange(i: string) {
    var date = new Date();
    var timezoneOffset = date.getTimezoneOffset();
    if (!this.startDate || !this.endDate) {
      this.startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() - timezoneOffset * 60 * 1000;
      this.endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() + 24 * 60 * 60 * 1000 - timezoneOffset * 60 * 1000;
    }
    this.utilDateService.rangeTimeData(i).subscribe((data: {}) => {
      this.startDate = data['startDate'];
      this.endDate = data['endDate'];
    });

    this.refreshData(this.startDate, this.endDate);
  }



  refreshData(startDate, endDate) {

    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
      this.reportService.getHeaderSearchData(this.id, startDate, endDate).subscribe((searchData: {}) => {

        var imp;
        var click;
        var cost;
        var profit;
        var revenue;
        var pCpm;
        var nCpm;
        if (searchData['aggregations']['userId_term']['buckets'][0]) {
          var UserAppdata = searchData['aggregations']['userId_term']['buckets'][0];
          this.id = this.id;

          this.name = UserAppdata['publisherName_term']['buckets'][0]['key'];
          this.reportService.userName = this.name;
          imp = UserAppdata['imps_sum']['value'];
          click = UserAppdata['clicks_sum']['value'];
          revenue = UserAppdata['netIncome_sum']['value'];
          var takeNetRevenue = UserAppdata['netIncome_sum']['value'];
          profit = UserAppdata['profit_sum']['value'];
          cost = revenue - profit
          pCpm = (imp != 0 ? (1000 * cost) / imp : 0),
            nCpm = (imp != 0 ? (1000 * takeNetRevenue) / imp : 0)
        }
        else {
          this.id = this.id;
          this.name = "";
          imp = 0;
          click = 0;
          revenue = 0;
          var takeNetRevenue = 0;
          profit = 0;
          cost = 0;
          pCpm = (imp != 0 ? (1000 * cost) / imp : 0),
            nCpm = (imp != 0 ? (1000 * takeNetRevenue) / imp : 0)

        }
        this.results = [{ name: 'Impression', value: imp }, { name: 'Click', value: click },
        { name: 'Revenue', value: revenue }, { name: 'Cost', value: cost }, { name: 'Profit', value: profit },
        { name: 'pCpm', value: pCpm }, { name: 'nCpm', value: nCpm }];
      });

    });
  }
  payments() {

    this.dialogService.open(PaymentsDialogComponent, {
    });
  }
  paymentDetail() {

    this.dialogService.open(PaymentsDetailDialogComponent, {
    });
  }
  edit() { }
  ngOnInit() {
    this.getDateRange("today");


  }
  ngOnDestroy() {
    this.alive = false;
  }
}
