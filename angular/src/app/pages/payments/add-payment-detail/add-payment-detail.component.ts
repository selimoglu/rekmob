import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { ReportData } from '../../../@core/interfaces/common/reportData';
import { SearchData } from '../../../@core/interfaces/common/searchData';

@Component({
  selector: 'add-payment-detail',
  templateUrl: 'add-payment-detail.component.html',
  styleUrls: ['add-payment-detail.component.scss'],
})
export class AddPaymentDetailComponent {

  userData = [];
  selectedUser = "";
  selectedAddress = "";
  selectedIban = "";
  selectedInvoiced = "";
  selectedPaypal = "";
  selectedTaxId = "";
  selectedTaxRate = "";
  selectedTckn = "";
  selectedUserId: number;
  selectedVatRate = "";
  constructor(protected ref: NbDialogRef<AddPaymentDetailComponent>, public reportService: ReportData, private searchService: SearchData) {
  }

  save() {
    this.selectedUserId = +this.selectedUserId;
    if (this.selectedUserId) {
      this.reportService.createPubPaymentDetail(this.selectedAddress, this.selectedIban, this.selectedInvoiced, this.selectedPaypal,
        this.selectedTaxId, this.selectedTaxRate, this.selectedTckn, this.selectedUserId, this.selectedVatRate).subscribe((data: {}) => {
          this.ref.close();
          window.location.reload();
        });
    }
  }
  onSearchUser(event) {
    event.term = event.term.toLocaleLowerCase();
    if (event.term.length > 2) {

      this.searchService.getSearchUserData(event.term).subscribe((data: {}) => {
        this.userData = [];
        if (data['hits'].total && data['hits'].total > 0) {
          data['hits']['hits'].forEach(hits => {
            this.userData.push({ name: hits['_source'].fullName, id: hits['_source'].id + "" });
          });
        }
        else {
          this.userData = [];
        }
      });
    }
    else {
      this.userData = [];
    }
  }
  close() {
    this.ref.close();

  }
}
