
import { Component, OnDestroy } from '@angular/core';

@Component({
  selector: 'payments',
  templateUrl: './payments.component.html',
})
export class PaymentsComponent implements OnDestroy {

  private alive = true;


  constructor() {
 
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
