import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { SearchData } from '../../../@core/interfaces/common/searchData';
import { ReportData } from '../../../@core/interfaces/common/reportData';

@Component({
  selector: 'add-payment',
  templateUrl: 'add-payment.component.html',
  styleUrls: ['add-payment.component.scss'],
})
export class AddPaymentComponent {

  userData = [];
  statusMap = [
    { name: 'Waiting invoice', id: 'WAITING_INVOICE' },
    { name: 'Paid', id: 'PAID' },
    { name: 'Wrong id', id: 'WRONG_ID' },
    { name: 'Wrong address', id: 'WRONG_ADDRESS' },
    { name: 'Wrong bank', id: 'WRONG_BANK' },
    { name: 'Pub later', id: 'PUB_LATER' },
    { name: 'Invoice approval', id: 'INVOICE_APPROVAL' },
    { name: 'Paying', id: 'PAYING' },
    { name: 'Waiting date', id: 'WAITING_DATE' },

  ];
  selectedAmount: number;
  selectedPaymentDate: string;
  selectedStatus: string;
  selectedUserId: number;
  date: string;
  startDate: string;
  constructor(protected ref: NbDialogRef<AddPaymentComponent>, private searchService: SearchData, public reportService: ReportData) {
    console.log(this.statusMap['WAITING_FOR_INVOICE']);
  }
  onSearchUser(event) {
    event.term = event.term.toLocaleLowerCase();
    if (event.term.length > 2) {

      this.searchService.getSearchUserData(event.term).subscribe((data: {}) => {
        this.userData = [];
        if (data['hits'].total && data['hits'].total > 0) {
          data['hits']['hits'].forEach(hits => {
            this.userData.push({ name: hits['_source'].fullName, id: hits['_source'].id + "" });
          });
        }
        else {
          this.userData = [];
        }
      });
    }
    else {
      this.userData = [];
    }
  }
  setDate() {
    var date = new Date(this.date);
    var timezoneOffset = date.getTimezoneOffset();
    var day = new Date(date).getDate();
    var month = new Date(date).getMonth() + 1;
    var year = new Date(date).getFullYear();
    this.startDate = year + "-" + (month < 10 ? "0" + month : "" + month) + "-" + (day < 10 ? "0" + day : "" + day);
  }
  save() {
    this.selectedUserId = +this.selectedUserId;
    this.reportService.createPubPayment(this.selectedAmount, this.startDate, this.selectedStatus, this.selectedUserId,
    ).subscribe((data: {}) => {
      this.ref.close();
      window.location.reload();
    });
  }

  close() {
    this.ref.close();
  }

}
