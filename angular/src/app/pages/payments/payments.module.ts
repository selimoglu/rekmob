import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { NbDialogModule } from '@nebular/theme';
import { PaymentsComponent } from './payments.component';

import { PaymentsReportTableComponent } from './payments-report-table/payments-report-table.component';
import { AddPaymentComponent } from './add-payment/add-payment.component';
import { AddPaymentDetailComponent } from './add-payment-detail/add-payment-detail.component';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    ThemeModule,
    Ng2SmartTableModule,
    NbDialogModule.forChild(),
    NgSelectModule,
  ],
  declarations: [
    PaymentsComponent,
    PaymentsReportTableComponent,
    AddPaymentComponent,
    AddPaymentDetailComponent,
  ],
  entryComponents: [AddPaymentComponent,AddPaymentDetailComponent],
})
export class PaymentsReportsModule { }
