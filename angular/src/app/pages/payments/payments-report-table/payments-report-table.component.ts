import { Component, Input, TemplateRef } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/interfaces/common/smart-table';
import { NbSidebarService, NbDialogService, NbDateService } from '@nebular/theme';
import { StateService } from '../../../@core/utils';
import { AddPaymentComponent } from '../add-payment/add-payment.component';
import { UtilDateService } from '../../../@core/mock/common/util-date.service';
import { AddPaymentDetailComponent } from '../add-payment-detail/add-payment-detail.component';
import { ReportData } from '../../../@core/interfaces/common/reportData';
@Component({
  selector: 'payments-report-table',
  styleUrls: ['./payments-report-table.component.scss'],
  templateUrl: './payments-report-table.component.html',
})
export class PaymentsReportTableComponent {
  settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      userName: {
        title: 'User Name',
        type: 'string',
      },
      amount: {
        title: 'Amount',
        type: 'number',
      },
      status: {
        title: 'Status',
        type: 'string',
      },
      taxRate: {
        title: 'Tax Rate',
        type: 'number',
      },
      vatRate: {
        title: 'Vat Rate',
        type: 'number',
      },
      paymentDate: {
        title: 'Payment Date',
        type: 'number',
      },
    },
  };
  typeTexts = [{ text: 'Month', value: 'month' },
  ]
  source: LocalDataSource = new LocalDataSource();
  sidebarEnd = false;
  currentTheme: string;
  startDate: string;
  endDate: number;
  id: string;
  sidebarState: boolean;
  type = "All";
  status = [
    { name: 'All', id: "All" },
    { name: 'Waiting invoice', id: 'WAITING_INVOICE' },
    { name: 'Paid', id: 'PAID' },
    { name: 'Wrong id', id: 'WRONG_ID' },
    { name: 'Wrong address', id: 'WRONG_ADDRESS' },
    { name: 'Wrong bank', id: 'WRONG_BANK' },
    { name: 'Pub later', id: 'PUB_LATER' },
    { name: 'Invoice approval', id: 'INVOICE_APPROVAL' },
    { name: 'Paying', id: 'PAYING' },
    { name: 'Waiting date', id: 'WAITING_DATE' },

  ];
  statusMap = {
    'WAITING_INVOICE': 'Waiting invoice',
    'PAID': 'Paid',
    'WRONG_ID': 'Wrong id',
    'WRONG_ADDRESS': 'Wrong address',
    'WRONG_BANK': 'Wrong bank',
    'TRANSFERRED': 'Transferred',
    'PUB_LATER': 'Pub later',
    'INVOICE_APPROVAL': 'Invoice approval',
    'PAYING': 'Paying',
    'WAITING_DATE': 'Waiting date',
  };
  selectedStatus: string;
  date = "";
  constructor(private reportService: ReportData, private utilDateService: UtilDateService, private service: SmartTableData,
     private sidebarService: NbSidebarService, protected stateService: StateService, private dialogService: NbDialogService) {

  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
  onUserRowSelect(event): void {

  }
  getStatus(i: string) {
    if (i == "All") {
      this.selectedStatus = null;
    }
    else {
      this.selectedStatus = i
    };
    this.refreshTableData();
  }
  setDate() {
    var date = new Date(this.date);
    var timezoneOffset = date.getTimezoneOffset();
    var day = new Date(date).getDate();
    var month = new Date(date).getMonth() + 1;
    var year = new Date(date).getFullYear();
    this.startDate = year + "-" + (month < 10 ? "0" + month : "" + month) + "-" + (day < 10 ? "0" + day : "" + day);
    this.refreshTableData();

  }

  refreshTableData(): void {
    if (!this.startDate) {
      var date = new Date();
      var timezoneOffset = date.getTimezoneOffset();
      var startDateMs = new Date(new Date((new Date(date.getFullYear(), date.getMonth(), 1).getTime()) - (86400000 * 1)).getFullYear(),
        new Date((new Date(date.getFullYear(), date.getMonth(), 1).getTime()) - (86400000 * 1)).getMonth(), 1).getTime();
      startDateMs = startDateMs - timezoneOffset * 60 * 1000;
      var day = new Date(startDateMs).getDate();
      var month = new Date(startDateMs).getMonth() + 1;
      var year = new Date(startDateMs).getFullYear();
      this.startDate = year + "-" + (month < 10 ? "0" + month : "" + month) + "-" + (day < 10 ? "0" + day : "" + day);

    }

    this.reportService.getPubPayments(this.startDate, this.selectedStatus).subscribe((data: {}) => {
      console.log(data);
      const tableData = [];
      if (data['response']) {

        data['response'].forEach(response => {

          tableData.push({
            userName: response["userName"], amount: response["amount"], status: this.statusMap[response["status"]],
            taxRate: response["taxRate"], vatRate: response["vatRate"], paymentDate: response["paymentDate"]
          });
        });

        this.source.load(tableData);
      }
    });
  }
  addPaymentDetail() {
    this.dialogService.open(AddPaymentDetailComponent, {
    });
  }
  addPayment() {
    this.dialogService.open(AddPaymentComponent, {
    });
  }
  ngOnInit() {
    this.selectedStatus = null;
    this.refreshTableData();
  }
}
