import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { ReportData } from '../../../@core/interfaces/common/reportData';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MessageService } from '../../../@core/mock/common/message-service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'custom-report-table',
  templateUrl: './custom-report-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class CustomReportTableComponent {

  settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
    },
  };
  source: LocalDataSource = new LocalDataSource();
  sidebarEnd = false;
  siteId: string;
  subscription: Subscription;
  constructor(public reportService: ReportData, private messageService: MessageService, ) {


  }
  setDecNumber(x) {
    return Number.parseFloat(x).toFixed(2);
  }
  ngOnInit() {

    this.subscription = this.messageService.getCustomColumns().subscribe(data => {
      var pubName;
      var date;
      var appName;
      const tableData = [];
      console.log(data);
      const options = [];
      var dataArr = data.text[0]['arr'];
      var columns = {}
      if (data.text[0])
        columns = data.text[0].columns;
      columns['impression'] = {
        title: 'Impression',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        }
      }
      columns['click'] = {
        title: 'Click',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        }
      }
      columns['revenue'] = {
        title: 'Revenue',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        }
      }
      columns['cost'] = {
        title: 'Cost',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        }
      }
      columns['revenueUsd'] = {
        title: 'Revenue(USD)',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        }
      }
      columns['costUsd'] = {
        title: 'Cost(USD)',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        }
      }
      columns['profit'] = {
        title: 'Profit',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        }
      }
      columns['eCpm'] = {
        title: 'eCPM',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        }
      }
      columns['pCpm'] = {
        title: 'pCPM',
        type: 'number',
        valuePrepareFunction: (number) => {
          return new Intl.NumberFormat().format(number);
        }
      }

      options.push(columns)
      this.settings.columns = options[0];
      this.settings = Object.assign({}, this.settings);
      var totalImp = 0;
      var totalClick = 0;
      var totalRevenue = 0;
      var totalRevenueUsd = 0;
      var totalProfit = 0;
      var totalCost = 0;
      var totalCostUsd = 0;
      var totalEcpm = 0;
      var totalPecpm = 0;
      dataArr.forEach(dataArr => {

        totalImp = totalImp + dataArr['metrics'].imps;
        totalClick = totalClick + dataArr['metrics'].clicks;
        totalRevenue = totalRevenue + dataArr['metrics'].netRevenue;
        totalRevenueUsd = totalRevenueUsd + dataArr['metrics'].netRevenueUsd;
        totalProfit = totalProfit + dataArr['metrics'].profit;
        totalCost = totalCost + dataArr['metrics'].pubRevenue;
        totalCostUsd = totalCostUsd + dataArr['metrics'].pubRevenueUsd;


      });
      totalEcpm = (totalImp != 0 ? (1000 * totalRevenue) / totalImp : 0);
      totalPecpm = (totalImp != 0 ? (1000 * totalCost) / totalImp : 0);
      var totImp = this.setDecNumber(totalImp);
      var totClick = this.setDecNumber(totalClick);
      var totRevenue = this.setDecNumber(totalRevenue);
      var totRevenueUsd = this.setDecNumber(totalRevenueUsd);
      var totProfit = this.setDecNumber(totalProfit);
      var totCost = this.setDecNumber(totalCost);
      var totCostUsd = this.setDecNumber(totalCostUsd);
      var totEcpm = this.setDecNumber(totalEcpm);
      var totPecpm = this.setDecNumber(totalPecpm);
      tableData.push({
        statTime: "-", userId: "-", appId: "-", appUnitId: "-", adSource: "-", unitSize: "-",
        accountManager: "-", appType: "-", appUrl2: "-", utm: "-",country:"-", impression: totImp, click: totClick,
        revenue: totRevenue, revenueUsd: totRevenueUsd, profit: totProfit, cost: totCost,
        costUsd: totCostUsd, eCpm: totEcpm, pCpm: totPecpm
      })

      dataArr.forEach(dataArr => {
        tableData.push({
          statTime: dataArr['dimensions'].Date,
          userId: dataArr['dimensions'].Publisher,
          appId: dataArr['dimensions']['App'],
          appUnitId: dataArr['dimensions']['App Unit'],
          adSource: dataArr['dimensions']['Ad Source'],
          unitSize: dataArr['dimensions']['Size'],
          accountManager: dataArr['dimensions']['Account Manager'],
          appType: dataArr['dimensions']['App Type'],
          appUrl2: dataArr['dimensions']['App Url'],
          utm: dataArr['dimensions']['Utm Source'],
          cc:dataArr['dimensions']['Country'],
          impression: dataArr['metrics'].imps,
          click: dataArr['metrics'].clicks,
          revenue: dataArr['metrics'].netRevenue,
          revenueUsd: dataArr['metrics'].netRevenueUsd,
          profit: dataArr['metrics'].profit,
          cost: dataArr['metrics'].pubRevenue,
          costUsd: dataArr['metrics'].pubRevenueUsd,
          eCpm: dataArr['metrics'].ecpm,
          pCpm: dataArr['metrics'].pecpm,
        });
      });

      this.source.load(tableData);

    });

  }
}
