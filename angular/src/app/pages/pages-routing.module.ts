import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomepageComponent } from './homepage/homepage.component';
import { CampaignDetailsComponent } from './campaign-details/campaign-details.component';
import { CampaignReportsComponent } from './campaign-reports/campaign-reports.component';
import { PublisherDetailComponent } from './publisher-detail/publisher-detail.component';
import { AccountFraudComponent } from './account-fraud/account-fraud.component';
import { SiteDetailComponent } from './site-detail/site-detail.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { CustomReportComponent } from './custom-report/custom-report.component';
import { PaymentsComponent } from './payments/payments.component';
const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'dashboard',
    component: DashboardComponent,
  },{
    path: 'homepage',
    component: HomepageComponent,
  }, {
    path: 'users',
    loadChildren: './users/users.module#UsersModule',
  },
  
  {
    path: 'account-fraud',
    component: AccountFraudComponent,
  },

  {
    path: 'campaign-details',
    component: CampaignDetailsComponent,
  },
  {
    path: 'campaign-reports',
    component: CampaignReportsComponent,
  },
  {
    path: 'publisher-detail',
    component: PublisherDetailComponent,
  },
  {
    path: 'site-detail/:id',
    component: SiteDetailComponent,
  },
  {
    path: 'publisher-detail/:id',
    component: UserDetailComponent,
  },
  {
    path: 'custom-report-table',
    component: CustomReportComponent,
  },
  {
    path: 'payments',
    component: PaymentsComponent,
  },

  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }, {
    path: '**',
    component: NotFoundComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
