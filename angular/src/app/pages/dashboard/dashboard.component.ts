/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */
import { Component, OnInit } from '@angular/core';
import { ReportData } from '../../@core/interfaces/common/reportData';
import { UtilDateService } from '../../@core/mock/common/util-date.service';
import { NbThemeService } from '@nebular/theme';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  flipped = false;

  toggleView() {
    this.flipped = !this.flipped;
  }
  adSources = [
    { name: 'REKMOB', id: 2 },
    { name: 'ANX', id: 3 },
    { name: 'HOUSE_AD', id: 4 },
    { name: 'ADX', id: 5 },
    { name: 'ADF', id: 6 },
    { name: 'BRT', id: 7 },
    { name: 'SR', id: 8 },
    { name: 'NATIVE', id: 9 },
    { name: 'PUBMATIC', id: 10 },
    { name: 'SPRINGSERVE', id: 11 },
    { name: 'RUBICON', id: 12 },
    { name: 'IMD', id: 15 },
    { name: 'PROP', id: 16 },
    { name: 'ADCASH', id: 17 },
    { name: 'ADSTERRA', id: 18 },
    { name: 'ADMAVEN', id: 19 },
  ];
  options: any = {};
  options2: any = {};
  themeSubscription: any;

  endDate: number;
  startDate: number;
  sefaValue: any;
  cerenValue: any;
  esraValue: any;
  melisValue: any;
  globalValue: any;

  impDisplay: any;
  impPop: any;
  impNative: any;
  clickDisplay: any;
  revenueDisplay: any;
  costDisplay: any;
  profitDisplay: any;
  clickPop: any;
  revenuePop: any;
  costPop: any;
  profitPop: any;
  clickNative: any;

  profitNative: any;
  revenueNative: any;
  costNative: any;


  type = "today";
  typeTexts = [{ text: 'Today', value: 'today' },
  { text: 'Yesterday', value: 'yesterday' },
  { text: 'Last Seven Days', value: 'lastSevenDays' },
  { text: 'This Month', value: 'thisMonth' },
  { text: 'Last Month', value: 'lastMonth' }];
  result = [];
  results2 = [];
  showLegend = true;
  showXAxis = true;
  showYAxis = true;
  xAxisLabel = 'Ad Sources';
  yAxisLabel = 'Revenue';
  colorScheme: any;
  constructor(private reportService: ReportData, private utilDateService: UtilDateService, private theme: NbThemeService) {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.colorScheme = {
        domain: ['#469990', '#AFEEEE', '#FFA500', '#42d4f4', '#800080', '#3cb44b', '#008080', '#a9a9a9',
          '#808000', '#4363d8', '#FFD700 ', '800000 ', '#fabebe ', '#f032e6', '#8B008B'],
      };
    });
  }
  getDateRange(i: string) {
    var date = new Date();
    var timezoneOffset = date.getTimezoneOffset();
    if (!this.startDate || !this.endDate) {

      this.startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() - timezoneOffset * 60 * 1000;
      this.endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime()
        + 24 * 60 * 60 * 1000 - timezoneOffset * 60 * 1000;

    }
    this.utilDateService.rangeTimeData(i).subscribe((data: {}) => {
      this.startDate = data['startDate'];
      this.endDate = data['endDate'];

    });
    this.refreshTableData(this.startDate, this.endDate);
    this.ngAfterViewInit(this.startDate, this.endDate);
  }
  refreshTableData(startDate, endDate): void {
    this.reportService.getImpClickProfitNative(startDate, endDate).subscribe((data: {}) => {

      this.impNative = data['aggregations']['imps_sum']['value'];
      this.clickNative = data['aggregations']['clicks_sum']['value'];
      this.revenueNative = data['aggregations']['netRevenue_sum']['value'];
      this.costNative = data['aggregations']['pubRevenue_sum']['value'];
      this.profitNative = data['aggregations']['netRevenue_sum']['value'] - data['aggregations']['pubRevenue_sum']['value'];

    });
    this.reportService.getImpClickProfitPop(startDate, endDate).subscribe((data: {}) => {

      this.impNative = data['aggregations']['imps_sum']['value'];
      this.clickPop = data['aggregations']['clicks_sum']['value'];
      this.revenuePop = data['aggregations']['netRevenue_sum']['value'];
      this.costPop = data['aggregations']['pubRevenue_sum']['value'];
      this.profitPop = data['aggregations']['netRevenue_sum']['value'] - data['aggregations']['pubRevenue_sum']['value'];

    });
    this.reportService.getImpClickProfitDisplay(startDate, endDate).subscribe((data: {}) => {

      this.impDisplay = data['aggregations']['imps_sum']['value'];
      this.clickDisplay = data['aggregations']['clicks_sum']['value'];
      this.revenueDisplay = data['aggregations']['netRevenue_sum']['value'];
      this.costDisplay = data['aggregations']['pubRevenue_sum']['value'];
      this.profitDisplay = data['aggregations']['netRevenue_sum']['value'] - data['aggregations']['pubRevenue_sum']['value'];

    });


    this.reportService.getAccValue(startDate, endDate).subscribe((accValue: {}) => {
      try {
        this.cerenValue = accValue['aggregations']['accountManager_term']['buckets'][0]['netRevenue_sum']['value'];
        this.sefaValue = accValue['aggregations']['accountManager_term']['buckets'][1]['netRevenue_sum']['value'];
        this.esraValue = accValue['aggregations']['accountManager_term']['buckets'][2]['netRevenue_sum']['value'];
        this.melisValue = accValue['aggregations']['accountManager_term']['buckets'][3]['netRevenue_sum']['value'];
        this.globalValue = accValue['aggregations']['accountManager_term']['buckets'][4]['netRevenue_sum']['value'];
        var sumGlobal = this.esraValue + this.melisValue + this.globalValue;
        var sumTurkey = this.cerenValue + this.sefaValue;
        this.result = [{ name: 'Global', value: sumGlobal }, { name: 'Turkey', value: sumTurkey }];
      }
      catch (e) {
        console.log("Account manager value is expected  ");
        this.cerenValue = 0;
        this.sefaValue = 0;
        this.esraValue = 0;
        this.melisValue = 0;
        this.globalValue = 0;
        this.result = [{ name: 'Global', value: 0 }, { name: 'Turkey', value: 0 }];
      }

    });

  }
  ngAfterViewInit(startDate, endDate) {
    if (!startDate || !endDate) {
      var date = new Date();
      var timezoneOffset = date.getTimezoneOffset();
      startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime();
      endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() + 86400000;
      startDate = startDate - timezoneOffset * 60 * 1000;
      endDate = endDate - timezoneOffset * 60 * 1000;
    }
    var adSourceArr = [];

    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      this.reportService.getAdSourceValue(startDate, endDate).subscribe((adSourceData: {}) => {
        var adSourceName = [];
        var barValue = [];
        adSourceData['aggregations']['adSource_term']['buckets'].forEach(buckets => {
          var i = 0;
          var j = 0;
          for (i = 0; i <= 15; i++) {
            if (this.adSources[i].id == buckets['key']) {
              var adSourceValue = Math.round(buckets['netRevenue_sum']['value']);
              var id = buckets['key'];
              if (id == 2) {
                var name = 'REKMOB';
              }
              else if (id == 3) {
                var name = 'ANX';
              }
              else if (id == 4) {
                var name = 'HOUSE_AD';
              }
              else if (id == 5) {
                var name = 'ADX';
              }
              else if (id == 6) {
                var name = 'ADF';
              }
              else if (id == 7) {
                var name = 'BRT';
              }
              else if (id == 8) {
                var name = 'SR';
              } else if (id == 9) {
                var name = 'NATIVE';
              }
              else if (id == 10) {
                var name = 'PUBMATIC';
              }
              else if (id == 11) {
                var name = 'SPRINGSERVE';
              }
              else if (id == 12) {
                var name = 'RUBICON';
              }
              else if (id == 15) {
                var name = 'IMD';
              }
              else if (id == 16) {
                var name = 'PROP';
              }
              else if (id == 17) {
                var name = 'ADCASH';
              }
              else if (id == 18) {
                var name = 'ADSTERRA';
              }
              else if (id == 19) {
                var name = 'ADMAVEN';
              }
              if (name && adSourceValue){
                barValue.push({ name: name, value: adSourceValue })
              adSourceArr.push({ value: adSourceValue, name: name });
              adSourceName.push(name);
            }
            }
          }
        });
        this.results2 = barValue;
        const colors = config.variables;
        const echarts: any = config.variables.echarts;
        this.options = {
          backgroundColor: echarts.bg,
          color: ['#469990', '#AFEEEE', '#FFA500', '#42d4f4', '#800080', '#3cb44b', '#008080', '#a9a9a9',
            '#808000', '#4363d8', '#FFD700 ', '800000 ', '#fabebe ', '#f032e6', '#8B008B'
          ],
          tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c} ({d}%)',
          },
          legend: {
            orient: 'vertical',
            right: 'right',
            data: [],
            textStyle: {
              color: echarts.textColor,
            },
          },
          series: [
            {
              name: 'Countries',
              type: 'pie',
              radius: '80%',
              center: ['50%', '50%'],

              data: [],
              itemStyle: {
                emphasis: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: echarts.itemHoverShadowColor,
                },
              },
              label: {
                normal: {
                  textStyle: {
                    color: echarts.textColor,
                  },
                },
              },
              labelLine: {
                normal: {
                  lineStyle: {
                    color: echarts.axisLineColor,
                  },
                },
              },
            },
          ],

        };
        this.options['legend']['data'] = adSourceName;
        this.options['series'][0]['data'] = adSourceArr;
      });
      this.reportService.getLast15Days().subscribe((data: {}) => {

        var dateArr = [];
        data['aggregations']['statTime_term']['buckets'].forEach(buckets => {
          var day = new Date(buckets['key']).getDate();
          var revenue = Math.round(buckets['netRevenue_sum']['value']);
          var cost = Math.round(buckets['pubRevenue_sum']['value']);
          var profit = Math.round(revenue - cost);
          dateArr.push({ day: day + "", revenue: revenue, cost: cost, profit: profit })

        });

        const colors2: any = config.variables;
        const echarts2: any = config.variables.echarts;
        this.options2 = {
          backgroundColor: echarts2.bg,
          color: ['#800080', '#AFEEEE', '#469990'],
          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'cross',
              label: {
                backgroundColor: echarts2.tooltipBackgroundColor,
              },
            },
          },
          legend: {
            data: ['Revenue', 'Cost', 'Profit'],
            textStyle: {
              color: echarts2.textColor,
            },
          },
          grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true,
          },
          xAxis: [
            {
              type: 'category',
              boundaryGap: false,
              data: [dateArr[0].day, dateArr[1].day, dateArr[2].day, dateArr[3].day, dateArr[4].day, dateArr[5].day,
              dateArr[6].day, dateArr[7].day, dateArr[8].day, dateArr[9].day, dateArr[10].day, dateArr[11].day, dateArr[12].day,
              dateArr[13].day, dateArr[14].day],
              axisTick: {
                alignWithLabel: true,
              },
              axisLine: {
                lineStyle: {
                  color: echarts2.axisLineColor,
                },
              },
              axisLabel: {
                textStyle: {
                  color: echarts2.textColor,
                },
              },
            },
          ],
          yAxis: [
            {
              type: 'value',
              axisLine: {
                lineStyle: {
                  color: echarts2.axisLineColor,
                },
              },
              splitLine: {
                lineStyle: {
                  color: echarts2.splitLineColor,
                },
              },
              axisLabel: {
                textStyle: {
                  color: echarts2.textColor,
                },
              },
            },
          ],
          series: [
            {
              name: 'Profit',
              type: 'line',
              stack: 'Total amount',
              areaStyle: { normal: { opacity: echarts2.areaOpacity } },
              data: [dateArr[0].profit, dateArr[1].profit, dateArr[2].profit, dateArr[3].profit, dateArr[4].profit, dateArr[5].profit,
              dateArr[6].profit, dateArr[7].profit, dateArr[8].profit, dateArr[9].profit, dateArr[10].profit, dateArr[11].profit,
              dateArr[12].profit, dateArr[13].profit, dateArr[14].profit],
            },
            {
              name: 'Cost',
              type: 'line',
              stack: 'Total amount',
              areaStyle: { normal: { opacity: echarts2.areaOpacity } },
              data: [dateArr[0].cost, dateArr[1].cost, dateArr[2].cost, dateArr[3].cost, dateArr[4].cost, dateArr[5].cost,
              dateArr[6].cost, dateArr[7].cost, dateArr[8].cost, dateArr[9].cost, dateArr[10].cost, dateArr[11].cost,
              dateArr[12].cost, dateArr[13].cost, dateArr[14].cost],
            },
            {
              name: 'Revenue',
              type: 'line',
              stack: 'Total amount',
              label: {
                normal: {
                  show: true,
                  position: 'top',
                  textStyle: {
                    color: echarts2.textColor,
                  },
                },
              },
              areaStyle: { normal: { opacity: echarts2.areaOpacity } },
              data: [dateArr[0].revenue, dateArr[1].revenue, dateArr[2].revenue, dateArr[3].revenue, dateArr[4].revenue, dateArr[5].revenue,
              dateArr[6].revenue, dateArr[7].revenue, dateArr[8].revenue, dateArr[9].revenue, dateArr[10].revenue, dateArr[11].revenue, dateArr[12].revenue,
              dateArr[13].revenue, dateArr[14].revenue],
            },
          ],

        };
      });
    });

  }

  ngOnInit() {

    this.getDateRange("today");

  }

}
