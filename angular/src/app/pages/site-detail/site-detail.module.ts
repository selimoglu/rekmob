/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { SiteDetailComponent } from './site-detail.component';
import { DayCellComponent } from './day-cell/day-cell.component';
import { D3AdvancedPieComponent } from './d3/d3-advanced-pie.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { Ng2SmartTableModule } from 'ng2-smart-table';
@NgModule({
  imports: [
    ThemeModule,
    NgxChartsModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    SiteDetailComponent,
    DayCellComponent,
    D3AdvancedPieComponent,
  ],
  providers: [
  ],
})
export class SiteDetailModule { }
