/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { DayCellComponent } from './day-cell/day-cell.component';
import { MessageService } from '../../@core/mock/common/message-service';
import { LocalDataSource } from 'ng2-smart-table';
import { Subscription } from 'rxjs';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ReportData } from '../../@core/interfaces/common/reportData';
import { switchMap } from 'rxjs/operators';
import { UtilDateService } from '../../@core/mock/common/util-date.service';
import { NbThemeService } from '@nebular/theme';
@Component({
  selector: 'site-detail',
  templateUrl: './site-detail.component.html',
  styleUrls: ['./site-detail.component.scss'],
  entryComponents: [DayCellComponent],
})
export class SiteDetailComponent implements OnDestroy, OnInit {

  settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      unitName: {
        title: 'Name',
        type: 'string',
      },
      unitKey: {
        title: 'Unit Key ',
        type: 'string',
      },
      anxPlacementId: {
        title: 'Anx Placement Id',
        type: 'string',
      },
      adxAccount: {
        title: 'Adx Account',
        type: 'string',
      },
      regionId: {
        title: 'Region Id',
        type: 'number ',
      },
    },
  };
  date = new Date();
  date2 = new Date();
  dayCellComponent = DayCellComponent;
  private alive = true;
  name: string;
  imp: number;
  click: number;
  pCpm: number;
  nCpm: number; 
  revenue: number
  cost: number;
  startDate: number;
  endDate: number;
  profit: number;
  appName: string;
  id: string;
  results = [];
  colorScheme: any;
  themeSubscription: any;
  appUrl: string;
  type = "today";
  typeTexts = [{ text: 'Today', value: 'today' },
  { text: 'Yesterday', value: 'yesterday' },
  { text: 'Last Seven Days', value: 'lastSevenDays' },
  { text: 'This Month', value: 'thisMonth' },
  { text: 'Last Month', value: 'lastMonth' }];
  source: LocalDataSource = new LocalDataSource();
  constructor(private reportService: ReportData, private theme: NbThemeService,
    private messageService: MessageService,
    private activatedRoute: ActivatedRoute,
    private utilDateService: UtilDateService, ) {


    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      const tableData = [];
      this.id = params.get('id');
      var unitName;
      var unitKey;
      var anxPlacementId;
      var adxAccount;
      var regionId;
      this.reportService.getAppUnitData(this.id).subscribe((data: {}) => {
        data['hits']['hits'].forEach(hits => {
          unitName = hits['_source']['unitName'];
          unitKey = hits['_source']['unitKey'];
          anxPlacementId = hits['_source']['anxPlacementId'];
          adxAccount = hits['_source']['adxAccount'];
          if (adxAccount == 0)
            adxAccount = "false";
          else if (adxAccount = 1)
            adxAccount = "true";
          regionId = hits['_source']['regionId'];
          tableData.push({ unitName: unitName, unitKey: unitKey, anxPlacementId: anxPlacementId, adxAccount: adxAccount, regionId: regionId })

        });

        this.source.load(tableData);

      });
    });

    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.colorScheme = {
        domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
      };
    });
  }
  getDateRange(i: string) {
    var date = new Date();
    var timezoneOffset = date.getTimezoneOffset();
    if (!this.startDate || !this.endDate) {
      this.startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() - timezoneOffset * 60 * 1000;
      this.endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() + 24 * 60 * 60 * 1000 - timezoneOffset * 60 * 1000;
    }
    this.utilDateService.rangeTimeData(i).subscribe((data: {}) => {
      this.startDate = data['startDate'];
      this.endDate = data['endDate'];
    });
    this.refreshData(this.startDate, this.endDate);
  }
  ngOnInit() {
    this.getDateRange("today");
  }

  refreshData(startDate, endDate) {

    var imp;
    var click;
    var cost;
    var profit; 
    var revenue;
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
      this.reportService.getAppValueData(this.id, startDate, endDate).subscribe((siteData: {}) => {
        console.log(siteData);
        imp = siteData['aggregations']['appId_term']['buckets'][0]['imps_sum'].value;
        click = siteData['aggregations']['appId_term']['buckets'][0]['clicks_sum'].value;
        revenue = siteData['aggregations']['appId_term']['buckets'][0]['netRevenue_sum'].value;
        cost = siteData['aggregations']['appId_term']['buckets'][0]['pubRevenue_sum'].value;
        profit = revenue - cost;
        this.appName = siteData['aggregations']['appId_term']['buckets'][0]['appName_tname']['buckets']['0']['key'];
        console.log(siteData);

        this.results = [{ name: 'Impression', value: imp }, { name: 'Click', value: click },
        { name: 'Revenue', value: revenue }, { name: 'Cost', value: cost }, { name: 'Profit', value: profit }];

      });

    });

  }
  ngOnDestroy() {
    this.alive = false;
  }
}
