/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { Subscription } from 'rxjs';
import { MessageService } from '../../../@core/mock/common/message-service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ReportData } from '../../../@core/interfaces/common/reportData';
import { UtilDateService } from '../../../@core/mock/common/util-date.service';

@Component({
  selector: 'site-detail-d3-advanced-pie',
  styleUrls: ['./d3.component.scss'],
  template: `
    <ngx-charts-advanced-pie-chart
      [scheme]="colorScheme"
      [results]="results">
    </ngx-charts-advanced-pie-chart>
  `,
})
export class D3AdvancedPieComponent implements OnDestroy, OnInit {
  results = [];
  colorScheme: any;
  themeSubscription: any;
  id: string;
  appName: string;
  appUrl: string;
  click: number;
  pCpm: number;
  nCpm: number;
  revenue: number
  cost: number;
  startDate: number;
  endDate: number;
  profit: number;

  constructor(private theme: NbThemeService, private messageService: MessageService, private reportService: ReportData,
    private activatedRoute: ActivatedRoute,
    private utilDateService: UtilDateService, ) {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.colorScheme = {
        domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
      };
    });
  }
  getDateRange(i: string) {
    var date = new Date();
    var timezoneOffset = date.getTimezoneOffset();
    if (!this.startDate || !this.endDate) {
      this.startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() - timezoneOffset * 60 * 1000;
      this.endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() + 24 * 60 * 60 * 1000 - timezoneOffset * 60 * 1000;
    }
    this.utilDateService.rangeTimeData(i).subscribe((data: {}) => {
      this.startDate = data['startDate'];
      this.endDate = data['endDate'];
    });
    this.refreshData(this.startDate, this.endDate);
  }

  refreshData(startDate, endDate) {

    var imp;
    var click;
    var cost;
    var profit;
    var revenue;
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
      this.reportService.getAppValueData(this.id,startDate,endDate).subscribe((siteData: {}) => {

        imp = siteData['aggregations']['appId_term']['buckets'][0]['imps_sum'].value;
        click = siteData['aggregations']['appId_term']['buckets'][0]['clicks_sum'].value;
        revenue = siteData['aggregations']['appId_term']['buckets'][0]['netRevenue_sum'].value;
        cost = siteData['aggregations']['appId_term']['buckets'][0]['pubRevenue_sum'].value;
        profit = siteData['aggregations']['appId_term']['buckets'][0]['imps_sum'].value;
        console.log(siteData);

        this.results = [{ name: 'Impression', value: imp }, { name: 'Click', value: click },
        { name: 'Revenue', value: revenue }, { name: 'Cost', value: cost }, { name: 'Profit', value: profit }];

      });

    });

  }

  ngOnInit() {
    this.getDateRange("today");

  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
