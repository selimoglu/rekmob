/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { NbMenuItem } from '@nebular/theme';
import { of as observableOf, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class PagesMenu {

  getMenu(): Observable<NbMenuItem[]> {

    const menu1 = [
      {
        title: 'HOMEPAGE',
        group: true,
      },
      {
        title: 'Dashboard',
        icon: 'nb-bar-chart',
        link: '/pages/dashboard',
        home: true,
        children: undefined,
      },
      {
        title: 'Reports',
        icon: 'nb-compose',
        link: '/pages/homepage',
        home: true,
        children: undefined,
      },
    ];
    const menu2 = [
      {
        title: 'PUBLISHER',
        group: true,
      },
      {
        title: 'Publishers',
        icon: 'nb-person',
        link: '/pages/publisher-detail',
        home: true,
        children: undefined,
      },
      {
        title: 'Fraud',
        icon: 'nb-compose',
        link: '/pages/account-fraud',
        home: true,
        children: undefined,
      },

    ];
    const menu3 = [
      {
        title: 'CAMPAIGN',
        group: true,
      },
      {
        title: 'Campaigns',
        icon: 'nb-star',
        link: '/pages/campaign-details',
        home: true,
        children: undefined,
      },

    ];
   const menu4 = [
      {
        title: 'PAYMENTS',
        group: true,
      },
      {
        title: 'Payments',
        icon: 'nb-checkmark',
        link: '/pages/payments',
        home: true,
        children: undefined,
      },
    ]; 

    return observableOf([...menu1, ...menu2, ...menu3, ...menu4]);
  }
}
