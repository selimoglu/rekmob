
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { PagesMenu } from './pages-menu';
import { DashboardModule } from './dashboard/dashboard.module';
import { HomepageModule } from './homepage/homepage.module';
import { CampaignDetailsModule } from './campaign-details/campaign-details.module';
import { CampaignReportsModule } from './campaign-reports/campaign-reports.module';
import { PublisherDetailModule } from './publisher-detail/publisher-detail.module';
import { AccountFraudModule } from './account-fraud/account-fraud.module';
import { SiteDetailModule } from './site-detail/site-detail.module';
import { UserDetailModule } from './user-detail/user-detail.module';

import { CustomReportModule } from './custom-report/custom-report.module';
import { PaymentsReportsModule } from './payments/payments.module';
const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    MiscellaneousModule,
    HomepageModule,
    CampaignDetailsModule,
    CampaignReportsModule,
    PublisherDetailModule,
    AccountFraudModule,
    SiteDetailModule,
    UserDetailModule,
    CustomReportModule,
    DashboardModule,
    PaymentsReportsModule
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ],
  providers: [
    PagesMenu,
  ],
})
export class PagesModule {
}
