
import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserData } from '../../interfaces/common/users';
import { SmartTableData } from '../../interfaces/common/smart-table';
import { SmartTableDataTwo } from '../../interfaces/common/smart-table-two';
import { ReportData } from '../../interfaces/common/reportData';
import { SmartTableDataThree } from '../../interfaces/common/smart-table-three';
import { UsersService } from './users.service';
import { SmartTableService } from './smart-table.service';
import { SmartTableServiceTwo } from './smart-table-two.service';
import { SmartTableServiceThree } from './smart-table-three.service';
import { PeriodsService } from './periods.service';
import { MessageService } from './message-service';
import { ReportService } from './report.service';
import { UtilDateService } from './util-date.service';
import { SearchService } from './search-service';
import { SearchData } from '../../interfaces/common/searchData';
import { StatData } from '../../interfaces/common/statData';
import { StatDataService } from'./statData.service';
const SERVICES = [
  { provide: UserData, useClass: UsersService },
  { provide: SmartTableData, useClass: SmartTableService },
  { provide: SmartTableDataTwo, useClass: SmartTableServiceTwo },
  { provide: SmartTableDataThree, useClass: SmartTableServiceThree },
  { provide: PeriodsService, useClass: PeriodsService },
  { provide: MessageService, useClass: MessageService },
  { provide: ReportData, useClass: ReportService },
  { provide: UtilDateService, useClass: UtilDateService },
  { provide: SearchData, useClass: SearchService },
  { provide: StatData, useClass: StatDataService },
];


@NgModule({
  imports: [CommonModule],
})
export class CommonMockModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: CommonMockModule,
      providers: [
        ...SERVICES,
      ],
    };
  }
}
