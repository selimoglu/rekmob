/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { Injectable } from '@angular/core';
import { SearchData } from '../../interfaces/common/searchData';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { statsData } from '../../interfaces/common/statData';
import { campaignStatsData } from '../../interfaces/common/statData';
import { of as observableOf, Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
@Injectable()
export class SearchService extends SearchData {
  endDate: number;
  startDate: number;
  appName: string;
  constructor(private httpClient: HttpClient, private route: ActivatedRoute) {
    super();
  }
  endpoint = 'https://www.epolen.com';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  getSearchUserData(userName): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": { "query": { "match": { "fullName": { "query": userName } } } },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=rekmob/user/_search?"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getSearchAppData(appName): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": { "query": { "match": { "appName": { "query": appName } } } },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=rekmob/app/_search?"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getSearchAppUnitData(appUnitName): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": { "query": { "match": { "unitName": { "query": appUnitName } } } },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=rekmob/appunit/_search?"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getSearchUtmSourceData(utmSourceName): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": { "query": { "match": { "utmSource": { "query": utmSourceName } } } },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=rekmob/user/_search?"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
}
