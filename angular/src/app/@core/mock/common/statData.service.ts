/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { Injectable } from '@angular/core';
import { StatData } from '../../interfaces/common/statData';
import { statsData } from '../../interfaces/common/statData';
import { campaignStatsData } from '../../interfaces/common/statData';
import { campaignStatsDataZero } from '../../interfaces/common/statData';
import { campaignTotal } from '../../interfaces/common/statData';
import { of as observableOf, Observable } from 'rxjs';
@Injectable()
export class StatDataService extends StatData {
  private totalData: statsData[] = [
    {
      id: 1,
      amount: 28,
      name: 'Revenue',
      isOn: false,
      type: 'Light',
      settings: {
        iconClass: 'fas fa-lira-sign',
        type: 'primary',
      },
    },
    {
      id: 2,
      amount: 28,
      name: 'Profit',
      isOn: false,
      type: 'RollerShades',
      settings: {
        iconClass: 'fas fa-hand-holding-usd',
        type: 'success',
      },
    },
    {
      id: 3,
      amount: 28,
      name: 'Impressions',
      isOn: false,
      type: 'WirelessAudio',
      settings: {
        iconClass: 'fas fa-bolt',
        type: 'info',
      },
    },
    {
      id: 4,
      amount: 28,
      name: 'Clicks',
      isOn: false,
      type: 'CoffeeMaker',
      settings: {
        iconClass: 'fas fa-mouse-pointer',
        type: 'warning',
      },
    },
  ];
  list(): Observable<statsData[]> {
    return observableOf(this.totalData);
  }
  private campaignTotalData: campaignStatsData[] = [
    {
      amount: 28,
      name: 'Impression',
      isOn: false,
      type: 'Light',
      settings: {
        iconClass: 'fas fa-bolt',
        type: 'primary',
      },
    },
    {
      amount: 28,
      name: 'Click',
      isOn: false,
      type: 'RollerShades',
      settings: {
        iconClass: 'fas fa-mouse-pointer',
        type: 'success',
      },
    },
    {
      amount: 28,
      name: 'Cost',
      isOn: false,
      type: 'WirelessAudio',
      settings: {
        iconClass: 'fas fa-lira-sign',
        type: 'info',
      },
    },
  ];
  campaignList(): Observable<campaignStatsData[]> {
    return observableOf(this.campaignTotalData);
  }
  private campaignTotalDataZero: campaignStatsDataZero[] = [
    {
      amount: 28,
      name: 'Impression',
      isOn: false,
      type: 'Light',
      settings: {
        iconClass: 'fas fa-bolt',
        type: 'primary',
      },
    },
    {
      amount: 28,
      name: 'Click',
      isOn: false,
      type: 'RollerShades',
      settings: {
        iconClass: 'fas fa-mouse-pointer',
        type: 'success',
      },
    },
    {
      amount: 28,
      name: 'Cost',
      isOn: false,
      type: 'WirelessAudio',
      settings: {
        iconClass: 'fas fa-lira-sign',
        type: 'info',
      },
    },
  ];
  campaignListZero(): Observable<campaignStatsDataZero[]> {
    return observableOf(this.campaignTotalDataZero);
  }
  private CampaignTotalName: campaignTotal[] = [
    {
      name: 'Total :',
      isOn: false,
      type: 'Light',
      settings: {
        iconClass: 'fas fa-hand-holding-usd',
        type: 'primary',
      },
    },
  ];
  campaignTot(): Observable<campaignTotal[]> {
    return observableOf(this.CampaignTotalName);
  }
  endDate: number;
  startDate: number;
  appName: string;
  constructor() {
    super();
  }

}
