
import { Injectable } from '@angular/core';
import { ReportData } from '../../interfaces/common/reportData';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { statsData } from '../../interfaces/common/statData';
import { campaignStatsData } from '../../interfaces/common/statData';
import { of as observableOf, Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
@Injectable()
export class ReportService extends ReportData {

  endDate: number;
  startDate: number;
  appName: string;
  customSearch: object;
  constructor(private httpClient: HttpClient, private route: ActivatedRoute) {
    super();
  }
  endpoint = 'https://www.epolen.com';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  getGeneralReportData(startDate, endDate): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": {
        "query": {
          "constant_score": {
            "filter": {
              "range": {
                "statTime":
                  { "gte": startDate, "lt": endDate }
              }
            }
          }
        }, "aggs": {
          "userId_term": {
            "terms":
              { "field": "userId", "order": { "netIncome_sum": "desc" }, "size": 15 }, "aggs":
            {
              "adsource_term": {
                "terms": { "field": "adSource" }, "aggs": {
                  "netIncome_sum":
                    { "sum": { "field": "netRevenue" } }, "income_sum": { "sum": { "field": "pubRevenue" } },
                  "clicks_sum": { "sum": { "field": "clicks" } }, "imps_sum": { "sum": { "field": "imps" } },
                  "profit_sum": {
                    "bucket_script": {
                      "buckets_path": { "my_var1": "netIncome_sum", "my_var2": "income_sum" },
                      "script": "params.my_var1 - params.my_var2"
                    }
                  }
                }
              }, "netIncomeUsd_sum": { "sum": { "field": "netRevenueUsd" } },
              "incomeUsd_sum": { "sum": { "field": "pubRevenueUsd" } }, "imps_sum": { "sum": { "field": "imps" } },
              "clicks_sum": { "sum": { "field": "clicks" } }, "publisherName_term": { "terms": { "field": "publisherName" } },
              "income_sum": { "sum": { "field": "pubRevenue" } }, "netIncome_sum": { "sum": { "field": "netRevenue" } },
              "profit_sum": {
                "bucket_script": {
                  "buckets_path": { "my_var1": "netIncome_sum", "my_var2": "income_sum" },
                  "script": "params.my_var1 - params.my_var2"
                }
              }
            }
          }
        }
      },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=publisher_stats/app_unit_stat/_search?size=0"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getAccountReportData(startDate, endDate): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": {
        "aggs": {
          "userId_term": {
            "terms": { "field": "userId", "size": 2000 }, "aggs":
            {
              "publisherName_tname": { "terms": { "field": "publisherName" } }, "imps_sum": { "sum": { "field": "imps" } },
              "clicks_sum": { "sum": { "field": "clicks" } }, "pubRevenue_sum": { "sum": { "field": "pubRevenue" } },
              "netRevenue_sum": { "sum": { "field": "netRevenue" } }, "pubRevenueUsd_sum": { "sum": { "field": "pubRevenueUsd" } },
              "netRevenueUsd_sum": { "sum": { "field": "netRevenueUsd" } }
            }
          }
        }, "query": {
          "bool": {
            "filter": {
              "bool":
              {
                "must": [{ "range": { "statTime": { "gte": startDate, "lt": endDate } } }, {
                  "bool": {
                    "should":
                      [{ "term": { "accountManager": "Sefa" } }]
                  }
                }]
              }
            }
          }
        }
      },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=publisher_stats/app_unit_stat/_search?size=0"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getTotalImpAndClickData(startDate, endDate): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": {
        "query": {
          "constant_score": {
            "filter": {
              "range":
                { "statTime": { "gte": startDate, "lt": endDate } }
            }
          }
        }, "aggs":
        {
          "imps_sum": { "sum": { "field": "imps" } }, "clicks_sum": {
            "sum":
              { "field": "clicks" }
          }, "adsource_term": {
            "terms": { "field": "adSource", "size": 10 },
            "aggs": { "imps_sum": { "sum": { "field": "imps" } }, "clicks_sum": { "sum": { "field": "clicks" } } }
          }
        }
      },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=publisher_stats/app_unit_stat/_search?size=0"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }

  getTotalRevenueAndProfitData(startDate, endDate): Observable<any> {
    var day = new Date(startDate).getDate();
    var month = new Date(startDate).getMonth() + 1;
    var year = new Date(startDate).getFullYear();
    var hour = new Date(startDate).getHours();
    var minute = new Date(startDate).getMinutes();
    var day2 = new Date(endDate).getDate();
    var month2 = new Date(endDate).getMonth() + 1;
    var year2 = new Date(endDate).getFullYear();
    var hour2 = new Date(endDate).getHours();
    var minute2 = new Date(endDate).getMinutes();
    const requestBody = {
      "contentType": "application/json",
      "params": {},
      "requestType": "GET",
      "url": "https://www.rekmob.com/api/elastic-proxy-crm?start=" + (day < 10 ? "0" + day : "" + day)
        + "-" + (month < 10 ? "0" + month : "" + month) + "-" + year + "T00:00" +
        "&end=" + (day2 < 10 ? "0" + day2 : "" + day2) + "-" + (month2 < 10 ? "0" + month2 : "" + month2)
        + "-" + year2 + "T00:00"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getSearchAppData(appName): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": { "query": { "match": { "appName": { "query": appName } } } },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=rekmob/app/_search?"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }

  getSiteData(siteId): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": { "query": { "match": { "id": siteId } } },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=rekmob/app/_search?size=100"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getUserData(): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": {
        "sort": [
          { "id": "desc" }
        ],
      },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=rekmob/user/_search?size=100"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getAppData(): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": {
        "sort": [
          { "id": "desc" }
        ],
      },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=rekmob/app/_search?size=100"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }

  getUserApps(userId): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": { "query": { "match": { "userId": userId } } },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=rekmob/app/_search?"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }

  getCampaigns(): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": {},
      "requestType": "GET",
      "url": "https://www.rekmob.com/api/admin-campaigns",
      "useOtherApiKey": true
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }

  getCustomData(query): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": query,
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=publisher_stats/app_unit_stat/_search?size=0",
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getHeaderSearchData(userId, startDate, endDate): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": {
        "query": {
          "constant_score": {
            "filter": {
              "bool": {
                "must": [{
                  "range": {
                    "statTime": {
                      "gte": startDate, "lt": endDate
                    }
                  }
                }, { "terms": { "userId": [userId] } }]
              }
            }
          }
        }, "aggs": {
          "userId_term": {
            "terms": { "field": "userId", "order": { "netIncome_sum": "desc" }, "size": 15 },
            "aggs": {
              "adsource_term": {
                "terms": { "field": "adSource" }, "aggs": {
                  "netIncome_sum": {
                    "sum":
                      { "field": "netRevenue" }
                  }, "income_sum": { "sum": { "field": "pubRevenue" } }, "clicks_sum":
                    { "sum": { "field": "clicks" } }, "imps_sum": { "sum": { "field": "imps" } }, "profit_sum": {
                      "bucket_script":
                      {
                        "buckets_path": { "my_var1": "netIncome_sum", "my_var2": "income_sum" }, "script":
                          "params.my_var1 - params.my_var2"
                      }
                    }
                }
              }, "netIncomeUsd_sum": { "sum": { "field": "netRevenueUsd" } },
              "incomeUsd_sum": { "sum": { "field": "pubRevenueUsd" } }, "imps_sum": { "sum": { "field": "imps" } }, "clicks_sum"
                : { "sum": { "field": "clicks" } }, "publisherName_term": { "terms": { "field": "publisherName" } }, "income_sum":
                { "sum": { "field": "pubRevenue" } }, "netIncome_sum": { "sum": { "field": "netRevenue" } }, "profit_sum": {
                  "bucket_script":
                    { "buckets_path": { "my_var1": "netIncome_sum", "my_var2": "income_sum" }, "script": "params.my_var1 - params.my_var2" }
                }
            }
          }
        }
      },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=publisher_stats/app_unit_stat/_search?size=0",
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getAppUnitData(siteId): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": { "query": { "match": { "appId": siteId } } },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=rekmob/appunit/_search?"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getAppValueData(siteId, startDate, endDate): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": {
        "aggs": {
          "appId_term": {
            "terms": { "field": "appId", "size": 2000 },
            "aggs": {
              "appName_tname": { "terms": { "field": "appName" } }, "imps_sum": {
                "sum":
                  { "field": "imps" }
              }, "clicks_sum": { "sum": { "field": "clicks" } }, "pubRevenue_sum":
                { "sum": { "field": "pubRevenue" } }, "netRevenue_sum": { "sum": { "field": "netRevenue" } },
              "pubRevenueUsd_sum": { "sum": { "field": "pubRevenueUsd" } }, "netRevenueUsd_sum": {
                "sum":
                  { "field": "netRevenueUsd" }
              }
            }
          }
        }, "query": {
          "bool": {
            "filter": {
              "bool": {
                "must": [{
                  "range": {
                    "statTime": { "gte": startDate, "lt": endDate }
                  }
                }, {
                  "bool": {
                    "should": [{
                      "term": {
                        "appId": siteId
                      }
                    }]
                  }
                }]
              }
            }
          }
        }
      },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=publisher_stats/app_unit_stat/_search?size=0"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }

  getAppSiteData(siteId): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": {
        "aggs": {
          "statTime_term": {
            "date_histogram": { "field": "statTime", "interval": "day" },
            "aggs": {
              "appId_term": {
                "terms": { "field": "appId", "size": 2000 }, "aggs": {
                  "appName_tname":
                    { "terms": { "field": "appName" } }, "imps_sum": { "sum": { "field": "imps" } }, "clicks_sum": { "sum": { "field": "clicks" } },
                  "pubRevenue_sum": { "sum": { "field": "pubRevenue" } }, "netRevenue_sum": { "sum": { "field": "netRevenue" } }, "pubRevenueUsd_sum"
                    : { "sum": { "field": "pubRevenueUsd" } }, "netRevenueUsd_sum": { "sum": { "field": "netRevenueUsd" } }
                }
              }, "statTime_tname": {
                "terms":
                  { "field": "statTime" }
              }
            }
          }
        }, "query": { "bool": { "filter": { "bool": { "must": [{ "range": { "statTime": { "gte": 1569456000000, "lt": 1569542400000 } } }, { "bool": { "should": [{ "term": { "appId": "22797" } }] } }] } } } }
      },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=publisher_stats/app_unit_stat/_search?size=0"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getAdSourceValue(startDate, endDate): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": {
        "aggs": {
          "adSource_term": {
            "terms": { "field": "adSource", "size": 2000 },
            "aggs": {
              "imps_sum": { "sum": { "field": "imps" } }, "clicks_sum": { "sum": { "field": "clicks" } },
              "pubRevenue_sum": { "sum": { "field": "pubRevenue" } }, "netRevenue_sum": { "sum": { "field": "netRevenue" } },
              "pubRevenueUsd_sum": { "sum": { "field": "pubRevenueUsd" } }, "netRevenueUsd_sum": { "sum": { "field": "netRevenueUsd" } }
            }
          }
        }, "query": { "bool": { "filter": { "bool": { "must": [{ "range": { "statTime": { "gte": startDate, "lt": endDate } } }] } } } }
      },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=publisher_stats/app_unit_stat/_search?size=0"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getAccValue(startDate, endDate): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": {
        "aggs": {
          "accountManager_term": {
            "terms": { "field": "accountManager", "size": 2000 },
            "aggs": {
              "accountManager_tname": { "terms": { "field": "accountManager" } }, "imps_sum": {
                "sum":
                  { "field": "imps" }
              }, "clicks_sum": { "sum": { "field": "clicks" } }, "pubRevenue_sum": { "sum": { "field": "pubRevenue" } },
              "netRevenue_sum": { "sum": { "field": "netRevenue" } }, "pubRevenueUsd_sum": { "sum": { "field": "pubRevenueUsd" } },
              "netRevenueUsd_sum": { "sum": { "field": "netRevenueUsd" } }
            }
          }
        }, "query": {
          "bool": {
            "filter": {
              "bool": {
                "must": [
                  { "range": { "statTime": { "gte": startDate, "lt": endDate } } }, {
                    "bool": {
                      "should": [{
                        "term":
                          { "accountManager": "Ceren" }
                      }, { "term": { "accountManager": "Sefa" } }, { "term": { "accountManager": "Esra" } },
                      { "term": { "accountManager": "Global" } }, { "term": { "accountManager": "Melis" } }]
                    }
                  }]
              }
            }
          }
        }
      },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=publisher_stats/app_unit_stat/_search?size=0"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getLast15Days(): Observable<any> {

    var timezoneOffset = new Date().getTimezoneOffset();
    var endDate = new Date().getTime() + 86400000;
    var startDate = endDate - (86400000 * 16);
    startDate = startDate - timezoneOffset * 60 * 1000;
    endDate = endDate - timezoneOffset * 60 * 1000;

    const requestBody = {
      "contentType": "application/json",
      "params": {
        "aggs": {
          "statTime_term": {
            "date_histogram": { "field": "statTime", "interval": "day" }
            , "aggs": {
              "statTime_tname": { "terms": { "field": "statTime" } }, "imps_sum": { "sum": { "field": "imps" } },
              "clicks_sum": { "sum": { "field": "clicks" } }, "pubRevenue_sum": { "sum": { "field": "pubRevenue" } }
              , "netRevenue_sum": { "sum": { "field": "netRevenue" } }, "pubRevenueUsd_sum": { "sum": { "field": "pubRevenueUsd" } }
              , "netRevenueUsd_sum": { "sum": { "field": "netRevenueUsd" } }
            }
          }
        }, "query": {
          "bool": {
            "filter": {
              "bool":
                { "must": [{ "range": { "statTime": { "gte": startDate, "lt": endDate } } }] }
            }
          }
        }
      },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=publisher_stats/app_unit_stat/_search?size=0"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getImpClickProfitNative(startDate, endDate): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": { "aggs": { "imps_sum": { "sum": { "field": "imps" } }, "clicks_sum": { "sum": { "field": "clicks" } }, "pubRevenue_sum": { "sum": { "field": "pubRevenue" } }, "netRevenue_sum": { "sum": { "field": "netRevenue" } }, "pubRevenueUsd_sum": { "sum": { "field": "pubRevenueUsd" } }, "netRevenueUsd_sum": { "sum": { "field": "netRevenueUsd" } } }, "query": { "bool": { "filter": { "bool": { "must": [{ "range": { "statTime": { "gte": startDate, "lt": endDate } } }, { "bool": { "should": [{ "term": { "unitSize": 25 } }] } }] } } } } },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=publisher_stats/app_unit_stat/_search?size=0"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getImpClickProfitPop(startDate, endDate): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": { "aggs": { "imps_sum": { "sum": { "field": "imps" } }, "clicks_sum": { "sum": { "field": "clicks" } }, "pubRevenue_sum": { "sum": { "field": "pubRevenue" } }, "netRevenue_sum": { "sum": { "field": "netRevenue" } }, "pubRevenueUsd_sum": { "sum": { "field": "pubRevenueUsd" } }, "netRevenueUsd_sum": { "sum": { "field": "netRevenueUsd" } } }, "query": { "bool": { "filter": { "bool": { "must": [{ "range": { "statTime": { "gte": startDate, "lt": endDate } } }, { "bool": { "should": [{ "term": { "unitSize": 28 } }, { "term": { "unitSize": 29 } }, { "term": { "unitSize": 30 } }] } }] } } } } },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=publisher_stats/app_unit_stat/_search?size=0"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getImpClickProfitDisplay(startDate, endDate): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": { "aggs": { "imps_sum": { "sum": { "field": "imps" } }, "clicks_sum": { "sum": { "field": "clicks" } }, "pubRevenue_sum": { "sum": { "field": "pubRevenue" } }, "netRevenue_sum": { "sum": { "field": "netRevenue" } }, "pubRevenueUsd_sum": { "sum": { "field": "pubRevenueUsd" } }, "netRevenueUsd_sum": { "sum": { "field": "netRevenueUsd" } } }, "query": { "bool": { "filter": { "bool": { "must": [{ "range": { "statTime": { "gte": startDate, "lt": endDate } } }, { "bool": { "should": [{ "term": { "unitSize": 0 } }, { "term": { "unitSize": 1 } }, { "term": { "unitSize": 2 } }, { "term": { "unitSize": 3 } }, { "term": { "unitSize": 10 } }, { "term": { "unitSize": 11 } }, { "term": { "unitSize": 9 } }, { "term": { "unitSize": 12 } }, { "term": { "unitSize": 13 } }, { "term": { "unitSize": 14 } }, { "term": { "unitSize": 16 } }, { "term": { "unitSize": 15 } }, { "term": { "unitSize": 17 } }, { "term": { "unitSize": 18 } }, { "term": { "unitSize": 19 } }, { "term": { "unitSize": 20 } }, { "term": { "unitSize": 21 } }, { "term": { "unitSize": 22 } }, { "term": { "unitSize": 23 } }, { "term": { "unitSize": 24 } }, { "term": { "unitSize": 26 } }, { "term": { "unitSize": 27 } }] } }] } } } } },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=publisher_stats/app_unit_stat/_search?size=0"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getLast30Days(userId): Observable<any> {

    var timezoneOffset = new Date().getTimezoneOffset();
    var endDate = new Date().getTime() + 86400000;
    var startDate = endDate - (86400000 * 31);
    startDate = startDate - timezoneOffset * 60 * 1000;
    endDate = endDate - timezoneOffset * 60 * 1000;

    const requestBody = {
      "contentType": "application/json",
      "params": {
        "aggs": {
          "statTime_term": {
            "date_histogram": { "field": "statTime", "interval": "day" },
            "aggs": {
              "userId_term": {
                "terms": { "field": "userId", "size": 2000 }, "aggs": {
                  "publisherName_tname": { "terms": { "field": "publisherName" } }, "imps_sum":
                    { "sum": { "field": "imps" } }, "clicks_sum": { "sum": { "field": "clicks" } }, "pubRevenue_sum":
                    { "sum": { "field": "pubRevenue" } }, "netRevenue_sum": { "sum": { "field": "netRevenue" } },
                  "pubRevenueUsd_sum": { "sum": { "field": "pubRevenueUsd" } }, "netRevenueUsd_sum": {
                    "sum":
                      { "field": "netRevenueUsd" }
                  }
                }
              }, "statTime_tname": { "terms": { "field": "statTime" } }
            }
          }
        },
        "query": {
          "bool": {
            "filter": {
              "bool": {
                "must": [{
                  "range": {
                    "statTime": {
                      "gte": startDate,
                      "lt": endDate
                    }
                  }
                }, { "bool": { "should": [{ "term": { "userId": userId } }] } }]
              }
            }
          }
        }
      },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=publisher_stats/app_unit_stat/_search?size=0"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getAppUnitForFraud(): Observable<any> {
    var date = new Date();
    var timezoneOffset = date.getTimezoneOffset();
    var startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime();
    var endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() + 86400000;
    startDate = startDate - timezoneOffset * 60 * 1000;
    endDate = endDate - timezoneOffset * 60 * 1000;

    const requestBody = {
      "contentType": "application/json",
      "params": { "aggs": { "appUnitId_term": { "terms": { "field": "appUnitId", "size": 2000 }, "aggs": { "appUnitName_tname": { "terms": { "field": "appUnitName" } }, "imps_sum": { "sum": { "field": "imps" } }, "clicks_sum": { "sum": { "field": "clicks" } }, "pubRevenue_sum": { "sum": { "field": "pubRevenue" } }, "netRevenue_sum": { "sum": { "field": "netRevenue" } }, "pubRevenueUsd_sum": { "sum": { "field": "pubRevenueUsd" } }, "netRevenueUsd_sum": { "sum": { "field": "netRevenueUsd" } } } } }, "query": { "bool": { "filter": { "bool": { "must": [{ "range": { "statTime": { "gte": startDate, "lt": endDate } } }, { "bool": { "should": [{ "term": { "unitSize": 0 } }, { "term": { "unitSize": 1 } }, { "term": { "unitSize": 2 } }, { "term": { "unitSize": 3 } }, { "term": { "unitSize": 10 } }, { "term": { "unitSize": 11 } }, { "term": { "unitSize": 12 } }, { "term": { "unitSize": 13 } }, { "term": { "unitSize": 14 } }, { "term": { "unitSize": 15 } }, { "term": { "unitSize": 17 } }, { "term": { "unitSize": 18 } }, { "term": { "unitSize": 16 } }, { "term": { "unitSize": 19 } }, { "term": { "unitSize": 20 } }, { "term": { "unitSize": 21 } }, { "term": { "unitSize": 22 } }, { "term": { "unitSize": 23 } }, { "term": { "unitSize": 24 } }, { "term": { "unitSize": 25 } }, { "term": { "unitSize": 26 } }, { "term": { "unitSize": 27 } }] } }] } } } } },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=publisher_stats/app_unit_stat/_search?size=0"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  createPubPaymentDetail(address, iban, invoiced, paypal, taxId, taxRate, tckn, userId, vatRate): Observable<any> {
    const requestBody = {
      "address": address,
      "iban": iban,
      "invoiced": invoiced,
      "paypal": paypal,
      "taxId": taxId,
      "taxRate": taxRate,
      "tckn": tckn,
      "userId": userId,
      "vatRate": vatRate
    };
    return this.httpClient.post<any>(this.endpoint + '/admin/pub-payment-detail/create',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  getPubPayments(paymentDate, statusType): Observable<any> {

    let url = this.endpoint + '/admin/pub-payment?paymentDate=' + paymentDate;
    if (statusType) {
      url += '&statusType=' + statusType;
    }

    return this.httpClient.get<any>(url, this.httpOptions).pipe();
  }

  getPubPayment(userId): Observable<any> {

    return this.httpClient.get<any>(this.endpoint + '/admin/pub-payment/' + userId, this.httpOptions).pipe();
  }
  getPubPaymentDetail(userId): Observable<any> {

    return this.httpClient.get<any>(this.endpoint + '/admin/pub-payment-detail/' + userId, this.httpOptions).pipe();
  }


  createPubPayment(amount, paymentDate, status, userId): Observable<any> {
    const requestBody = {
      "amount": amount,
      "paymentDate": paymentDate,
      "status": status,
      "userId": userId
    };
    return this.httpClient.post<any>(this.endpoint + '/admin/pub-payment/create',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
  banUser(userId): Observable<any> {

    return this.httpClient.post<any>(this.endpoint + '/admin/user/ban/' + userId,
      this.httpOptions).pipe();
  }
  banAppUnit(appUnitId): Observable<any> {

    return this.httpClient.post<any>(this.endpoint + '/admin/user/ban/' + appUnitId,
      this.httpOptions).pipe();
  }
  getAppUnitForUser(appIds): Observable<any> {
    const requestBody = {
      "contentType": "application/json",
      "params": {
        "query": {
          "bool": {
            "filter": {
              "terms": {
                "appId": appIds
              }
            }
          }
        }
      },
      "requestType": "POST",
      "url": "http://www.rekmob.com/api/elastic-proxy?path=rekmob/appunit/_search?"
    };
    return this.httpClient.post<any>(this.endpoint + '/reports/request',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }
}
