import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';

@Injectable()
export class MessageService {
  private subject = new Subject<any>();
  private subjecttwo = new Subject<any>();
  private subjectthree = new Subject<any>();
  private customTime = new Subject<any>();
  private queryData = new Subject<any>();
  private sidebarState = false;
  private customColumnsData = new Subject<any>();
  constructor() { }


  sendData(message: object) {
    this.subject.next({ text: message });
  }

  getData(): Observable<any> {
    return this.subject.asObservable();
  }
  sendGeneralData(message: object) {
    this.subjecttwo.next({ text: message });
  }
  getGeneralData(): Observable<any> {
    return this.subjecttwo.asObservable();
  }
  sendAppData(message: object) {
    this.subjectthree.next({ text: message });
  }
  getAppData(): Observable<any> {
    return this.subjectthree.asObservable();
  }
  sendCustomTime(message: object) {
    this.customTime.next({ text: message });
  }
  getCustomTime(): Observable<any> {
    return this.customTime.asObservable();
  }
  sendQuery(message: object) {
    this.queryData.next({ text: message });
  }
  getQuery(): Observable<any> {
    return this.queryData.asObservable();
  }
  setSidebarState(sidebarState: boolean) {
    this.sidebarState = sidebarState;
  }
  getSidebarState(): boolean {
    return this.sidebarState;
  }
  sendCustomColumns(message: object) {
    this.customColumnsData.next({ text: message });
  }
  getCustomColumns(): Observable<any> {
    return this.customColumnsData.asObservable();
  }

}  
