
import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class UtilDateService {
  private subject = new Subject<any>();
  private subjecttwo = new Subject<any>();
  rangeTimeData(i): Observable<any> {
    var startDate;
    var endDate;
    var date = new Date();
    var timezoneOffset=date.getTimezoneOffset();
    if (i == "today") {
      startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime();
      endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() + 86400000;
    }
    else if (i == "yesterday") {
      endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime();
      startDate = endDate - 86400000;
    }
    else if (i == "lastSevenDays") {
      endDate = new Date().getTime() + 86400000;
      startDate =endDate- (86400000 * 7);
    }
    else if (i == "thisMonth") {
      startDate = new Date(date.getFullYear(), date.getMonth(), 1).getTime();
      endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() + 86400000;
    }
    else if (i == "lastMonth") {
      startDate = new Date(new Date((new Date(date.getFullYear(), date.getMonth(), 1).getTime()) - (86400000 * 1)).getFullYear(), new Date((new Date(date.getFullYear(), date.getMonth(), 1).getTime()) - (86400000 * 1)).getMonth(), 1).getTime();
      endDate = new Date(date.getFullYear(), date.getMonth(), 1).getTime();
    }
    startDate = startDate - timezoneOffset*60*1000;
    endDate = endDate - timezoneOffset*60*1000;
    var data={
      startDate,
      endDate
    }
    this.subject.next(data);
    return this.subject.asObservable();
  }
}
