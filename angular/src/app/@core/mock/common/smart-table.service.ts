/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { Injectable } from '@angular/core';
import { SmartTableData } from '../../interfaces/common/smart-table';

@Injectable()
export class SmartTableService extends SmartTableData {

  data = [{
    id: 1,
    firstName: 'Mark',
    impression: 15,
    click: 5,
    pCpm: 1212,
    nCpm: 124,
    revenue: 123,
    cost: 123,
    profit: 123,
  }, {
    id: 2,
    firstName: 'Jacob',
    impression: 20,
    click: 90,
    pCpm: 324,
    nCpm: 121,
    revenue: 123,
    cost: 123,
    profit: 123,
  }, {
    id: 3,
    firstName: 'Larry',
    impression: 30,
    click: 100,
    pCpm: 114,
    nCpm: 1234,
    revenue: 123,
    cost: 123,
    profit: 123,
  }, {
    id: 4,
    firstName: 'John',
    impression: 40,
    click: 15,
    pCpm: 1123,
    nCpm: 1233,
    revenue: 123,
    cost: 123,
    profit: 123,
  }, {
    id: 5,
    firstName: 'Jack',
    impression: 50,
    click: 25,
    pCpm: 2334,
    nCpm: 5223,
    revenue: 123,
    cost: 123,
    profit: 123,
  }, {
    id: 6,
    firstName: 'Ann',
    impression: 60,
    click: 35,
    pCpm: 343,
    nCpm: 232,
    revenue: 123,
    cost: 123,
    profit: 123,
  }, {
    id: 7,
    firstName: 'Barbara',
    impression: 2112,
    click: 24524,
    pCpm: 4334,
    nCpm: 224,
    revenue: 123,
    cost: 123,
    profit: 123,
  }, {
    id: 8,
    firstName: 'Sevan',
    impression: 4543,
    click: 443,
    pCpm: 343,
    nCpm: 343,
    revenue: 123,
    cost: 123,
    profit: 123,
  }, {
    id: 9,
    firstName: 'Ruben',
    impression: 4355,
    click: 4353,
    pCpm: 5534,
    nCpm: 254,
    revenue: 123,
    cost: 123,
    profit: 123,
  }, {
    id: 10,
    firstName: 'Karen',
    impression: 12333,
    click: 43545,
    pCpm: 4355,
    nCpm: 435,
    revenue: 123,
    cost: 123,
    profit: 123,
  }, {
    id: 11,
    firstName: 'Mark',
    impression: 'Otto',
    click: '@mark',
    pCpm: 'mark@gmail.com',
    nCpm: '38',
    revenue: 123,
    cost: 123,
    profit: 123,
  }, {
    id: 12,
    firstName: 'Jacob',
    impression: 'Thornton',
    click: '@jacob',
    pCpm: 'jacob@yandex.ru',
    nCpm: '48',
    revenue: 123,
    cost: 123,
    profit: 123,
  }, {
    id: 13,
    firstName: 'Haik',
    impression: 'Hakob',
    click: '@haik',
    pCpm: 'haik@outlook.com',
    nCpm: '48',
    revenue: 123,
    cost: 123,
    profit: 123,
  }, {
    id: 14,
    firstName: 'Garegin',
    impression: 'Jirair',
    click: '@garegin',
    pCpm: 'garegin@gmail.com',
    nCpm: '40',
    revenue: 123,
    cost: 123,
    profit: 123,
  }, {
    id: 15,
    firstName: 'Krikor',
    impression: 'Bedros',
    click: '@krikor',
    pCpm: 'krikor@yandex.ru',
    nCpm: '32',
    revenue: 123,
    cost: 123,
    profit: 123,
  }, {
    'id': 16,
    'firstName': 'Francisca',
    'impression': 'Brady',
    'click': '@Gibson',
    'pCpm': 'franciscagibson@comtours.com',
    'nCpm': 11,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 17,
    'firstName': 'Tillman',
    'impression': 'Figueroa',
    'click': '@Snow',
    'pCpm': 'tillmansnow@comtours.com',
    'nCpm': 34,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 18,
    'firstName': 'Jimenez',
    'impression': 'Morris',
    'click': '@Bryant',
    'pCpm': 'jimenezbryant@comtours.com',
    'nCpm': 45,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 19,
    'firstName': 'Sandoval',
    'impression': 'Jacobson',
    'click': '@Mcbride',
    'pCpm': 'sandovalmcbride@comtours.com',
    'nCpm': 32,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 20,
    'firstName': 'Griffin',
    'impression': 'Torres',
    'click': '@Charles',
    'pCpm': 'griffincharles@comtours.com',
    'nCpm': 32,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 21,
    'firstName': 'Cora',
    'impression': 'Parker',
    'click': '@Caldwell',
    'pCpm': 'coracaldwell@comtours.com',
    'nCpm': 27,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 22,
    'firstName': 'Cindy',
    'impression': 'Bond',
    'click': '@Velez',
    'pCpm': 'cindyvelez@comtours.com',
    'nCpm': 24,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 23,
    'firstName': 'Frieda',
    'impression': 'Tyson',
    'click': '@Craig',
    'pCpm': 'friedacraig@comtours.com',
    'nCpm': 45,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 24,
    'firstName': 'Cote',
    'impression': 'Holcomb',
    'click': '@Rowe',
    'pCpm': 'coterowe@comtours.com',
    'nCpm': 20,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 25,
    'firstName': 'Trujillo',
    'impression': 'Mejia',
    'click': '@Valenzuela',
    'pCpm': 'trujillovalenzuela@comtours.com',
    'nCpm': 16,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 26,
    'firstName': 'Pruitt',
    'impression': 'Shepard',
    'click': '@Sloan',
    'pCpm': 'pruittsloan@comtours.com',
    'nCpm': 44,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 27,
    'firstName': 'Sutton',
    'impression': 'Ortega',
    'click': '@Black',
    'pCpm': 'suttonblack@comtours.com',
    'nCpm': 42,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 28,
    'firstName': 'Marion',
    'impression': 'Heath',
    'click': '@Espinoza',
    'pCpm': 'marionespinoza@comtours.com',
    'nCpm': 47,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 29,
    'firstName': 'Newman',
    'impression': 'Hicks',
    'click': '@Keith',
    'pCpm': 'newmankeith@comtours.com',
    'nCpm': 15,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 30,
    'firstName': 'Boyle',
    'impression': 'Larson',
    'click': '@Summers',
    'pCpm': 'boylesummers@comtours.com',
    'nCpm': 32,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 31,
    'firstName': 'Haynes',
    'impression': 'Vinson',
    'click': '@Mckenzie',
    'pCpm': 'haynesmckenzie@comtours.com',
    'nCpm': 15,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 32,
    'firstName': 'Miller',
    'impression': 'Acosta',
    'click': '@Young',
    'pCpm': 'milleryoung@comtours.com',
    'nCpm': 55,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 33,
    'firstName': 'Johnston',
    'impression': 'Brown',
    'click': '@Knight',
    'pCpm': 'johnstonknight@comtours.com',
    'nCpm': 29,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 34,
    'firstName': 'Lena',
    'impression': 'Pitts',
    'click': '@Forbes',
    'pCpm': 'lenaforbes@comtours.com',
    'nCpm': 25,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 35,
    'firstName': 'Terrie',
    'impression': 'Kennedy',
    'click': '@Branch',
    'pCpm': 'terriebranch@comtours.com',
    'nCpm': 37,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 36,
    'firstName': 'Louise',
    'impression': 'Aguirre',
    'click': '@Kirby',
    'pCpm': 'louisekirby@comtours.com',
    'nCpm': 44,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 37,
    'firstName': 'David',
    'impression': 'Patton',
    'click': '@Sanders',
    'pCpm': 'davidsanders@comtours.com',
    'nCpm': 26,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 38,
    'firstName': 'Holden',
    'impression': 'Barlow',
    'click': '@Mckinney',
    'pCpm': 'holdenmckinney@comtours.com',
    'nCpm': 11,
  }, {
    'id': 39,
    'firstName': 'Baker',
    'impression': 'Rivera',
    'click': '@Montoya',
    'pCpm': 'bakermontoya@comtours.com',
    'nCpm': 47,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 40,
    'firstName': 'Belinda',
    'impression': 'Lloyd',
    'click': '@Calderon',
    'pCpm': 'belindacalderon@comtours.com',
    'nCpm': 21,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 41,
    'firstName': 'Pearson',
    'impression': 'Patrick',
    'click': '@Clements',
    'pCpm': 'pearsonclements@comtours.com',
    'nCpm': 42,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 42,
    'firstName': 'Alyce',
    'impression': 'Mckee',
    'click': '@Daugherty',
    'pCpm': 'alycedaugherty@comtours.com',
    'nCpm': 55,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 43,
    'firstName': 'Valencia',
    'impression': 'Spence',
    'click': '@Olsen',
    'pCpm': 'valenciaolsen@comtours.com',
    'nCpm': 20,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 44,
    'firstName': 'Leach',
    'impression': 'Holcomb',
    'click': '@Humphrey',
    'pCpm': 'leachhumphrey@comtours.com',
    'nCpm': 28,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 45,
    'firstName': 'Moss',
    'impression': 'Baxter',
    'click': '@Fitzpatrick',
    'pCpm': 'mossfitzpatrick@comtours.com',
    'nCpm': 51,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 46,
    'firstName': 'Jeanne',
    'impression': 'Cooke',
    'click': '@Ward',
    'pCpm': 'jeanneward@comtours.com',
    'nCpm': 59,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 47,
    'firstName': 'Wilma',
    'impression': 'Briggs',
    'click': '@Kidd',
    'pCpm': 'wilmakidd@comtours.com',
    'nCpm': 53,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 48,
    'firstName': 'Beatrice',
    'impression': 'Perry',
    'click': '@Gilbert',
    'pCpm': 'beatricegilbert@comtours.com',
    'nCpm': 39,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 49,
    'firstName': 'Whitaker',
    'impression': 'Hyde',
    'click': '@Mcdonald',
    'pCpm': 'whitakermcdonald@comtours.com',
    'nCpm': 35,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 50,
    'firstName': 'Rebekah',
    'impression': 'Duran',
    'click': '@Gross',
    'pCpm': 'rebekahgross@comtours.com',
    'nCpm': 40,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 51,
    'firstName': 'Earline',
    'impression': 'Mayer',
    'click': '@Woodward',
    'pCpm': 'earlinewoodward@comtours.com',
    'nCpm': 52,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 52,
    'firstName': 'Moran',
    'impression': 'Baxter',
    'click': '@Johns',
    'pCpm': 'moranjohns@comtours.com',
    'nCpm': 20,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 53,
    'firstName': 'Nanette',
    'impression': 'Hubbard',
    'click': '@Cooke',
    'pCpm': 'nanettecooke@comtours.com',
    'nCpm': 55,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 54,
    'firstName': 'Dalton',
    'impression': 'Walker',
    'click': '@Hendricks',
    'pCpm': 'daltonhendricks@comtours.com',
    'nCpm': 25,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 55,
    'firstName': 'Bennett',
    'impression': 'Blake',
    'click': '@Pena',
    'pCpm': 'bennettpena@comtours.com',
    'nCpm': 13,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 56,
    'firstName': 'Kellie',
    'impression': 'Horton',
    'click': '@Weiss',
    'pCpm': 'kellieweiss@comtours.com',
    'nCpm': 48,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 57,
    'firstName': 'Hobbs',
    'impression': 'Talley',
    'click': '@Sanford',
    'pCpm': 'hobbssanford@comtours.com',
    'nCpm': 28,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 58,
    'firstName': 'Mcguire',
    'impression': 'Donaldson',
    'click': '@Roman',
    'pCpm': 'mcguireroman@comtours.com',
    'nCpm': 38,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 59,
    'firstName': 'Rodriquez',
    'impression': 'Saunders',
    'click': '@Harper',
    'pCpm': 'rodriquezharper@comtours.com',
    'nCpm': 20,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }, {
    'id': 60,
    'firstName': 'Lou',
    'impression': 'Conner',
    'click': '@Sanchez',
    'pCpm': 'lousanchez@comtours.com',
    'nCpm': 16,
    'revenue': 123,
    'cost': 123,
    'profit': 123,
  }];

  getData() {
    return this.data;
  }
}
