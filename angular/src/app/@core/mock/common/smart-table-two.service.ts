/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { Injectable } from '@angular/core';
import { SmartTableDataTwo } from '../../interfaces/common/smart-table-two';

@Injectable()
export class SmartTableServiceTwo extends SmartTableDataTwo {

  data = [{
    name: 'Mark',
    email: 'mark@gmail.com',
    active: "true",
    credit: 12,
    profit: 412,
    user: "Edit user",
    loginAs: 123,
    publisherProfit: "Kazanç detayları",
    advertiserCredit: "Kredi detayları",
  }, {
    name: 'John',
    email: 'john@gmail.com',
    active: "true",
    credit: 412,
    profit: 412,
    user: "Edit user",
    loginAs: 123,
    publisherProfit: "Kazanç detayları",
    advertiserCredit: "Kredi detayları",
  }, {
    name: 'Ali',
    email: 'ali@gmail.com',
    active: "true",
    credit: 12,
    profit: 412,
    user: "Edit user",
    loginAs: 123,
    publisherProfit: "Kazanç detayları",
    advertiserCredit: "Kredi detayları",
  }, {
    name: 'Veli',
    email: 'veli@gmail.com',
    active: "true",
    credit: 2412,
    profit: 412,
    user: "Edit user",
    loginAs: 123,
    publisherProfit: "Kazanç detayları",
    advertiserCredit: "Kredi detayları",
  }, {
    name: 'Serkan',
    email: 'serkan@gmail.com',
    active: "true",
    credit: 112,
    profit: 4412,
    user: "Edit user",
    loginAs: 123,
    publisherProfit: "Kazanç detayları",
    advertiserCredit: "Kredi detayları",
  }, {
    name: 'mustafa',
    email: 'mustafa@gmail.com',
    active: "true",
    credit: 2412,
    profit: 12,
    user: "Edit user",
    loginAs: 123,
    publisherProfit: "Kazanç detayları",
    advertiserCredit: "Kredi detayları",
  }, {
    name: 'murat',
    email: 'murat@gmail.com',
    active: "true",
    credit: 112,
    profit: 1412,
    user: "Edit user",
    loginAs: 123,
    publisherProfit: "Kazanç detayları",
    advertiserCredit: "Kredi detayları",
  }, {
    name: 'recep',
    email: 'recep@gmail.com',
    active: "true",
    credit: 412,
    profit: 412,
    user: "Edit user",
    loginAs: 123,
    publisherProfit: "Kazanç detayları",
    advertiserCredit: "Kredi detayları",
  }, {
    name: 'abdullah',
    email: 'mark@gmail.com',
    active: "true",
    credit: 2412,
    profit: 412,
    user: "Edit user",
    loginAs: 123,
    publisherProfit: "Kazanç detayları",
    advertiserCredit: "Kredi detayları",
  }, {
    name: 'abdullah',
    email: 'abdullah@gmail.com',
    active: "true",
    credit: 412,
    profit: 1412,
    user: "Edit user",
    loginAs: 123,
    publisherProfit: "Kazanç detayları",
    advertiserCredit: "Kredi detayları",
  }, {
    name: 'polat',
    email: 'polat@gmail.com',
    active: "true",
    credit: 412,
    profit: 122,
    user: "Edit user",
    loginAs: 123,
    publisherProfit: "Kazanç detayları",
    advertiserCredit: "Kredi detayları",
  }, {
    name: 'tunday',
    email: 'tunday@gmail.com',
    active: "true",
    credit: 1412,
    profit: 1242,
    user: "Edit user",
    loginAs: 123,
    publisherProfit: "Kazanç detayları",
    advertiserCredit: "Kredi detayları",
  }, {
    name: 'Marker',
    email: 'mark@gmail.com',
    active: "true",
    credit: 1412,
    profit: 12,
    user: "Edit user",
    loginAs: 123,
    publisherProfit: "Kazanç detayları",
    advertiserCredit: "Kredi detayları",
  }, {
    name: 'Marker',
    email: 'mark@gmail.com',
    active: "true",
    credit: 1212,
    profit: 1212,
    user: "Edit user",
    loginAs: 123,
    publisherProfit: "Kazanç detayları",
    advertiserCredit: "Kredi detayları",
  }, {
    name: 'Marker',
    email: 'mark@gmail.com',
    active: "true",
    credit: 122,
    profit: 12412,
    user: "Edit user",
    loginAs: 123,
    publisherProfit: "Kazanç detayları",
    advertiserCredit: "Kredi detayları",
  }];

  getData() {
    return this.data;
  }
}
