/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { Injectable } from '@angular/core';
import { SmartTableDataThree } from '../../interfaces/common/smart-table-three';

@Injectable()
export class SmartTableServiceThree extends SmartTableDataThree {

  data = [{
    name: 'Mark',
    email: 'mark@gmail.com',
    appName: 'TestbyTest.com',
    appUrl: 'http://TestbyTest.com',
    kindUrl: 'BrandSafe',
    createDate: '2019-7-5 13:50:42',
    status: 'Onaylandı',
    preview: 'Önizle',
    ads: 'reklamstore.com rekmob.com appnexus.com 1619 adform.com 1431 pubmatic.com 156547',
  }, {
    name: 'ali',
    email: 'ali@gmail.com',
    appName: 'befaral@just-email.com	',
    appUrl: 'http://TestbyTest.com',
    kindUrl: 'Aggressive',
    createDate: '2019-7-5 13:50:42',
    status: 'Onaylandı',
    preview: 'Önizle',
    ads: '',
  }, {
    name: 'veli',
    email: 'veli@gmail.com',
    appName: 'test12323.mobpick.com	',
    appUrl: 'http://TestbyTest.com',
    kindUrl: 'BrandSafe',
    createDate: '2019-7-5 13:50:42',
    status: 'Onaylandı',
    preview: 'Önizle',
    ads: 'reklamstore.com rekmob.com appnexus.com 1619 adform.com 1431 pubmatic.com 156547',
  }, {
    name: 'volkan',
    email: 'volkan@gmail.com',
    appName: 'test',
    appUrl: 'http://TestbyTest.com',
    kindUrl: 'Aggressive',
    createDate: '2019-7-5 13:50:42',
    status: 'Onaylandı',
    preview: 'Önizle',
    ads: '',
  }, {
    name: 'tuncay',
    email: 'tuncay@gmail.com',
    appName: 'test',
    appUrl: 'http://TestbyTest.com',
    kindUrl: 'BrandSafe',
    createDate: '2019-7-5 13:50:42',
    status: 'Onaylandı',
    preview: 'Önizle',
    ads: '',
  }, {
    name: 'serhat',
    email: 'serhat@gmail.com',
    appName: 'test',
    appUrl: 'http://TestbyTest.com',
    kindUrl: 'BrandSafe',
    createDate: '2019-7-5 13:50:42',
    status: 'Onaylandı',
    preview: 'Önizle',
    ads: 'reklamstore.com rekmob.com appnexus.com 1619 adform.com 1431 pubmatic.com 156547',
  }, {
    name: 'semih',
    email: 'semih@gmail.com',
    appName: 'test',
    appUrl: 'http://TestbyTest.com',
    kindUrl: 'Aggressive',
    createDate: '2019-7-5 13:50:42',
    status: 'Onaylandı',
    preview: 'Önizle',
    ads: '',
  }, {
    name: 'sami',
    email: 'sami@gmail.com',
    appName: 'test',
    appUrl: 'http://TestbyTest.com',
    kindUrl: 'BrandSafe',
    createDate: '2019-7-5 13:50:42',
    status: 'Onaylandı',
    preview: 'Önizle',
    ads: 'reklamstore.com rekmob.com appnexus.com 1619 adform.com 1431 pubmatic.com 156547',
  }, {
    name: 'temel',
    email: 'temel@gmail.com',
    appName: 'test',
    appUrl: 'http://TestbyTest.com',
    kindUrl: 'BrandSafe',
    createDate: '2019-7-5 13:50:42',
    status: 'Onaylandı',
    preview: 'Önizle',
    ads: '',
  }, {
    name: 'dursun',
    email: 'dursun@gmail.com',
    appName: 'test',
    appUrl: 'http://TestbyTest.com',
    kindUrl: 'Aggressive',
    createDate: '2019-7-5 13:50:42',
    status: 'Onaylandı',
    preview: 'Önizle',
    ads: '',
  }, {
    name: 'mehmet',
    email: 'mehmet@gmail.com',
    appName: 'test',
    appUrl: 'http://TestbyTest.com',
    kindUrl: 'Aggressive',
    createDate: '2019-7-5 13:50:42',
    status: 'Onaylandı',
    preview: 'Önizle',
    ads: '',
  }, {
    name: 'murat',
    email: 'murat@gmail.com',
    appName: 'test',
    appUrl: 'http://TestbyTest.com',
    kindUrl: 'Aggressive',
    createDate: '2019-7-5 13:50:42',
    status: 'Onaylandı',
    preview: 'Önizle',
    ads: 'reklamstore.com rekmob.com appnexus.com 1619 adform.com 1431 pubmatic.com 156547',
  }, {
    name: 'Mark',
    email: 'mark@gmail.com',
    appName: 'test',
    appUrl: 'http://TestbyTest.com',
    kindUrl: 'Aggressive',
    createDate: '2019-7-5 13:50:42',
    status: 'Onaylandı',
    preview: 'Önizle',
    ads: '',
  }, {
    name: 'Mark',
    email: 'mark@gmail.com',
    appName: 'test',
    appUrl: 'http://TestbyTest.com',
    kindUrl: 'Aggressive',
    createDate: '2019-7-5 13:50:42',
    status: 'Onaylandı',
    preview: 'Önizle',
    ads: '',
  }, {
    name: 'Mark',
    email: 'mark@gmail.com',
    appName: 'test',
    appUrl: 'http://TestbyTest.com',
    kindUrl: 'Aggressive',
    createDate: '2019-7-5 13:50:42',
    status: 'Onaylandı',
    preview: 'Önizle',
    ads: 'd',
  }];

  getData() {
    return this.data;
  }
}
