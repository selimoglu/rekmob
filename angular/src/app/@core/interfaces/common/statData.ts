import { Observable } from 'rxjs';

export interface StatsViewSettings {
  iconClass: string;
  type: string;
}

export interface statsData {
  id: number;
  isOn: boolean;
  name: string;
  amount: number;
  type: string;
  settings?: StatsViewSettings;
}
export interface campaignStatsData {
  isOn: boolean;
  name: string;
  amount: number;
  type: string;
  settings?: StatsViewSettings;
}
export interface campaignStatsDataZero {
  isOn: boolean;
  name: string;
  amount: number;
  type: string;
  settings?: StatsViewSettings;
}
export interface campaignTotal {
  isOn: boolean;
  name: string;
  type: string;
  settings?: StatsViewSettings;
}
export abstract class StatData {
  abstract list(): Observable<statsData[]>;
  abstract campaignList(): Observable<campaignStatsData[]>;
  abstract campaignListZero(): Observable<campaignStatsDataZero[]>;
  abstract campaignTot(): Observable<campaignTotal[]>;
}

export abstract class TotalData {
  abstract list(): Observable<statsData[]>;
  abstract edit(statData: statsData): Observable<statsData>;
}
export abstract class CampaignTotalData {
  abstract list(): Observable<campaignStatsData[]>;
  abstract edit(campaignStatData: campaignStatsData): Observable<campaignStatsData>;
}
export abstract class CampaignTotalDataZero {
  abstract list(): Observable<campaignStatsDataZero[]>;
  abstract edit(campaignStatDataZero: campaignStatsDataZero): Observable<campaignStatsDataZero>;
}
export abstract class CampaignTotalName {
  abstract list(): Observable<campaignTotal[]>;
  abstract edit(campaignTot: campaignTotal): Observable<campaignTotal>;
}

