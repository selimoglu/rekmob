
import { Observable } from 'rxjs';
import { statsData } from '../common/statData';
import { campaignStatsData } from '../common/statData';
export abstract class SearchData {
  abstract getSearchUserData(userName): Observable<any>;
  abstract getSearchAppData(appName): Observable<any>;
  abstract getSearchAppUnitData(appUnitName): Observable<any>;
  abstract getSearchUtmSourceData(utmSourceName): Observable<any>;
}
