
import { Observable } from 'rxjs';
import { statsData } from '../common/statData';
import { campaignStatsData } from '../common/statData';
export abstract class ReportData {
  customSearchObject = {
    isSelectedDate: false,
    isSelectedUser: false,
    isSelectedApp: false,
    isSelectedAppUnit: false,
    isSelectedAddSource: false,
    isSelectedSize: false,
    isSelectedAccManager: false,
    isSelectedAppType: false,
    isSelectedAppUrl: false,
    isSelectedUtmSource: false,
    isSelectedCc: false
  };
  isSelectedAppUrl = false;
  selecteds = {};
  date = new Date();
  timezoneOffset = this.date.getTimezoneOffset();
  rowData = {};
  userId = {};
  userName = {};
  rangeTime = {

    startDate: new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate()).getTime() - this.timezoneOffset * 60 * 1000,
    endDate: new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate()).getTime() + 24 * 60 * 60 * 1000 - this.timezoneOffset * 60 * 1000,
    controlData: 0
  }

  abstract getSearchAppData(appName): Observable<any>;
  abstract getSiteData(siteId): Observable<any>;
  abstract getAppUnitData(siteId): Observable<any>;
  abstract getGeneralReportData(startDate, endDate): Observable<any>;
  abstract getAccountReportData(startDate, endDate): Observable<any>;
  abstract getTotalImpAndClickData(startDate, endDate): Observable<any>;
  abstract getTotalRevenueAndProfitData(startDate, endDate): Observable<any>;
  abstract getUserData(): Observable<any>;
  abstract getAppData(): Observable<any>;
  abstract getUserApps(userId): Observable<any>;
  abstract getCampaigns(): Observable<any>;
  abstract getCustomData(query): Observable<any>;
  abstract getHeaderSearchData(userId, startDate, endDate): Observable<any>;
  abstract getAppValueData(siteId, startDate, endDate): Observable<any>;
  abstract getAppSiteData(siteId): Observable<any>;
  abstract getAdSourceValue(startDate, endDate): Observable<any>;
  abstract getAccValue(startDate, endDate): Observable<any>;
  abstract getLast15Days(): Observable<any>;
  abstract getImpClickProfitNative(startDate, endDate): Observable<any>;
  abstract getImpClickProfitPop(startDate, endDate): Observable<any>;
  abstract getImpClickProfitDisplay(startDate, endDate): Observable<any>;
  abstract getLast30Days(userId): Observable<any>;
  abstract getAppUnitForFraud(): Observable<any>;
  abstract createPubPaymentDetail(address, iban, invoiced, paypal, taxId, taxRate, tckn, userId, vatRate): Observable<any>;
  abstract getPubPayments(paymentDate, statusType): Observable<any>;
  abstract createPubPayment(amount, paymentDate, status, userId): Observable<any>;
  abstract banUser(userId): Observable<any>;
  abstract getAppUnitForUser(appIds): Observable<any>;
  abstract banAppUnit(appUnitId): Observable<any>;
  abstract getPubPayment(userId): Observable<any>;
  abstract getPubPaymentDetail(userId): Observable<any>;
}
